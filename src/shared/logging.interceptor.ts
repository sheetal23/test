import {
    Injectable,
    NestInterceptor,
    ExecutionContext,
    Logger,
    CallHandler,
    BadGatewayException,
  } from '@nestjs/common';
  
  //import { GqlExecutionContext } from '@nestjs/graphql';
  import { Observable, throwError } from 'rxjs';
  import { tap, catchError} from 'rxjs/operators';
  import { BaseExceptionFilter } from '@nestjs/core';
  
  //@Injectable()
  export class LoggingInterceptor implements NestInterceptor {
    intercept(
      context: ExecutionContext, next: CallHandler
    ): Observable<any> {
      var now = Date.now();
      var req = context.switchToHttp().getRequest();
      var res = context.switchToHttp().getResponse();
      var code= res.statusCode
      var method = req.method;
      var auth = req.headers.authorization;
      var url = req.url;
      var query = req.body ? JSON.stringify(req.body) : JSON.stringify(req.query);
      console.log(code)
      console.log(auth)
      // var code= res.getStatus;
      if (req) {
        
       // console.log(req)
        return next
        .handle()
        .pipe(
          tap(() =>
            Logger.log(
              `${method} ${url} ${Date.now() - now}ms`,
              context.getClass().name,
            ),
          //   Logger.error(`${method} ${url}`,JSON.stringify(errorResponse),
          //   'BaseExceptionFilter')
          ),
        );
      // } else {
      //   const ctx: any = GqlExecutionContext.create(context);
      //   const resolverName = ctx.constructorRef.name;
      //   const info = ctx.getInfo();
  
      //   return call$.pipe(
      //     tap(() =>
      //       Logger.log(
      //         `${info.parentType} "${info.fieldName}" ${Date.now() - now}ms`,
      //         resolverName,
      //       ),
      //     ),
      //   );
      }
      else{
          
          return next
          .handle()
          .pipe(
               tap(() =>
         
          Logger.error(`${method} ${url}`,
          'BaseExceptionFilter')
        ),
          );
      }
    }
  }
  
  
  