import { Entity, Column, PrimaryGeneratedColumn,BeforeInsert,ManyToOne,OneToMany, JoinColumn, Code} from 'typeorm';
import * as crypto from 'crypto';
import * as md5 from 'md5';

@Entity()
export class Logs {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({default: null, type:"text", collation:"utf8mb4_bin"})
  path: string;

  @Column({default: null})
  code: Number;

  @Column({default: null})
  message: string;

  @Column({default: null, type:"text", collation:"utf8mb4_bin"})
  params: string;

  @Column({default: null, type:"text", collation:"utf8mb4_bin"})
  query: string;

  @Column({default: null})
  method: string ;

  @Column({default: null})
  auth: string ;

  @Column({ type: 'timestamp', default: null})
  created_at: Date;

}