import { Injectable, Param } from '@nestjs/common';
import { Users } from '../user/entities/user.entity';
import * as jwt from 'jsonwebtoken';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, getRepository,Between, DeleteResult, UpdateResult, getConnection } from 'typeorm';

import {
    ExceptionFilter,
    Catch,
    ArgumentsHost,
    Logger,
    HttpException,
    HttpStatus,
  } from '@nestjs/common';
import { Logs } from './entities/log.entity';
  @Injectable()
  @Catch()
  export class HttpErrorFilter implements ExceptionFilter {
    constructor(
        @InjectRepository(Logs)
        private readonly logRepository: Repository<Logs>,

    ) {}
    catch(exception: HttpException, host: ArgumentsHost) {
      const ctx = host.switchToHttp();
      const response = ctx.getResponse();
      const request = ctx.getRequest();
      const status = exception.getStatus
        ? exception.getStatus()
        : HttpStatus.INTERNAL_SERVER_ERROR;
  //console.log(request)
      const errorResponse = {
        code: status,
        created_at: new Date(),
        path: request.url,
        params : JSON.stringify(request.body),
        query: JSON.stringify(request.query),
        auth:request.headers.authorization,
        method: request.method,
        message:
          status !== HttpStatus.INTERNAL_SERVER_ERROR
            ? exception.message.error || exception.message || null
            : 'Internal server error',
      };

      const error = {
        error:exception.message.error,
        statusCode: status,
        message:exception.message.message
         
      };
  
      if (status === HttpStatus.INTERNAL_SERVER_ERROR) {
        Logger.error(
          `${request.method} ${request.url}`,
          exception.stack,
          'ExceptionFilter',
        );
      } else {
        Logger.error(
          `${request.method} ${request.url}`,
          JSON.stringify(error),
          'ExceptionFilter',
        );
      }
        this.logRepository.save(errorResponse)
      response.status(status).json(error);
    }
  }
  