import {MiddlewareConsumer, Module, NestModule, RequestMethod} from '@nestjs/common';
// import { EventService } from './event.service';
// import { EventController } from './event.controller';
//import { Events } from '../event/entities/event.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthMiddleware } from '../user/user.authmiddleware';
import { Users } from '../user/entities/user.entity';
import { UserModule } from '../user/user.module';

import { Followers } from '../user/entities/follow.entity';
// import { NotificationService } from '../notification/notification.service';
import { ChatController } from './chat.controller';
import { ChatService } from './chat.service';
import { ChatGateway } from './chat.gateway';
import { Chats } from './entities/chat.entity';
import { ChatTos } from './entities/chat-to.entity';



@Module({
  imports: [
    TypeOrmModule.forFeature([Chats]),
     TypeOrmModule.forFeature([ChatTos]),
    TypeOrmModule.forFeature([Users]),
  //  TypeOrmModule.forFeature([Followers]),
    UserModule
  ],
  controllers: [ChatController],
  providers: [ChatService,ChatGateway],
//   providers: [EventService],
//   controllers: [EventController]
})
export class ChatModule implements NestModule {
  public configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(AuthMiddleware)
      .forRoutes(
          {path: 'chat/message', method: RequestMethod.POST},
          {path: 'chat/messages', method: RequestMethod.GET},
          {path: 'chat', method: RequestMethod.GET},
          {path: 'chat', method: RequestMethod.DELETE},
          {path: 'chat/message', method: RequestMethod.DELETE},
        );
  }
}
