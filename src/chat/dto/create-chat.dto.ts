import { IsNotEmpty, Min, MinLength } from 'class-validator';

export class CreateChatDto {

    readonly id: number;  

    @IsNotEmpty()
    readonly message: string;

    @IsNotEmpty()
    readonly local_identifier: string;

    // @IsNotEmpty()
    // readonly from_id: number;

    readonly event_id: number;

    readonly message_type: number;

    readonly team_id: number;

    readonly to_id: number;

    readonly reply_id: number;

    readonly forward_id: number;

    readonly delete_type: number;

    readonly to_ids: Array<number>;
}