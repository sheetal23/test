import { Injectable } from '@nestjs/common';
import { Users } from '../user/entities/user.entity';
import * as jwt from 'jsonwebtoken';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, getRepository,Between, DeleteResult, UpdateResult, getConnection } from 'typeorm';
import { validate } from 'class-validator';
import { HttpException } from '@nestjs/common/exceptions/http.exception';
import { HttpStatus, Render } from '@nestjs/common';
import {CreateChatDto} from './dto';
import { MailerService } from '@nest-modules/mailer';
import * as NodeGeocoder from 'node-geocoder';
import * as crypto from 'crypto';
import * as md5 from 'md5';
import { networkInterfaces } from 'os';
import { Chats } from './entities/chat.entity';
import { ChatTos } from './entities/chat-to.entity';
import { ignoreElements } from 'rxjs/operators';

@Injectable()
export class ChatService {
    constructor(
        @InjectRepository(Chats)
        private readonly chatRepository: Repository<Chats>,
        @InjectRepository(Users)
        private readonly userRepository: Repository<Users>,
        @InjectRepository(ChatTos)
        private readonly chatToRepository: Repository<ChatTos>,
        private readonly mailerService: MailerService
    ) {}


    /*--------------------------------------------------------------------------
    ----------------------------------------------------------------------------
     * Api Functions
    ----------------------------------------------------------------------------
    --------------------------------------------------------------------------*/  
    async sendMessage(logged_id: number,dto: CreateChatDto) {

        const {id,message,to_id,message_type,event_id,team_id,reply_id, forward_id, local_identifier} = dto;
       
          
        let new_chat= new Chats();
        new_chat.from_id = logged_id;
        new_chat.message = message;
        new_chat.message_type = message_type;
        new_chat.reply_id = reply_id ? reply_id :null;
        new_chat.forward_id = forward_id ? forward_id :null;
        new_chat.local_identifier = local_identifier ? local_identifier :null;
        const savedChat = await this.chatRepository.save(new_chat);
       console.log("savedChat.id")
      // console.log(savedChat)
      
        console.log(1)
        var new_chat_to:any= new ChatTos();
        new_chat_to.to_id = to_id;
        new_chat_to.chat_id = savedChat.id;
    
     
      console.log("new_chat_to")
    //  console.log(new_chat_to)
        const savedChatTo = await this.chatToRepository.save(new_chat_to);

        console.log("new_chat")
       // console.log(savedChatTo.id)
         //return ({message:"Message sent"});
      } 

    async readMessage(logged_id: number,dto: CreateChatDto) {

      const {local_identifier} = dto;
      var msg= await getRepository(Chats)
      .createQueryBuilder("chats")
      .select("id")
      .where("local_identifier=:local_identifier", {local_identifier:local_identifier})
      .getRawOne();
      await getConnection()
            .createQueryBuilder()
            .update(ChatTos)
            .set({ 
            is_read : 1
            })
            .where("chat_id = :chat_id", { chat_id: msg.id })
            .andWhere("to_id = :to_id", { to_id: logged_id })
            .execute()

      
        //return ({message:"Message sent"});
    } 


    async deleteMessage(logged_id: number,dto: CreateChatDto) {

      const {local_identifier,delete_type} = dto;
      const current_time= new Date();
      var msg= await getRepository(Chats)
      .createQueryBuilder("chats")
      .select("*")
      .where("local_identifier=:local_identifier", {local_identifier:local_identifier})
      .getRawOne();
      if(msg.from_id==logged_id)
      {
        //delete for me
        if(delete_type==1){
          await getConnection()
          .createQueryBuilder()
          .update(Chats)
          .set({ 
            is_deleted : 1
            })
          .where("id = :id", { chat_id: msg.id })
          .execute()
        }
        //delete for all
        if(delete_type==2){
          await getConnection()
          .createQueryBuilder()
          .update(Chats)
          .set({ 
            deleted_at: current_time
            })
          .where("id = :id", { id: msg.id })
          .execute()
        }
      }
      else{
        await getConnection()
        .createQueryBuilder()
        .update(ChatTos)
        .set({ 
          is_deleted : 1
          })
        .where("chat_id = :chat_id", { chat_id: msg.id })
        .andWhere("to_id = :to_id", { to_id: logged_id })
        .execute()
      }
      

      
        //return ({message:"Message sent"});
    } 

    async getMessages(logged_id: number,req) {

      var user_id= req.query.user_id;
      var type = req.query.type;
      var event_id=req.query.event_id;
      var team_id=req.query.team_id;
      var last_chat_id=req.query.last_chat_id;
      var type=req.query.type;
      var skip = 0;
      var limit = req.query.limit ? req.query.limit : 50;
      if(req.query.page) {
          const page = req.query.page;
          var skip = limit * page;
      }
      
      // if(type==1)
      // {  
        var qb=  await getRepository(Chats)
        .createQueryBuilder("chats")
        .leftJoin(ChatTos,"chat_tos","chat_tos.chat_id=chats.id")
        .select("*,(SELECT profile_image FROM users WHERE id=chats.from_id) as user_profile_picture,(SELECT is_online FROM users WHERE id=chats.from_id) as user_is_online");
        if(type==1){
          qb.where('((chats.from_id = :logged_id AND chat_tos.to_id=:user_id) OR (chats.from_id = :user_id AND chat_tos.to_id=:logged_id) )', {logged_id : logged_id, user_id:user_id})
          qb.andWhere("(chats.event_id is null AND chats.team_id is null)")
        }
         
      const updateRead=   await getRepository(ChatTos)
      .createQueryBuilder("ChatTos")
     // .leftJoin(Chats,"chats","chats.id=chat_tos.chat_id")
      .update(ChatTos)
      .set({is_read:1})
      .where('(((SELECT from_id FROM chats WHERE id=chat_tos.chat_id)=:user_id AND chat_tos.to_id=:logged_id) OR ((SELECT event_id FROM chats WHERE id=chat_tos.chat_id)=:event_id OR (SELECT team_id FROM chats WHERE id=chat_tos.chat_id)=:team_id AND chat_tos.to_id=:logged_id) )', {user_id:user_id,logged_id : logged_id, event_id: event_id, team_id:team_id})
      .execute();
      
      var chat=await qb.offset(skip).limit(limit).getRawMany();

      return chat;
    } 

    async getList(logged_id: number,req) {

      //var user_id= req.query.user_id;
      var last_chat_id=req.query.last_chat_id;
      var type=req.query.type;
      
      var skip = 0;
      var limit = req.query.limit ? req.query.limit : 50;
      if(req.query.page) {
          const page = req.query.page;
          var skip = limit * page;
      }
      const userQb = await getRepository(ChatTos)
      .createQueryBuilder("chat_tos")
      .leftJoin(Chats,"chats","chats.id=chat_tos.chat_id")
      .select("count('id')")
      .where("chat_tos.is_read='0' AND chat_tos.to_id="+logged_id+" AND chats.from_id=user_id");

      const qb=  await getRepository(Chats)
      .createQueryBuilder("chats")
      .leftJoin(ChatTos,"chat_tos","chat_tos.chat_id=chats.id")
      .select("*,if(chats.from_id="+logged_id+",chat_tos.to_id,chats.from_id) as user_id,"
      + " if(chats.from_id="+logged_id+",(SELECT profile_image FROM users WHERE id=chat_tos.to_id),(SELECT profile_image FROM users WHERE id=chats.from_id)) AS user_profile_image,"
      + " if(chats.from_id="+logged_id+",(SELECT CONCAT(first_name,last_name) FROM users WHERE id=chat_tos.to_id),(SELECT CONCAT(first_name,last_name) FROM users WHERE id=chats.from_id)) AS user_name,if(chats.from_id="+logged_id+",(SELECT is_online FROM users WHERE id=chat_tos.to_id),(SELECT is_online FROM users WHERE id=chats.from_id)) AS user_is_online,IFNULL(event_id,team_id) AS group_id");
      if(userQb){
        qb.addSelect("("+ userQb.getQuery() +") AS unread_msgs")
      }
      if(type==1){
        qb.where('(chats.from_id = :logged_id OR chat_tos.to_id=:logged_id) AND event_id IS NULL AND team_id IS NULL', {logged_id : logged_id})
        // .andWhere("(chats.id in (select max(id) from chats where (chats.from_id="+logged_id+" or chat_tos.to_id="+logged_id+") ))")
        qb.groupBy("greatest(from_id, to_id),least(from_id,to_id)");
      }
     
      var chats= await qb.offset(skip).limit(limit).orderBy("chats.id","DESC").getRawMany();
      var total= await qb.getCount();

      return {chats, total};
    } 
}
