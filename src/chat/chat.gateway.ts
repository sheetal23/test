import {
  SubscribeMessage,
  WebSocketGateway,
  OnGatewayInit,
  WsResponse,
  OnGatewayConnection,
  OnGatewayDisconnect,
  WebSocketServer} from '@nestjs/websockets';
  import { InjectRepository } from '@nestjs/typeorm';
import { Logger, Req } from '@nestjs/common';
import { Socket, Server } from 'socket.io';
import { ChatService }  from './chat.service';
import {CreateChatDto} from './dto';
import { Chats } from './entities/chat.entity';
import { ChatTos } from './entities/chat-to.entity';
import { Users } from '../user/entities/user.entity';
import { Repository, getRepository,Between, DeleteResult, UpdateResult, getConnection } from 'typeorm';
import { UserService } from '../user/user.service';

@WebSocketGateway({ namespace: '/chat' })
export class ChatGateway implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {
  constructor(
    @InjectRepository(Chats)
    private readonly chatRepository: Repository<Chats>,
    @InjectRepository(Users)
    private readonly userRepository: Repository<Users>,
    @InjectRepository(ChatTos)
    private readonly chatToRepository: Repository<ChatTos>,
    private readonly userService: UserService,
    private readonly chatService: ChatService
) {}
  @WebSocketServer() wss: Server;

  private logger: Logger = new Logger('ChatGateway');
  wsClients=[];
  afterInit(server: Server) {
    console.log('Initialized!')
    this.logger.log('Initialized!');
  }

  handleDisconnect(client: Socket) {
    console.log("disconnected")
    this.logger.log(`Client disconnected: ${client.id}`);
  }

  // handleConnection(client: Socket, ...args: any[]) {
  //   this.logger.log(`Client connected:    ${client.id}`);
  // }
  handleConnection(client: any) {
    this.wsClients.push(client);
  }


  @SubscribeMessage('msgToServer')
  async handleMessage(@Req() request, client:Socket,dto: CreateChatDto) {

    // const {id,message,to_id,message_type,event_id,team_id } = dto;
    const jwtData = await this.userService.jwtData(request.headers.authorization);
    const logged_id = jwtData.id; 
    this.chatService.sendMessage(logged_id, dto);
    console.log("savedChat")
    client.send('msgToServer', dto);
      return ({message:"Message sent"});
    } 

    @SubscribeMessage('readMessage')
    async readMessage(@Req() request, client:Socket,dto: CreateChatDto) {

    // const {id,message,to_id,message_type,event_id,team_id } = dto;
    const jwtData = await this.userService.jwtData(request.headers.authorization);
    const logged_id = jwtData.id; 
    this.chatService.readMessage(logged_id, dto);
    console.log("readChat")
    client.emit('readMessage', dto);
      return ({message:"Message read"});
    } 

    @SubscribeMessage('deleteMessage')
    async deleteMessage(@Req() request, client:Socket,dto: CreateChatDto) {

    // const {id,message,to_id,message_type,event_id,team_id } = dto;
    const jwtData = await this.userService.jwtData(request.headers.authorization);
    const logged_id = jwtData.id; 
    this.chatService.deleteMessage(logged_id, dto);
    console.log(" delete Chat")
    client.emit('deleteMessage', dto);
      return ({message:"Message deleted"});
    } 

    // @SubscribeMessage('chatToServer')
    // this.wss.to(message.room).emit('chatToClient', message);
  //}

}

