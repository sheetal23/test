import { Entity, Column, PrimaryGeneratedColumn,BeforeInsert,ManyToOne,OneToMany, JoinColumn} from 'typeorm';
import { Users } from '../../user/entities/user.entity';
import { ChatTos } from '../entities/chat-to.entity';

import * as crypto from 'crypto';
import * as md5 from 'md5';

@Entity()
export class Chats {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(type => Users, Users => Users.Chats, {
    cascade: true,
    onDelete: "CASCADE",
    onUpdate: "CASCADE"
  })
  @JoinColumn({ name: "from_id"})
  from_id: number;
  
  @Column({default: null, collation:"utf8mb4_bin"})
  message: string;

  @Column({default: null})
  reply_id: number;

  @Column({default: null})
  forward_id: number;

  @Column({default: null})
  local_identifier: string;

  @Column({default: 0, comment:"0-not delete, 1-deleted"})
  is_deleted: number;

  @Column({default: null, type:"tinyint", comment:"1-text, 2-video, 3-audio, 4-image"})
  message_type: number;

  @Column({ type: 'timestamp', default: () => "CURRENT_TIMESTAMP"})
  created_at: Date;

  @Column({ type: 'timestamp', default: null})
  updated_at: Date;

  @Column({ type: 'timestamp', default: null})
  deleted_at: Date;

  //relations

  @OneToMany(type => ChatTos, ChatTos => ChatTos.chat_id)
  ChatTos: ChatTos[];


}