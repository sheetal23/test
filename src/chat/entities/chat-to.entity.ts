import { Entity, Column, PrimaryGeneratedColumn,BeforeInsert,ManyToOne,OneToMany, JoinColumn} from 'typeorm';
import { Users } from '../../user/entities/user.entity';
import { Chats } from '../entities/chat.entity';

import * as crypto from 'crypto';
import * as md5 from 'md5';

@Entity()
export class ChatTos {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(type => Users, Users => Users.ChatTos, {
    cascade: true,
    onDelete: "CASCADE",
    onUpdate: "CASCADE"
  })
  @JoinColumn({ name: "to_id"})
  to_id: number;

  @Column({default: 0, comment:"0-unread, 1-read"})
  is_read: number;

  @Column({default: 0, comment:"0-not delete, 1-deleted"})
  is_deleted: number;


  @ManyToOne(type => Chats, Chats => Chats.ChatTos, {
    cascade: true,
    onDelete: "CASCADE",
    onUpdate: "CASCADE"
  })
  @JoinColumn({ name: "chat_id"})
    chat_id: number;

}