import { Controller, Post, Body, UsePipes, Req, HttpCode, Put, Get, Delete, UseInterceptors } from '@nestjs/common';
import { ChatService } from './chat.service';
import { UserService } from '../user/user.service';
import {CreateChatDto} from './dto';
import { ValidationPipe } from '../shared/pipes/validation.pipe';
import {
    ApiUseTags,
    ApiBearerAuth
  } from '@nestjs/swagger';

  @ApiBearerAuth()
@ApiUseTags('chat')
@Controller('chat')
export class ChatController {
    constructor(private readonly userService: UserService, private readonly chatService: ChatService) {}
  
    @UsePipes(new ValidationPipe)
    @Post('message')
    @HttpCode(200)
    async sendMessage(@Req() request, @Body() chatData : CreateChatDto ) {
        const jwtData = await this.userService.jwtData(request.headers.authorization);
        console.log(request.headers.authorization)
        return await this.chatService.sendMessage(jwtData.id,chatData);
    }

    @Get('messages')
    @HttpCode(200)
    async getMessages(@Req() request) {
        const jwtData = await this.userService.jwtData(request.headers.authorization);
        console.log(request.headers.authorization)
        return await this.chatService.getMessages(jwtData.id,request);
    }

    @Get()
    @HttpCode(200)
    async getList(@Req() request) {
        const jwtData = await this.userService.jwtData(request.headers.authorization);
        console.log(request.headers.authorization)
        return await this.chatService.getList(jwtData.id,request);
    }

}
