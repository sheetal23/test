import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import * as bodyParser from 'body-parser';
//import { WsAdapter } from './common/adapters/ws-adapter.ts';
import * as WsAdapter from '@nestjs/websockets';
import "reflect-metadata";


async function bootstrap() {

  const appOptions = {cors: true};
  const app = await NestFactory.create(AppModule, appOptions);
  app.setGlobalPrefix('api');

  const options = new DocumentBuilder()
    .setTitle('Pgraters')
    .setDescription('Pgraters Api List')
    .setVersion('1.0')
    .setBasePath('api')
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('/docs', app, document);

  app.use(bodyParser.json({limit: '50mb'}));
  app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
  //app.useWebSocketAdapter(new WsAdapter());
  await app.listen(process.env.PORT);
}
bootstrap();