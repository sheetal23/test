import { IsNotEmpty } from 'class-validator';

export class ChangeEmailDto {

  @IsNotEmpty()
  readonly password: string;

  @IsNotEmpty()
  readonly email: string;

}