import { IsNotEmpty, IsEmail, Min, MinLength } from 'class-validator';

export class CreateUserDto {

  @IsNotEmpty()
  @IsEmail()
  readonly email: string;

  @IsNotEmpty()
  @MinLength(8)
  readonly password: string;

  @IsNotEmpty()
  readonly first_name: string;

  readonly last_name: string;

  @IsNotEmpty()
  readonly user_name: string;

  readonly position: string;

  readonly location: string;

  readonly latitude: number;

  readonly longitude: number;

  readonly device_id: string;

  readonly device_type: number;

  readonly device_token: string;
}