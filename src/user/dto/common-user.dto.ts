import { IsNotEmpty } from 'class-validator';

export class CommonUserDto {

  @IsNotEmpty()
  readonly user_id: number;

  @IsNotEmpty()
  readonly feedback: string;

}