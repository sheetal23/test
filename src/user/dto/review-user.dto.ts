import { IsNotEmpty } from 'class-validator';

export class ReviewUserDto {

    readonly id: number ;
    readonly user_id: number;
    readonly review: string;
    readonly review_status: number;
}