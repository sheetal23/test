import {MiddlewareConsumer, Module, NestModule, RequestMethod} from '@nestjs/common';
import { UserController } from './user.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Users } from './entities/user.entity';
import { UserLogins } from './entities/user-login.entity';
import { UserReviews } from './entities/user-review.entity';
import { UserRatings } from './entities/user-rating.entity';
import { Followers } from './entities/follow.entity';
import { Blocks } from './entities/block.entity';
import { UserService } from './user.service';
import { AuthMiddleware } from './user.authmiddleware';
import { Feedbacks } from './entities/feedback.entity';
//import { NotificationModule } from '../notification/notification.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Users]),
    TypeOrmModule.forFeature([UserLogins]),
    TypeOrmModule.forFeature([Followers]),
    TypeOrmModule.forFeature([Blocks]),
    TypeOrmModule.forFeature([Feedbacks]),
    TypeOrmModule.forFeature([UserReviews]),
    TypeOrmModule.forFeature([UserRatings]),
  //  NotificationModule

  ],
  providers: [UserService],
  controllers: [
    UserController
  ],
  exports: [UserService]
})
export class UserModule implements NestModule {
  public configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(AuthMiddleware)
      .forRoutes(
          {path: 'user/deactivate', method: RequestMethod.POST},
          {path: 'user/change-password', method: RequestMethod.PUT},
          //{path: 'user/change-email', method: RequestMethod.PUT},
          {path: 'user', method: RequestMethod.PUT},
          {path: 'user', method: RequestMethod.GET},
          {path: 'user/review', method: RequestMethod.POST},
          {path: 'user/block', method: RequestMethod.POST},
          {path: 'user/follow', method: RequestMethod.POST},
          {path: 'user/followers', method: RequestMethod.GET},
          {path: 'user/reviews', method: RequestMethod.GET},
          {path: 'user/rating', method: RequestMethod.POST},
          {path: 'user/listing', method: RequestMethod.GET},
          {path: 'user/leaderboard', method: RequestMethod.GET},
          {path: 'user/online', method: RequestMethod.PUT},
          {path: 'user/review-enable', method: RequestMethod.PUT},
          {path: 'user/feedback', method: RequestMethod.POST}

        );
  }
}