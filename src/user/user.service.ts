import { Injectable } from '@nestjs/common';
import * as jwt from 'jsonwebtoken';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, getRepository, DeleteResult, UpdateResult, getConnection } from 'typeorm';
import { Users } from './entities/user.entity';
import { UserLogins } from './entities/user-login.entity';
import { UserReviews } from './entities/user-review.entity';
import { UserRatings } from './entities/user-rating.entity';
import { Followers } from './entities/follow.entity';
import { Feedbacks } from './entities/feedback.entity';
import { Blocks } from './entities/block.entity';
import { validate } from 'class-validator';
import { HttpException } from '@nestjs/common/exceptions/http.exception';
import { HttpStatus, Render } from '@nestjs/common';
import {CreateUserDto, ChangePasswordDto, ChangeEmailDto, EditProfileDto,ReviewUserDto, LoginUserDto, CommonUserDto, UserRatingDto} from './dto';
import { MailerService } from '@nest-modules/mailer';
import * as crypto from 'crypto';
import * as md5 from 'md5';
import { exportDefaultSpecifier } from 'babel-types';

import { empty } from 'rxjs';


@Injectable()
export class UserService {
    constructor(
        @InjectRepository(Users)
        private readonly userRepository: Repository<Users>,
        @InjectRepository(UserLogins)
        private readonly userLoginsRepository: Repository<UserLogins>,
        @InjectRepository(UserReviews)
        private readonly userReviewsRepository: Repository<UserReviews>,
        @InjectRepository(UserRatings)
        private readonly userRatingsRepository: Repository<UserRatings>,
        @InjectRepository(Blocks)
        private readonly userBlockRepository: Repository<Blocks>,
        @InjectRepository(Followers)
        private readonly userFollowRepository: Repository<Followers>,
        @InjectRepository(Feedbacks)
        private readonly feedbackRepository: Repository<Feedbacks>,
       
        private readonly mailerService: MailerService,

    ) {}

    /*--------------------------------------------------------------------------
    ----------------------------------------------------------------------------
     * Api Functions
    ----------------------------------------------------------------------------
    --------------------------------------------------------------------------*/  

    // register Api
    async create(dto: CreateUserDto) {

        // check uniqueness of username/email
        const {email, password, first_name, last_name,location, latitude, longitude, user_name, position, device_id, device_token,device_type} = dto;
          const pre_user= await getRepository(Users)
       .createQueryBuilder("users")
       .where("user_name= :user_name",{user_name:user_name})
       .getOne();

        if(pre_user){
          throw new HttpException({error: 'bad_request', message: 'User name already exist'}, HttpStatus.BAD_REQUEST);
        }
       else{
        const qb = await getRepository(Users)
        .createQueryBuilder('users')
        .orWhere('users.email = :email', { email });
  
      const user = await qb.getOne();
      const token = await this.generateRandomString(30);
  
      if (user) {
        // const errors = {username: 'name and email must be unique.'};
        // throw new HttpException({message: 'Input data validation failed', errors}, HttpStatus.BAD_REQUEST);
        throw new HttpException({error: 'bad_request', message: 'email must be unique.'}, HttpStatus.BAD_REQUEST);
  
      }
  
      // create new user
      let newUser = new Users();
      newUser.email = email;
      newUser.password = password;
      newUser.first_name = first_name;
      newUser.last_name = last_name;
      newUser.user_name = user_name;
      newUser.position = position;
      // newUser.location = location;
      // newUser.latitude = latitude;
      // newUser.longitude = longitude;
      //newUser.profile_image = null;
      newUser.email_verification_key = token;
      // newUser.new_email = email;

      try {
        await this
        .mailerService
        .sendMail({
          to: email, // sender address
            subject: 'Email Verification', // Subject line
            template: 'src/user/views/invitation',
            context: {  // Data to be sent to template engine.
              url: process.env.URL,
              token: token,
              text: "Welcome to PGRaters. Thank you for joining us. Connect with other raters on the playground and start rating them. Please click on the following link to verify your account:",
              button: "VERIFY ACCOUNT",
              first_name: first_name,
              last_name: last_name

            }
          // to: email, // sender address
          // subject: 'Testing Nest MailerModule ✔', // Subject line
          // text: 'Hi,Click on the link below to verify your email.Click Here', // plaintext body
          // html: '<p>Hi,</p><p>Click on the link below to verify your email.</p><p><span style="background-color: rgb(255, 255, 255);"><a href="http://18.222.230.168:8003/verify-email/' + token + '"><span style="font-family: Arial;">Click Here</span></a></span></p>', // HTML body content
        })
        .then(() => {})
        .catch(() => {});
      }  
      catch(err) {
        throw new HttpException({error: 'bad_request', message: 'Something went wrong.'}, HttpStatus.BAD_REQUEST);
      }
  
      const errors = await validate(newUser);
      if (errors.length > 0) {
        throw new HttpException({error: 'bad_request', message: 'email must be unique.'}, HttpStatus.BAD_REQUEST);
  
      } else {
        const savedUser = await this.userRepository.save(newUser);
        // create new login
          let loginDevice = new UserLogins();
          loginDevice.user_id=newUser.id;
          loginDevice.device_id = device_id;
          loginDevice.device_token = device_token;
          loginDevice.device_type = device_type;
      const savedLogin = await this.userLoginsRepository.save(loginDevice); 
      return await this.userRepository.findOne(newUser.id);
        
      }
    }
        
    
    }

    // login Api
    async login(dto: LoginUserDto){

      const {email, password, facebook_id, google_id,first_name,user_name, last_name, profile_image, device_id, device_token,device_type, location, latitude,longitude,identifier } = dto;
      var first_time=0;
      console.log(1)
      if(!facebook_id && !google_id) {
        if(!identifier || !password) {
          throw new HttpException({error: 'bad_request', message: 'Email and password are required.'}, HttpStatus.BAD_REQUEST);
        }
        // if(!email|| !password) {
        //   throw new HttpException({error: 'bad_request', message: 'Email and password are required.'}, HttpStatus.BAD_REQUEST);
        // }
      }
      console.log(2)
      const qb = await getRepository(Users)
        .createQueryBuilder('users')
        .addSelect("users.password");
        console.log(3)
      // if(email) {
      //   console.log(4)
      //   qb.where('users.email = :email', { email });
      // }
      if(identifier) {
        console.log(4)
        qb.where('(users.email = :identifier  OR  users.user_name = :identifier)', { identifier });
      }
      if(email) {
        qb.andWhere('users.email = :email', { email });
      }
      if(!password) {
        console.log(5)
        if(facebook_id) {
         console.log(6)
          qb.orWhere('users.facebook_id = :facebook_id', { facebook_id });
        }
        if(google_id) {
          console.log(7)
          qb.orWhere('users.google_id = :google_id', { google_id });
        }
      }
  
      const user = await qb.getOne();
      console.log(user)
      if (user) {
        var user_id = user.id;
        console.log(8)
        if(user.deleted_at != null) {
          console.log(9)
          throw new HttpException({error: 'bad_request', message: 'Your account is deactivated.'}, HttpStatus.BAD_REQUEST);
        }
        if(password) {
          console.log(10)
          // const pass = crypto.createHmac('sha256', password).digest('hex');
          const pass = md5(password);
          if(user.password != pass) 
          {
            console.log(11)
            throw new HttpException({error: 'bad_request', message: 'Incorrect login credentials.'}, HttpStatus.BAD_REQUEST);
          }
        }
        
        else {
          console.log(user)
          console.log(12)
          const update = await getConnection()
          .createQueryBuilder()
          .update(Users)
          .where("id = :id", { id: user.id });
          console.log(13)
          if(email) {
            update.set({ email: email })
          }
          if(facebook_id) {
            update.set({ facebook_id: facebook_id
            })
              // if(user.facebook_id != null){
              //   update.set({  profile_image:profile_image
              //   })    
              // }
             
          }
          if(google_id) {
            update.set({ google_id: google_id})
            // if(user.google_id != null){
            //   update.set({  profile_image:profile_image
            //   })    
            // }
          }
          update.execute();
          
        }
        console.log(14)
      }
      else {
        console.log(15)
        if(facebook_id || google_id) {
          // create new user
          let user1 = new Users();
          user1.email = email;
          user1.password = password ? md5(password) :null;
          user1.facebook_id = facebook_id ? facebook_id : null;
          user1.google_id = google_id ? google_id : null;
          user1.first_name = first_name ? first_name : null;
          user1.last_name = last_name ? last_name : null;
          user1.user_name = user_name ? user_name : null;
          // user1.location = location ? location : null;
          // user1.latitude = latitude ? latitude : null;
          // user1.longitude = longitude ? longitude : null;
          user1.profile_image = profile_image ? profile_image : null;
          const newUser = await this.userRepository.save(user1);
          first_time=1;
          var user_id = newUser.id;
          // create new login
          
        }
        else {
          console.log(16)
          throw new HttpException({error: 'bad_request', message: 'Incorrect login credentials.'}, HttpStatus.BAD_REQUEST);
        }
      }
    
      const loggedInUser = await getRepository(UserLogins)
      .createQueryBuilder("user_logins")
      .where("user_logins.device_token = :device_token", { device_token })
      .getOne();

      if (loggedInUser) {
        console.log(18)
        await getConnection()
      .createQueryBuilder()
      .delete()
      .from(UserLogins)
      .where("user_logins.device_token = :device_token", { device_token })
      .execute();
      }
      console.log(19)
      let loginDevice = new UserLogins();
          loginDevice.user_id=user_id;
          loginDevice.device_id = device_id;
          loginDevice.device_token = device_token;
          loginDevice.device_type = device_type;
          await this.userLoginsRepository.save(loginDevice);
          console.log("user_id:"+user_id)
          const data = await this.userRepository.findOne(user_id);
          console.log("data")
          console.log(data)
          let detail: any = data;
          detail.first_time = first_time;
          console.log("detail")
          console.log(detail)  
          return detail;  
           
    }

     // edit profile
     async editProfile(id: number, dto: EditProfileDto): Promise<object> {
      const {first_name,last_name, user_name, profile_image, age, height, gender, position, fav_nba_team ,fav_nba_player, current_team ,currency, session_range , location ,latitude, longitude,delete_image,
      review_enabled} = dto;
      const current_time = new Date();
      const data = await getRepository(Users)
     .createQueryBuilder('users')
     .where('users.user_name = :user_name', { user_name })
     .andWhere('id != :user_id', {user_id: id})
     .getOne();
      if (data) {
        throw new HttpException({error: 'bad_request', message: 'Username already exists.'}, HttpStatus.BAD_REQUEST);
      }
      const qb = await this.userRepository.findOne(id);
      console.log("profile_image")
      console.log("profile_image"+profile_image)
      if(delete_image==1){
        console.log(1)
          var image: any=null
        
     
      }else{
        console.log(2)
        var image: any =profile_image? profile_image : qb.profile_image
      }
      console.log("image")
      console.log(image)
      await getConnection()
            .createQueryBuilder()
            .update(Users)
            .set({ first_name : first_name? first_name : qb.first_name,
              last_name : last_name? last_name : qb.last_name,
              user_name : user_name? user_name : qb.user_name,
            //  profile_image : profile_image? profile_image : qb.profile_image,
              profile_image : image,
              age : age ? age : qb.age ,
              height : height ? height : qb.height ,
              gender : gender ? gender : qb.gender,
              position : position ? position : qb.position, 
              fav_nba_player : fav_nba_player ? fav_nba_player : qb.fav_nba_player,
              fav_nba_team : fav_nba_team ? fav_nba_team : qb.fav_nba_team,
              current_team : current_team ? current_team : qb.current_team ,
              session_range : session_range ? session_range : qb.session_range , 
              location : location ? location : qb.location, 
              latitude : latitude ? latitude : qb.latitude, 
              longitude : longitude ? longitude : qb.longitude,
              currency : currency ? currency : qb.currency, 
              review_enabled : review_enabled? review_enabled : qb.review_enabled,
              updated_at: current_time })
            .where("id = :id", { id: id })
            .execute();

      return {id: id, message:'Profile Updated Successfully.'};
    }

    // change password
    async changePassword(id: number, dto: ChangePasswordDto): Promise<object> {

      const {old_password, new_password} = dto;
      const qb = await getRepository(Users)
                .createQueryBuilder('users')
                .addSelect("users.password")
                .where('users.id = :id', { id })
                .getOne();
    
      // const old_pass = crypto.createHmac('sha256', old_password).digest('hex');
      const old_pass = md5(old_password);
      console.log(qb.password)
      console.log(old_pass)
      console.log(old_password)
      if(qb.password != old_pass) {
        throw new HttpException({error: 'bad_request', message: 'Wrong Old Password.'}, HttpStatus.BAD_REQUEST);
      }

      // const new_pass = crypto.createHmac('sha256', new_password).digest('hex');
      const new_pass = md5(new_password);

      await getConnection()
            .createQueryBuilder()
            .update(Users)
            .set({ password: new_pass})
            .where("id = :id", { id })
            .execute();

      return {id: id, message:'Password Updated Successfully.'};
    }

     // forgot password
     async forgotPassword(email:string): Promise<object> {
      
      const token = await this.generateRandomString(30);
      console.log(token)
      
      const user = await getRepository(Users)
      .createQueryBuilder('users')
      .where('email = :email', { email })
      .getOne()

      if (!user) {
        throw new HttpException({error: 'bad_request', message: 'User Not Found.'}, HttpStatus.BAD_REQUEST);
  
      }

      //update reset token
     
      await getConnection()
      .createQueryBuilder()
      .update(Users)
      .set({ 
          password_reset_key: token
      })
      .where("email = :email", { email })
      .execute();
      try {
        await this
        .mailerService
        .sendMail({
          // .sendMail({
             to: email, // sender address
               subject: 'PGRaters Password Reset', // Subject line
               template: 'src/user/views/reset-password',
               context: {  // Data to be sent to template engine.
                 url: process.env.URL,
                 token: token,
                 text: "We've received a request to reset your password, If you didn't make the request, just ignore this email. Otherwise, you can reset your password using this link:",
                 first_name: user.first_name,
                 last_name: user.last_name
   
               }
             // to: email, // sender address
             // subject: 'Testing Nest MailerModule ✔', // Subject line
             // text: 'Hi,Click on the link below to verify your email.Click Here', // plaintext body
             // html: '<p>Hi,</p><p>Click on the link below to verify your email.</p><p><span style="background-color: rgb(255, 255, 255);"><a href="http://18.222.230.168:8003/verify-email/' + token + '"><span style="font-family: Arial;">Click Here</span></a></span></p>', // HTML body content
         //  })
           
           // to: users[i].email, // sender address
           // subject: 'Testing Nest MailerModule ✔', // Subject line
           // text: 'Hi,Click on the link below to verify your email.Click Here', // plaintext body
           // html: '<p>Hi,</p><p>Click on the link below to verify your email.</p><p><span style="background-color: rgb(255, 255, 255);"><a href="http://http://18.223.231.202:5002/verify-email/' + users[i].email_verification_key+ '"><span style="font-family: Arial;">Click Here</span></a></span></p>', // HTML body content
         })
        // .sendMail({
        //   to: email, // sender address
        //   subject: 'Testing Nest MailerModule ✔', // Subject line
        //   text: 'Hi,Click on the link below to reset your password.Click Here', // plaintext body
        //   html: '<p>Hi,</p><p>Click on the link below to reset your password.</p><p><span style="background-color: rgb(255, 255, 255);"><a href="http://localhost:3009/reset-password/' + token + '"><span style="font-family: Arial;">Click Here</span></a></span></p>', // HTML body content
        // })
        .then(() => {})
        .catch(() => {});

        return {message:'Email has been sent on your email id.'};
      }  
      catch(err) {
        throw new HttpException({error: 'bad_request', message: 'Something went wrong.'}, HttpStatus.BAD_REQUEST);
      }
      
    }

    // reset password
    async resetPassword(token:string, req): Promise<object> {
  
      const reset = await getRepository(Users)
      .createQueryBuilder('users')
      .where('password_reset_key = :token', { token })
      .getOne();

      if (!reset) {
        throw new HttpException({error: 'bad_request', message: 'Invalid Token.'}, HttpStatus.BAD_REQUEST);
  
      }
      const user_id = reset.id;

    // if(req.body.password) {
        const new_pass = md5(req.body.password);
        console.log(new_pass)

        await getConnection()
                    .createQueryBuilder()
                    .update(Users)
                    .set({ password: new_pass, password_reset_key : null})
                    .where("email = :email", { email: reset.email })
                    .execute();

     // }
      // else {
      //   throw new HttpException({error: 'bad_request', message: 'Password and Password Confirmation do not match.'}, HttpStatus.BAD_REQUEST);
      // }
      return {message:'Reset Password Complete.'};
      
    }

    // verify email
    async verifyEmail(token:string, req): Promise<object> {

      const verify = await getRepository(Users)
      .createQueryBuilder('users')
      .where('email_verification_key = :token', { token })
      .getOne();

      if (!verify) {
        throw new HttpException({error: 'bad_request', message: 'Invalid Token.'}, HttpStatus.BAD_REQUEST);
  
      }

      await getConnection()
                  .createQueryBuilder()
                  .update(Users)
                  .set({ 
                    // email: verify.new_email, 
                    is_email_verified: 1, 
                    email_verification_key: null
                  })
                  .where("id = :user_id", { user_id: verify.id })
                  .execute();
      
      return {message:'Email Verified Successfully.'};
      
    }

    // resend verify email
    async resendVerifyEmail(req): Promise<object> {

      const token = await this.generateRandomString(30);
      const email = req.body.email;
      await getConnection()
      .createQueryBuilder()
      .update(Users)
      .set({ 
          email_verification_key: token
      })
      .where("email = :email", { email })
      .execute();

      try {
        await this
        .mailerService
        .sendMail({
          to: email, // sender address
          subject: 'Testing Nest MailerModule ✔', // Subject line
          text: 'Hi,Click on the link below to verify your email.Click Here', // plaintext body
          html: '<p>Hi,</p><p>Click on the link below to verify your email.</p><p><span style="background-color: rgb(255, 255, 255);"><a href="http://localhost:3009/verify-email/' + token + '"><span style="font-family: Arial;">Click Here</span></a></span></p>', // HTML body content
        })
        .then(() => {})
        .catch(() => {});
      }  
      catch(err) {
        throw new HttpException({error: 'bad_request', message: 'Something went wrong.'}, HttpStatus.BAD_REQUEST);
      }
      
      return {message:'Verification link sent on your email.'};
    }

    //review user
    async userReview(logged_id: number, dto:ReviewUserDto) {
      const {id, user_id, review}  =dto
      if(id){
        const qb = await this.userReviewsRepository.findOne(id);
        await getConnection()
            .createQueryBuilder()
            .update(UserReviews)
            .set({ review : review? review: qb.review})
            .where("id = :id", { id: id })
            .execute();
      }
      else{
        const user = await getRepository(UserReviews)
        .createQueryBuilder("user_reviews")
        .where("user_reviews.user_id_1 = :user_id", { user_id:logged_id })
        .andWhere("user_reviews.user_id_2 = :id", { id: user_id })
        .getCount();
        if(user>=5){
          throw new HttpException({error: 'bad_request', message: 'You have already given reviews'}, HttpStatus.BAD_REQUEST);
        }
    
        let new_review = new UserReviews();
        new_review.user_id_1 = logged_id;
        new_review.user_id_2 = user_id;
        new_review.review = review;
        const savedReview = await this.userReviewsRepository.save(new_review);

        console.log("start")
        var user1 =await this.userRepository.findOne({
         where: {
           id: logged_id
         }
       });
        var notifMessage=user1.first_name+" "+user1.last_name+" left review on your profile";
        //follow notification
      
        return {message:"Review successfully given"};
      }
          
      //return user;
    }

    //block user
    async blockUser(logged_id: number, dto:CommonUserDto) {
      const {user_id}  =dto
        const user = await getRepository(Blocks)
        .createQueryBuilder("blocks")
        .where("blocks.user_id_1 = :user_id", { user_id:logged_id })
        .andWhere("blocks.user_id_2 = :id", { id: user_id })
        .getOne();

        if(user){
          console.log(user)
          await getRepository(Blocks)
          .createQueryBuilder("blocks")
          .delete()
          .from(Blocks)
          .where("blocks.user_id_2 = :id", { id: user_id })
          .execute();
        }
        else{
          let block = new Blocks();
          block.user_id_1 = logged_id;
          block.user_id_2 = user_id;
          
          const savedReview = await this.userBlockRepository.save(block);
        }      
        return {message:"successful"};
    }

    //follow user
    async followUser(logged_id: number, dto:CommonUserDto) {
      const {user_id}  =dto
       //for notification
          var user1 =await this.userRepository.findOne({
          where: {
            id: logged_id
          }
        });
        const user = await getRepository(Followers)
        .createQueryBuilder("followers")
        .where("followers.user_id_1 = :user_id", { user_id:logged_id })
        .andWhere("followers.user_id_2 = :id", { id: user_id })
        .getOne();
        if(user){
          console.log(user)
          await getRepository(Followers)
          .createQueryBuilder("followers")
          .delete()
          .from(Followers)
          .where("followers.user_id_2 = :id", { id: user_id })
          .execute();
       //   var notifMessage="has unfollowed you";
          }
        else{
          let follow = new Followers();
          follow.user_id_1 = logged_id;
          follow.user_id_2 = user_id;
          var notifMessage=user1.first_name+" "+user1.last_name+" has followed you";
        const savedReview = await this.userFollowRepository.save(follow);
        console.log("start")
        //follow notification
   
      
        }  
        return {message:"successful"};
    }

    //listing of followers/following API
    async getFollowers(logged_id:any ,req){

      var type =req.query.type;
      var skip = 0;
         var limit = req.query.limit ? req.query.limit : 10;
         if(req.query.page) {
             const page = req.query.page;
             var skip = limit * page;
         }

      if(req.query.user_id){
        var user_id=req.query.user_id
      }
      else{
        var user_id=logged_id
      }
      const qb= await getRepository(Followers)
      .createQueryBuilder("followers")
      
      if(type==1){
        qb.leftJoinAndSelect(Users, "users", "users.id = followers.user_id_1")
        .select("users.id, users.user_name, users.first_name,IF(TIMESTAMPDIFF(MINUTE, users.`updated_at`,UTC_TIMESTAMP)>5,0,1) AS is_online, users.last_name, users.profile_image,users.position,(SELECT count(id) FROM followers WHERE user_id_1 = " + logged_id + " AND user_id_2=users.id ) AS is_follow,((SELECT ROUND(AVG(passing_ability),1) FROM `user_ratings` where user_id_2=users.id AND passing_ability IS NOT NULL) + (SELECT ROUND(AVG(shooting_ability),1) FROM `user_ratings` where user_id_2=users.id AND shooting_ability IS NOT NULL) +( SELECT ROUND(AVG(dribbling_ability),1) FROM `user_ratings` where user_id_2=users.id AND dribbling_ability IS NOT NULL)+ ( SELECT ROUND(AVG(steals),1) FROM `user_ratings` where user_id_2=users.id AND steals IS NOT NULL)+( SELECT ROUND(AVG(rebounds),1) FROM `user_ratings` where user_id_2=users.id AND rebounds IS NOT NULL)+( SELECT ROUND(AVG(blocks),1) FROM `user_ratings` where user_id_2=users.id AND blocks IS NOT NULL))/6 AS avg_rating")
        .where("followers.user_id_2="+ user_id)
        .offset(skip).limit(limit).getRawMany();
      }
      if(type==2){
        qb.leftJoinAndSelect(Users, "users", "users.id = followers.user_id_2")
        .select("users.id, users.user_name,IF(TIMESTAMPDIFF(MINUTE, users.`updated_at`,UTC_TIMESTAMP)>5,0,1) AS is_online,users.first_name, users.last_name, users.profile_image,users.position,(SELECT count(id) FROM followers WHERE user_id_1 = " + logged_id + " AND user_id_2=users.id ) AS is_follow,((SELECT ROUND(AVG(passing_ability),1) FROM `user_ratings` where user_id_2=users.id AND passing_ability IS NOT NULL) + (SELECT ROUND(AVG(shooting_ability),1) FROM `user_ratings` where user_id_2=users.id AND shooting_ability IS NOT NULL) +( SELECT ROUND(AVG(dribbling_ability),1) FROM `user_ratings` where user_id_2=users.id AND dribbling_ability IS NOT NULL)+ ( SELECT ROUND(AVG(steals),1) FROM `user_ratings` where user_id_2=users.id AND steals IS NOT NULL)+( SELECT ROUND(AVG(rebounds),1) FROM `user_ratings` where user_id_2=users.id AND rebounds IS NOT NULL)+( SELECT ROUND(AVG(blocks),1) FROM `user_ratings` where user_id_2=users.id AND blocks IS NOT NULL))/6 AS avg_rating")
        .where("followers.user_id_1="+ user_id)
        .offset(skip).limit(limit).getRawMany();
      }
      const count = await qb.getCount();
      var data= await qb.getRawMany();
        return {data, count};
    }

    //rate user
    async rateUser(logged_id: number, dto:UserRatingDto) {
          const {user_id, passing_ability, shooting_ability, dribbling_ability, steals, rebounds, blocks,fitness, speed, attendence, is_session_rating, is_offensive, is_defensive, is_more}  =dto

          const current_time = new Date();
          const rating = await getRepository(UserRatings)
          .createQueryBuilder("user_ratings")
          .where("user_ratings.user_id_1 = :user_id", { user_id:logged_id })
          .andWhere("user_ratings.user_id_2 = :id", { id: user_id })
         .getOne();
         if(rating){
          await getConnection()
          .createQueryBuilder()
          .update(UserRatings)
          .set({ user_id_1 : logged_id ? logged_id : rating.user_id_1,
            user_id_2 : user_id? user_id : rating.user_id_2,
            passing_ability : passing_ability? passing_ability : rating.passing_ability,
            shooting_ability : shooting_ability? shooting_ability : rating.shooting_ability,
            dribbling_ability : dribbling_ability ? dribbling_ability : rating.dribbling_ability ,
            steals : steals ? steals : rating.steals ,
            rebounds : rebounds ? rebounds : rating.rebounds,
            blocks : blocks ? blocks : rating.blocks, 
            fitness : fitness ? fitness : rating.fitness,
            speed : speed ? speed : rating.speed,
            attendence : attendence ? attendence : rating.attendence ,
            is_session_rating : is_session_rating ? is_session_rating : rating.is_session_rating , 
            offensive_at : is_offensive == 1 ? current_time : rating.offensive_at, 
            defensive_at : is_defensive ==1 ? current_time : rating.defensive_at,
            more_at : is_more == 1 ? current_time : rating.more_at,
            updated_at: current_time })
          .where("id = :id", { id: rating.id })
          .execute();

         }else{
          let new_rating = new UserRatings();
          new_rating.user_id_1 = logged_id;
          new_rating.user_id_2 = user_id;  
          new_rating.passing_ability= passing_ability;
          new_rating.shooting_ability= shooting_ability;
          new_rating.dribbling_ability= dribbling_ability;
          new_rating.steals= steals;
          new_rating.rebounds= rebounds;
          new_rating.blocks= blocks;
          new_rating.fitness= fitness;
          new_rating.speed= speed;
          new_rating.attendence= attendence;
          new_rating.is_session_rating=is_session_rating;
          new_rating.offensive_at=is_offensive ==1? current_time : null;
          new_rating.defensive_at=is_defensive ==1? current_time : null;
          new_rating.more_at=is_more==1 ? current_time :null;
        
        const savedReview = await this.userRatingsRepository.save(new_rating);
         }
         console.log("start")
         var user =await this.userRepository.findOne({
          where: {
            id: logged_id
          }
        });
         var notifMessage=user.first_name+" "+user.last_name+" rated your game";
         //follow notification
       
            
        return {message:"successfully rated"};
    }

    //deactivate account
    async deactivate(id: number): Promise<object> {
      const current_time = new Date();
      await getConnection()
            .createQueryBuilder()
            .update(Users)
            .set({ deleted_at: current_time })
            .where("id = :id", { id: id })
            .execute();

      return {id: id, message:'Deactivated Successfully.'};
    }

    //Log out 
    async logOut(id: number, req): Promise<object> {
      const device_token = req.body.device_token;
      await getRepository(UserLogins)
      .createQueryBuilder('user_logins')
      .where("user_logins.device_token = :device_token", { device_token })
      .andWhere("user_logins.user_id = :id", { id :id })
      .delete()
      .execute();
      return {id: id, message:'logged out Successfully.'};
    }

     // Is Online API
     async userOnline(logged_id: number, req){
      const is_online= req.body.is_online ;
      const current_time = new Date();

      const qb=  await getRepository(Users)
                  .createQueryBuilder("users")
                  .update(Users)
                  .set({  is_online: is_online})
                  .where("id = :user_id", { user_id: logged_id })
                  .execute()
        return ({message:"successful"	})
    }

    //feedback
    async feedback(logged_id: number, dto:CommonUserDto) {
      const {feedback}  =dto
      
    
        let new_feedback = new Feedbacks();
        new_feedback.user_id = logged_id;
        new_feedback.feedback = feedback;
        const savedFeedback = await this.feedbackRepository.save(new_feedback);
        return {message:"Feedback sent"};
      
          
      //return user;
    }

    // resend email
    async resendEmail(): Promise<object> {
      const users = await getRepository(Users)
                  .createQueryBuilder('users')
                  .select("first_name,last_name,email_verification_key, email ")
                  .where('users.is_email_verified =0')
                  .andWhere("(MOD(DATEDIFF(CURRENT_TIMESTAMP,users.created_at),3)=0)")
                  .getRawMany()
                  for(const i in users){
                    try {
                      await this
                      .mailerService
                      .sendMail({
                       // .sendMail({
                          to: users[i].email, // sender address
                            subject: 'Email Verification', // Subject line
                            template: 'src/user/views/verification-reminder',
                            context: {  // Data to be sent to template engine.
                              url: process.env.URL,
                              token: users[i].email_verification_key,
                              text: "Click on the link below to verify your email.Click Here",
                             // button: "VERIFY ACCOUNT",
                              first_name: users[i].first_name,
                              last_name: users[i].last_name
                
                            }
                      })
                      .then(() => {})
                      .catch(() => {});
                    }  
                    catch(err) {
                      throw new HttpException({error: 'bad_request', message: 'Something went wrong.'}, HttpStatus.BAD_REQUEST);
                    }
                  }
     
      
      return {message:'Verifification link sent on your email.'};
    }


    // profile API
    async profile(id: any, req) {
    
      if(req.query.id) {
        var user_id = req.query.id;
      }
      else {
        var user_id = id;
      }
   
      const data=await this.userRepository.findOne(user_id);
      //user ratings
      const ratings = await getRepository(UserRatings)
      .createQueryBuilder("user_ratings")
      .select("((SELECT ROUND(AVG(passing_ability),1) FROM `user_ratings` where user_id_2="+ user_id +" AND passing_ability IS NOT NULL)+(SELECT ROUND(AVG(shooting_ability),1) FROM `user_ratings` where user_id_2="+ user_id +" AND shooting_ability IS NOT NULL) +( SELECT ROUND(AVG(dribbling_ability),1) FROM `user_ratings` where user_id_2="+ user_id +" AND dribbling_ability IS NOT NULL))/3 AS offense_ability,(SELECT ROUND(AVG(passing_ability),1) FROM `user_ratings` where user_id_2="+ user_id +" AND passing_ability IS NOT NULL) AS  passing_ability, (SELECT ROUND(AVG(shooting_ability),1) FROM `user_ratings` where user_id_2="+ user_id +" AND shooting_ability IS NOT NULL) AS shooting_ability ,( SELECT ROUND(AVG(dribbling_ability),1) FROM `user_ratings` where user_id_2="+ user_id +" AND dribbling_ability IS NOT NULL) AS dribbling_ability,"
      +"(( SELECT ROUND(AVG(steals),1) FROM `user_ratings` where user_id_2="+ user_id + " AND steals IS NOT NULL) +( SELECT ROUND(AVG(rebounds),1) FROM `user_ratings` where user_id_2="+ user_id +" AND rebounds IS NOT NULL)+( SELECT ROUND(AVG(blocks),1) FROM `user_ratings` where user_id_2="+ user_id +" AND blocks IS NOT NULL))/3 AS defense_ability,"
      +" ( SELECT ROUND(AVG(steals),1) FROM `user_ratings` where user_id_2="+ user_id + " AND steals IS NOT NULL) AS steals,"
      +"( SELECT ROUND(AVG(rebounds),1) FROM `user_ratings` where user_id_2="+ user_id +" AND rebounds IS NOT NULL) AS rebounds,"
      +"( SELECT ROUND(AVG(blocks),1) FROM `user_ratings` where user_id_2="+ user_id +" AND blocks IS NOT NULL) AS blocks,"
      +"( SELECT ROUND(AVG(fitness),1) FROM `user_ratings` where user_id_2="+ user_id +" AND fitness IS NOT NULL) AS fitness,"
      +"( SELECT ROUND(AVG(speed),1) FROM `user_ratings` where user_id_2="+ user_id +" AND speed IS NOT NULL) AS speed,"
      +"( SELECT ROUND(AVG(attendence),1) FROM `user_ratings` where user_id_2="+ user_id +" AND attendence IS NOT NULL) AS attendence")
      .andWhere('user_id_2 = :user_id', {user_id : user_id})
      .getRawOne();

      //review array
      // const reviews = await getRepository(UserReviews) 
      // .createQueryBuilder("user_reviews")
      // .leftJoinAndSelect(Users, "users", "users.id = user_reviews.user_id_1")
      // .select(["user_reviews.*", "users.first_name as first_name", "users.last_name as last_name","users.user_name as user_name","users.profile_image AS profile_image"])
      // .addSelect(" ( CASE "
      //   + " WHEN TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, user_reviews.`created_at`) != 0 THEN"
      //   + " IF(abs(TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, user_reviews.`created_at`)) > 1 , CONCAT(abs(TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, user_reviews.`created_at`)) ,' years ago'), CONCAT(abs(TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, user_reviews.`created_at`)) ,' year ago'))"
      //   + " WHEN TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, user_reviews.`created_at`) != 0 THEN"
      //   + " IF(abs(TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, user_reviews.`created_at`)) > 1 , CONCAT(abs(TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, user_reviews.`created_at`)) ,' months ago'), CONCAT(abs(TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, user_reviews.`created_at`)) ,' month ago'))"
        
      //   + " WHEN HOUR(TIMEDIFF(UTC_TIMESTAMP, user_reviews.`created_at`)) != 0 THEN" 
      //   + " IF(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, user_reviews.`created_at`))) < 24 , IF(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, user_reviews.`created_at`))) > 1 , CONCAT(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, user_reviews.`created_at`))) ,' hours ago'), CONCAT(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, user_reviews.`created_at`))) ,' hour ago'))," 
      //   + " IF(DATEDIFF(UTC_TIMESTAMP, user_reviews.`created_at`) > 1 , CONCAT(DATEDIFF(UTC_TIMESTAMP, user_reviews.`created_at`) ,' days ago'), CONCAT(DATEDIFF(UTC_TIMESTAMP, user_reviews.`created_at`) ,' day ago')))"
        
      //   + " WHEN MINUTE(TIMEDIFF(UTC_TIMESTAMP, user_reviews.`created_at`)) != 0 THEN CONCAT(MINUTE(TIMEDIFF(UTC_TIMESTAMP, user_reviews.`created_at`)) ,' min ago')"
      //   + " ELSE "
      //   + " CONCAT(SECOND(TIMEDIFF(UTC_TIMESTAMP, user_reviews.`created_at`)) ,'s ago')"
      //   + " END  )", "reviewed_since")
      // .where("user_reviews.user_id_2 = :user_id", { user_id: user_id })
      // .andWhere("user_reviews.review_status=1")
      // .getRawMany();
    //console.log(reviews)
      // response
      let detail:any = data;
      detail.user_id = user_id;
      detail.ratings = ratings ? ratings : null;
      //detail.reviews = reviews;
      return detail;  
    }

    // User Listing api
    async userListing(user_id : any , req){
        // const user = await this.userRepository.findOne({ id: user_id });
        var event_id = req.query.event_id;
        var team_id = req.query.team_id;
        var type = req.query.type;
        var keyword = req.query.keyword;
        var skip = 0;
        var limit = req.query.limit ? req.query.limit : 1000;
        if(req.query.page) {
            const page = req.query.page;
            var skip = limit * page;
        }
        const qb = await getRepository(Users)
       .createQueryBuilder("users")
       .select("id, user_name,first_name, last_name, profile_image,position,(SELECT count(id) FROM followers WHERE user_id_1 = " + user_id + " AND user_id_2=users.id ) AS is_follow,((SELECT ROUND(AVG(passing_ability),1) FROM `user_ratings` where user_id_2=users.id AND passing_ability IS NOT NULL) + (SELECT ROUND(AVG(shooting_ability),1) FROM `user_ratings` where user_id_2=users.id AND shooting_ability IS NOT NULL) +( SELECT ROUND(AVG(dribbling_ability),1) FROM `user_ratings` where user_id_2=users.id AND dribbling_ability IS NOT NULL)+ ( SELECT ROUND(AVG(steals),1) FROM `user_ratings` where user_id_2=users.id AND steals IS NOT NULL)+( SELECT ROUND(AVG(rebounds),1) FROM `user_ratings` where user_id_2=users.id AND rebounds IS NOT NULL)+( SELECT ROUND(AVG(blocks),1) FROM `user_ratings` where user_id_2=users.id AND blocks IS NOT NULL))/6 AS avg_rating")
       .where("users.id != "+user_id);
       
    
       if(keyword){
        qb.andWhere('(user_name like :keyword OR first_name like :keyword OR last_name like :keyword)', {keyword : '%' + keyword + '%'});
       }
       const users = await qb.offset(skip).limit(limit).orderBy("is_follow","DESC").addOrderBy("id","DESC").getRawMany();
       const count = await qb.getCount();
         return{ users, count};
      //  const users = qb.getRawMany();

      //    return users ;      
    }

    //user leaderboard api
   async userLeaderboard( logged_id: any, req ){  
      //calculate pgr of each user
      var global = req.query.global;
      var keyword = req.query.keyword;
      var latitude = req.query.latitude;
      var longitude = req.query.longitude;
      var skip = 0;
      var limit = req.query.limit ? req.query.limit : 10;
      if(req.query.page) {
          const page = req.query.page;
          var skip = limit * page;
      }
      const qb = await getRepository(Users)
      .createQueryBuilder("users")
      .select("id,user_name,first_name,profile_image,position,last_name,location, latitude,IF(TIMESTAMPDIFF(MINUTE, users.`updated_at`,UTC_TIMESTAMP)>5,0,1) AS is_online, longitude,"
      +" IFNULL((((SELECT ROUND(AVG(passing_ability),1) FROM `user_ratings` where user_id_2=users.id AND passing_ability IS NOT NULL) + (SELECT ROUND(AVG(shooting_ability),1) FROM `user_ratings` where user_id_2=users.id AND shooting_ability IS NOT NULL) +( SELECT ROUND(AVG(dribbling_ability),1) FROM `user_ratings` where user_id_2=users.id AND dribbling_ability IS NOT NULL)+ ( SELECT ROUND(AVG(steals),1) FROM `user_ratings` where user_id_2=users.id AND steals IS NOT NULL)+( SELECT ROUND(AVG(rebounds),1) FROM `user_ratings` where user_id_2=users.id AND rebounds IS NOT NULL)+( SELECT ROUND(AVG(blocks),1) FROM `user_ratings` where user_id_2=users.id AND blocks IS NOT NULL))/3)*10,0) AS pgr")
      .where("IFNULL((((SELECT ROUND(AVG(passing_ability),1) FROM `user_ratings` where user_id_2=users.id AND passing_ability IS NOT NULL) + (SELECT ROUND(AVG(shooting_ability),1) FROM `user_ratings` where user_id_2=users.id AND shooting_ability IS NOT NULL) +( SELECT ROUND(AVG(dribbling_ability),1) FROM `user_ratings` where user_id_2=users.id AND dribbling_ability IS NOT NULL)+ ( SELECT ROUND(AVG(steals),1) FROM `user_ratings` where user_id_2=users.id AND steals IS NOT NULL)+( SELECT ROUND(AVG(rebounds),1) FROM `user_ratings` where user_id_2=users.id AND rebounds IS NOT NULL)+( SELECT ROUND(AVG(blocks),1) FROM `user_ratings` where user_id_2=users.id AND blocks IS NOT NULL))/3)*10,0)> 0")
      
   
     
      if(global==0) {
        if(!latitude || !longitude) {
          throw new HttpException({error: 'bad_request', message: 'Please add lat and lng for this location.'}, HttpStatus.BAD_REQUEST);
        }
        qb.addSelect("IFNULL(ROUND(( 6371 * acos (cos ( radians( " + latitude + " ) ) * cos( radians( users.latitude) )"
        + " * cos( radians( users.longitude ) - radians( " + longitude + " ) ) + sin ( radians( " + latitude + " ) )"
        + " * sin( radians( users.latitude ) ) ) ), 2), 0)", "distance")
        .andWhere("IFNULL(ROUND(( 6371 * acos (cos ( radians( " + latitude + " ) ) * cos( radians( users.latitude ) )"
        + " * cos( radians( users.longitude ) - radians( " + longitude + " ) ) + sin ( radians( " + latitude + " ) )"
        + " * sin( radians( users.latitude ) ) ) ), 2), 0) < 500")

      }

      const data1 = await qb.orderBy("pgr", "DESC").addOrderBy('id', "ASC").getRawMany();
      if(keyword){
        qb.andWhere('(user_name like :keyword OR last_name like :keyword OR first_name like :keyword )', {keyword : '%' + keyword + '%'})
      }
      const data = await qb.orderBy("pgr", "DESC").addOrderBy('id', "ASC").offset(skip).limit(limit).getRawMany();
      //count all results
      const total = await qb.getCount();
      var count= 0;
      
      for (const i in data1) {
        data1[i].rank = count + 1;
        // data1[i].avg_rating = (data1[i].pgr)/20.0;
        count = count + 1;
      }
      console.log(data1.length)
      console.log(data.length)
      for (const i in data) {
        // data1[i].rank = count + 1;
        data[i].avg_rating = (data[i].pgr)/20.0;
        for (const j in data1) {
          if(data[i].id == data1[j].id) {
            console.log("i"+data[i].id)
            console.log("j"+data1[j].id)
            console.log("jrank"+data1[j].rank)
            data[i].rank = data1[j].rank;
          }
          // data1[i].rank = count + 1;
          // data1[i].avg_rating = (data1[i].pgr)/20.0;
          // count = count + 1;
        }
        // count = count + 1;
      }
      return {data,total}
   }

   // all reviews API
   async findAllReviews(logged_id: any, req) {
   
      var user_id = req.query.id;
      var skip = 0;
      var limit = req.query.limit ? req.query.limit : 10;
      if(req.query.page) {
        const page = req.query.page;
        var skip = limit * page;
    }
    //review array
    const qb = await getRepository(UserReviews) 
    .createQueryBuilder("user_reviews")
    .leftJoinAndSelect(Users, "users", "users.id = user_reviews.user_id_1")
    .select(["user_reviews.*", "users.first_name as first_name", "users.last_name as last_name","users.user_name as user_name","users.profile_image AS profile_image"])
    .addSelect(" ( CASE "
      + " WHEN TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, user_reviews.`created_at`) != 0 THEN"
      + " IF(abs(TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, user_reviews.`created_at`)) > 1 , CONCAT(abs(TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, user_reviews.`created_at`)) ,' years ago'), CONCAT(abs(TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, user_reviews.`created_at`)) ,' year ago'))"
      + " WHEN TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, user_reviews.`created_at`) != 0 THEN"
      + " IF(abs(TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, user_reviews.`created_at`)) > 1 , CONCAT(abs(TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, user_reviews.`created_at`)) ,' months ago'), CONCAT(abs(TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, user_reviews.`created_at`)) ,' month ago'))"
      
      + " WHEN HOUR(TIMEDIFF(UTC_TIMESTAMP, user_reviews.`created_at`)) != 0 THEN" 
      + " IF(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, user_reviews.`created_at`))) < 24 , IF(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, user_reviews.`created_at`))) > 1 , CONCAT(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, user_reviews.`created_at`))) ,' hours ago'), CONCAT(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, user_reviews.`created_at`))) ,' hour ago'))," 
      + " IF(DATEDIFF(UTC_TIMESTAMP, user_reviews.`created_at`) > 1 , CONCAT(DATEDIFF(UTC_TIMESTAMP, user_reviews.`created_at`) ,' days ago'), CONCAT(DATEDIFF(UTC_TIMESTAMP, user_reviews.`created_at`) ,' day ago')))"
      
      + " WHEN MINUTE(TIMEDIFF(UTC_TIMESTAMP, user_reviews.`created_at`)) != 0 THEN CONCAT(MINUTE(TIMEDIFF(UTC_TIMESTAMP, user_reviews.`created_at`)) ,' min ago')"
      + " ELSE "
      + " CONCAT(SECOND(TIMEDIFF(UTC_TIMESTAMP, user_reviews.`created_at`)) ,'s ago')"
      + " END  )", "reviewed_since")
    .where("user_reviews.user_id_2 = :user_id", { user_id: user_id })
    if(logged_id!=user_id){
      qb.andWhere("user_reviews.review_status=1");
    } 
    const reviews=await qb.offset(skip).limit(limit)
    .orderBy("user_reviews.id","DESC")
    .getRawMany();
    const count = await qb.getCount();
    return {reviews:reviews,count:count};  
  }

    // review enable disable Api
    async reviewEnable(logged_id: number,dto: ReviewUserDto) {

      const {id,review_status} = dto;
     
      const review= await getRepository(UserReviews)
      .createQueryBuilder("user_reviews")
      .update(UserReviews)
      .set({review_status:review_status})
      .where("id= :id",{id:id})
      .execute();

        return ({message:"successful"});
      }  
     
   // checkVersion API
   async checkVersion( req): Promise<object> {
     var version= req.body.version;
     if(version>=3){
      return {message:'success'};
     }
else{
      throw new HttpException({error: 'bad_request', message: 'Update app to the latest version'}, HttpStatus.BAD_REQUEST);

    }
  }


    /*--------------------------------------------------------------------------
    ----------------------------------------------------------------------------
     * Access Token Functions
    ----------------------------------------------------------------------------
    --------------------------------------------------------------------------*/        


    // create jwt token
    public async createToken(userData) {
      
      // const expiresIn = 3600,
      const expiresIn = '90 days',
        secretOrKey = process.env.SECRET;
      const user = { id: userData.id, email: userData.email };
      console.log(user)
      const token = jwt.sign(user, secretOrKey, { expiresIn });
      return {
        expires_in: expiresIn,
        access_token: token
      };
    }

    // validate user with jwt token
    public async validateUser(signedUser): Promise<boolean> {
      // console.log('2')
      try {
        const user = await this.userRepository.findOne({ id: signedUser.id });
        return user ? true : false;
      } catch (err) {
        return false;
      }
    }

    //data from jwt token
    public async jwtData(token) {
      try {
        const user: any = jwt.verify(token, process.env.SECRET);

        console.log(user)
        if(user) {
          const current_time = new Date();

          const qb=  await getRepository(Users)
                  .createQueryBuilder("events")
                  .update(Users)
                  .set({ updated_at : current_time})
                  .where("id = :user_id", { user_id:user.id })
                  .execute()
          return user;
        }
        else {
          throw new HttpException({error: 'bad_request', message: 'User not Found.'}, HttpStatus.BAD_REQUEST);
        }
      } catch (err) {
        throw new HttpException({error: 'bad_request', message: 'User not Found.'}, HttpStatus.BAD_REQUEST);
      }
    }

    public async generateRandomString(length): Promise<string> {
      var text = "";
      const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

      for (var i = 0; i < length; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

      return text;
    }

/*--------------------------------------------------------------------------
    ----------------------------------------------------------------------------
     * Common Functions
    ----------------------------------------------------------------------------
    --------------------------------------------------------------------------*/        



    //get profile
    public async getProfile(user_id,latitude,longitude, logged_id){
      try {
     //all users data
      const user = await getRepository(Users)
      .createQueryBuilder("users")
      .select("*,IF(TIMESTAMPDIFF(MINUTE, users.`updated_at`,UTC_TIMESTAMP)>5,0,1) AS is_online,DATE_FORMAT(users.age, '%Y-%m-%d') AS age,(SELECT count(id) FROM event_members WHERE member_id = users.id AND is_admin=1) AS session_organized, (SELECT count(id) FROM event_members WHERE member_id = users.id AND is_join=1) AS session_attended,(SELECT count(id) FROM user_ratings WHERE user_id_2 = users.id) AS total_ratings, (SELECT count(id) FROM followers WHERE user_id_1 = users.id) AS following, (SELECT count(id) FROM followers WHERE user_id_2 = users.id) AS followers,(SELECT count(id) FROM followers WHERE user_id_1 = " + logged_id + " AND user_id_2=users.id ) AS is_follow,"
      +" IFNULL((((SELECT ROUND(AVG(passing_ability),1) FROM `user_ratings` where user_id_2=users.id AND passing_ability IS NOT NULL) + (SELECT ROUND(AVG(shooting_ability),1) FROM `user_ratings` where user_id_2=users.id AND shooting_ability IS NOT NULL) +( SELECT ROUND(AVG(dribbling_ability),1) FROM `user_ratings` where user_id_2=users.id AND dribbling_ability IS NOT NULL)+ ( SELECT ROUND(AVG(steals),1) FROM `user_ratings` where user_id_2=users.id AND steals IS NOT NULL)+( SELECT ROUND(AVG(rebounds),1) FROM `user_ratings` where user_id_2=users.id AND rebounds IS NOT NULL)+( SELECT ROUND(AVG(blocks),1) FROM `user_ratings` where user_id_2=users.id AND blocks IS NOT NULL))/3)*10,0) AS pgr ")
      .where('id = :user_id', {user_id : user_id})
      .getRawOne();

    
      //calculate pgr of each user
      const leaders = await getRepository(Users)
      .createQueryBuilder("users")
      .select("users.id,"
      +" ROUND(IFNULL((((SELECT ROUND(AVG(passing_ability),1) FROM `user_ratings` where user_id_2=users.id AND passing_ability IS NOT NULL) + (SELECT ROUND(AVG(shooting_ability),1) FROM `user_ratings` where user_id_2=users.id AND shooting_ability IS NOT NULL) +( SELECT ROUND(AVG(dribbling_ability),1) FROM `user_ratings` where user_id_2=users.id AND dribbling_ability IS NOT NULL)+ ( SELECT ROUND(AVG(steals),1) FROM `user_ratings` where user_id_2=users.id AND steals IS NOT NULL)+( SELECT ROUND(AVG(rebounds),1) FROM `user_ratings` where user_id_2=users.id AND rebounds IS NOT NULL)+( SELECT ROUND(AVG(blocks),1) FROM `user_ratings` where user_id_2=users.id AND blocks IS NOT NULL))/3)*10,0),1) AS pgr")
      .andWhere("IFNULL(ROUND(( 6371 * acos (cos ( radians( " + latitude + " ) ) * cos( radians( users.latitude ) )"
      + " * cos( radians( users.longitude ) - radians( " + longitude + " ) ) + sin ( radians( " + latitude + " ) )"
      + " * sin( radians( users.latitude ) ) ) ), 2), 0) < 500")
      .getRawMany();
      console.log("leaders")
      console.log(leaders)
      var count=0;
      if(leaders){
        // var x;
         for(const i in leaders){
           console.log("leader"+leaders[i].pgr)
           console.log("user"+user.pgr)
           if(leaders[i].pgr> user.pgr){
            console.log(1)
             count++;          
           }
           else if(leaders[i].pgr == user.pgr){
           console.log(2)
             if(leaders[i].id<user.id){
              console.log(3)
               count++;
               
             }
           }
         }
      }
      const rank=count + 1;
 
   
 
      let  data : any = user;
        data.rank= rank;       
        data.avg_rating = (data.pgr)/20.0;
        console.log(data)
        return data ;
      } catch (err) {
        console.log(err)
        return "Profile doesn't exist";
      }
    }
}
