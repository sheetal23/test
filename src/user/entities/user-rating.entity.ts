import { Entity, Column, PrimaryGeneratedColumn,BeforeInsert,ManyToOne,JoinColumn} from 'typeorm';
import { Users } from './user.entity';
import * as crypto from 'crypto';
import * as md5 from 'md5';

@Entity()
export class UserRatings {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(type => Users, Users => Users.UserRatings1, {
    cascade: true,
    onDelete: "CASCADE",
    onUpdate: "CASCADE"
  })
  @JoinColumn({ name: "user_id_1"})
  user_id_1: number;

  @ManyToOne(type => Users, Users => Users.UserRatings2, {
    cascade: true,
    onDelete: "CASCADE",
    onUpdate: "CASCADE"
  })
  @JoinColumn({ name: "user_id_2"})
  user_id_2: number;

  @Column({default: null})
  passing_ability: number;

  @Column({default: null})
  shooting_ability: number;

  @Column({default: null})
  dribbling_ability: number;

  @Column({default: null})
  steals: number;

  @Column({default: null})
  rebounds: number;

  @Column({default: null})
  fitness: number;

  @Column({default: null})
  speed: number;

  @Column({default: null})
  blocks: number;

  @Column({default: null})
  attendence: number;

  @Column({default: 0, comment :" 0-No, 1-Yes"})
  is_session_rating: number;

  @Column({ type: 'timestamp', default: null})
  defensive_at: Date;

  @Column({ type: 'timestamp', default: null})
  offensive_at: Date;

  @Column({ type: 'timestamp', default: null})
  more_at: Date;

  @Column({ type: 'timestamp', default: () => "CURRENT_TIMESTAMP"})
  created_at: Date;

  @Column({ type: 'timestamp', default: null})
  updated_at: Date;
}