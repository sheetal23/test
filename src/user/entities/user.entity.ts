import { Entity, Column, PrimaryGeneratedColumn,BeforeInsert, OneToMany } from 'typeorm';
import * as md5 from 'md5';
import { UserLogins } from './user-login.entity';
import { Followers } from './follow.entity';

import { UserRatings } from './user-rating.entity';
import { UserReviews } from './user-review.entity';
import { Blocks } from './block.entity';
import { Feedbacks } from './feedback.entity';
import { Chats } from '../../chat/entities/chat.entity';
import { ChatTos } from '../../chat/entities/chat-to.entity';

@Entity()
export class Users {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({default: null})
  first_name: string;

  @Column({default: null})
  last_name: string;

  @Column({default: null, unique: true })
  user_name: string;

  @BeforeInsert()
  hashPassword() {
    this.password = this.password ? md5(this.password) : null;
  }
  @Column({select: false, default: null})
  password: string;

  @Column({default: null, type: 'date'})
  age: Date;  

  @Column({default: null,type:"double"})
  height: number;

  @Column({default: null, type:"tinyint", comment:"2-male,1-female"})
  gender: number;

  @Column({default: null})
  main_sport: string;

  @Column({default: null, type:"tinyint", comment:"1-paused,2-unpaused"})
  is_paused: number;

  @Column({default: null})
  location: string;

  @Column({default: null,type:"double"})
  latitude: number;

  @Column({default: null,type:"double"})
  longitude: number;

  @Column({default: null})
  email: string;

  @Column({default: null})
  facebook_id: string;

  @Column({default: null})
  google_id: string;

  @Column({default: null })
  profile_image: string;

  @Column({default: null ,comment :"point_guard, small_forward, power_forward, shooting_forward, center"})
  position: string;

  @Column({default: null})
  email_verification_key: string;
 
  @Column({default: 1, type:"tinyint"})
  is_email_verified:number;

  @Column({default: null})
  password_reset_key: string;

  @Column({default: 1,type:"boolean", comment:"1-enable,2-disable"})
  review_enabled: number;

  @Column({default: null})
  fav_nba_team: string;

  @Column({default: null})
  fav_nba_player: string;

  @Column({default: null})
  current_team: string;

  @Column({default: null})
  current_city: string;

  @Column({default: null, collation:"utf8mb4_bin"})
  currency: string;

  @Column({default: 200, type:"double"})
  session_range: number;

  @Column({default: null, type:"boolean"})
  is_online: number;

  @Column({ type: 'timestamp', default: () => "CURRENT_TIMESTAMP"})
  created_at: Date;

  @Column({ type: 'timestamp', default: null})
  updated_at: Date;

  @Column({ type: 'timestamp', default: null})
  deleted_at: Date;

  

  // Relations

  
  @OneToMany(type => UserLogins, UserLogins => UserLogins.user_id)
  UserLogins: UserLogins[];



  @OneToMany(type => UserRatings, UserRatings => UserRatings.user_id_1)
  UserRatings1: UserRatings[];

  @OneToMany(type => UserRatings, UserRatings => UserRatings.user_id_2)
  UserRatings2: UserRatings[];

  @OneToMany(type => UserReviews, UserReviews => UserReviews.user_id_1)
  UserReviews1: UserReviews[];

  @OneToMany(type => UserReviews, UserReviews => UserReviews.user_id_2)
  UserReviews2: UserReviews[];
  // @OneToMany(type => UserReviews, UserReviews => UserReviews.user_id_2)
  // UserReviews2: UserReviews[];

  @OneToMany(type => Followers, Followers => Followers.user_id_1)
  Followers1: Followers[];

  @OneToMany(type => Followers, Followers => Followers.user_id_2)
  Followers2: Followers[];

  @OneToMany(type => Blocks, Blocks => Blocks.user_id_1)
  Blocks1: Followers[];

  @OneToMany(type => Blocks, Blocks => Blocks.user_id_2)
  Blocks2:Blocks[];

  

  @OneToMany(type => Feedbacks, Feedbacks => Feedbacks.user_id)
  Feedbacks: Feedbacks[];

  @OneToMany(type => Chats, Chats => Chats.from_id)
  Chats: Chats[];

  @OneToMany(type => ChatTos, ChatTos => ChatTos.to_id)
  ChatTos: ChatTos[];
}