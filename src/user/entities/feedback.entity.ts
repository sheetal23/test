import { Entity, Column, PrimaryGeneratedColumn,BeforeInsert,ManyToOne,JoinColumn} from 'typeorm';
import { Users } from './user.entity';
import * as crypto from 'crypto';
import * as md5 from 'md5';

@Entity()
export class Feedbacks {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(type => Users, Users => Users.Feedbacks, {
    cascade: true,
    onDelete: "CASCADE",
    onUpdate: "CASCADE"
  })
  @JoinColumn({ name: "user_id"})
  user_id: number;

  @Column({default:null})
  feedback: string;

  @Column({ type: 'timestamp', default: () => "CURRENT_TIMESTAMP"})
  created_at: Date;

  @Column({ type: 'timestamp', default: null})
  updated_at: Date;
}