import { Entity, Column, PrimaryGeneratedColumn,BeforeInsert,ManyToOne,JoinColumn} from 'typeorm';
import { Users } from './user.entity';
import * as crypto from 'crypto';
import * as md5 from 'md5';

@Entity()
export class UserReviews {
  @PrimaryGeneratedColumn()
  id: number;
  
  @ManyToOne(type => Users, users => users.UserReviews1, {
    cascade: true,
    onDelete: "CASCADE",
    onUpdate: "CASCADE"
  })
  @JoinColumn({ name: "user_id_1"})
  user_id_1: number;

  @ManyToOne(type => Users, users => users.UserReviews2, {
    cascade: true,
    onDelete: "CASCADE",
    onUpdate: "CASCADE"
  })
  @JoinColumn({ name: "user_id_2"})
  user_id_2: number;

  @Column({default: null, collation:"utf8mb4_bin"})
  review: string;

  @Column({default: 1, type: "boolean", comment:"1-enable,2-disable"})
  review_status: number;

  @Column({ type: 'timestamp', default: () => "CURRENT_TIMESTAMP"})
  created_at: Date;

  @Column({ type: 'timestamp', default: null})
  updated_at: Date;
}