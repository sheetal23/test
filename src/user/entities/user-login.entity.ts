import { Entity, Column, PrimaryGeneratedColumn,BeforeInsert,ManyToOne,JoinColumn} from 'typeorm';
import { Users } from './user.entity';
import * as crypto from 'crypto';
import * as md5 from 'md5';

@Entity()
export class UserLogins {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(type => Users, Users => Users.UserLogins, {
    cascade: true,
    onDelete: "CASCADE",
    onUpdate: "CASCADE",
    eager: true
  })
  @JoinColumn({ name: "user_id"})
  user_id: number;
  

  @Column({default: null})
  device_id: string;

  @Column({default: null})
  device_token: string;

  @Column({default: null, type:"tinyint"})
  device_type: number ;

  @Column({ type: 'timestamp', default: () => "CURRENT_TIMESTAMP"})
  created_at: Date;

  @Column({ type: 'timestamp', default: null})
  updated_at: Date;

  @Column({ type: 'timestamp', default: null})
  deleted_at: Date;

  

  // Relations

  
//   @OneToMany(type => Comments, Comments => Comments.user_id)
//   Comments: Comments[];

//   @OneToMany(type => Likes, Likes => Likes.user_id)
//   Likes: Likes[];

//   @OneToMany(type => Notifications, Notifications => Notifications.user_id_1)
//   Notifications1: Notifications[];

//   @OneToMany(type => Notifications, Notifications => Notifications.user_id_2)
//   Notifications2: Notifications[];

//   @OneToMany(type => PasswordResets, PasswordResets => PasswordResets.user_id)
//   PasswordResets: PasswordResets[];

//   @OneToMany(type => Pitches, Pitches => Pitches.user_id)
//   Pitches: Pitches[];

//   @OneToMany(type => Projects, Projects => Projects.user_id)
//   Projects: Projects[];
}