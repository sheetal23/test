import { Entity, Column, PrimaryGeneratedColumn,BeforeInsert,ManyToOne,JoinColumn} from 'typeorm';
import { Users } from './user.entity';
import * as crypto from 'crypto';
import * as md5 from 'md5';

@Entity()
export class Blocks {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(type => Users, Users => Users.Blocks1, {
    cascade: true,
    onDelete: "CASCADE",
    onUpdate: "CASCADE"
  })
  @JoinColumn({ name: "user_id_1"})
  user_id_1: number;

  @ManyToOne(type => Users, Users => Users.Blocks2, {
    cascade: true,
    onDelete: "CASCADE",
    onUpdate: "CASCADE"
  })
  @JoinColumn({ name: "user_id_2"})
  user_id_2: number;

  @Column({ type: 'timestamp', default: () => "CURRENT_TIMESTAMP"})
  created_at: Date;
}