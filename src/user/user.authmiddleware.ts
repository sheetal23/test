import { HttpException } from '@nestjs/common/exceptions/http.exception';
import { NestMiddleware, HttpStatus, Injectable } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import * as jwt from 'jsonwebtoken';
import { UserService } from './user.service';

@Injectable()
export class AuthMiddleware implements NestMiddleware {
  constructor(private readonly userService: UserService) {}

  async use(req: Request, res: Response, next: NextFunction) {
    const authHeaders = req.headers.authorization;
    
    if (authHeaders) {
      
      try {
        const decoded: any = jwt.verify(authHeaders, process.env.SECRET);
        const user = await this.userService.validateUser(decoded);

        if (!user) {
          // throw new HttpException('User not found.', HttpStatus.FORBIDDEN);
          throw new HttpException({error: 'forbidden', message: 'User not Found.'}, HttpStatus.FORBIDDEN);
        }
  
        req.user = user;
        next();

      } catch (err) {
        // throw new HttpException('Invalid Token.', HttpStatus.FORBIDDEN);
        throw new HttpException({error: 'forbidden', message: 'Invalid Token.'}, HttpStatus.FORBIDDEN);
      }

    } else {
      // throw new HttpException('Not Authorized.', HttpStatus.FORBIDDEN);
      throw new HttpException({error: 'forbidden', message: 'Not Authorized.'}, HttpStatus.FORBIDDEN);
    }
  }
}
