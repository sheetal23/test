import { Controller, Post, Body, UsePipes, Req, HttpCode, Put, Get, Delete } from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDto, ChangeEmailDto, ChangePasswordDto, EditProfileDto, LoginUserDto, ReviewUserDto, CommonUserDto, UserRatingDto} from './dto';
import { ValidationPipe } from '../shared/pipes/validation.pipe';
import {
    ApiUseTags,
    ApiBearerAuth
  } from '@nestjs/swagger';
  

@ApiBearerAuth()
@ApiUseTags('user')
@Controller('user')
export class UserController {
    constructor(private readonly userService: UserService) {}
    
    /*--------------------------------------------------------------------------
    ----------------------------------------------------------------------------
     * Api's
    ----------------------------------------------------------------------------
    --------------------------------------------------------------------------*/ 

    @UsePipes(new ValidationPipe)
    @Post('signup')
    @HttpCode(200)
    async create(@Body() userData: CreateUserDto) {
        
        const user = await this.userService.create(userData);
        console.log(user)
        const user_token = await this.userService.createToken(user);
        const profile = await this.userService.getProfile(user.id,null,null,user.id);
        const userDetail = {profile,session_id: user_token.access_token}

        return userDetail;
    }

    @Post('version')
    @HttpCode(200)
    async checkVersion(@Req() request) {
        return await this.userService.checkVersion(request);
    }

    @UsePipes(new ValidationPipe)
    @Post('signin')
    @HttpCode(200)
    async login(@Body() userData: LoginUserDto) {
        
        const user = await this.userService.login(userData);
        const user_token = await this.userService.createToken(user);
        console.log("user")
        console.log(user)
        console.log(user.id)
        const profile = await this.userService.getProfile(user.id,user.latitude,user.longitude,user.id);
        console.log("profile")
        console.log(profile)
        profile.first_time=user.first_time;
        const userDetail = {profile,session_id: user_token.access_token}

        return userDetail;
    }

    @Get('')
    @HttpCode(200)
    async profile(@Req() request) {
        const jwtData = await this.userService.jwtData(request.headers.authorization);
        console.log(request.headers.authorization)
        const user_data = await this.userService.profile(jwtData.id,request);
        const data1 = await this.userService.getProfile(user_data.user_id,user_data.latitude,user_data.longitude,jwtData.id);

        let detail: any = data1;
        detail.ratings = user_data.ratings;
        detail.reviews = user_data.reviews;
       
        return detail;
    }

    @Delete('')
    @HttpCode(200)
    async deactivate(@Req() request) {

        const jwtData = await this.userService.jwtData(request.headers.authorization);
        return await this.userService.deactivate(jwtData.id);

    }

    @Get('followers')
    @HttpCode(200)
    async getFollowers(@Req() request) {

        const jwtData = await this.userService.jwtData(request.headers.authorization);
        return await this.userService.getFollowers(jwtData.id, request);

    }
    
    @Get('reviews')
    @HttpCode(200)
    async findAllReviews(@Req() request) {

        const jwtData = await this.userService.jwtData(request.headers.authorization);
        return await this.userService.findAllReviews(jwtData.id, request);

    }

    @Get('leaderboard')
    @HttpCode(200)
    async userLeaderboard(@Req() request) {

        const jwtData = await this.userService.jwtData(request.headers.authorization);
        return await this.userService.userLeaderboard(jwtData.id,request);

    }

    @Delete('logout')
    @HttpCode(200)
    async logOut(@Req() request) {

        const jwtData = await this.userService.jwtData(request.headers.authorization);
        console.log(request.headers.authorization)
        return await this.userService.logOut(jwtData.id, request);

    }

    @Post('review')
    @HttpCode(200)
    async userReview(@Req() request,@Body() reviewData :ReviewUserDto) {

        const jwtData = await this.userService.jwtData(request.headers.authorization);
        return await this.userService.userReview(jwtData.id, reviewData);

    }

    @Post('block')
    @HttpCode(200)
    async blockUser(@Req() request,@Body() blockData :CommonUserDto) {

        const jwtData = await this.userService.jwtData(request.headers.authorization);
        return await this.userService.blockUser(jwtData.id, blockData);

    }

    @Post('feedback')
    @HttpCode(200)
    async feedback(@Req() request,@Body() feedbackData :CommonUserDto) {

        const jwtData = await this.userService.jwtData(request.headers.authorization);
        return await this.userService.feedback(jwtData.id, feedbackData);

    }

    @Post('follow')
    @HttpCode(200)
    async followUser(@Req() request,@Body() followData :CommonUserDto) {

        const jwtData = await this.userService.jwtData(request.headers.authorization);
        return await this.userService.followUser(jwtData.id, followData);

    }

    @Post('rating')
    @HttpCode(200)
    async rateUser(@Req() request,@Body() ratingData:UserRatingDto) {

        const jwtData = await this.userService.jwtData(request.headers.authorization);
        return await this.userService.rateUser(jwtData.id, ratingData);

    }

    @Get('listing')
    @HttpCode(200)
    async userListing(@Req() request) {

        const jwtData = await this.userService.jwtData(request.headers.authorization);
        return await this.userService.userListing(jwtData.id, request);

    }

    @UsePipes(new ValidationPipe)
    @Put('change-password')
    @HttpCode(200)
    async changePassword(@Req() request, @Body() userData: ChangePasswordDto) {

        const jwtData = await this.userService.jwtData(request.headers.authorization);
        return await this.userService.changePassword(jwtData.id, userData);

    }

    // @UsePipes(new ValidationPipe)
    // @Put('change-email')
    // @HttpCode(200)
    // async changeEmail(@Req() request, @Body() userData: ChangeEmailDto) {

    //     const jwtData = await this.userService.jwtData(request.headers.authorization);
    //     return await this.userService.changeEmail(jwtData.id, userData);

    // }

    @UsePipes(new ValidationPipe)
    @Put('')
    @HttpCode(200)
    async editProfile(@Req() request, @Body() userData: EditProfileDto) {

        const jwtData = await this.userService.jwtData(request.headers.authorization);
        return await this.userService.editProfile(jwtData.id, userData);

    }

    @Post('forgot-password')
    @HttpCode(200)
    async forgotPassword(@Req() request) {

        const email = request.body.email;
        return await this.userService.forgotPassword(email);

    }

    @Post('reset-password')
    @HttpCode(200)
    async resetPassword(@Req() request) {

        const token = request.body.token;
        return await this.userService.resetPassword(token, request);

    }

    @Post('verify-email')
    @HttpCode(200)
    async verifyEmail(@Req() request) {

        const token = request.body.token;
        return await this.userService.verifyEmail(token, request);

    }

    @Put('resend-verify-email')
    @HttpCode(200)
    async resendVerifyEmail(@Req() request) {

        return await this.userService.resendVerifyEmail(request);

    }

    @Put('is-online')
    @HttpCode(200)
    async userOnline(@Req() request) {

        const jwtData = await this.userService.jwtData(request.headers.authorization);
        return await this.userService.userOnline(jwtData.id,request);

    }

    @Put('review-enable')
    @HttpCode(200)
    async reviewEnable(@Req() request, @Body() dto: ReviewUserDto) {

        const jwtData = await this.userService.jwtData(request.headers.authorization);
        return await this.userService.reviewEnable(jwtData.id,dto);

    }
    @Get('verification-reminder')
    @HttpCode(200)
    async resendEmail() {
        return await this.userService.resendEmail();

    }
}
