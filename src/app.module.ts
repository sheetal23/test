import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserController } from './user/user.controller';
import { UserService } from './user/user.service';
import { UserModule } from './user/user.module';
import { Users } from './user/entities/user.entity';
import { UserLogins } from './user/entities/user-login.entity';
import { Blocks } from './user/entities/block.entity';
import { Followers } from './user/entities/follow.entity';
import { UserReviews } from './user/entities/user-review.entity';
import { Feedbacks } from './user/entities/feedback.entity';
import { UserRatings } from './user/entities/user-rating.entity';

// import { Categories } from './common/entities/category.entity'
import { TypeOrmModule } from '@nestjs/typeorm';
import { Connection } from 'typeorm';
import { HandlebarsAdapter, MailerModule } from '@nest-modules/mailer';
//import { PostVideos } from './post/entities/post-video.entity';
import { APP_INTERCEPTOR, APP_FILTER } from '@nestjs/core';
import { LoggingInterceptor } from './shared/logging.interceptor';
import { HttpErrorFilter } from './shared/http-error.filter';
import { Logs } from './shared/entities/log.entity';
import { Chats } from './chat/entities/chat.entity';
import { ChatTos } from './chat/entities/chat-to.entity';
import { ChatController } from './chat/chat.controller';
import { ChatService } from './chat/chat.service';
import { ChatModule } from './chat/chat.module';
import { ChatGateway } from './chat/chat.gateway';
require('dotenv').config();




@Module({
  imports: [
    TypeOrmModule.forFeature([Users]),
    TypeOrmModule.forFeature([UserLogins]),
    TypeOrmModule.forFeature([UserReviews]),
    TypeOrmModule.forFeature([UserRatings]),
    TypeOrmModule.forFeature([Followers]),
    TypeOrmModule.forFeature([Blocks]),
    TypeOrmModule.forFeature([Feedbacks]),
    
    TypeOrmModule.forFeature([Chats]),
    TypeOrmModule.forFeature([ChatTos]),
    TypeOrmModule.forFeature([Logs]),
    TypeOrmModule.forRoot(),
    UserModule,
    MailerModule.forRoot({
      transport: process.env.TRANSPORT,
      defaults: {
        from:'"Team" <sheetal.henceforth@gmail.com>'
        
      },
      template: {
        adapter: new HandlebarsAdapter(), // or new PugAdapter()
        options: {
          strict: true,
        },
      },
    })
    
  ],
  controllers: [
    AppController,
    UserController,
   
    ChatController
  ],
  providers: [
    UserService,
    AppService,
    ChatService,
    ChatGateway,
    {
      provide :APP_INTERCEPTOR,
      useClass:LoggingInterceptor
    },
    {
      provide: APP_FILTER,
      useClass: HttpErrorFilter,
    },
  ]
})
export class AppModule {
  constructor(private readonly connection: Connection) {}
}