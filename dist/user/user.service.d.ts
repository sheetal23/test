import { Repository } from 'typeorm';
import { Users } from './entities/user.entity';
import { UserLogins } from './entities/user-login.entity';
import { UserReviews } from './entities/user-review.entity';
import { UserRatings } from './entities/user-rating.entity';
import { Followers } from './entities/follow.entity';
import { Feedbacks } from './entities/feedback.entity';
import { Blocks } from './entities/block.entity';
import { CreateUserDto, ChangePasswordDto, EditProfileDto, ReviewUserDto, LoginUserDto, CommonUserDto, UserRatingDto } from './dto';
import { MailerService } from '@nest-modules/mailer';
export declare class UserService {
    private readonly userRepository;
    private readonly userLoginsRepository;
    private readonly userReviewsRepository;
    private readonly userRatingsRepository;
    private readonly userBlockRepository;
    private readonly userFollowRepository;
    private readonly feedbackRepository;
    private readonly mailerService;
    constructor(userRepository: Repository<Users>, userLoginsRepository: Repository<UserLogins>, userReviewsRepository: Repository<UserReviews>, userRatingsRepository: Repository<UserRatings>, userBlockRepository: Repository<Blocks>, userFollowRepository: Repository<Followers>, feedbackRepository: Repository<Feedbacks>, mailerService: MailerService);
    create(dto: CreateUserDto): Promise<Users>;
    login(dto: LoginUserDto): Promise<any>;
    editProfile(id: number, dto: EditProfileDto): Promise<object>;
    changePassword(id: number, dto: ChangePasswordDto): Promise<object>;
    forgotPassword(email: string): Promise<object>;
    resetPassword(token: string, req: any): Promise<object>;
    verifyEmail(token: string, req: any): Promise<object>;
    resendVerifyEmail(req: any): Promise<object>;
    userReview(logged_id: number, dto: ReviewUserDto): Promise<{
        message: string;
    }>;
    blockUser(logged_id: number, dto: CommonUserDto): Promise<{
        message: string;
    }>;
    followUser(logged_id: number, dto: CommonUserDto): Promise<{
        message: string;
    }>;
    getFollowers(logged_id: any, req: any): Promise<{
        data: any[];
        count: number;
    }>;
    rateUser(logged_id: number, dto: UserRatingDto): Promise<{
        message: string;
    }>;
    deactivate(id: number): Promise<object>;
    logOut(id: number, req: any): Promise<object>;
    userOnline(logged_id: number, req: any): Promise<{
        message: string;
    }>;
    feedback(logged_id: number, dto: CommonUserDto): Promise<{
        message: string;
    }>;
    resendEmail(): Promise<object>;
    profile(id: any, req: any): Promise<any>;
    userListing(user_id: any, req: any): Promise<{
        users: any[];
        count: number;
    }>;
    userLeaderboard(logged_id: any, req: any): Promise<{
        data: any[];
        total: number;
    }>;
    findAllReviews(logged_id: any, req: any): Promise<{
        reviews: any[];
        count: number;
    }>;
    reviewEnable(logged_id: number, dto: ReviewUserDto): Promise<{
        message: string;
    }>;
    checkVersion(req: any): Promise<object>;
    createToken(userData: any): Promise<{
        expires_in: string;
        access_token: string;
    }>;
    validateUser(signedUser: any): Promise<boolean>;
    jwtData(token: any): Promise<any>;
    generateRandomString(length: any): Promise<string>;
    getProfile(user_id: any, latitude: any, longitude: any, logged_id: any): Promise<any>;
}
