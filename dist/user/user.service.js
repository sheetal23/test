"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const jwt = require("jsonwebtoken");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const user_entity_1 = require("./entities/user.entity");
const user_login_entity_1 = require("./entities/user-login.entity");
const user_review_entity_1 = require("./entities/user-review.entity");
const user_rating_entity_1 = require("./entities/user-rating.entity");
const follow_entity_1 = require("./entities/follow.entity");
const feedback_entity_1 = require("./entities/feedback.entity");
const block_entity_1 = require("./entities/block.entity");
const class_validator_1 = require("class-validator");
const http_exception_1 = require("@nestjs/common/exceptions/http.exception");
const common_2 = require("@nestjs/common");
const mailer_1 = require("@nest-modules/mailer");
const md5 = require("md5");
let UserService = class UserService {
    constructor(userRepository, userLoginsRepository, userReviewsRepository, userRatingsRepository, userBlockRepository, userFollowRepository, feedbackRepository, mailerService) {
        this.userRepository = userRepository;
        this.userLoginsRepository = userLoginsRepository;
        this.userReviewsRepository = userReviewsRepository;
        this.userRatingsRepository = userRatingsRepository;
        this.userBlockRepository = userBlockRepository;
        this.userFollowRepository = userFollowRepository;
        this.feedbackRepository = feedbackRepository;
        this.mailerService = mailerService;
    }
    create(dto) {
        return __awaiter(this, void 0, void 0, function* () {
            const { email, password, first_name, last_name, location, latitude, longitude, user_name, position, device_id, device_token, device_type } = dto;
            const pre_user = yield typeorm_2.getRepository(user_entity_1.Users)
                .createQueryBuilder("users")
                .where("user_name= :user_name", { user_name: user_name })
                .getOne();
            if (pre_user) {
                throw new http_exception_1.HttpException({ error: 'bad_request', message: 'User name already exist' }, common_2.HttpStatus.BAD_REQUEST);
            }
            else {
                const qb = yield typeorm_2.getRepository(user_entity_1.Users)
                    .createQueryBuilder('users')
                    .orWhere('users.email = :email', { email });
                const user = yield qb.getOne();
                const token = yield this.generateRandomString(30);
                if (user) {
                    throw new http_exception_1.HttpException({ error: 'bad_request', message: 'email must be unique.' }, common_2.HttpStatus.BAD_REQUEST);
                }
                let newUser = new user_entity_1.Users();
                newUser.email = email;
                newUser.password = password;
                newUser.first_name = first_name;
                newUser.last_name = last_name;
                newUser.user_name = user_name;
                newUser.position = position;
                newUser.email_verification_key = token;
                try {
                    yield this
                        .mailerService
                        .sendMail({
                        to: email,
                        subject: 'Email Verification',
                        template: 'src/user/views/invitation',
                        context: {
                            url: process.env.URL,
                            token: token,
                            text: "Welcome to PGRaters. Thank you for joining us. Connect with other raters on the playground and start rating them. Please click on the following link to verify your account:",
                            button: "VERIFY ACCOUNT",
                            first_name: first_name,
                            last_name: last_name
                        }
                    })
                        .then(() => { })
                        .catch(() => { });
                }
                catch (err) {
                    throw new http_exception_1.HttpException({ error: 'bad_request', message: 'Something went wrong.' }, common_2.HttpStatus.BAD_REQUEST);
                }
                const errors = yield class_validator_1.validate(newUser);
                if (errors.length > 0) {
                    throw new http_exception_1.HttpException({ error: 'bad_request', message: 'email must be unique.' }, common_2.HttpStatus.BAD_REQUEST);
                }
                else {
                    const savedUser = yield this.userRepository.save(newUser);
                    let loginDevice = new user_login_entity_1.UserLogins();
                    loginDevice.user_id = newUser.id;
                    loginDevice.device_id = device_id;
                    loginDevice.device_token = device_token;
                    loginDevice.device_type = device_type;
                    const savedLogin = yield this.userLoginsRepository.save(loginDevice);
                    return yield this.userRepository.findOne(newUser.id);
                }
            }
        });
    }
    login(dto) {
        return __awaiter(this, void 0, void 0, function* () {
            const { email, password, facebook_id, google_id, first_name, user_name, last_name, profile_image, device_id, device_token, device_type, location, latitude, longitude, identifier } = dto;
            var first_time = 0;
            console.log(1);
            if (!facebook_id && !google_id) {
                if (!identifier || !password) {
                    throw new http_exception_1.HttpException({ error: 'bad_request', message: 'Email and password are required.' }, common_2.HttpStatus.BAD_REQUEST);
                }
            }
            console.log(2);
            const qb = yield typeorm_2.getRepository(user_entity_1.Users)
                .createQueryBuilder('users')
                .addSelect("users.password");
            console.log(3);
            if (identifier) {
                console.log(4);
                qb.where('(users.email = :identifier  OR  users.user_name = :identifier)', { identifier });
            }
            if (email) {
                qb.andWhere('users.email = :email', { email });
            }
            if (!password) {
                console.log(5);
                if (facebook_id) {
                    console.log(6);
                    qb.orWhere('users.facebook_id = :facebook_id', { facebook_id });
                }
                if (google_id) {
                    console.log(7);
                    qb.orWhere('users.google_id = :google_id', { google_id });
                }
            }
            const user = yield qb.getOne();
            console.log(user);
            if (user) {
                var user_id = user.id;
                console.log(8);
                if (user.deleted_at != null) {
                    console.log(9);
                    throw new http_exception_1.HttpException({ error: 'bad_request', message: 'Your account is deactivated.' }, common_2.HttpStatus.BAD_REQUEST);
                }
                if (password) {
                    console.log(10);
                    const pass = md5(password);
                    if (user.password != pass) {
                        console.log(11);
                        throw new http_exception_1.HttpException({ error: 'bad_request', message: 'Incorrect login credentials.' }, common_2.HttpStatus.BAD_REQUEST);
                    }
                }
                else {
                    console.log(user);
                    console.log(12);
                    const update = yield typeorm_2.getConnection()
                        .createQueryBuilder()
                        .update(user_entity_1.Users)
                        .where("id = :id", { id: user.id });
                    console.log(13);
                    if (email) {
                        update.set({ email: email });
                    }
                    if (facebook_id) {
                        update.set({ facebook_id: facebook_id
                        });
                    }
                    if (google_id) {
                        update.set({ google_id: google_id });
                    }
                    update.execute();
                }
                console.log(14);
            }
            else {
                console.log(15);
                if (facebook_id || google_id) {
                    let user1 = new user_entity_1.Users();
                    user1.email = email;
                    user1.password = password ? md5(password) : null;
                    user1.facebook_id = facebook_id ? facebook_id : null;
                    user1.google_id = google_id ? google_id : null;
                    user1.first_name = first_name ? first_name : null;
                    user1.last_name = last_name ? last_name : null;
                    user1.user_name = user_name ? user_name : null;
                    user1.profile_image = profile_image ? profile_image : null;
                    const newUser = yield this.userRepository.save(user1);
                    first_time = 1;
                    var user_id = newUser.id;
                }
                else {
                    console.log(16);
                    throw new http_exception_1.HttpException({ error: 'bad_request', message: 'Incorrect login credentials.' }, common_2.HttpStatus.BAD_REQUEST);
                }
            }
            const loggedInUser = yield typeorm_2.getRepository(user_login_entity_1.UserLogins)
                .createQueryBuilder("user_logins")
                .where("user_logins.device_token = :device_token", { device_token })
                .getOne();
            if (loggedInUser) {
                console.log(18);
                yield typeorm_2.getConnection()
                    .createQueryBuilder()
                    .delete()
                    .from(user_login_entity_1.UserLogins)
                    .where("user_logins.device_token = :device_token", { device_token })
                    .execute();
            }
            console.log(19);
            let loginDevice = new user_login_entity_1.UserLogins();
            loginDevice.user_id = user_id;
            loginDevice.device_id = device_id;
            loginDevice.device_token = device_token;
            loginDevice.device_type = device_type;
            yield this.userLoginsRepository.save(loginDevice);
            console.log("user_id:" + user_id);
            const data = yield this.userRepository.findOne(user_id);
            console.log("data");
            console.log(data);
            let detail = data;
            detail.first_time = first_time;
            console.log("detail");
            console.log(detail);
            return detail;
        });
    }
    editProfile(id, dto) {
        return __awaiter(this, void 0, void 0, function* () {
            const { first_name, last_name, user_name, profile_image, age, height, gender, position, fav_nba_team, fav_nba_player, current_team, currency, session_range, location, latitude, longitude, delete_image, review_enabled } = dto;
            const current_time = new Date();
            const data = yield typeorm_2.getRepository(user_entity_1.Users)
                .createQueryBuilder('users')
                .where('users.user_name = :user_name', { user_name })
                .andWhere('id != :user_id', { user_id: id })
                .getOne();
            if (data) {
                throw new http_exception_1.HttpException({ error: 'bad_request', message: 'Username already exists.' }, common_2.HttpStatus.BAD_REQUEST);
            }
            const qb = yield this.userRepository.findOne(id);
            console.log("profile_image");
            console.log("profile_image" + profile_image);
            if (delete_image == 1) {
                console.log(1);
                var image = null;
            }
            else {
                console.log(2);
                var image = profile_image ? profile_image : qb.profile_image;
            }
            console.log("image");
            console.log(image);
            yield typeorm_2.getConnection()
                .createQueryBuilder()
                .update(user_entity_1.Users)
                .set({ first_name: first_name ? first_name : qb.first_name,
                last_name: last_name ? last_name : qb.last_name,
                user_name: user_name ? user_name : qb.user_name,
                profile_image: image,
                age: age ? age : qb.age,
                height: height ? height : qb.height,
                gender: gender ? gender : qb.gender,
                position: position ? position : qb.position,
                fav_nba_player: fav_nba_player ? fav_nba_player : qb.fav_nba_player,
                fav_nba_team: fav_nba_team ? fav_nba_team : qb.fav_nba_team,
                current_team: current_team ? current_team : qb.current_team,
                session_range: session_range ? session_range : qb.session_range,
                location: location ? location : qb.location,
                latitude: latitude ? latitude : qb.latitude,
                longitude: longitude ? longitude : qb.longitude,
                currency: currency ? currency : qb.currency,
                review_enabled: review_enabled ? review_enabled : qb.review_enabled,
                updated_at: current_time })
                .where("id = :id", { id: id })
                .execute();
            return { id: id, message: 'Profile Updated Successfully.' };
        });
    }
    changePassword(id, dto) {
        return __awaiter(this, void 0, void 0, function* () {
            const { old_password, new_password } = dto;
            const qb = yield typeorm_2.getRepository(user_entity_1.Users)
                .createQueryBuilder('users')
                .addSelect("users.password")
                .where('users.id = :id', { id })
                .getOne();
            const old_pass = md5(old_password);
            console.log(qb.password);
            console.log(old_pass);
            console.log(old_password);
            if (qb.password != old_pass) {
                throw new http_exception_1.HttpException({ error: 'bad_request', message: 'Wrong Old Password.' }, common_2.HttpStatus.BAD_REQUEST);
            }
            const new_pass = md5(new_password);
            yield typeorm_2.getConnection()
                .createQueryBuilder()
                .update(user_entity_1.Users)
                .set({ password: new_pass })
                .where("id = :id", { id })
                .execute();
            return { id: id, message: 'Password Updated Successfully.' };
        });
    }
    forgotPassword(email) {
        return __awaiter(this, void 0, void 0, function* () {
            const token = yield this.generateRandomString(30);
            console.log(token);
            const user = yield typeorm_2.getRepository(user_entity_1.Users)
                .createQueryBuilder('users')
                .where('email = :email', { email })
                .getOne();
            if (!user) {
                throw new http_exception_1.HttpException({ error: 'bad_request', message: 'User Not Found.' }, common_2.HttpStatus.BAD_REQUEST);
            }
            yield typeorm_2.getConnection()
                .createQueryBuilder()
                .update(user_entity_1.Users)
                .set({
                password_reset_key: token
            })
                .where("email = :email", { email })
                .execute();
            try {
                yield this
                    .mailerService
                    .sendMail({
                    to: email,
                    subject: 'PGRaters Password Reset',
                    template: 'src/user/views/reset-password',
                    context: {
                        url: process.env.URL,
                        token: token,
                        text: "We've received a request to reset your password, If you didn't make the request, just ignore this email. Otherwise, you can reset your password using this link:",
                        first_name: user.first_name,
                        last_name: user.last_name
                    }
                })
                    .then(() => { })
                    .catch(() => { });
                return { message: 'Email has been sent on your email id.' };
            }
            catch (err) {
                throw new http_exception_1.HttpException({ error: 'bad_request', message: 'Something went wrong.' }, common_2.HttpStatus.BAD_REQUEST);
            }
        });
    }
    resetPassword(token, req) {
        return __awaiter(this, void 0, void 0, function* () {
            const reset = yield typeorm_2.getRepository(user_entity_1.Users)
                .createQueryBuilder('users')
                .where('password_reset_key = :token', { token })
                .getOne();
            if (!reset) {
                throw new http_exception_1.HttpException({ error: 'bad_request', message: 'Invalid Token.' }, common_2.HttpStatus.BAD_REQUEST);
            }
            const user_id = reset.id;
            const new_pass = md5(req.body.password);
            console.log(new_pass);
            yield typeorm_2.getConnection()
                .createQueryBuilder()
                .update(user_entity_1.Users)
                .set({ password: new_pass, password_reset_key: null })
                .where("email = :email", { email: reset.email })
                .execute();
            return { message: 'Reset Password Complete.' };
        });
    }
    verifyEmail(token, req) {
        return __awaiter(this, void 0, void 0, function* () {
            const verify = yield typeorm_2.getRepository(user_entity_1.Users)
                .createQueryBuilder('users')
                .where('email_verification_key = :token', { token })
                .getOne();
            if (!verify) {
                throw new http_exception_1.HttpException({ error: 'bad_request', message: 'Invalid Token.' }, common_2.HttpStatus.BAD_REQUEST);
            }
            yield typeorm_2.getConnection()
                .createQueryBuilder()
                .update(user_entity_1.Users)
                .set({
                is_email_verified: 1,
                email_verification_key: null
            })
                .where("id = :user_id", { user_id: verify.id })
                .execute();
            return { message: 'Email Verified Successfully.' };
        });
    }
    resendVerifyEmail(req) {
        return __awaiter(this, void 0, void 0, function* () {
            const token = yield this.generateRandomString(30);
            const email = req.body.email;
            yield typeorm_2.getConnection()
                .createQueryBuilder()
                .update(user_entity_1.Users)
                .set({
                email_verification_key: token
            })
                .where("email = :email", { email })
                .execute();
            try {
                yield this
                    .mailerService
                    .sendMail({
                    to: email,
                    subject: 'Testing Nest MailerModule ✔',
                    text: 'Hi,Click on the link below to verify your email.Click Here',
                    html: '<p>Hi,</p><p>Click on the link below to verify your email.</p><p><span style="background-color: rgb(255, 255, 255);"><a href="http://localhost:3009/verify-email/' + token + '"><span style="font-family: Arial;">Click Here</span></a></span></p>',
                })
                    .then(() => { })
                    .catch(() => { });
            }
            catch (err) {
                throw new http_exception_1.HttpException({ error: 'bad_request', message: 'Something went wrong.' }, common_2.HttpStatus.BAD_REQUEST);
            }
            return { message: 'Verification link sent on your email.' };
        });
    }
    userReview(logged_id, dto) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id, user_id, review } = dto;
            if (id) {
                const qb = yield this.userReviewsRepository.findOne(id);
                yield typeorm_2.getConnection()
                    .createQueryBuilder()
                    .update(user_review_entity_1.UserReviews)
                    .set({ review: review ? review : qb.review })
                    .where("id = :id", { id: id })
                    .execute();
            }
            else {
                const user = yield typeorm_2.getRepository(user_review_entity_1.UserReviews)
                    .createQueryBuilder("user_reviews")
                    .where("user_reviews.user_id_1 = :user_id", { user_id: logged_id })
                    .andWhere("user_reviews.user_id_2 = :id", { id: user_id })
                    .getCount();
                if (user >= 5) {
                    throw new http_exception_1.HttpException({ error: 'bad_request', message: 'You have already given reviews' }, common_2.HttpStatus.BAD_REQUEST);
                }
                let new_review = new user_review_entity_1.UserReviews();
                new_review.user_id_1 = logged_id;
                new_review.user_id_2 = user_id;
                new_review.review = review;
                const savedReview = yield this.userReviewsRepository.save(new_review);
                console.log("start");
                var user1 = yield this.userRepository.findOne({
                    where: {
                        id: logged_id
                    }
                });
                var notifMessage = user1.first_name + " " + user1.last_name + " left review on your profile";
                return { message: "Review successfully given" };
            }
        });
    }
    blockUser(logged_id, dto) {
        return __awaiter(this, void 0, void 0, function* () {
            const { user_id } = dto;
            const user = yield typeorm_2.getRepository(block_entity_1.Blocks)
                .createQueryBuilder("blocks")
                .where("blocks.user_id_1 = :user_id", { user_id: logged_id })
                .andWhere("blocks.user_id_2 = :id", { id: user_id })
                .getOne();
            if (user) {
                console.log(user);
                yield typeorm_2.getRepository(block_entity_1.Blocks)
                    .createQueryBuilder("blocks")
                    .delete()
                    .from(block_entity_1.Blocks)
                    .where("blocks.user_id_2 = :id", { id: user_id })
                    .execute();
            }
            else {
                let block = new block_entity_1.Blocks();
                block.user_id_1 = logged_id;
                block.user_id_2 = user_id;
                const savedReview = yield this.userBlockRepository.save(block);
            }
            return { message: "successful" };
        });
    }
    followUser(logged_id, dto) {
        return __awaiter(this, void 0, void 0, function* () {
            const { user_id } = dto;
            var user1 = yield this.userRepository.findOne({
                where: {
                    id: logged_id
                }
            });
            const user = yield typeorm_2.getRepository(follow_entity_1.Followers)
                .createQueryBuilder("followers")
                .where("followers.user_id_1 = :user_id", { user_id: logged_id })
                .andWhere("followers.user_id_2 = :id", { id: user_id })
                .getOne();
            if (user) {
                console.log(user);
                yield typeorm_2.getRepository(follow_entity_1.Followers)
                    .createQueryBuilder("followers")
                    .delete()
                    .from(follow_entity_1.Followers)
                    .where("followers.user_id_2 = :id", { id: user_id })
                    .execute();
            }
            else {
                let follow = new follow_entity_1.Followers();
                follow.user_id_1 = logged_id;
                follow.user_id_2 = user_id;
                var notifMessage = user1.first_name + " " + user1.last_name + " has followed you";
                const savedReview = yield this.userFollowRepository.save(follow);
                console.log("start");
            }
            return { message: "successful" };
        });
    }
    getFollowers(logged_id, req) {
        return __awaiter(this, void 0, void 0, function* () {
            var type = req.query.type;
            var skip = 0;
            var limit = req.query.limit ? req.query.limit : 10;
            if (req.query.page) {
                const page = req.query.page;
                var skip = limit * page;
            }
            if (req.query.user_id) {
                var user_id = req.query.user_id;
            }
            else {
                var user_id = logged_id;
            }
            const qb = yield typeorm_2.getRepository(follow_entity_1.Followers)
                .createQueryBuilder("followers");
            if (type == 1) {
                qb.leftJoinAndSelect(user_entity_1.Users, "users", "users.id = followers.user_id_1")
                    .select("users.id, users.user_name, users.first_name,IF(TIMESTAMPDIFF(MINUTE, users.`updated_at`,UTC_TIMESTAMP)>5,0,1) AS is_online, users.last_name, users.profile_image,users.position,(SELECT count(id) FROM followers WHERE user_id_1 = " + logged_id + " AND user_id_2=users.id ) AS is_follow,((SELECT ROUND(AVG(passing_ability),1) FROM `user_ratings` where user_id_2=users.id AND passing_ability IS NOT NULL) + (SELECT ROUND(AVG(shooting_ability),1) FROM `user_ratings` where user_id_2=users.id AND shooting_ability IS NOT NULL) +( SELECT ROUND(AVG(dribbling_ability),1) FROM `user_ratings` where user_id_2=users.id AND dribbling_ability IS NOT NULL)+ ( SELECT ROUND(AVG(steals),1) FROM `user_ratings` where user_id_2=users.id AND steals IS NOT NULL)+( SELECT ROUND(AVG(rebounds),1) FROM `user_ratings` where user_id_2=users.id AND rebounds IS NOT NULL)+( SELECT ROUND(AVG(blocks),1) FROM `user_ratings` where user_id_2=users.id AND blocks IS NOT NULL))/6 AS avg_rating")
                    .where("followers.user_id_2=" + user_id)
                    .offset(skip).limit(limit).getRawMany();
            }
            if (type == 2) {
                qb.leftJoinAndSelect(user_entity_1.Users, "users", "users.id = followers.user_id_2")
                    .select("users.id, users.user_name,IF(TIMESTAMPDIFF(MINUTE, users.`updated_at`,UTC_TIMESTAMP)>5,0,1) AS is_online,users.first_name, users.last_name, users.profile_image,users.position,(SELECT count(id) FROM followers WHERE user_id_1 = " + logged_id + " AND user_id_2=users.id ) AS is_follow,((SELECT ROUND(AVG(passing_ability),1) FROM `user_ratings` where user_id_2=users.id AND passing_ability IS NOT NULL) + (SELECT ROUND(AVG(shooting_ability),1) FROM `user_ratings` where user_id_2=users.id AND shooting_ability IS NOT NULL) +( SELECT ROUND(AVG(dribbling_ability),1) FROM `user_ratings` where user_id_2=users.id AND dribbling_ability IS NOT NULL)+ ( SELECT ROUND(AVG(steals),1) FROM `user_ratings` where user_id_2=users.id AND steals IS NOT NULL)+( SELECT ROUND(AVG(rebounds),1) FROM `user_ratings` where user_id_2=users.id AND rebounds IS NOT NULL)+( SELECT ROUND(AVG(blocks),1) FROM `user_ratings` where user_id_2=users.id AND blocks IS NOT NULL))/6 AS avg_rating")
                    .where("followers.user_id_1=" + user_id)
                    .offset(skip).limit(limit).getRawMany();
            }
            const count = yield qb.getCount();
            var data = yield qb.getRawMany();
            return { data, count };
        });
    }
    rateUser(logged_id, dto) {
        return __awaiter(this, void 0, void 0, function* () {
            const { user_id, passing_ability, shooting_ability, dribbling_ability, steals, rebounds, blocks, fitness, speed, attendence, is_session_rating, is_offensive, is_defensive, is_more } = dto;
            const current_time = new Date();
            const rating = yield typeorm_2.getRepository(user_rating_entity_1.UserRatings)
                .createQueryBuilder("user_ratings")
                .where("user_ratings.user_id_1 = :user_id", { user_id: logged_id })
                .andWhere("user_ratings.user_id_2 = :id", { id: user_id })
                .getOne();
            if (rating) {
                yield typeorm_2.getConnection()
                    .createQueryBuilder()
                    .update(user_rating_entity_1.UserRatings)
                    .set({ user_id_1: logged_id ? logged_id : rating.user_id_1,
                    user_id_2: user_id ? user_id : rating.user_id_2,
                    passing_ability: passing_ability ? passing_ability : rating.passing_ability,
                    shooting_ability: shooting_ability ? shooting_ability : rating.shooting_ability,
                    dribbling_ability: dribbling_ability ? dribbling_ability : rating.dribbling_ability,
                    steals: steals ? steals : rating.steals,
                    rebounds: rebounds ? rebounds : rating.rebounds,
                    blocks: blocks ? blocks : rating.blocks,
                    fitness: fitness ? fitness : rating.fitness,
                    speed: speed ? speed : rating.speed,
                    attendence: attendence ? attendence : rating.attendence,
                    is_session_rating: is_session_rating ? is_session_rating : rating.is_session_rating,
                    offensive_at: is_offensive == 1 ? current_time : rating.offensive_at,
                    defensive_at: is_defensive == 1 ? current_time : rating.defensive_at,
                    more_at: is_more == 1 ? current_time : rating.more_at,
                    updated_at: current_time })
                    .where("id = :id", { id: rating.id })
                    .execute();
            }
            else {
                let new_rating = new user_rating_entity_1.UserRatings();
                new_rating.user_id_1 = logged_id;
                new_rating.user_id_2 = user_id;
                new_rating.passing_ability = passing_ability;
                new_rating.shooting_ability = shooting_ability;
                new_rating.dribbling_ability = dribbling_ability;
                new_rating.steals = steals;
                new_rating.rebounds = rebounds;
                new_rating.blocks = blocks;
                new_rating.fitness = fitness;
                new_rating.speed = speed;
                new_rating.attendence = attendence;
                new_rating.is_session_rating = is_session_rating;
                new_rating.offensive_at = is_offensive == 1 ? current_time : null;
                new_rating.defensive_at = is_defensive == 1 ? current_time : null;
                new_rating.more_at = is_more == 1 ? current_time : null;
                const savedReview = yield this.userRatingsRepository.save(new_rating);
            }
            console.log("start");
            var user = yield this.userRepository.findOne({
                where: {
                    id: logged_id
                }
            });
            var notifMessage = user.first_name + " " + user.last_name + " rated your game";
            return { message: "successfully rated" };
        });
    }
    deactivate(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const current_time = new Date();
            yield typeorm_2.getConnection()
                .createQueryBuilder()
                .update(user_entity_1.Users)
                .set({ deleted_at: current_time })
                .where("id = :id", { id: id })
                .execute();
            return { id: id, message: 'Deactivated Successfully.' };
        });
    }
    logOut(id, req) {
        return __awaiter(this, void 0, void 0, function* () {
            const device_token = req.body.device_token;
            yield typeorm_2.getRepository(user_login_entity_1.UserLogins)
                .createQueryBuilder('user_logins')
                .where("user_logins.device_token = :device_token", { device_token })
                .andWhere("user_logins.user_id = :id", { id: id })
                .delete()
                .execute();
            return { id: id, message: 'logged out Successfully.' };
        });
    }
    userOnline(logged_id, req) {
        return __awaiter(this, void 0, void 0, function* () {
            const is_online = req.body.is_online;
            const current_time = new Date();
            const qb = yield typeorm_2.getRepository(user_entity_1.Users)
                .createQueryBuilder("users")
                .update(user_entity_1.Users)
                .set({ is_online: is_online })
                .where("id = :user_id", { user_id: logged_id })
                .execute();
            return ({ message: "successful" });
        });
    }
    feedback(logged_id, dto) {
        return __awaiter(this, void 0, void 0, function* () {
            const { feedback } = dto;
            let new_feedback = new feedback_entity_1.Feedbacks();
            new_feedback.user_id = logged_id;
            new_feedback.feedback = feedback;
            const savedFeedback = yield this.feedbackRepository.save(new_feedback);
            return { message: "Feedback sent" };
        });
    }
    resendEmail() {
        return __awaiter(this, void 0, void 0, function* () {
            const users = yield typeorm_2.getRepository(user_entity_1.Users)
                .createQueryBuilder('users')
                .select("first_name,last_name,email_verification_key, email ")
                .where('users.is_email_verified =0')
                .andWhere("(MOD(DATEDIFF(CURRENT_TIMESTAMP,users.created_at),3)=0)")
                .getRawMany();
            for (const i in users) {
                try {
                    yield this
                        .mailerService
                        .sendMail({
                        to: users[i].email,
                        subject: 'Email Verification',
                        template: 'src/user/views/verification-reminder',
                        context: {
                            url: process.env.URL,
                            token: users[i].email_verification_key,
                            text: "Click on the link below to verify your email.Click Here",
                            first_name: users[i].first_name,
                            last_name: users[i].last_name
                        }
                    })
                        .then(() => { })
                        .catch(() => { });
                }
                catch (err) {
                    throw new http_exception_1.HttpException({ error: 'bad_request', message: 'Something went wrong.' }, common_2.HttpStatus.BAD_REQUEST);
                }
            }
            return { message: 'Verifification link sent on your email.' };
        });
    }
    profile(id, req) {
        return __awaiter(this, void 0, void 0, function* () {
            if (req.query.id) {
                var user_id = req.query.id;
            }
            else {
                var user_id = id;
            }
            const data = yield this.userRepository.findOne(user_id);
            const ratings = yield typeorm_2.getRepository(user_rating_entity_1.UserRatings)
                .createQueryBuilder("user_ratings")
                .select("((SELECT ROUND(AVG(passing_ability),1) FROM `user_ratings` where user_id_2=" + user_id + " AND passing_ability IS NOT NULL)+(SELECT ROUND(AVG(shooting_ability),1) FROM `user_ratings` where user_id_2=" + user_id + " AND shooting_ability IS NOT NULL) +( SELECT ROUND(AVG(dribbling_ability),1) FROM `user_ratings` where user_id_2=" + user_id + " AND dribbling_ability IS NOT NULL))/3 AS offense_ability,(SELECT ROUND(AVG(passing_ability),1) FROM `user_ratings` where user_id_2=" + user_id + " AND passing_ability IS NOT NULL) AS  passing_ability, (SELECT ROUND(AVG(shooting_ability),1) FROM `user_ratings` where user_id_2=" + user_id + " AND shooting_ability IS NOT NULL) AS shooting_ability ,( SELECT ROUND(AVG(dribbling_ability),1) FROM `user_ratings` where user_id_2=" + user_id + " AND dribbling_ability IS NOT NULL) AS dribbling_ability,"
                + "(( SELECT ROUND(AVG(steals),1) FROM `user_ratings` where user_id_2=" + user_id + " AND steals IS NOT NULL) +( SELECT ROUND(AVG(rebounds),1) FROM `user_ratings` where user_id_2=" + user_id + " AND rebounds IS NOT NULL)+( SELECT ROUND(AVG(blocks),1) FROM `user_ratings` where user_id_2=" + user_id + " AND blocks IS NOT NULL))/3 AS defense_ability,"
                + " ( SELECT ROUND(AVG(steals),1) FROM `user_ratings` where user_id_2=" + user_id + " AND steals IS NOT NULL) AS steals,"
                + "( SELECT ROUND(AVG(rebounds),1) FROM `user_ratings` where user_id_2=" + user_id + " AND rebounds IS NOT NULL) AS rebounds,"
                + "( SELECT ROUND(AVG(blocks),1) FROM `user_ratings` where user_id_2=" + user_id + " AND blocks IS NOT NULL) AS blocks,"
                + "( SELECT ROUND(AVG(fitness),1) FROM `user_ratings` where user_id_2=" + user_id + " AND fitness IS NOT NULL) AS fitness,"
                + "( SELECT ROUND(AVG(speed),1) FROM `user_ratings` where user_id_2=" + user_id + " AND speed IS NOT NULL) AS speed,"
                + "( SELECT ROUND(AVG(attendence),1) FROM `user_ratings` where user_id_2=" + user_id + " AND attendence IS NOT NULL) AS attendence")
                .andWhere('user_id_2 = :user_id', { user_id: user_id })
                .getRawOne();
            let detail = data;
            detail.user_id = user_id;
            detail.ratings = ratings ? ratings : null;
            return detail;
        });
    }
    userListing(user_id, req) {
        return __awaiter(this, void 0, void 0, function* () {
            var event_id = req.query.event_id;
            var team_id = req.query.team_id;
            var type = req.query.type;
            var keyword = req.query.keyword;
            var skip = 0;
            var limit = req.query.limit ? req.query.limit : 1000;
            if (req.query.page) {
                const page = req.query.page;
                var skip = limit * page;
            }
            const qb = yield typeorm_2.getRepository(user_entity_1.Users)
                .createQueryBuilder("users")
                .select("id, user_name,first_name, last_name, profile_image,position,(SELECT count(id) FROM followers WHERE user_id_1 = " + user_id + " AND user_id_2=users.id ) AS is_follow,((SELECT ROUND(AVG(passing_ability),1) FROM `user_ratings` where user_id_2=users.id AND passing_ability IS NOT NULL) + (SELECT ROUND(AVG(shooting_ability),1) FROM `user_ratings` where user_id_2=users.id AND shooting_ability IS NOT NULL) +( SELECT ROUND(AVG(dribbling_ability),1) FROM `user_ratings` where user_id_2=users.id AND dribbling_ability IS NOT NULL)+ ( SELECT ROUND(AVG(steals),1) FROM `user_ratings` where user_id_2=users.id AND steals IS NOT NULL)+( SELECT ROUND(AVG(rebounds),1) FROM `user_ratings` where user_id_2=users.id AND rebounds IS NOT NULL)+( SELECT ROUND(AVG(blocks),1) FROM `user_ratings` where user_id_2=users.id AND blocks IS NOT NULL))/6 AS avg_rating")
                .where("users.id != " + user_id);
            if (keyword) {
                qb.andWhere('(user_name like :keyword OR first_name like :keyword OR last_name like :keyword)', { keyword: '%' + keyword + '%' });
            }
            const users = yield qb.offset(skip).limit(limit).orderBy("is_follow", "DESC").addOrderBy("id", "DESC").getRawMany();
            const count = yield qb.getCount();
            return { users, count };
        });
    }
    userLeaderboard(logged_id, req) {
        return __awaiter(this, void 0, void 0, function* () {
            var global = req.query.global;
            var keyword = req.query.keyword;
            var latitude = req.query.latitude;
            var longitude = req.query.longitude;
            var skip = 0;
            var limit = req.query.limit ? req.query.limit : 10;
            if (req.query.page) {
                const page = req.query.page;
                var skip = limit * page;
            }
            const qb = yield typeorm_2.getRepository(user_entity_1.Users)
                .createQueryBuilder("users")
                .select("id,user_name,first_name,profile_image,position,last_name,location, latitude,IF(TIMESTAMPDIFF(MINUTE, users.`updated_at`,UTC_TIMESTAMP)>5,0,1) AS is_online, longitude,"
                + " IFNULL((((SELECT ROUND(AVG(passing_ability),1) FROM `user_ratings` where user_id_2=users.id AND passing_ability IS NOT NULL) + (SELECT ROUND(AVG(shooting_ability),1) FROM `user_ratings` where user_id_2=users.id AND shooting_ability IS NOT NULL) +( SELECT ROUND(AVG(dribbling_ability),1) FROM `user_ratings` where user_id_2=users.id AND dribbling_ability IS NOT NULL)+ ( SELECT ROUND(AVG(steals),1) FROM `user_ratings` where user_id_2=users.id AND steals IS NOT NULL)+( SELECT ROUND(AVG(rebounds),1) FROM `user_ratings` where user_id_2=users.id AND rebounds IS NOT NULL)+( SELECT ROUND(AVG(blocks),1) FROM `user_ratings` where user_id_2=users.id AND blocks IS NOT NULL))/3)*10,0) AS pgr")
                .where("IFNULL((((SELECT ROUND(AVG(passing_ability),1) FROM `user_ratings` where user_id_2=users.id AND passing_ability IS NOT NULL) + (SELECT ROUND(AVG(shooting_ability),1) FROM `user_ratings` where user_id_2=users.id AND shooting_ability IS NOT NULL) +( SELECT ROUND(AVG(dribbling_ability),1) FROM `user_ratings` where user_id_2=users.id AND dribbling_ability IS NOT NULL)+ ( SELECT ROUND(AVG(steals),1) FROM `user_ratings` where user_id_2=users.id AND steals IS NOT NULL)+( SELECT ROUND(AVG(rebounds),1) FROM `user_ratings` where user_id_2=users.id AND rebounds IS NOT NULL)+( SELECT ROUND(AVG(blocks),1) FROM `user_ratings` where user_id_2=users.id AND blocks IS NOT NULL))/3)*10,0)> 0");
            if (global == 0) {
                if (!latitude || !longitude) {
                    throw new http_exception_1.HttpException({ error: 'bad_request', message: 'Please add lat and lng for this location.' }, common_2.HttpStatus.BAD_REQUEST);
                }
                qb.addSelect("IFNULL(ROUND(( 6371 * acos (cos ( radians( " + latitude + " ) ) * cos( radians( users.latitude) )"
                    + " * cos( radians( users.longitude ) - radians( " + longitude + " ) ) + sin ( radians( " + latitude + " ) )"
                    + " * sin( radians( users.latitude ) ) ) ), 2), 0)", "distance")
                    .andWhere("IFNULL(ROUND(( 6371 * acos (cos ( radians( " + latitude + " ) ) * cos( radians( users.latitude ) )"
                    + " * cos( radians( users.longitude ) - radians( " + longitude + " ) ) + sin ( radians( " + latitude + " ) )"
                    + " * sin( radians( users.latitude ) ) ) ), 2), 0) < 500");
            }
            const data1 = yield qb.orderBy("pgr", "DESC").addOrderBy('id', "ASC").getRawMany();
            if (keyword) {
                qb.andWhere('(user_name like :keyword OR last_name like :keyword OR first_name like :keyword )', { keyword: '%' + keyword + '%' });
            }
            const data = yield qb.orderBy("pgr", "DESC").addOrderBy('id', "ASC").offset(skip).limit(limit).getRawMany();
            const total = yield qb.getCount();
            var count = 0;
            for (const i in data1) {
                data1[i].rank = count + 1;
                count = count + 1;
            }
            console.log(data1.length);
            console.log(data.length);
            for (const i in data) {
                data[i].avg_rating = (data[i].pgr) / 20.0;
                for (const j in data1) {
                    if (data[i].id == data1[j].id) {
                        console.log("i" + data[i].id);
                        console.log("j" + data1[j].id);
                        console.log("jrank" + data1[j].rank);
                        data[i].rank = data1[j].rank;
                    }
                }
            }
            return { data, total };
        });
    }
    findAllReviews(logged_id, req) {
        return __awaiter(this, void 0, void 0, function* () {
            var user_id = req.query.id;
            var skip = 0;
            var limit = req.query.limit ? req.query.limit : 10;
            if (req.query.page) {
                const page = req.query.page;
                var skip = limit * page;
            }
            const qb = yield typeorm_2.getRepository(user_review_entity_1.UserReviews)
                .createQueryBuilder("user_reviews")
                .leftJoinAndSelect(user_entity_1.Users, "users", "users.id = user_reviews.user_id_1")
                .select(["user_reviews.*", "users.first_name as first_name", "users.last_name as last_name", "users.user_name as user_name", "users.profile_image AS profile_image"])
                .addSelect(" ( CASE "
                + " WHEN TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, user_reviews.`created_at`) != 0 THEN"
                + " IF(abs(TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, user_reviews.`created_at`)) > 1 , CONCAT(abs(TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, user_reviews.`created_at`)) ,' years ago'), CONCAT(abs(TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, user_reviews.`created_at`)) ,' year ago'))"
                + " WHEN TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, user_reviews.`created_at`) != 0 THEN"
                + " IF(abs(TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, user_reviews.`created_at`)) > 1 , CONCAT(abs(TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, user_reviews.`created_at`)) ,' months ago'), CONCAT(abs(TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, user_reviews.`created_at`)) ,' month ago'))"
                + " WHEN HOUR(TIMEDIFF(UTC_TIMESTAMP, user_reviews.`created_at`)) != 0 THEN"
                + " IF(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, user_reviews.`created_at`))) < 24 , IF(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, user_reviews.`created_at`))) > 1 , CONCAT(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, user_reviews.`created_at`))) ,' hours ago'), CONCAT(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, user_reviews.`created_at`))) ,' hour ago')),"
                + " IF(DATEDIFF(UTC_TIMESTAMP, user_reviews.`created_at`) > 1 , CONCAT(DATEDIFF(UTC_TIMESTAMP, user_reviews.`created_at`) ,' days ago'), CONCAT(DATEDIFF(UTC_TIMESTAMP, user_reviews.`created_at`) ,' day ago')))"
                + " WHEN MINUTE(TIMEDIFF(UTC_TIMESTAMP, user_reviews.`created_at`)) != 0 THEN CONCAT(MINUTE(TIMEDIFF(UTC_TIMESTAMP, user_reviews.`created_at`)) ,' min ago')"
                + " ELSE "
                + " CONCAT(SECOND(TIMEDIFF(UTC_TIMESTAMP, user_reviews.`created_at`)) ,'s ago')"
                + " END  )", "reviewed_since")
                .where("user_reviews.user_id_2 = :user_id", { user_id: user_id });
            if (logged_id != user_id) {
                qb.andWhere("user_reviews.review_status=1");
            }
            const reviews = yield qb.offset(skip).limit(limit)
                .orderBy("user_reviews.id", "DESC")
                .getRawMany();
            const count = yield qb.getCount();
            return { reviews: reviews, count: count };
        });
    }
    reviewEnable(logged_id, dto) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id, review_status } = dto;
            const review = yield typeorm_2.getRepository(user_review_entity_1.UserReviews)
                .createQueryBuilder("user_reviews")
                .update(user_review_entity_1.UserReviews)
                .set({ review_status: review_status })
                .where("id= :id", { id: id })
                .execute();
            return ({ message: "successful" });
        });
    }
    checkVersion(req) {
        return __awaiter(this, void 0, void 0, function* () {
            var version = req.body.version;
            if (version >= 3) {
                return { message: 'success' };
            }
            else {
                throw new http_exception_1.HttpException({ error: 'bad_request', message: 'Update app to the latest version' }, common_2.HttpStatus.BAD_REQUEST);
            }
        });
    }
    createToken(userData) {
        return __awaiter(this, void 0, void 0, function* () {
            const expiresIn = '90 days', secretOrKey = process.env.SECRET;
            const user = { id: userData.id, email: userData.email };
            console.log(user);
            const token = jwt.sign(user, secretOrKey, { expiresIn });
            return {
                expires_in: expiresIn,
                access_token: token
            };
        });
    }
    validateUser(signedUser) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const user = yield this.userRepository.findOne({ id: signedUser.id });
                return user ? true : false;
            }
            catch (err) {
                return false;
            }
        });
    }
    jwtData(token) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const user = jwt.verify(token, process.env.SECRET);
                console.log(user);
                if (user) {
                    const current_time = new Date();
                    const qb = yield typeorm_2.getRepository(user_entity_1.Users)
                        .createQueryBuilder("events")
                        .update(user_entity_1.Users)
                        .set({ updated_at: current_time })
                        .where("id = :user_id", { user_id: user.id })
                        .execute();
                    return user;
                }
                else {
                    throw new http_exception_1.HttpException({ error: 'bad_request', message: 'User not Found.' }, common_2.HttpStatus.BAD_REQUEST);
                }
            }
            catch (err) {
                throw new http_exception_1.HttpException({ error: 'bad_request', message: 'User not Found.' }, common_2.HttpStatus.BAD_REQUEST);
            }
        });
    }
    generateRandomString(length) {
        return __awaiter(this, void 0, void 0, function* () {
            var text = "";
            const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            for (var i = 0; i < length; i++)
                text += possible.charAt(Math.floor(Math.random() * possible.length));
            return text;
        });
    }
    getProfile(user_id, latitude, longitude, logged_id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const user = yield typeorm_2.getRepository(user_entity_1.Users)
                    .createQueryBuilder("users")
                    .select("*,IF(TIMESTAMPDIFF(MINUTE, users.`updated_at`,UTC_TIMESTAMP)>5,0,1) AS is_online,DATE_FORMAT(users.age, '%Y-%m-%d') AS age,(SELECT count(id) FROM event_members WHERE member_id = users.id AND is_admin=1) AS session_organized, (SELECT count(id) FROM event_members WHERE member_id = users.id AND is_join=1) AS session_attended,(SELECT count(id) FROM user_ratings WHERE user_id_2 = users.id) AS total_ratings, (SELECT count(id) FROM followers WHERE user_id_1 = users.id) AS following, (SELECT count(id) FROM followers WHERE user_id_2 = users.id) AS followers,(SELECT count(id) FROM followers WHERE user_id_1 = " + logged_id + " AND user_id_2=users.id ) AS is_follow,"
                    + " IFNULL((((SELECT ROUND(AVG(passing_ability),1) FROM `user_ratings` where user_id_2=users.id AND passing_ability IS NOT NULL) + (SELECT ROUND(AVG(shooting_ability),1) FROM `user_ratings` where user_id_2=users.id AND shooting_ability IS NOT NULL) +( SELECT ROUND(AVG(dribbling_ability),1) FROM `user_ratings` where user_id_2=users.id AND dribbling_ability IS NOT NULL)+ ( SELECT ROUND(AVG(steals),1) FROM `user_ratings` where user_id_2=users.id AND steals IS NOT NULL)+( SELECT ROUND(AVG(rebounds),1) FROM `user_ratings` where user_id_2=users.id AND rebounds IS NOT NULL)+( SELECT ROUND(AVG(blocks),1) FROM `user_ratings` where user_id_2=users.id AND blocks IS NOT NULL))/3)*10,0) AS pgr ")
                    .where('id = :user_id', { user_id: user_id })
                    .getRawOne();
                const leaders = yield typeorm_2.getRepository(user_entity_1.Users)
                    .createQueryBuilder("users")
                    .select("users.id,"
                    + " ROUND(IFNULL((((SELECT ROUND(AVG(passing_ability),1) FROM `user_ratings` where user_id_2=users.id AND passing_ability IS NOT NULL) + (SELECT ROUND(AVG(shooting_ability),1) FROM `user_ratings` where user_id_2=users.id AND shooting_ability IS NOT NULL) +( SELECT ROUND(AVG(dribbling_ability),1) FROM `user_ratings` where user_id_2=users.id AND dribbling_ability IS NOT NULL)+ ( SELECT ROUND(AVG(steals),1) FROM `user_ratings` where user_id_2=users.id AND steals IS NOT NULL)+( SELECT ROUND(AVG(rebounds),1) FROM `user_ratings` where user_id_2=users.id AND rebounds IS NOT NULL)+( SELECT ROUND(AVG(blocks),1) FROM `user_ratings` where user_id_2=users.id AND blocks IS NOT NULL))/3)*10,0),1) AS pgr")
                    .andWhere("IFNULL(ROUND(( 6371 * acos (cos ( radians( " + latitude + " ) ) * cos( radians( users.latitude ) )"
                    + " * cos( radians( users.longitude ) - radians( " + longitude + " ) ) + sin ( radians( " + latitude + " ) )"
                    + " * sin( radians( users.latitude ) ) ) ), 2), 0) < 500")
                    .getRawMany();
                console.log("leaders");
                console.log(leaders);
                var count = 0;
                if (leaders) {
                    for (const i in leaders) {
                        console.log("leader" + leaders[i].pgr);
                        console.log("user" + user.pgr);
                        if (leaders[i].pgr > user.pgr) {
                            console.log(1);
                            count++;
                        }
                        else if (leaders[i].pgr == user.pgr) {
                            console.log(2);
                            if (leaders[i].id < user.id) {
                                console.log(3);
                                count++;
                            }
                        }
                    }
                }
                const rank = count + 1;
                let data = user;
                data.rank = rank;
                data.avg_rating = (data.pgr) / 20.0;
                console.log(data);
                return data;
            }
            catch (err) {
                console.log(err);
                return "Profile doesn't exist";
            }
        });
    }
};
UserService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(user_entity_1.Users)),
    __param(1, typeorm_1.InjectRepository(user_login_entity_1.UserLogins)),
    __param(2, typeorm_1.InjectRepository(user_review_entity_1.UserReviews)),
    __param(3, typeorm_1.InjectRepository(user_rating_entity_1.UserRatings)),
    __param(4, typeorm_1.InjectRepository(block_entity_1.Blocks)),
    __param(5, typeorm_1.InjectRepository(follow_entity_1.Followers)),
    __param(6, typeorm_1.InjectRepository(feedback_entity_1.Feedbacks)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        mailer_1.MailerService])
], UserService);
exports.UserService = UserService;
//# sourceMappingURL=user.service.js.map