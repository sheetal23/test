export declare class EditProfileDto {
    readonly first_name: string;
    readonly last_name: string;
    readonly user_name: string;
    readonly profile_image: string;
    readonly age: Date;
    readonly height: number;
    readonly gender: number;
    readonly position: string;
    readonly fav_nba_team: string;
    readonly fav_nba_player: string;
    readonly current_team: string;
    readonly session_range: number;
    readonly location: string;
    readonly currency: string;
    readonly latitude: number;
    readonly longitude: number;
    readonly review_enabled: number;
    readonly delete_image: number;
}
