export declare class UserRatingDto {
    readonly user_id: number;
    readonly passing_ability: number;
    readonly shooting_ability: number;
    readonly dribbling_ability: number;
    readonly steals: number;
    readonly rebounds: number;
    readonly blocks: number;
    readonly fitness: number;
    readonly speed: number;
    readonly attendence: number;
    readonly is_session_rating: number;
    readonly is_offensive: number;
    readonly is_defensive: number;
    readonly is_more: number;
}
