export declare class ReviewUserDto {
    readonly id: number;
    readonly user_id: number;
    readonly review: string;
    readonly review_status: number;
}
