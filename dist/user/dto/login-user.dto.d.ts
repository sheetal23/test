export declare class LoginUserDto {
    readonly email: string;
    readonly password: string;
    readonly facebook_id: string;
    readonly google_id: string;
    readonly user_name: string;
    readonly first_name: string;
    readonly last_name: string;
    readonly profile_image: string;
    readonly device_id: string;
    readonly location: string;
    readonly identifier: string;
    readonly latitude: number;
    readonly longitude: number;
    readonly device_type: number;
    readonly device_token: string;
}
