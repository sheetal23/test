export declare class CommonUserDto {
    readonly user_id: number;
    readonly feedback: string;
}
