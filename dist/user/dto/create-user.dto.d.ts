export declare class CreateUserDto {
    readonly email: string;
    readonly password: string;
    readonly first_name: string;
    readonly last_name: string;
    readonly user_name: string;
    readonly position: string;
    readonly location: string;
    readonly latitude: number;
    readonly longitude: number;
    readonly device_id: string;
    readonly device_type: number;
    readonly device_token: string;
}
