"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const user_service_1 = require("./user.service");
const dto_1 = require("./dto");
const validation_pipe_1 = require("../shared/pipes/validation.pipe");
const swagger_1 = require("@nestjs/swagger");
let UserController = class UserController {
    constructor(userService) {
        this.userService = userService;
    }
    create(userData) {
        return __awaiter(this, void 0, void 0, function* () {
            const user = yield this.userService.create(userData);
            console.log(user);
            const user_token = yield this.userService.createToken(user);
            const profile = yield this.userService.getProfile(user.id, null, null, user.id);
            const userDetail = { profile, session_id: user_token.access_token };
            return userDetail;
        });
    }
    checkVersion(request) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.userService.checkVersion(request);
        });
    }
    login(userData) {
        return __awaiter(this, void 0, void 0, function* () {
            const user = yield this.userService.login(userData);
            const user_token = yield this.userService.createToken(user);
            console.log("user");
            console.log(user);
            console.log(user.id);
            const profile = yield this.userService.getProfile(user.id, user.latitude, user.longitude, user.id);
            console.log("profile");
            console.log(profile);
            profile.first_time = user.first_time;
            const userDetail = { profile, session_id: user_token.access_token };
            return userDetail;
        });
    }
    profile(request) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            console.log(request.headers.authorization);
            const user_data = yield this.userService.profile(jwtData.id, request);
            const data1 = yield this.userService.getProfile(user_data.user_id, user_data.latitude, user_data.longitude, jwtData.id);
            let detail = data1;
            detail.ratings = user_data.ratings;
            detail.reviews = user_data.reviews;
            return detail;
        });
    }
    deactivate(request) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            return yield this.userService.deactivate(jwtData.id);
        });
    }
    getFollowers(request) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            return yield this.userService.getFollowers(jwtData.id, request);
        });
    }
    findAllReviews(request) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            return yield this.userService.findAllReviews(jwtData.id, request);
        });
    }
    userLeaderboard(request) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            return yield this.userService.userLeaderboard(jwtData.id, request);
        });
    }
    logOut(request) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            console.log(request.headers.authorization);
            return yield this.userService.logOut(jwtData.id, request);
        });
    }
    userReview(request, reviewData) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            return yield this.userService.userReview(jwtData.id, reviewData);
        });
    }
    blockUser(request, blockData) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            return yield this.userService.blockUser(jwtData.id, blockData);
        });
    }
    feedback(request, feedbackData) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            return yield this.userService.feedback(jwtData.id, feedbackData);
        });
    }
    followUser(request, followData) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            return yield this.userService.followUser(jwtData.id, followData);
        });
    }
    rateUser(request, ratingData) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            return yield this.userService.rateUser(jwtData.id, ratingData);
        });
    }
    userListing(request) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            return yield this.userService.userListing(jwtData.id, request);
        });
    }
    changePassword(request, userData) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            return yield this.userService.changePassword(jwtData.id, userData);
        });
    }
    editProfile(request, userData) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            return yield this.userService.editProfile(jwtData.id, userData);
        });
    }
    forgotPassword(request) {
        return __awaiter(this, void 0, void 0, function* () {
            const email = request.body.email;
            return yield this.userService.forgotPassword(email);
        });
    }
    resetPassword(request) {
        return __awaiter(this, void 0, void 0, function* () {
            const token = request.body.token;
            return yield this.userService.resetPassword(token, request);
        });
    }
    verifyEmail(request) {
        return __awaiter(this, void 0, void 0, function* () {
            const token = request.body.token;
            return yield this.userService.verifyEmail(token, request);
        });
    }
    resendVerifyEmail(request) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.userService.resendVerifyEmail(request);
        });
    }
    userOnline(request) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            return yield this.userService.userOnline(jwtData.id, request);
        });
    }
    reviewEnable(request, dto) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            return yield this.userService.reviewEnable(jwtData.id, dto);
        });
    }
    resendEmail() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.userService.resendEmail();
        });
    }
};
__decorate([
    common_1.UsePipes(new validation_pipe_1.ValidationPipe),
    common_1.Post('signup'),
    common_1.HttpCode(200),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.CreateUserDto]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "create", null);
__decorate([
    common_1.Post('version'),
    common_1.HttpCode(200),
    __param(0, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "checkVersion", null);
__decorate([
    common_1.UsePipes(new validation_pipe_1.ValidationPipe),
    common_1.Post('signin'),
    common_1.HttpCode(200),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.LoginUserDto]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "login", null);
__decorate([
    common_1.Get(''),
    common_1.HttpCode(200),
    __param(0, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "profile", null);
__decorate([
    common_1.Delete(''),
    common_1.HttpCode(200),
    __param(0, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "deactivate", null);
__decorate([
    common_1.Get('followers'),
    common_1.HttpCode(200),
    __param(0, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "getFollowers", null);
__decorate([
    common_1.Get('reviews'),
    common_1.HttpCode(200),
    __param(0, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "findAllReviews", null);
__decorate([
    common_1.Get('leaderboard'),
    common_1.HttpCode(200),
    __param(0, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "userLeaderboard", null);
__decorate([
    common_1.Delete('logout'),
    common_1.HttpCode(200),
    __param(0, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "logOut", null);
__decorate([
    common_1.Post('review'),
    common_1.HttpCode(200),
    __param(0, common_1.Req()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, dto_1.ReviewUserDto]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "userReview", null);
__decorate([
    common_1.Post('block'),
    common_1.HttpCode(200),
    __param(0, common_1.Req()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, dto_1.CommonUserDto]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "blockUser", null);
__decorate([
    common_1.Post('feedback'),
    common_1.HttpCode(200),
    __param(0, common_1.Req()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, dto_1.CommonUserDto]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "feedback", null);
__decorate([
    common_1.Post('follow'),
    common_1.HttpCode(200),
    __param(0, common_1.Req()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, dto_1.CommonUserDto]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "followUser", null);
__decorate([
    common_1.Post('rating'),
    common_1.HttpCode(200),
    __param(0, common_1.Req()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, dto_1.UserRatingDto]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "rateUser", null);
__decorate([
    common_1.Get('listing'),
    common_1.HttpCode(200),
    __param(0, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "userListing", null);
__decorate([
    common_1.UsePipes(new validation_pipe_1.ValidationPipe),
    common_1.Put('change-password'),
    common_1.HttpCode(200),
    __param(0, common_1.Req()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, dto_1.ChangePasswordDto]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "changePassword", null);
__decorate([
    common_1.UsePipes(new validation_pipe_1.ValidationPipe),
    common_1.Put(''),
    common_1.HttpCode(200),
    __param(0, common_1.Req()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, dto_1.EditProfileDto]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "editProfile", null);
__decorate([
    common_1.Post('forgot-password'),
    common_1.HttpCode(200),
    __param(0, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "forgotPassword", null);
__decorate([
    common_1.Post('reset-password'),
    common_1.HttpCode(200),
    __param(0, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "resetPassword", null);
__decorate([
    common_1.Post('verify-email'),
    common_1.HttpCode(200),
    __param(0, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "verifyEmail", null);
__decorate([
    common_1.Put('resend-verify-email'),
    common_1.HttpCode(200),
    __param(0, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "resendVerifyEmail", null);
__decorate([
    common_1.Put('is-online'),
    common_1.HttpCode(200),
    __param(0, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "userOnline", null);
__decorate([
    common_1.Put('review-enable'),
    common_1.HttpCode(200),
    __param(0, common_1.Req()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, dto_1.ReviewUserDto]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "reviewEnable", null);
__decorate([
    common_1.Get('verification-reminder'),
    common_1.HttpCode(200),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], UserController.prototype, "resendEmail", null);
UserController = __decorate([
    swagger_1.ApiBearerAuth(),
    swagger_1.ApiUseTags('user'),
    common_1.Controller('user'),
    __metadata("design:paramtypes", [user_service_1.UserService])
], UserController);
exports.UserController = UserController;
//# sourceMappingURL=user.controller.js.map