import { UserService } from './user.service';
import { CreateUserDto, ChangePasswordDto, EditProfileDto, LoginUserDto, ReviewUserDto, CommonUserDto, UserRatingDto } from './dto';
export declare class UserController {
    private readonly userService;
    constructor(userService: UserService);
    create(userData: CreateUserDto): Promise<{
        profile: any;
        session_id: string;
    }>;
    checkVersion(request: any): Promise<object>;
    login(userData: LoginUserDto): Promise<{
        profile: any;
        session_id: string;
    }>;
    profile(request: any): Promise<any>;
    deactivate(request: any): Promise<object>;
    getFollowers(request: any): Promise<{
        data: any[];
        count: number;
    }>;
    findAllReviews(request: any): Promise<{
        reviews: any[];
        count: number;
    }>;
    userLeaderboard(request: any): Promise<{
        data: any[];
        total: number;
    }>;
    logOut(request: any): Promise<object>;
    userReview(request: any, reviewData: ReviewUserDto): Promise<{
        message: string;
    }>;
    blockUser(request: any, blockData: CommonUserDto): Promise<{
        message: string;
    }>;
    feedback(request: any, feedbackData: CommonUserDto): Promise<{
        message: string;
    }>;
    followUser(request: any, followData: CommonUserDto): Promise<{
        message: string;
    }>;
    rateUser(request: any, ratingData: UserRatingDto): Promise<{
        message: string;
    }>;
    userListing(request: any): Promise<{
        users: any[];
        count: number;
    }>;
    changePassword(request: any, userData: ChangePasswordDto): Promise<object>;
    editProfile(request: any, userData: EditProfileDto): Promise<object>;
    forgotPassword(request: any): Promise<object>;
    resetPassword(request: any): Promise<object>;
    verifyEmail(request: any): Promise<object>;
    resendVerifyEmail(request: any): Promise<object>;
    userOnline(request: any): Promise<{
        message: string;
    }>;
    reviewEnable(request: any, dto: ReviewUserDto): Promise<{
        message: string;
    }>;
    resendEmail(): Promise<object>;
}
