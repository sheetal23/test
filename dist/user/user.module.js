"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const user_controller_1 = require("./user.controller");
const typeorm_1 = require("@nestjs/typeorm");
const user_entity_1 = require("./entities/user.entity");
const user_login_entity_1 = require("./entities/user-login.entity");
const user_review_entity_1 = require("./entities/user-review.entity");
const user_rating_entity_1 = require("./entities/user-rating.entity");
const follow_entity_1 = require("./entities/follow.entity");
const block_entity_1 = require("./entities/block.entity");
const user_service_1 = require("./user.service");
const user_authmiddleware_1 = require("./user.authmiddleware");
const feedback_entity_1 = require("./entities/feedback.entity");
let UserModule = class UserModule {
    configure(consumer) {
        consumer
            .apply(user_authmiddleware_1.AuthMiddleware)
            .forRoutes({ path: 'user/deactivate', method: common_1.RequestMethod.POST }, { path: 'user/change-password', method: common_1.RequestMethod.PUT }, { path: 'user', method: common_1.RequestMethod.PUT }, { path: 'user', method: common_1.RequestMethod.GET }, { path: 'user/review', method: common_1.RequestMethod.POST }, { path: 'user/block', method: common_1.RequestMethod.POST }, { path: 'user/follow', method: common_1.RequestMethod.POST }, { path: 'user/followers', method: common_1.RequestMethod.GET }, { path: 'user/reviews', method: common_1.RequestMethod.GET }, { path: 'user/rating', method: common_1.RequestMethod.POST }, { path: 'user/listing', method: common_1.RequestMethod.GET }, { path: 'user/leaderboard', method: common_1.RequestMethod.GET }, { path: 'user/online', method: common_1.RequestMethod.PUT }, { path: 'user/review-enable', method: common_1.RequestMethod.PUT }, { path: 'user/feedback', method: common_1.RequestMethod.POST });
    }
};
UserModule = __decorate([
    common_1.Module({
        imports: [
            typeorm_1.TypeOrmModule.forFeature([user_entity_1.Users]),
            typeorm_1.TypeOrmModule.forFeature([user_login_entity_1.UserLogins]),
            typeorm_1.TypeOrmModule.forFeature([follow_entity_1.Followers]),
            typeorm_1.TypeOrmModule.forFeature([block_entity_1.Blocks]),
            typeorm_1.TypeOrmModule.forFeature([feedback_entity_1.Feedbacks]),
            typeorm_1.TypeOrmModule.forFeature([user_review_entity_1.UserReviews]),
            typeorm_1.TypeOrmModule.forFeature([user_rating_entity_1.UserRatings]),
        ],
        providers: [user_service_1.UserService],
        controllers: [
            user_controller_1.UserController
        ],
        exports: [user_service_1.UserService]
    })
], UserModule);
exports.UserModule = UserModule;
//# sourceMappingURL=user.module.js.map