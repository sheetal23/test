export declare class UserLogins {
    id: number;
    user_id: number;
    device_id: string;
    device_token: string;
    device_type: number;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
}
