export declare class UserReviews {
    id: number;
    user_id_1: number;
    user_id_2: number;
    review: string;
    review_status: number;
    created_at: Date;
    updated_at: Date;
}
