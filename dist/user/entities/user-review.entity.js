"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const user_entity_1 = require("./user.entity");
let UserReviews = class UserReviews {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], UserReviews.prototype, "id", void 0);
__decorate([
    typeorm_1.ManyToOne(type => user_entity_1.Users, users => users.UserReviews1, {
        cascade: true,
        onDelete: "CASCADE",
        onUpdate: "CASCADE"
    }),
    typeorm_1.JoinColumn({ name: "user_id_1" }),
    __metadata("design:type", Number)
], UserReviews.prototype, "user_id_1", void 0);
__decorate([
    typeorm_1.ManyToOne(type => user_entity_1.Users, users => users.UserReviews2, {
        cascade: true,
        onDelete: "CASCADE",
        onUpdate: "CASCADE"
    }),
    typeorm_1.JoinColumn({ name: "user_id_2" }),
    __metadata("design:type", Number)
], UserReviews.prototype, "user_id_2", void 0);
__decorate([
    typeorm_1.Column({ default: null, collation: "utf8mb4_bin" }),
    __metadata("design:type", String)
], UserReviews.prototype, "review", void 0);
__decorate([
    typeorm_1.Column({ default: 1, type: "boolean", comment: "1-enable,2-disable" }),
    __metadata("design:type", Number)
], UserReviews.prototype, "review_status", void 0);
__decorate([
    typeorm_1.Column({ type: 'timestamp', default: () => "CURRENT_TIMESTAMP" }),
    __metadata("design:type", Date)
], UserReviews.prototype, "created_at", void 0);
__decorate([
    typeorm_1.Column({ type: 'timestamp', default: null }),
    __metadata("design:type", Date)
], UserReviews.prototype, "updated_at", void 0);
UserReviews = __decorate([
    typeorm_1.Entity()
], UserReviews);
exports.UserReviews = UserReviews;
//# sourceMappingURL=user-review.entity.js.map