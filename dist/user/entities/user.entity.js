"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const md5 = require("md5");
const user_login_entity_1 = require("./user-login.entity");
const follow_entity_1 = require("./follow.entity");
const user_rating_entity_1 = require("./user-rating.entity");
const user_review_entity_1 = require("./user-review.entity");
const block_entity_1 = require("./block.entity");
const feedback_entity_1 = require("./feedback.entity");
const chat_entity_1 = require("../../chat/entities/chat.entity");
const chat_to_entity_1 = require("../../chat/entities/chat-to.entity");
let Users = class Users {
    hashPassword() {
        this.password = this.password ? md5(this.password) : null;
    }
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], Users.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ default: null }),
    __metadata("design:type", String)
], Users.prototype, "first_name", void 0);
__decorate([
    typeorm_1.Column({ default: null }),
    __metadata("design:type", String)
], Users.prototype, "last_name", void 0);
__decorate([
    typeorm_1.Column({ default: null, unique: true }),
    __metadata("design:type", String)
], Users.prototype, "user_name", void 0);
__decorate([
    typeorm_1.BeforeInsert(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], Users.prototype, "hashPassword", null);
__decorate([
    typeorm_1.Column({ select: false, default: null }),
    __metadata("design:type", String)
], Users.prototype, "password", void 0);
__decorate([
    typeorm_1.Column({ default: null, type: 'date' }),
    __metadata("design:type", Date)
], Users.prototype, "age", void 0);
__decorate([
    typeorm_1.Column({ default: null, type: "double" }),
    __metadata("design:type", Number)
], Users.prototype, "height", void 0);
__decorate([
    typeorm_1.Column({ default: null, type: "tinyint", comment: "2-male,1-female" }),
    __metadata("design:type", Number)
], Users.prototype, "gender", void 0);
__decorate([
    typeorm_1.Column({ default: null }),
    __metadata("design:type", String)
], Users.prototype, "main_sport", void 0);
__decorate([
    typeorm_1.Column({ default: null, type: "tinyint", comment: "1-paused,2-unpaused" }),
    __metadata("design:type", Number)
], Users.prototype, "is_paused", void 0);
__decorate([
    typeorm_1.Column({ default: null }),
    __metadata("design:type", String)
], Users.prototype, "location", void 0);
__decorate([
    typeorm_1.Column({ default: null, type: "double" }),
    __metadata("design:type", Number)
], Users.prototype, "latitude", void 0);
__decorate([
    typeorm_1.Column({ default: null, type: "double" }),
    __metadata("design:type", Number)
], Users.prototype, "longitude", void 0);
__decorate([
    typeorm_1.Column({ default: null }),
    __metadata("design:type", String)
], Users.prototype, "email", void 0);
__decorate([
    typeorm_1.Column({ default: null }),
    __metadata("design:type", String)
], Users.prototype, "facebook_id", void 0);
__decorate([
    typeorm_1.Column({ default: null }),
    __metadata("design:type", String)
], Users.prototype, "google_id", void 0);
__decorate([
    typeorm_1.Column({ default: null }),
    __metadata("design:type", String)
], Users.prototype, "profile_image", void 0);
__decorate([
    typeorm_1.Column({ default: null, comment: "point_guard, small_forward, power_forward, shooting_forward, center" }),
    __metadata("design:type", String)
], Users.prototype, "position", void 0);
__decorate([
    typeorm_1.Column({ default: null }),
    __metadata("design:type", String)
], Users.prototype, "email_verification_key", void 0);
__decorate([
    typeorm_1.Column({ default: 1, type: "tinyint" }),
    __metadata("design:type", Number)
], Users.prototype, "is_email_verified", void 0);
__decorate([
    typeorm_1.Column({ default: null }),
    __metadata("design:type", String)
], Users.prototype, "password_reset_key", void 0);
__decorate([
    typeorm_1.Column({ default: 1, type: "boolean", comment: "1-enable,2-disable" }),
    __metadata("design:type", Number)
], Users.prototype, "review_enabled", void 0);
__decorate([
    typeorm_1.Column({ default: null }),
    __metadata("design:type", String)
], Users.prototype, "fav_nba_team", void 0);
__decorate([
    typeorm_1.Column({ default: null }),
    __metadata("design:type", String)
], Users.prototype, "fav_nba_player", void 0);
__decorate([
    typeorm_1.Column({ default: null }),
    __metadata("design:type", String)
], Users.prototype, "current_team", void 0);
__decorate([
    typeorm_1.Column({ default: null }),
    __metadata("design:type", String)
], Users.prototype, "current_city", void 0);
__decorate([
    typeorm_1.Column({ default: null, collation: "utf8mb4_bin" }),
    __metadata("design:type", String)
], Users.prototype, "currency", void 0);
__decorate([
    typeorm_1.Column({ default: 200, type: "double" }),
    __metadata("design:type", Number)
], Users.prototype, "session_range", void 0);
__decorate([
    typeorm_1.Column({ default: null, type: "boolean" }),
    __metadata("design:type", Number)
], Users.prototype, "is_online", void 0);
__decorate([
    typeorm_1.Column({ type: 'timestamp', default: () => "CURRENT_TIMESTAMP" }),
    __metadata("design:type", Date)
], Users.prototype, "created_at", void 0);
__decorate([
    typeorm_1.Column({ type: 'timestamp', default: null }),
    __metadata("design:type", Date)
], Users.prototype, "updated_at", void 0);
__decorate([
    typeorm_1.Column({ type: 'timestamp', default: null }),
    __metadata("design:type", Date)
], Users.prototype, "deleted_at", void 0);
__decorate([
    typeorm_1.OneToMany(type => user_login_entity_1.UserLogins, UserLogins => UserLogins.user_id),
    __metadata("design:type", Array)
], Users.prototype, "UserLogins", void 0);
__decorate([
    typeorm_1.OneToMany(type => user_rating_entity_1.UserRatings, UserRatings => UserRatings.user_id_1),
    __metadata("design:type", Array)
], Users.prototype, "UserRatings1", void 0);
__decorate([
    typeorm_1.OneToMany(type => user_rating_entity_1.UserRatings, UserRatings => UserRatings.user_id_2),
    __metadata("design:type", Array)
], Users.prototype, "UserRatings2", void 0);
__decorate([
    typeorm_1.OneToMany(type => user_review_entity_1.UserReviews, UserReviews => UserReviews.user_id_1),
    __metadata("design:type", Array)
], Users.prototype, "UserReviews1", void 0);
__decorate([
    typeorm_1.OneToMany(type => user_review_entity_1.UserReviews, UserReviews => UserReviews.user_id_2),
    __metadata("design:type", Array)
], Users.prototype, "UserReviews2", void 0);
__decorate([
    typeorm_1.OneToMany(type => follow_entity_1.Followers, Followers => Followers.user_id_1),
    __metadata("design:type", Array)
], Users.prototype, "Followers1", void 0);
__decorate([
    typeorm_1.OneToMany(type => follow_entity_1.Followers, Followers => Followers.user_id_2),
    __metadata("design:type", Array)
], Users.prototype, "Followers2", void 0);
__decorate([
    typeorm_1.OneToMany(type => block_entity_1.Blocks, Blocks => Blocks.user_id_1),
    __metadata("design:type", Array)
], Users.prototype, "Blocks1", void 0);
__decorate([
    typeorm_1.OneToMany(type => block_entity_1.Blocks, Blocks => Blocks.user_id_2),
    __metadata("design:type", Array)
], Users.prototype, "Blocks2", void 0);
__decorate([
    typeorm_1.OneToMany(type => feedback_entity_1.Feedbacks, Feedbacks => Feedbacks.user_id),
    __metadata("design:type", Array)
], Users.prototype, "Feedbacks", void 0);
__decorate([
    typeorm_1.OneToMany(type => chat_entity_1.Chats, Chats => Chats.from_id),
    __metadata("design:type", Array)
], Users.prototype, "Chats", void 0);
__decorate([
    typeorm_1.OneToMany(type => chat_to_entity_1.ChatTos, ChatTos => ChatTos.to_id),
    __metadata("design:type", Array)
], Users.prototype, "ChatTos", void 0);
Users = __decorate([
    typeorm_1.Entity()
], Users);
exports.Users = Users;
//# sourceMappingURL=user.entity.js.map