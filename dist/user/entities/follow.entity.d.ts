export declare class Followers {
    id: number;
    user_id_1: number;
    user_id_2: number;
    created_at: Date;
}
