export declare class Feedbacks {
    id: number;
    user_id: number;
    feedback: string;
    created_at: Date;
    updated_at: Date;
}
