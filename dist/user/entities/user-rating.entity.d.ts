export declare class UserRatings {
    id: number;
    user_id_1: number;
    user_id_2: number;
    passing_ability: number;
    shooting_ability: number;
    dribbling_ability: number;
    steals: number;
    rebounds: number;
    fitness: number;
    speed: number;
    blocks: number;
    attendence: number;
    is_session_rating: number;
    defensive_at: Date;
    offensive_at: Date;
    more_at: Date;
    created_at: Date;
    updated_at: Date;
}
