import { Users } from '../user/entities/user.entity';
import { Events } from '../event/entities/event.entity';
import { Notifications } from './entities/notification.entity';
import { NotificationTos } from './entities/notification-to.entity';
import { Repository } from 'typeorm';
import { NotificationDto } from './dto';
import { MailerService } from '@nest-modules/mailer';
import { Teams } from '../team/entities/team.entity';
import { UserLogins } from '../user/entities/user-login.entity';
import { Posts } from '../post/entities/post.entity';
export declare class NotificationService {
    private readonly notificationsRepository;
    private readonly notificationTosRepository;
    private readonly userRepository;
    private readonly userLoginRepository;
    private readonly eventRepository;
    private readonly teamRepository;
    private readonly postRepository;
    private readonly mailerService;
    constructor(notificationsRepository: Repository<Notifications>, notificationTosRepository: Repository<NotificationTos>, userRepository: Repository<Users>, userLoginRepository: Repository<UserLogins>, eventRepository: Repository<Events>, teamRepository: Repository<Teams>, postRepository: Repository<Posts>, mailerService: MailerService);
    save(type: any, from_id: any, event_id: any, post_id: any, subject: any, message: any, to_ids: any, to_id: any): Promise<void>;
    findAll(logged_id: number, req: any): Promise<{
        data: any[];
        count: number;
    }>;
    deleteNotif(logged_id: number, req: any): Promise<{
        message: string;
    }>;
    readNotif(logged_id: number, dto: NotificationDto): Promise<{
        message: string;
    }>;
    readNotifEvent(logged_id: any, user_id: any, event_id: any, post_id: any): Promise<void>;
    findUnread(logged_id: number): Promise<{
        unread_notif_count: number;
    }>;
    dummyPush(dto: NotificationDto): Promise<{
        message: string;
    }>;
    push(fcm_ids: any, data: any, notification: any, collapse_key?: any): Promise<void>;
}
