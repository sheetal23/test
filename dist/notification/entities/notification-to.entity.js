"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const user_entity_1 = require("../../user/entities/user.entity");
const notification_entity_1 = require("./notification.entity");
let NotificationTos = class NotificationTos {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], NotificationTos.prototype, "id", void 0);
__decorate([
    typeorm_1.ManyToOne(type => user_entity_1.Users, Users => Users.NotificationTos, {
        cascade: true,
        onDelete: "CASCADE",
        onUpdate: "CASCADE"
    }),
    typeorm_1.JoinColumn({ name: "to_id" }),
    __metadata("design:type", Number)
], NotificationTos.prototype, "to_id", void 0);
__decorate([
    typeorm_1.ManyToOne(type => notification_entity_1.Notifications, Notifications => Notifications.NotificationTos, {
        cascade: true,
        onDelete: "CASCADE",
        onUpdate: "CASCADE"
    }),
    typeorm_1.JoinColumn({ name: "notif_id" }),
    __metadata("design:type", Number)
], NotificationTos.prototype, "notif_id", void 0);
__decorate([
    typeorm_1.Column({ type: "tinyint", comment: "0-unread, 1-read", default: 0 }),
    __metadata("design:type", Number)
], NotificationTos.prototype, "is_read", void 0);
__decorate([
    typeorm_1.Column({ type: 'timestamp', default: () => "CURRENT_TIMESTAMP" }),
    __metadata("design:type", Date)
], NotificationTos.prototype, "created_at", void 0);
__decorate([
    typeorm_1.Column({ type: 'timestamp', default: null }),
    __metadata("design:type", Date)
], NotificationTos.prototype, "updated_at", void 0);
NotificationTos = __decorate([
    typeorm_1.Entity()
], NotificationTos);
exports.NotificationTos = NotificationTos;
//# sourceMappingURL=notification-to.entity.js.map