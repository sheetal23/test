import { NotificationTos } from './notification-to.entity';
export declare class Notifications {
    id: number;
    user_id: number;
    event_id: number;
    post_id: number;
    type: number;
    message: string;
    created_at: Date;
    updated_at: Date;
    NotificationTos: NotificationTos[];
}
