"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const user_entity_1 = require("../../user/entities/user.entity");
const event_entity_1 = require("../../event/entities/event.entity");
const post_entity_1 = require("../../post/entities/post.entity");
const notification_to_entity_1 = require("./notification-to.entity");
let Notifications = class Notifications {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], Notifications.prototype, "id", void 0);
__decorate([
    typeorm_1.ManyToOne(type => user_entity_1.Users, Users => Users.Notifications, {
        cascade: true,
        onDelete: "CASCADE",
        onUpdate: "CASCADE"
    }),
    typeorm_1.JoinColumn({ name: "user_id" }),
    __metadata("design:type", Number)
], Notifications.prototype, "user_id", void 0);
__decorate([
    typeorm_1.ManyToOne(type => event_entity_1.Events, Events => Events.Notifications, {
        cascade: true,
        onDelete: "CASCADE",
        onUpdate: "CASCADE"
    }),
    typeorm_1.JoinColumn({ name: "event_id" }),
    __metadata("design:type", Number)
], Notifications.prototype, "event_id", void 0);
__decorate([
    typeorm_1.ManyToOne(type => post_entity_1.Posts, Posts => Posts.Notifications, {
        cascade: true,
        onDelete: "CASCADE",
        onUpdate: "CASCADE"
    }),
    typeorm_1.JoinColumn({ name: "post_id" }),
    __metadata("design:type", Number)
], Notifications.prototype, "post_id", void 0);
__decorate([
    typeorm_1.Column({ default: null }),
    __metadata("design:type", Number)
], Notifications.prototype, "type", void 0);
__decorate([
    typeorm_1.Column({ default: null, collation: "utf8mb4_unicode_ci", charset: "utf8mb4" }),
    __metadata("design:type", String)
], Notifications.prototype, "message", void 0);
__decorate([
    typeorm_1.Column({ type: 'timestamp', default: () => "CURRENT_TIMESTAMP" }),
    __metadata("design:type", Date)
], Notifications.prototype, "created_at", void 0);
__decorate([
    typeorm_1.Column({ type: 'timestamp', default: null }),
    __metadata("design:type", Date)
], Notifications.prototype, "updated_at", void 0);
__decorate([
    typeorm_1.OneToMany(type => notification_to_entity_1.NotificationTos, NotificationTos => NotificationTos.notif_id),
    __metadata("design:type", Array)
], Notifications.prototype, "NotificationTos", void 0);
Notifications = __decorate([
    typeorm_1.Entity()
], Notifications);
exports.Notifications = Notifications;
//# sourceMappingURL=notification.entity.js.map