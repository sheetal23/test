export declare class NotificationTos {
    id: number;
    to_id: number;
    notif_id: number;
    is_read: number;
    created_at: Date;
    updated_at: Date;
}
