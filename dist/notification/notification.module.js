"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const user_module_1 = require("../user/user.module");
const typeorm_1 = require("@nestjs/typeorm");
const notification_entity_1 = require("./entities/notification.entity");
const notification_to_entity_1 = require("./entities/notification-to.entity");
const user_entity_1 = require("../user/entities/user.entity");
const event_entity_1 = require("../event/entities/event.entity");
const notification_controller_1 = require("./notification.controller");
const notification_service_1 = require("./notification.service");
const team_entity_1 = require("../team/entities/team.entity");
const user_login_entity_1 = require("../user/entities/user-login.entity");
const post_entity_1 = require("../post/entities/post.entity");
let NotificationModule = class NotificationModule {
};
NotificationModule = __decorate([
    common_1.Module({ imports: [
            typeorm_1.TypeOrmModule.forFeature([notification_entity_1.Notifications]),
            typeorm_1.TypeOrmModule.forFeature([notification_to_entity_1.NotificationTos]),
            typeorm_1.TypeOrmModule.forFeature([user_entity_1.Users]),
            typeorm_1.TypeOrmModule.forFeature([user_login_entity_1.UserLogins]),
            typeorm_1.TypeOrmModule.forFeature([team_entity_1.Teams]),
            typeorm_1.TypeOrmModule.forFeature([event_entity_1.Events]),
            typeorm_1.TypeOrmModule.forFeature([post_entity_1.Posts]),
            user_module_1.UserModule
        ],
        providers: [notification_service_1.NotificationService],
        controllers: [notification_controller_1.NotificationController],
        exports: [
            notification_service_1.NotificationService
        ]
    })
], NotificationModule);
exports.NotificationModule = NotificationModule;
//# sourceMappingURL=notification.module.js.map