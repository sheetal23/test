import { UserService } from '../user/user.service';
import { NotificationService } from './notification.service';
import { NotificationDto } from './dto';
export declare class NotificationController {
    private readonly userService;
    private readonly notificationService;
    constructor(userService: UserService, notificationService: NotificationService);
    findAll(request: any): Promise<{
        data: any[];
        count: number;
    }>;
    deleteNotif(request: any): Promise<{
        message: string;
    }>;
    readNotif(request: any, dto: NotificationDto): Promise<{
        message: string;
    }>;
    findUnread(request: any): Promise<{
        unread_notif_count: number;
    }>;
    dummyPush(dto: NotificationDto): Promise<{
        message: string;
    }>;
}
