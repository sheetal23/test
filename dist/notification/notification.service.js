"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const user_entity_1 = require("../user/entities/user.entity");
const event_entity_1 = require("../event/entities/event.entity");
const notification_entity_1 = require("./entities/notification.entity");
const notification_to_entity_1 = require("./entities/notification-to.entity");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const FCM = require("fcm-push");
const mailer_1 = require("@nest-modules/mailer");
const team_entity_1 = require("../team/entities/team.entity");
const user_login_entity_1 = require("../user/entities/user-login.entity");
const post_entity_1 = require("../post/entities/post.entity");
const post_image_entity_1 = require("../post/entities/post-image.entity");
var serverKey = 'AAAAxst58Dw:APA91bELfF_cgF9hRO1D4VtX0ogWb42XbPklj-Dv1Ei0ywcLgteFGSwJOm317H_T7jO-eR4XFZCsaH8hrNCWLyDZN3rLza691nvjLfqzbUodzZSFKhfpfBFO9DlLC7BquO0Ja63erntt';
var fcm = new FCM(serverKey);
let NotificationService = class NotificationService {
    constructor(notificationsRepository, notificationTosRepository, userRepository, userLoginRepository, eventRepository, teamRepository, postRepository, mailerService) {
        this.notificationsRepository = notificationsRepository;
        this.notificationTosRepository = notificationTosRepository;
        this.userRepository = userRepository;
        this.userLoginRepository = userLoginRepository;
        this.eventRepository = eventRepository;
        this.teamRepository = teamRepository;
        this.postRepository = postRepository;
        this.mailerService = mailerService;
    }
    save(type, from_id, event_id, post_id, subject, message, to_ids, to_id) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log("in notif");
            if (from_id) {
                console.log("from");
                var sender = yield typeorm_2.getRepository(user_entity_1.Users)
                    .createQueryBuilder("users")
                    .select("first_name, last_name, user_name, email")
                    .where("id = :from_id", { from_id: from_id })
                    .getRawOne();
                console.log(sender);
            }
            if (sender) {
                console.log("sender");
                let notification = new notification_entity_1.Notifications();
                notification.type = type;
                notification.user_id = from_id;
                notification.message = message;
                notification.event_id = event_id ? event_id : null;
                notification.post_id = post_id ? post_id : null;
                const savedNotification = yield this.notificationsRepository.save(notification);
                if (savedNotification) {
                    console.log("saved notif");
                    if (to_ids) {
                        const notif_tos = [];
                        for (const i in to_ids) {
                            const nt = new notification_to_entity_1.NotificationTos();
                            nt.notif_id = savedNotification.id;
                            nt.to_id = to_ids[i];
                            notif_tos.push(nt);
                        }
                        yield this.notificationTosRepository.save(notif_tos);
                    }
                    if (to_id) {
                        const nt = new notification_to_entity_1.NotificationTos();
                        nt.notif_id = savedNotification.id;
                        nt.to_id = to_id;
                        yield this.notificationTosRepository.save(nt);
                    }
                }
                if (savedNotification) {
                    const notifications = yield typeorm_2.getRepository(notification_entity_1.Notifications)
                        .createQueryBuilder("notifications")
                        .leftJoin(user_entity_1.Users, "users", "users.id = notifications.user_id")
                        .leftJoin(notification_to_entity_1.NotificationTos, "notification_tos", "notification_tos.notif_id = notifications.id")
                        .select("notifications.*,notification_tos.is_read,notification_tos.to_id, users.first_name, users.last_name, users.user_name, users.email,users.profile_image")
                        .addSelect(" ( CASE "
                        + " WHEN TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, notifications.`created_at`) != 0 THEN"
                        + " IF(abs(TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, notifications.`created_at`)) > 1 , CONCAT(abs(TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, notifications.`created_at`)) ,' years ago'), CONCAT(abs(TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, notifications.`created_at`)) ,' year ago'))"
                        + " WHEN TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, notifications.`created_at`) != 0 THEN"
                        + " IF(abs(TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, notifications.`created_at`)) > 1 , CONCAT(abs(TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, notifications.`created_at`)) ,' months ago'), CONCAT(abs(TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, notifications.`created_at`)) ,' month ago'))"
                        + " WHEN HOUR(TIMEDIFF(UTC_TIMESTAMP, notifications.`created_at`)) != 0 THEN"
                        + " IF(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, notifications.`created_at`))) < 24 , IF(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, notifications.`created_at`))) > 1 , CONCAT(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, notifications.`created_at`))) ,' hrs ago'), CONCAT(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, notifications.`created_at`))) ,' hr ago')),"
                        + " IF(DATEDIFF(UTC_TIMESTAMP, notifications.`created_at`) > 1 , CONCAT(DATEDIFF(UTC_TIMESTAMP, notifications.`created_at`) ,' days ago'), CONCAT(DATEDIFF(UTC_TIMESTAMP, notifications.`created_at`) ,' day ago')))"
                        + " WHEN MINUTE(TIMEDIFF(UTC_TIMESTAMP, notifications.`created_at`)) != 0 THEN CONCAT(MINUTE(TIMEDIFF(UTC_TIMESTAMP, notifications.`created_at`)) ,' min ago')"
                        + " ELSE "
                        + " CONCAT(SECOND(TIMEDIFF(UTC_TIMESTAMP, notifications.`created_at`)) ,'s ago')"
                        + " END  )", "time_since")
                        .where("notifications.id = :notif_id", { notif_id: savedNotification.id })
                        .getRawOne();
                    console.log("notifications");
                    if (event_id) {
                        var event = yield typeorm_2.getRepository(event_entity_1.Events)
                            .createQueryBuilder("events")
                            .leftJoin(user_entity_1.Users, "users", "users.id = events.user_id")
                            .select("events.id AS event_id, events.title ,events.image AS event_image,events.venue,events.privacy_type, users.id AS user_id, users.first_name, users.last_name, users.email")
                            .where("events.id= :event_id", { event_id: event_id })
                            .getRawOne();
                        console.log("event");
                    }
                    if (post_id) {
                        var post = yield typeorm_2.getRepository(post_entity_1.Posts)
                            .createQueryBuilder("posts")
                            .leftJoin(post_image_entity_1.PostImages, "post_images", "post_images.post_id = posts.id")
                            .leftJoin(user_entity_1.Users, "users", "users.id = posts.user_id")
                            .select("posts.id AS post_id, posts.caption,IFNULL((SELECT image FROM post_images WHERE post_id=posts.id LIMIT 1),(SELECT video_thumb FROM post_images WHERE post_id=posts.id LIMIT 1)) AS post_image, users.id AS user_id, users.first_name, users.last_name, users.email")
                            .where("posts.id= :post_id", { post_id: post_id })
                            .getRawOne();
                        console.log("post");
                    }
                    console.log(to_ids);
                    if (to_ids) {
                        if (to_ids.length > 0) {
                            var notif_tos = yield typeorm_2.getRepository(user_entity_1.Users)
                                .createQueryBuilder("users")
                                .leftJoin(user_login_entity_1.UserLogins, "user_logins", "user_logins.user_id= users.id")
                                .select("users.id AS user_id, users.first_name, users.last_name, users.email,user_logins.device_token, user_logins.device_type")
                                .where("users.id IN (:to_ids)", { to_ids: to_ids })
                                .getRawMany();
                            console.log("notif_tos");
                        }
                    }
                    if (to_id) {
                        var notif_to = yield typeorm_2.getRepository(user_entity_1.Users)
                            .createQueryBuilder("users")
                            .leftJoin(user_login_entity_1.UserLogins, "user_logins", "user_logins.user_id= users.id")
                            .select("users.id AS user_id, users.first_name, users.last_name, users.email,user_logins.device_token,user_logins.device_type")
                            .where("users.id =" + to_id)
                            .getRawOne();
                        console.log(33);
                    }
                    let detail = notifications;
                    if (event) {
                        detail.event_title = event.title;
                        var title = event.title;
                    }
                    if (post) {
                        detail.post_caption = post.caption;
                        var title = post.title;
                    }
                    console.log("detail");
                    console.log(detail);
                    var data = detail;
                    console.log("data");
                    console.log(data);
                    const notification = {
                        title: title ? title : null,
                        body: data.message
                    };
                    console.log(34);
                    console.log(notification);
                    var fcm_ids = [];
                    if (notif_tos) {
                        console.log(35);
                        for (const i in notif_tos) {
                            var unread_notif_count = yield typeorm_2.getRepository(notification_to_entity_1.NotificationTos)
                                .createQueryBuilder("notificationTos")
                                .where("to_id = :to_id", { to_id: notif_tos[i].user_id })
                                .andWhere("is_read=0")
                                .getCount();
                            data.unread_count = unread_notif_count;
                            fcm_ids.push(notif_tos[i].device_token);
                            if (notif_tos[i].device_type == 1) {
                                var notif = null;
                            }
                            else {
                                var notif = notification;
                            }
                        }
                        this.push(fcm_ids, data, notif);
                    }
                    if (notif_to) {
                        console.log(36);
                        var unread_notif_count = yield typeorm_2.getRepository(notification_to_entity_1.NotificationTos)
                            .createQueryBuilder("notificationTos")
                            .where("to_id = :to_id", { to_id: notif_to.user_id })
                            .andWhere("is_read=0")
                            .getCount();
                        data.unread_count = unread_notif_count;
                        fcm_ids.push(notif_to.device_token);
                        if (notif_to.device_type == 1) {
                            this.push(fcm_ids, data, null);
                        }
                        else {
                            this.push(fcm_ids, data, notification);
                        }
                    }
                }
            }
        });
    }
    findAll(logged_id, req) {
        return __awaiter(this, void 0, void 0, function* () {
            var skip = 0;
            var limit = req.query.limit ? req.query.limit : 10;
            if (req.query.page) {
                const page = req.query.page;
                var skip = limit * page;
            }
            const qb = yield typeorm_2.getRepository(notification_entity_1.Notifications)
                .createQueryBuilder("notifications")
                .leftJoin(user_entity_1.Users, "users", "users.id = notifications.user_id")
                .leftJoin(notification_to_entity_1.NotificationTos, "notification_tos", "notification_tos.notif_id = notifications.id")
                .leftJoin(event_entity_1.Events, "events", "events.id = notifications.event_id")
                .leftJoin(post_entity_1.Posts, "posts", "posts.id = notifications.post_id")
                .select("notifications.*,notifications.id AS notif_id, notification_tos.is_read,notification_tos.to_id, notification_tos.id,users.first_name, users.last_name, users.user_name,users.profile_image, users.email,events.title,events.image AS event_image,events.deleted_at AS event_deleted_at, events.cancelled_at AS event_cancelled_at, events.session_type, events.privacy_type,posts.id AS post_id,posts.caption,IFNULL((SELECT image FROM post_images WHERE post_id=posts.id LIMIT 1),(SELECT video_thumb FROM post_images WHERE post_id=posts.id LIMIT 1)) AS post_image")
                .addSelect(" ( CASE "
                + " WHEN TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, notifications.`created_at`) != 0 THEN"
                + " IF(abs(TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, notifications.`created_at`)) > 1 , CONCAT(abs(TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, notifications.`created_at`)) ,' years ago'), CONCAT(abs(TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, notifications.`created_at`)) ,' year ago'))"
                + " WHEN TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, notifications.`created_at`) != 0 THEN"
                + " IF(abs(TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, notifications.`created_at`)) > 1 , CONCAT(abs(TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, notifications.`created_at`)) ,' months ago'), CONCAT(abs(TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, notifications.`created_at`)) ,' month ago'))"
                + " WHEN HOUR(TIMEDIFF(UTC_TIMESTAMP, notifications.`created_at`)) != 0 THEN"
                + " IF(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, notifications.`created_at`))) < 24 , IF(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, notifications.`created_at`))) > 1 , CONCAT(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, notifications.`created_at`))) ,' hrs ago'), CONCAT(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, notifications.`created_at`))) ,' hr ago')),"
                + " IF(DATEDIFF(UTC_TIMESTAMP, notifications.`created_at`) > 1 , CONCAT(DATEDIFF(UTC_TIMESTAMP, notifications.`created_at`) ,' days ago'), CONCAT(DATEDIFF(UTC_TIMESTAMP, notifications.`created_at`) ,' day ago')))"
                + " WHEN MINUTE(TIMEDIFF(UTC_TIMESTAMP, notifications.`created_at`)) != 0 THEN CONCAT(MINUTE(TIMEDIFF(UTC_TIMESTAMP, notifications.`created_at`)) ,' min ago')"
                + " ELSE "
                + " CONCAT(SECOND(TIMEDIFF(UTC_TIMESTAMP, notifications.`created_at`)) ,'s ago')"
                + " END  )", "time_since")
                .where("notification_tos.to_id= :user_id", { user_id: logged_id });
            const data = yield qb.offset(skip).limit(limit).orderBy("notification_tos.id", "DESC").getRawMany();
            const count = yield qb.getCount();
            return { data, count };
        });
    }
    deleteNotif(logged_id, req) {
        return __awaiter(this, void 0, void 0, function* () {
            const alert_id = req.query.id;
            if (alert_id) {
                yield typeorm_2.getRepository(notification_to_entity_1.NotificationTos)
                    .createQueryBuilder("notificationTos")
                    .delete()
                    .where("id = :alert_id", { alert_id: alert_id })
                    .execute();
            }
            else {
                yield typeorm_2.getRepository(notification_to_entity_1.NotificationTos)
                    .createQueryBuilder("notificationTos")
                    .delete()
                    .where("to_id = :to_id", { to_id: logged_id })
                    .execute();
            }
            return ({ message: "Notifications deleted" });
        });
    }
    readNotif(logged_id, dto) {
        return __awaiter(this, void 0, void 0, function* () {
            const { notif_id } = dto;
            const qb = yield typeorm_2.getRepository(notification_to_entity_1.NotificationTos)
                .createQueryBuilder("notificationTos")
                .update({ is_read: 1 })
                .where("to_id = :to_id", { to_id: logged_id })
                .execute();
            return ({ message: "Notification read" });
        });
    }
    readNotifEvent(logged_id, user_id, event_id, post_id) {
        return __awaiter(this, void 0, void 0, function* () {
            var notifIds = [];
            const qb = yield typeorm_2.getRepository(notification_entity_1.Notifications)
                .createQueryBuilder("notifications")
                .leftJoin(notification_to_entity_1.NotificationTos, "notification_tos", "notification_tos.notif_id = notifications.id")
                .select("notification_tos.id as id")
                .where("notification_tos.to_id = :logged_id", { logged_id });
            if (event_id) {
                qb.andWhere("notifications.type IN(1,2,7,8,9,10,12,13,15,16,17,19,20,22,26,28)");
                qb.andWhere("notifications.event_id =:event_id", { event_id });
            }
            if (post_id) {
                qb.andWhere("notifications.type IN(23,24,25)");
                qb.andWhere("notifications.post_id =:post_id", { post_id });
            }
            if (user_id) {
                qb.andWhere("notifications.type IN(5,11,27)");
                qb.andWhere("notifications.user_id =:user_id", { user_id });
            }
            var notifs = yield qb.getRawMany();
            for (const i in notifs) {
                notifIds.push(notifs[i].id);
            }
            console.log("notif");
            console.log(notifs);
            console.log(notifIds);
            if (notifIds.length > 0) {
                const qb1 = yield typeorm_2.getRepository(notification_to_entity_1.NotificationTos)
                    .createQueryBuilder("notificationTos")
                    .update({ is_read: 1 })
                    .where("to_id = :to_id", { to_id: logged_id })
                    .andWhere("id IN (:notifIds)", { notifIds })
                    .execute();
            }
        });
    }
    findUnread(logged_id) {
        return __awaiter(this, void 0, void 0, function* () {
            var unread_notif_count = yield typeorm_2.getRepository(notification_to_entity_1.NotificationTos)
                .createQueryBuilder("notificationTos")
                .where("to_id = :to_id", { to_id: logged_id })
                .andWhere("is_read=0")
                .getCount();
            return { unread_notif_count };
        });
    }
    dummyPush(dto) {
        return __awaiter(this, void 0, void 0, function* () {
            const { fcm_id, message, title, device_type, notif_type } = dto;
            var notification = {
                title: title ? title : null,
                body: message
            };
            console.log(notification);
            var data = {
                type: notif_type,
                title: title,
                message: message
            };
            console.log(data);
            if (device_type == 1) {
                this.push(fcm_id, data, null);
            }
            else {
                this.push(fcm_id, data, notification);
            }
            return ({ message: "sent" });
        });
    }
    push(fcm_ids, data, notification, collapse_key = null) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log("push start");
            console.log(data);
            console.log(fcm_ids);
            console.log(notification);
            for (const i in fcm_ids) {
                var message = {
                    to: fcm_ids[i],
                    data: data,
                    notification: notification
                };
                console.log("message");
                console.log(message);
                fcm.send(message)
                    .then(function (res) {
                    console.log("Successfully sent with response: ", res);
                })
                    .catch(function (err) {
                    console.log("Something has gone wrong!");
                    console.error(err);
                });
            }
        });
    }
};
NotificationService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(notification_entity_1.Notifications)),
    __param(1, typeorm_1.InjectRepository(notification_to_entity_1.NotificationTos)),
    __param(2, typeorm_1.InjectRepository(user_entity_1.Users)),
    __param(3, typeorm_1.InjectRepository(user_login_entity_1.UserLogins)),
    __param(4, typeorm_1.InjectRepository(event_entity_1.Events)),
    __param(5, typeorm_1.InjectRepository(team_entity_1.Teams)),
    __param(6, typeorm_1.InjectRepository(post_entity_1.Posts)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        mailer_1.MailerService])
], NotificationService);
exports.NotificationService = NotificationService;
//# sourceMappingURL=notification.service.js.map