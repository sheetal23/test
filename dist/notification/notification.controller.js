"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const user_service_1 = require("../user/user.service");
const notification_service_1 = require("./notification.service");
const dto_1 = require("./dto");
const swagger_1 = require("@nestjs/swagger");
let NotificationController = class NotificationController {
    constructor(userService, notificationService) {
        this.userService = userService;
        this.notificationService = notificationService;
    }
    findAll(request) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            console.log(request.headers.authorization);
            return yield this.notificationService.findAll(jwtData.id, request);
        });
    }
    deleteNotif(request) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            console.log(request.headers.authorization);
            return yield this.notificationService.deleteNotif(jwtData.id, request);
        });
    }
    readNotif(request, dto) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            console.log(request.headers.authorization);
            return yield this.notificationService.readNotif(jwtData.id, dto);
        });
    }
    findUnread(request) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            console.log(request.headers.authorization);
            return yield this.notificationService.findUnread(jwtData.id);
        });
    }
    dummyPush(dto) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.notificationService.dummyPush(dto);
        });
    }
};
__decorate([
    common_1.Get('alerts'),
    common_1.HttpCode(200),
    __param(0, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], NotificationController.prototype, "findAll", null);
__decorate([
    common_1.Delete(),
    common_1.HttpCode(200),
    __param(0, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], NotificationController.prototype, "deleteNotif", null);
__decorate([
    common_1.Put('read'),
    common_1.HttpCode(200),
    __param(0, common_1.Req()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, dto_1.NotificationDto]),
    __metadata("design:returntype", Promise)
], NotificationController.prototype, "readNotif", null);
__decorate([
    common_1.Get('unread'),
    common_1.HttpCode(200),
    __param(0, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], NotificationController.prototype, "findUnread", null);
__decorate([
    common_1.Post(),
    common_1.HttpCode(200),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.NotificationDto]),
    __metadata("design:returntype", Promise)
], NotificationController.prototype, "dummyPush", null);
NotificationController = __decorate([
    swagger_1.ApiBearerAuth(),
    swagger_1.ApiUseTags('notification'),
    common_1.Controller('notification'),
    __metadata("design:paramtypes", [user_service_1.UserService, notification_service_1.NotificationService])
], NotificationController);
exports.NotificationController = NotificationController;
//# sourceMappingURL=notification.controller.js.map