export declare class NotificationDto {
    readonly notif_id: number;
    readonly alert_id: number;
    readonly fcm_id: Array<string>;
    readonly device_type: number;
    readonly message: string;
    readonly title: string;
    readonly notif_type: number;
}
