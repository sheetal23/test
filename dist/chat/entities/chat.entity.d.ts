import { ChatTos } from '../entities/chat-to.entity';
export declare class Chats {
    id: number;
    from_id: number;
    message: string;
    reply_id: number;
    forward_id: number;
    local_identifier: string;
    is_deleted: number;
    message_type: number;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
    ChatTos: ChatTos[];
}
