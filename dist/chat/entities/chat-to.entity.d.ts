export declare class ChatTos {
    id: number;
    to_id: number;
    is_read: number;
    is_deleted: number;
    chat_id: number;
}
