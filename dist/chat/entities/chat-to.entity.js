"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const user_entity_1 = require("../../user/entities/user.entity");
const chat_entity_1 = require("../entities/chat.entity");
let ChatTos = class ChatTos {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], ChatTos.prototype, "id", void 0);
__decorate([
    typeorm_1.ManyToOne(type => user_entity_1.Users, Users => Users.ChatTos, {
        cascade: true,
        onDelete: "CASCADE",
        onUpdate: "CASCADE"
    }),
    typeorm_1.JoinColumn({ name: "to_id" }),
    __metadata("design:type", Number)
], ChatTos.prototype, "to_id", void 0);
__decorate([
    typeorm_1.Column({ default: 0, comment: "0-unread, 1-read" }),
    __metadata("design:type", Number)
], ChatTos.prototype, "is_read", void 0);
__decorate([
    typeorm_1.Column({ default: 0, comment: "0-not delete, 1-deleted" }),
    __metadata("design:type", Number)
], ChatTos.prototype, "is_deleted", void 0);
__decorate([
    typeorm_1.ManyToOne(type => chat_entity_1.Chats, Chats => Chats.ChatTos, {
        cascade: true,
        onDelete: "CASCADE",
        onUpdate: "CASCADE"
    }),
    typeorm_1.JoinColumn({ name: "chat_id" }),
    __metadata("design:type", Number)
], ChatTos.prototype, "chat_id", void 0);
ChatTos = __decorate([
    typeorm_1.Entity()
], ChatTos);
exports.ChatTos = ChatTos;
//# sourceMappingURL=chat-to.entity.js.map