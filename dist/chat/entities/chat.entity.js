"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const user_entity_1 = require("../../user/entities/user.entity");
const chat_to_entity_1 = require("../entities/chat-to.entity");
let Chats = class Chats {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], Chats.prototype, "id", void 0);
__decorate([
    typeorm_1.ManyToOne(type => user_entity_1.Users, Users => Users.Chats, {
        cascade: true,
        onDelete: "CASCADE",
        onUpdate: "CASCADE"
    }),
    typeorm_1.JoinColumn({ name: "from_id" }),
    __metadata("design:type", Number)
], Chats.prototype, "from_id", void 0);
__decorate([
    typeorm_1.Column({ default: null, collation: "utf8mb4_bin" }),
    __metadata("design:type", String)
], Chats.prototype, "message", void 0);
__decorate([
    typeorm_1.Column({ default: null }),
    __metadata("design:type", Number)
], Chats.prototype, "reply_id", void 0);
__decorate([
    typeorm_1.Column({ default: null }),
    __metadata("design:type", Number)
], Chats.prototype, "forward_id", void 0);
__decorate([
    typeorm_1.Column({ default: null }),
    __metadata("design:type", String)
], Chats.prototype, "local_identifier", void 0);
__decorate([
    typeorm_1.Column({ default: 0, comment: "0-not delete, 1-deleted" }),
    __metadata("design:type", Number)
], Chats.prototype, "is_deleted", void 0);
__decorate([
    typeorm_1.Column({ default: null, type: "tinyint", comment: "1-text, 2-video, 3-audio, 4-image" }),
    __metadata("design:type", Number)
], Chats.prototype, "message_type", void 0);
__decorate([
    typeorm_1.Column({ type: 'timestamp', default: () => "CURRENT_TIMESTAMP" }),
    __metadata("design:type", Date)
], Chats.prototype, "created_at", void 0);
__decorate([
    typeorm_1.Column({ type: 'timestamp', default: null }),
    __metadata("design:type", Date)
], Chats.prototype, "updated_at", void 0);
__decorate([
    typeorm_1.Column({ type: 'timestamp', default: null }),
    __metadata("design:type", Date)
], Chats.prototype, "deleted_at", void 0);
__decorate([
    typeorm_1.OneToMany(type => chat_to_entity_1.ChatTos, ChatTos => ChatTos.chat_id),
    __metadata("design:type", Array)
], Chats.prototype, "ChatTos", void 0);
Chats = __decorate([
    typeorm_1.Entity()
], Chats);
exports.Chats = Chats;
//# sourceMappingURL=chat.entity.js.map