"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const websockets_1 = require("@nestjs/websockets");
const typeorm_1 = require("@nestjs/typeorm");
const common_1 = require("@nestjs/common");
const chat_service_1 = require("./chat.service");
const dto_1 = require("./dto");
const chat_entity_1 = require("./entities/chat.entity");
const chat_to_entity_1 = require("./entities/chat-to.entity");
const user_entity_1 = require("../user/entities/user.entity");
const typeorm_2 = require("typeorm");
const user_service_1 = require("../user/user.service");
let ChatGateway = class ChatGateway {
    constructor(chatRepository, userRepository, chatToRepository, userService, chatService) {
        this.chatRepository = chatRepository;
        this.userRepository = userRepository;
        this.chatToRepository = chatToRepository;
        this.userService = userService;
        this.chatService = chatService;
        this.logger = new common_1.Logger('ChatGateway');
        this.wsClients = [];
    }
    afterInit(server) {
        console.log('Initialized!');
        this.logger.log('Initialized!');
    }
    handleDisconnect(client) {
        console.log("disconnected");
        this.logger.log(`Client disconnected: ${client.id}`);
    }
    handleConnection(client) {
        this.wsClients.push(client);
    }
    handleMessage(request, client, dto) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            const logged_id = jwtData.id;
            this.chatService.sendMessage(logged_id, dto);
            console.log("savedChat");
            client.send('msgToServer', dto);
            return ({ message: "Message sent" });
        });
    }
    readMessage(request, client, dto) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            const logged_id = jwtData.id;
            this.chatService.readMessage(logged_id, dto);
            console.log("readChat");
            client.emit('readMessage', dto);
            return ({ message: "Message read" });
        });
    }
    deleteMessage(request, client, dto) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            const logged_id = jwtData.id;
            this.chatService.deleteMessage(logged_id, dto);
            console.log(" delete Chat");
            client.emit('deleteMessage', dto);
            return ({ message: "Message deleted" });
        });
    }
};
__decorate([
    websockets_1.WebSocketServer(),
    __metadata("design:type", Object)
], ChatGateway.prototype, "wss", void 0);
__decorate([
    websockets_1.SubscribeMessage('msgToServer'),
    __param(0, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, dto_1.CreateChatDto]),
    __metadata("design:returntype", Promise)
], ChatGateway.prototype, "handleMessage", null);
__decorate([
    websockets_1.SubscribeMessage('readMessage'),
    __param(0, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, dto_1.CreateChatDto]),
    __metadata("design:returntype", Promise)
], ChatGateway.prototype, "readMessage", null);
__decorate([
    websockets_1.SubscribeMessage('deleteMessage'),
    __param(0, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, dto_1.CreateChatDto]),
    __metadata("design:returntype", Promise)
], ChatGateway.prototype, "deleteMessage", null);
ChatGateway = __decorate([
    websockets_1.WebSocketGateway({ namespace: '/chat' }),
    __param(0, typeorm_1.InjectRepository(chat_entity_1.Chats)),
    __param(1, typeorm_1.InjectRepository(user_entity_1.Users)),
    __param(2, typeorm_1.InjectRepository(chat_to_entity_1.ChatTos)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        user_service_1.UserService,
        chat_service_1.ChatService])
], ChatGateway);
exports.ChatGateway = ChatGateway;
//# sourceMappingURL=chat.gateway.js.map