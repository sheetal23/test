import { ChatService } from './chat.service';
import { UserService } from '../user/user.service';
import { CreateChatDto } from './dto';
export declare class ChatController {
    private readonly userService;
    private readonly chatService;
    constructor(userService: UserService, chatService: ChatService);
    sendMessage(request: any, chatData: CreateChatDto): Promise<void>;
    getMessages(request: any): Promise<any[]>;
    getList(request: any): Promise<{
        chats: any[];
        total: number;
    }>;
}
