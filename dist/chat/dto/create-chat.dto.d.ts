export declare class CreateChatDto {
    readonly id: number;
    readonly message: string;
    readonly local_identifier: string;
    readonly event_id: number;
    readonly message_type: number;
    readonly team_id: number;
    readonly to_id: number;
    readonly reply_id: number;
    readonly forward_id: number;
    readonly delete_type: number;
    readonly to_ids: Array<number>;
}
