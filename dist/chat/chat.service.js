"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const user_entity_1 = require("../user/entities/user.entity");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const mailer_1 = require("@nest-modules/mailer");
const chat_entity_1 = require("./entities/chat.entity");
const chat_to_entity_1 = require("./entities/chat-to.entity");
let ChatService = class ChatService {
    constructor(chatRepository, userRepository, chatToRepository, mailerService) {
        this.chatRepository = chatRepository;
        this.userRepository = userRepository;
        this.chatToRepository = chatToRepository;
        this.mailerService = mailerService;
    }
    sendMessage(logged_id, dto) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id, message, to_id, message_type, event_id, team_id, reply_id, forward_id, local_identifier } = dto;
            let new_chat = new chat_entity_1.Chats();
            new_chat.from_id = logged_id;
            new_chat.message = message;
            new_chat.message_type = message_type;
            new_chat.reply_id = reply_id ? reply_id : null;
            new_chat.forward_id = forward_id ? forward_id : null;
            new_chat.local_identifier = local_identifier ? local_identifier : null;
            const savedChat = yield this.chatRepository.save(new_chat);
            console.log("savedChat.id");
            console.log(1);
            var new_chat_to = new chat_to_entity_1.ChatTos();
            new_chat_to.to_id = to_id;
            new_chat_to.chat_id = savedChat.id;
            console.log("new_chat_to");
            const savedChatTo = yield this.chatToRepository.save(new_chat_to);
            console.log("new_chat");
        });
    }
    readMessage(logged_id, dto) {
        return __awaiter(this, void 0, void 0, function* () {
            const { local_identifier } = dto;
            var msg = yield typeorm_2.getRepository(chat_entity_1.Chats)
                .createQueryBuilder("chats")
                .select("id")
                .where("local_identifier=:local_identifier", { local_identifier: local_identifier })
                .getRawOne();
            yield typeorm_2.getConnection()
                .createQueryBuilder()
                .update(chat_to_entity_1.ChatTos)
                .set({
                is_read: 1
            })
                .where("chat_id = :chat_id", { chat_id: msg.id })
                .andWhere("to_id = :to_id", { to_id: logged_id })
                .execute();
        });
    }
    deleteMessage(logged_id, dto) {
        return __awaiter(this, void 0, void 0, function* () {
            const { local_identifier, delete_type } = dto;
            const current_time = new Date();
            var msg = yield typeorm_2.getRepository(chat_entity_1.Chats)
                .createQueryBuilder("chats")
                .select("*")
                .where("local_identifier=:local_identifier", { local_identifier: local_identifier })
                .getRawOne();
            if (msg.from_id == logged_id) {
                if (delete_type == 1) {
                    yield typeorm_2.getConnection()
                        .createQueryBuilder()
                        .update(chat_entity_1.Chats)
                        .set({
                        is_deleted: 1
                    })
                        .where("id = :id", { chat_id: msg.id })
                        .execute();
                }
                if (delete_type == 2) {
                    yield typeorm_2.getConnection()
                        .createQueryBuilder()
                        .update(chat_entity_1.Chats)
                        .set({
                        deleted_at: current_time
                    })
                        .where("id = :id", { id: msg.id })
                        .execute();
                }
            }
            else {
                yield typeorm_2.getConnection()
                    .createQueryBuilder()
                    .update(chat_to_entity_1.ChatTos)
                    .set({
                    is_deleted: 1
                })
                    .where("chat_id = :chat_id", { chat_id: msg.id })
                    .andWhere("to_id = :to_id", { to_id: logged_id })
                    .execute();
            }
        });
    }
    getMessages(logged_id, req) {
        return __awaiter(this, void 0, void 0, function* () {
            var user_id = req.query.user_id;
            var type = req.query.type;
            var event_id = req.query.event_id;
            var team_id = req.query.team_id;
            var last_chat_id = req.query.last_chat_id;
            var type = req.query.type;
            var skip = 0;
            var limit = req.query.limit ? req.query.limit : 50;
            if (req.query.page) {
                const page = req.query.page;
                var skip = limit * page;
            }
            var qb = yield typeorm_2.getRepository(chat_entity_1.Chats)
                .createQueryBuilder("chats")
                .leftJoin(chat_to_entity_1.ChatTos, "chat_tos", "chat_tos.chat_id=chats.id")
                .select("*,(SELECT profile_image FROM users WHERE id=chats.from_id) as user_profile_picture,(SELECT is_online FROM users WHERE id=chats.from_id) as user_is_online");
            if (type == 1) {
                qb.where('((chats.from_id = :logged_id AND chat_tos.to_id=:user_id) OR (chats.from_id = :user_id AND chat_tos.to_id=:logged_id) )', { logged_id: logged_id, user_id: user_id });
                qb.andWhere("(chats.event_id is null AND chats.team_id is null)");
            }
            const updateRead = yield typeorm_2.getRepository(chat_to_entity_1.ChatTos)
                .createQueryBuilder("ChatTos")
                .update(chat_to_entity_1.ChatTos)
                .set({ is_read: 1 })
                .where('(((SELECT from_id FROM chats WHERE id=chat_tos.chat_id)=:user_id AND chat_tos.to_id=:logged_id) OR ((SELECT event_id FROM chats WHERE id=chat_tos.chat_id)=:event_id OR (SELECT team_id FROM chats WHERE id=chat_tos.chat_id)=:team_id AND chat_tos.to_id=:logged_id) )', { user_id: user_id, logged_id: logged_id, event_id: event_id, team_id: team_id })
                .execute();
            var chat = yield qb.offset(skip).limit(limit).getRawMany();
            return chat;
        });
    }
    getList(logged_id, req) {
        return __awaiter(this, void 0, void 0, function* () {
            var last_chat_id = req.query.last_chat_id;
            var type = req.query.type;
            var skip = 0;
            var limit = req.query.limit ? req.query.limit : 50;
            if (req.query.page) {
                const page = req.query.page;
                var skip = limit * page;
            }
            const userQb = yield typeorm_2.getRepository(chat_to_entity_1.ChatTos)
                .createQueryBuilder("chat_tos")
                .leftJoin(chat_entity_1.Chats, "chats", "chats.id=chat_tos.chat_id")
                .select("count('id')")
                .where("chat_tos.is_read='0' AND chat_tos.to_id=" + logged_id + " AND chats.from_id=user_id");
            const qb = yield typeorm_2.getRepository(chat_entity_1.Chats)
                .createQueryBuilder("chats")
                .leftJoin(chat_to_entity_1.ChatTos, "chat_tos", "chat_tos.chat_id=chats.id")
                .select("*,if(chats.from_id=" + logged_id + ",chat_tos.to_id,chats.from_id) as user_id,"
                + " if(chats.from_id=" + logged_id + ",(SELECT profile_image FROM users WHERE id=chat_tos.to_id),(SELECT profile_image FROM users WHERE id=chats.from_id)) AS user_profile_image,"
                + " if(chats.from_id=" + logged_id + ",(SELECT CONCAT(first_name,last_name) FROM users WHERE id=chat_tos.to_id),(SELECT CONCAT(first_name,last_name) FROM users WHERE id=chats.from_id)) AS user_name,if(chats.from_id=" + logged_id + ",(SELECT is_online FROM users WHERE id=chat_tos.to_id),(SELECT is_online FROM users WHERE id=chats.from_id)) AS user_is_online,IFNULL(event_id,team_id) AS group_id");
            if (userQb) {
                qb.addSelect("(" + userQb.getQuery() + ") AS unread_msgs");
            }
            if (type == 1) {
                qb.where('(chats.from_id = :logged_id OR chat_tos.to_id=:logged_id) AND event_id IS NULL AND team_id IS NULL', { logged_id: logged_id });
                qb.groupBy("greatest(from_id, to_id),least(from_id,to_id)");
            }
            var chats = yield qb.offset(skip).limit(limit).orderBy("chats.id", "DESC").getRawMany();
            var total = yield qb.getCount();
            return { chats, total };
        });
    }
};
ChatService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(chat_entity_1.Chats)),
    __param(1, typeorm_1.InjectRepository(user_entity_1.Users)),
    __param(2, typeorm_1.InjectRepository(chat_to_entity_1.ChatTos)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        mailer_1.MailerService])
], ChatService);
exports.ChatService = ChatService;
//# sourceMappingURL=chat.service.js.map