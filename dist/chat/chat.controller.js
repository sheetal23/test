"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const chat_service_1 = require("./chat.service");
const user_service_1 = require("../user/user.service");
const dto_1 = require("./dto");
const validation_pipe_1 = require("../shared/pipes/validation.pipe");
const swagger_1 = require("@nestjs/swagger");
let ChatController = class ChatController {
    constructor(userService, chatService) {
        this.userService = userService;
        this.chatService = chatService;
    }
    sendMessage(request, chatData) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            console.log(request.headers.authorization);
            return yield this.chatService.sendMessage(jwtData.id, chatData);
        });
    }
    getMessages(request) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            console.log(request.headers.authorization);
            return yield this.chatService.getMessages(jwtData.id, request);
        });
    }
    getList(request) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            console.log(request.headers.authorization);
            return yield this.chatService.getList(jwtData.id, request);
        });
    }
};
__decorate([
    common_1.UsePipes(new validation_pipe_1.ValidationPipe),
    common_1.Post('message'),
    common_1.HttpCode(200),
    __param(0, common_1.Req()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, dto_1.CreateChatDto]),
    __metadata("design:returntype", Promise)
], ChatController.prototype, "sendMessage", null);
__decorate([
    common_1.Get('messages'),
    common_1.HttpCode(200),
    __param(0, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ChatController.prototype, "getMessages", null);
__decorate([
    common_1.Get(),
    common_1.HttpCode(200),
    __param(0, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ChatController.prototype, "getList", null);
ChatController = __decorate([
    swagger_1.ApiBearerAuth(),
    swagger_1.ApiUseTags('chat'),
    common_1.Controller('chat'),
    __metadata("design:paramtypes", [user_service_1.UserService, chat_service_1.ChatService])
], ChatController);
exports.ChatController = ChatController;
//# sourceMappingURL=chat.controller.js.map