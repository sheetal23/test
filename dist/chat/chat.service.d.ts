import { Users } from '../user/entities/user.entity';
import { Repository } from 'typeorm';
import { CreateChatDto } from './dto';
import { MailerService } from '@nest-modules/mailer';
import { Chats } from './entities/chat.entity';
import { ChatTos } from './entities/chat-to.entity';
export declare class ChatService {
    private readonly chatRepository;
    private readonly userRepository;
    private readonly chatToRepository;
    private readonly mailerService;
    constructor(chatRepository: Repository<Chats>, userRepository: Repository<Users>, chatToRepository: Repository<ChatTos>, mailerService: MailerService);
    sendMessage(logged_id: number, dto: CreateChatDto): Promise<void>;
    readMessage(logged_id: number, dto: CreateChatDto): Promise<void>;
    deleteMessage(logged_id: number, dto: CreateChatDto): Promise<void>;
    getMessages(logged_id: number, req: any): Promise<any[]>;
    getList(logged_id: number, req: any): Promise<{
        chats: any[];
        total: number;
    }>;
}
