import { OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect } from '@nestjs/websockets';
import { Socket, Server } from 'socket.io';
import { ChatService } from './chat.service';
import { CreateChatDto } from './dto';
import { Chats } from './entities/chat.entity';
import { ChatTos } from './entities/chat-to.entity';
import { Users } from '../user/entities/user.entity';
import { Repository } from 'typeorm';
import { UserService } from '../user/user.service';
export declare class ChatGateway implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {
    private readonly chatRepository;
    private readonly userRepository;
    private readonly chatToRepository;
    private readonly userService;
    private readonly chatService;
    constructor(chatRepository: Repository<Chats>, userRepository: Repository<Users>, chatToRepository: Repository<ChatTos>, userService: UserService, chatService: ChatService);
    wss: Server;
    private logger;
    wsClients: any[];
    afterInit(server: Server): void;
    handleDisconnect(client: Socket): void;
    handleConnection(client: any): void;
    handleMessage(request: any, client: Socket, dto: CreateChatDto): Promise<{
        message: string;
    }>;
    readMessage(request: any, client: Socket, dto: CreateChatDto): Promise<{
        message: string;
    }>;
    deleteMessage(request: any, client: Socket, dto: CreateChatDto): Promise<{
        message: string;
    }>;
}
