import { Users } from '../user/entities/user.entity';
import { Events } from '../event/entities/event.entity';
import { EventMembers } from '../event/entities/event-member.entity';
import { EventReviews } from '../event/entities/event-review.entity';
import { Repository } from 'typeorm';
import { CreateEventDto, AddEventMemberDto, EventRequestDto } from './dto';
import { MailerService } from '@nest-modules/mailer';
import { EventTeamMembers } from './entities/event-team-member.entity';
import { NotificationService } from '../notification/notification.service';
import { Followers } from '../user/entities/follow.entity';
export declare class EventService {
    private readonly eventRepository;
    private readonly eventMemberRepository;
    private readonly eventReviewRepository;
    private readonly eventTeamMemberRepository;
    private readonly userRepository;
    private readonly followRepository;
    private readonly mailerService;
    private readonly notificationService;
    constructor(eventRepository: Repository<Events>, eventMemberRepository: Repository<EventMembers>, eventReviewRepository: Repository<EventReviews>, eventTeamMemberRepository: Repository<EventTeamMembers>, userRepository: Repository<Users>, followRepository: Repository<Followers>, mailerService: MailerService, notificationService: NotificationService);
    create(logged_id: number, dto: CreateEventDto): Promise<{
        message: string;
    }>;
    addEventMember(id: number, dto: AddEventMemberDto): Promise<{
        message: string;
    }>;
    applyEvent(logged_id: number, dto: AddEventMemberDto): Promise<{
        message: string;
    }>;
    acceptEvent(logged_id: number, req: any): Promise<{
        message: string;
    }>;
    confirmEvent(logged_id: number, dto: AddEventMemberDto): Promise<{
        message: string;
    }>;
    addAdmin(logged_id: number, dto: AddEventMemberDto): Promise<{
        message: string;
    }>;
    leftEvent(logged_id: number, req: any): Promise<{
        message: string;
    }>;
    deleteEvent(logged_id: number, req: any): Promise<{
        message: string;
    }>;
    deleteMember(logged_id: number, req: any): Promise<{
        message: string;
    }>;
    deleteAdmin(logged_id: number, req: any): Promise<{
        message: string;
    }>;
    markFull(logged_id: number, dto: EventRequestDto): Promise<{
        message: string;
    }>;
    declineInvitation(logged_id: number, req: any): Promise<{
        message: string;
    }>;
    eventList(logged_id: number, req: any): Promise<{
        data: any[];
        count: number;
    }>;
    eventDetail(logged_id: number, req: any): Promise<any>;
    closeEvent(): Promise<{
        message: string;
    }>;
    startEvent(): Promise<{
        message: string;
    }>;
    review(logged_id: number, dto: EventRequestDto): Promise<{
        message: string;
    }>;
    findAllReviews(id: any, req: any): Promise<{
        reviews: any[];
        count: number;
    }>;
    eventTeams(logged_id: number, req: any): Promise<{
        team_a: any[];
        team_b: any[];
        extras: any[];
    }>;
    eventTeamDivision(event_id: any): Promise<void>;
}
