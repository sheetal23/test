import { EventService } from './event.service';
import { UserService } from '../user/user.service';
import { CreateEventDto, AddEventMemberDto, EventRequestDto } from './dto';
export declare class EventController {
    private readonly userService;
    private readonly eventService;
    constructor(userService: UserService, eventService: EventService);
    event(request: any, dto: CreateEventDto): Promise<{
        message: string;
    }>;
    addEventMember(request: any, dto: AddEventMemberDto): Promise<{
        message: string;
    }>;
    applyEvent(request: any, dto: AddEventMemberDto): Promise<{
        message: string;
    }>;
    acceptEvent(request: any, dto: AddEventMemberDto): Promise<{
        message: string;
    }>;
    confirmEvent(request: any, dto: AddEventMemberDto): Promise<{
        message: string;
    }>;
    addAdmin(request: any, dto: AddEventMemberDto): Promise<{
        message: string;
    }>;
    deleteMember(request: any): Promise<{
        message: string;
    }>;
    leftEvent(request: any): Promise<{
        message: string;
    }>;
    declineInvitation(request: any): Promise<{
        message: string;
    }>;
    deleteEvent(request: any): Promise<{
        message: string;
    }>;
    deleteAdmin(request: any): Promise<{
        message: string;
    }>;
    markFull(request: any, dto: EventRequestDto): Promise<{
        message: string;
    }>;
    eventList(request: any): Promise<{
        data: any[];
        count: number;
    }>;
    eventDetail(request: any): Promise<any>;
    eventTeams(request: any): Promise<{
        team_a: any[];
        team_b: any[];
        extras: any[];
    }>;
    review(request: any, dto: EventRequestDto): Promise<{
        message: string;
    }>;
    findAllReviews(request: any): Promise<{
        reviews: any[];
        count: number;
    }>;
    closeEvent(): Promise<{
        message: string;
    }>;
    startEvent(): Promise<{
        message: string;
    }>;
}
