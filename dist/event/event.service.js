"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const user_entity_1 = require("../user/entities/user.entity");
const event_entity_1 = require("../event/entities/event.entity");
const event_member_entity_1 = require("../event/entities/event-member.entity");
const event_review_entity_1 = require("../event/entities/event-review.entity");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const http_exception_1 = require("@nestjs/common/exceptions/http.exception");
const common_2 = require("@nestjs/common");
const mailer_1 = require("@nest-modules/mailer");
const event_team_member_entity_1 = require("./entities/event-team-member.entity");
const notification_service_1 = require("../notification/notification.service");
const follow_entity_1 = require("../user/entities/follow.entity");
let EventService = class EventService {
    constructor(eventRepository, eventMemberRepository, eventReviewRepository, eventTeamMemberRepository, userRepository, followRepository, mailerService, notificationService) {
        this.eventRepository = eventRepository;
        this.eventMemberRepository = eventMemberRepository;
        this.eventReviewRepository = eventReviewRepository;
        this.eventTeamMemberRepository = eventTeamMemberRepository;
        this.userRepository = userRepository;
        this.followRepository = followRepository;
        this.mailerService = mailerService;
        this.notificationService = notificationService;
    }
    create(logged_id, dto) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id, title, session_type, image, venue, latitude, longitude, date, from_time, to_time, currency, max_players, min_players, price_type, price, privacy_type, player_ids, description } = dto;
            const eventMembers = [];
            let newEvent = new event_entity_1.Events();
            newEvent.user_id = logged_id;
            newEvent.title = title;
            newEvent.image = image;
            newEvent.session_type = session_type;
            newEvent.venue = venue;
            newEvent.latitude = latitude;
            newEvent.longitude = longitude;
            newEvent.date = date;
            newEvent.from_time = from_time;
            newEvent.to_time = to_time;
            newEvent.max_players = max_players;
            newEvent.min_players = min_players;
            newEvent.privacy_type = privacy_type;
            newEvent.price_type = price_type;
            newEvent.price = price;
            newEvent.currency = currency;
            newEvent.description = description;
            newEvent.status = 1;
            var user = yield this.userRepository.findOne({
                where: {
                    id: logged_id
                }
            });
            if (id) {
                const current_time = new Date();
                const qb = yield this.eventRepository.findOne(id);
                yield typeorm_2.getConnection()
                    .createQueryBuilder()
                    .update(event_entity_1.Events)
                    .set({ title: title ? title : qb.title,
                    session_type: session_type ? session_type : qb.session_type,
                    venue: venue ? venue : qb.venue,
                    image: image ? image : qb.image,
                    latitude: latitude ? latitude : qb.latitude,
                    longitude: longitude ? longitude : qb.longitude,
                    date: date ? date : qb.date,
                    from_time: from_time ? from_time : qb.from_time,
                    to_time: to_time ? to_time : qb.to_time,
                    max_players: max_players ? max_players : qb.max_players,
                    min_players: min_players ? min_players : qb.min_players,
                    privacy_type: privacy_type ? privacy_type : qb.privacy_type,
                    description: description ? description : qb.description,
                    price_type: price_type ? price_type : qb.price_type,
                    currency: currency ? currency : qb.currency,
                    price: price ? price : qb.price,
                    updated_at: current_time,
                    status: 1
                })
                    .where("id = :id", { id: id })
                    .execute();
                var joinMembers = [];
                const qb1 = yield typeorm_2.getRepository(event_member_entity_1.EventMembers)
                    .createQueryBuilder("event_members")
                    .select("member_id")
                    .where("event_id = :event_id", { event_id: id })
                    .andWhere(" ( event_members.member_id is not null)")
                    .andWhere(" (is_join=1 OR is_admin=1)")
                    .andWhere("member_id !=" + logged_id);
                var joinMemberIds = yield qb1.getRawMany();
                var joined_members_count = yield qb1.getCount();
                for (const i in joinMemberIds) {
                    joinMembers.push(joinMemberIds[i].member_id);
                }
                if (joined_members_count < max_players) {
                    var remaining_members = max_players - joined_members_count;
                    if (remaining_members > 0) {
                        var queued_members = [];
                        var joined_queued_members = [];
                        var queuedMemberIds = yield typeorm_2.getRepository(event_member_entity_1.EventMembers)
                            .createQueryBuilder("event_members")
                            .select("id,member_id")
                            .where(" event_id = :event_id", { event_id: id })
                            .andWhere("(is_join!=1 AND is_applied=1)")
                            .orderBy("id", "ASC")
                            .limit(remaining_members)
                            .getRawMany();
                        for (const i in queuedMemberIds) {
                            queued_members.push(queuedMemberIds[i].id);
                            if (queuedMemberIds[i].member_id != null) {
                                joined_queued_members.push(queuedMemberIds[i].member_id);
                            }
                        }
                        if (queued_members.length > 0) {
                            console.log(1);
                            console.log(queued_members);
                            yield typeorm_2.getRepository(event_member_entity_1.EventMembers)
                                .createQueryBuilder("event_members")
                                .update(event_member_entity_1.EventMembers)
                                .set({ is_join: 1,
                                is_applied: 0
                            })
                                .where(" event_id = :event_id", { event_id: id })
                                .andWhere(" id IN (:queued_members)", { queued_members: queued_members })
                                .execute();
                            for (const i in joined_queued_members) {
                                var user_info = yield this.userRepository.findOne({
                                    where: {
                                        id: joined_queued_members[i]
                                    }
                                });
                                this.notificationService.save(16, joined_queued_members[i], id, null, null, user_info.user_name + " " + user_info.last_name + "  Joined event " + title, joinMembers, null);
                            }
                        }
                    }
                }
                if (joinMembers) {
                    console.log(joinMembers);
                    console.log("start");
                    if (price_type == 2 || price_type == 3) {
                        this.notificationService.save(6, logged_id, id, null, null, "your event has been set to payment mode. So kindly do payments as soon as possible", joinMembers, null);
                    }
                    this.notificationService.save(16, logged_id, id, null, null, "Event has been updated ", joinMembers, null);
                }
            }
            else {
                var savedEvent = yield this.eventRepository.save(newEvent);
            }
            if (!id) {
                let em = new event_member_entity_1.EventMembers();
                em.is_admin = 1;
                em.event_id = newEvent ? newEvent.id : id;
                em.member_id = logged_id;
                em.is_join = 1;
                console.log("loop");
                yield this.eventMemberRepository.save(em);
            }
            const oldMembers = [];
            const oldMemberIds = yield typeorm_2.getRepository(event_member_entity_1.EventMembers)
                .createQueryBuilder("event_members")
                .select("member_id")
                .where("event_id = :event_id", { event_id: newEvent.id ? newEvent.id : id })
                .getRawMany();
            for (const i in oldMemberIds) {
                oldMembers.push(oldMemberIds[i].member_id);
            }
            if (oldMembers != []) {
                if (player_ids) {
                    var newMembers = player_ids.filter(function (obj) { return oldMembers.indexOf(obj) == -1; });
                }
                else {
                    newMembers = [];
                }
            }
            else {
                newMembers = player_ids;
            }
            console.log(oldMembers);
            console.log(player_ids);
            if (newMembers.length > 0) {
                console.log(newMembers);
                for (const i in newMembers) {
                    const em = new event_member_entity_1.EventMembers();
                    em.event_id = newEvent.id ? newEvent.id : id;
                    em.member_id = newMembers[i];
                    em.is_invited = 1;
                    console.log("loop");
                    eventMembers.push(em);
                }
                var to_ids = newMembers;
                yield this.eventMemberRepository.save(eventMembers);
            }
            if (privacy_type == 1) {
                var message = user.first_name + " " + user.last_name + " is inviting you for public session " + title;
                var notifMessage = user.first_name + " " + user.last_name + " has been posted a nearby public session " + title;
                var followers = [];
                const followerIds = yield typeorm_2.getRepository(follow_entity_1.Followers)
                    .createQueryBuilder("followers")
                    .select("user_id_1")
                    .where("user_id_2 = :logged_id", { logged_id: logged_id })
                    .getRawMany();
                for (const i in followerIds) {
                    followers.push(followerIds[i].user_id_1);
                }
                console.log("followers");
                console.log(followers);
                console.log("start");
                this.notificationService.save(22, logged_id, newEvent.id, null, null, notifMessage, followers, null);
            }
            if (privacy_type == 2) {
                var message = user.first_name + " " + user.last_name + " is inviting you for private session";
            }
            console.log("start");
            if (to_ids) {
                this.notificationService.save(1, logged_id, savedEvent.id, null, null, message, to_ids, null);
            }
            return ({ message: "Event added" });
        });
    }
    addEventMember(id, dto) {
        return __awaiter(this, void 0, void 0, function* () {
            const { event_id, member_ids } = dto;
            var event = yield this.eventRepository.findOne(event_id);
            const current_time = new Date();
            if (event.from_time < current_time || event.is_notif == 1) {
                throw new http_exception_1.HttpException({ error: 'bad_request', message: 'You cannot add members in this session' }, common_2.HttpStatus.BAD_REQUEST);
            }
            const oldMembers = [];
            const oldMemberIds = yield typeorm_2.getRepository(event_member_entity_1.EventMembers)
                .createQueryBuilder("event_members")
                .select("member_id")
                .where("event_id = :event_id", { event_id: event_id })
                .getRawMany();
            for (const i in oldMemberIds) {
                oldMembers.push(oldMemberIds[i].member_id);
            }
            console.log(oldMembers);
            if (oldMembers != []) {
                if (member_ids) {
                    var newMembers = member_ids.filter(function (obj) { return oldMembers.indexOf(obj) == -1; });
                }
                else {
                    newMembers = [];
                }
            }
            else {
                newMembers = member_ids;
            }
            if (newMembers.length > 0) {
                console.log(newMembers);
                const eventMembers = [];
                for (const i in newMembers) {
                    const em = new event_member_entity_1.EventMembers();
                    em.event_id = event_id;
                    em.member_id = newMembers[i];
                    em.is_invited = 1;
                    console.log("loop");
                    eventMembers.push(em);
                }
                yield this.eventMemberRepository.save(eventMembers);
            }
            const oldAdminIds = yield typeorm_2.getRepository(event_member_entity_1.EventMembers)
                .createQueryBuilder("event_members")
                .select("member_id")
                .where("event_id = :event_id", { event_id: event_id })
                .andWhere("is_admin=1 AND is_join=0 AND is_applied=0 AND is_invited=0")
                .andWhere("member_id IN (" + member_ids + ")")
                .getRawMany();
            console.log(oldAdminIds);
            const oldAdmins = [];
            if (oldAdminIds) {
                for (const i in oldAdminIds) {
                    oldAdmins.push(oldAdminIds[i].member_id);
                }
            }
            if (oldAdmins.length > 0) {
                for (const i in oldAdmins) {
                    var adminInvite = yield typeorm_2.getRepository(event_member_entity_1.EventMembers)
                        .createQueryBuilder("event_members")
                        .update(event_member_entity_1.EventMembers)
                        .set({
                        is_invited: 1
                    })
                        .where("event_id = :event_id", { event_id: event_id })
                        .andWhere("member_id = :member_id", { member_id: oldAdmins[i] })
                        .execute();
                }
            }
            if (newMembers.length > 0) {
                if (event.privacy_type == 1) {
                    var message = "You have been invited for public session";
                }
                if (event.privacy_type == 2) {
                    var message = "You have been invited for private session";
                }
                console.log("start");
                this.notificationService.save(1, id, event_id, null, null, message, newMembers, null);
            }
            return ({ message: "Member added to event" });
        });
    }
    applyEvent(logged_id, dto) {
        return __awaiter(this, void 0, void 0, function* () {
            const { event_id, type, member_name } = dto;
            const current_time = new Date();
            var event = yield typeorm_2.getRepository(event_entity_1.Events)
                .createQueryBuilder("events")
                .select("user_id,from_time,title,is_notif")
                .where("id=:event_id", { event_id: event_id })
                .getRawOne();
            if (event.from_time < current_time || event.is_notif == 1) {
                throw new http_exception_1.HttpException({ error: 'bad_request', message: 'You cannot apply for this session' }, common_2.HttpStatus.BAD_REQUEST);
            }
            var userInfo = yield this.userRepository.findOne({
                where: {
                    id: logged_id
                }
            });
            var max_members_count = yield typeorm_2.getRepository(event_entity_1.Events)
                .createQueryBuilder("events")
                .select("max_players")
                .where("id = :event_id", { event_id: event_id })
                .getRawOne();
            console.log(max_members_count);
            var joined_members_count = yield typeorm_2.getRepository(event_member_entity_1.EventMembers)
                .createQueryBuilder("event_members")
                .where("event_id = :event_id", { event_id: event_id })
                .andWhere("is_join =1")
                .getCount();
            console.log(joined_members_count);
            var remaining_members = max_members_count.max_players - joined_members_count;
            console.log(remaining_members);
            if (type == 1) {
                var user = yield typeorm_2.getRepository(event_member_entity_1.EventMembers)
                    .createQueryBuilder("event_members")
                    .where("event_id = :event_id", { event_id: event_id })
                    .andWhere("member_id = :member_id", { member_id: logged_id })
                    .andWhere("((is_admin=1  AND is_join=0  AND is_applied=0 AND is_invited=0) OR is_admin=1 OR is_applied=1)")
                    .getRawOne();
                if (user) {
                    if (remaining_members > 0) {
                        yield typeorm_2.getRepository(event_member_entity_1.EventMembers)
                            .createQueryBuilder("event_members")
                            .update(event_member_entity_1.EventMembers)
                            .set({ is_join: 1,
                            is_applied: 0 })
                            .where("event_id = :event_id", { event_id: event_id })
                            .andWhere("member_id = :member_id", { member_id: logged_id })
                            .execute();
                    }
                    console.log("start");
                    if (userInfo.id != event.user_id) {
                        this.notificationService.save(8, logged_id, event_id, null, null, userInfo.first_name + " " + userInfo.last_name + " joined the session " + event.title, null, event.user_id);
                    }
                }
                else {
                    const qb = yield typeorm_2.getRepository(event_member_entity_1.EventMembers)
                        .createQueryBuilder("event_members")
                        .insert()
                        .into(event_member_entity_1.EventMembers);
                    if (remaining_members > 0) {
                        qb.values({ event_id: event_id,
                            member_id: logged_id,
                            is_join: 1 });
                        if (userInfo.id != event.user_id) {
                            var notifMessage = userInfo.first_name + " " + userInfo.last_name + " joined the session " + event.title;
                        }
                        var notifType = 8;
                    }
                    else {
                        qb.values({ event_id: event_id,
                            member_id: logged_id, is_applied: 1 });
                        var notifMessage = userInfo.first_name + " " + userInfo.last_name + " applied for session " + event.title;
                        var notifType = 2;
                    }
                    qb.execute();
                    console.log("start");
                    this.notificationService.save(notifType, logged_id, event_id, null, null, notifMessage, null, event.user_id);
                }
            }
            if (type == 2) {
                const qb = yield typeorm_2.getRepository(event_member_entity_1.EventMembers)
                    .createQueryBuilder("event_members")
                    .insert()
                    .into(event_member_entity_1.EventMembers);
                if (remaining_members > 0) {
                    qb.values({ event_id: event_id,
                        added_by: logged_id,
                        member_name: member_name,
                        is_join: 1 });
                }
                else {
                    qb.values({ event_id: event_id,
                        added_by: logged_id,
                        member_name: member_name,
                        is_applied: 1 });
                }
                qb.execute();
                console.log("start");
                this.notificationService.save(2, logged_id, event_id, null, null, userInfo.first_name + " " + userInfo.last_name + " has added a plus one (+1) player to " + event.title, null, event.user_id);
            }
            return ({ message: "Applied successfully" });
        });
    }
    acceptEvent(logged_id, req) {
        return __awaiter(this, void 0, void 0, function* () {
            const event_id = req.body.event_id;
            var event = yield typeorm_2.getRepository(event_entity_1.Events)
                .createQueryBuilder("events")
                .select("user_id")
                .where("id=:event_id", { event_id: event_id })
                .getRawOne();
            var max_members_count = yield typeorm_2.getRepository(event_entity_1.Events)
                .createQueryBuilder("events")
                .select("max_players")
                .where("id = :event_id", { event_id: event_id })
                .getRawOne();
            console.log(max_members_count);
            var joined_members_count = yield typeorm_2.getRepository(event_member_entity_1.EventMembers)
                .createQueryBuilder("event_members")
                .where("event_id = :event_id", { event_id: event_id })
                .andWhere("is_join =1")
                .getCount();
            console.log(joined_members_count);
            var remaining_members = max_members_count.max_players - joined_members_count;
            console.log(remaining_members);
            if (remaining_members > 0) {
                yield typeorm_2.getRepository(event_member_entity_1.EventMembers)
                    .createQueryBuilder("event_members")
                    .update(event_member_entity_1.EventMembers)
                    .set({ is_join: 1,
                    is_invited: 0
                })
                    .where("event_id = :event_id", { event_id: event_id })
                    .andWhere("member_id = :member_id", { member_id: logged_id })
                    .execute();
                var message = "Joined successfully";
                var user = yield this.userRepository.findOne({
                    where: {
                        id: logged_id
                    }
                });
                var notifMessage = user.first_name + " " + user.last_name + " accepted your session invitation";
                var notifType = 8;
                this.notificationService.save(notifType, logged_id, event_id, null, null, notifMessage, null, event.user_id);
            }
            else {
                yield typeorm_2.getRepository(event_member_entity_1.EventMembers)
                    .createQueryBuilder("event_members")
                    .update(event_member_entity_1.EventMembers)
                    .set({ is_invited: 0,
                    is_applied: 1
                })
                    .where("event_id = :event_id", { event_id: event_id })
                    .andWhere("member_id = :member_id", { member_id: logged_id })
                    .execute();
                var message = "Sorry! Session is full. You are in queue";
                var notifMessage = " You have been placed in waiting list for event";
                var notifType = 13;
                this.notificationService.save(notifType, event.user_id, event_id, null, null, notifMessage, null, logged_id);
            }
            console.log(event.user_id);
            console.log("start");
            return ({ message: message });
        });
    }
    confirmEvent(logged_id, dto) {
        return __awaiter(this, void 0, void 0, function* () {
            const { event_id, member_id } = dto;
            yield typeorm_2.getRepository(event_member_entity_1.EventMembers)
                .createQueryBuilder("event_members")
                .update(event_member_entity_1.EventMembers)
                .set({ is_join: 1,
                is_applied: 0
            })
                .where("event_id = :event_id", { event_id: event_id })
                .andWhere("member_id = :member_id", { member_id: member_id })
                .execute();
            return ({ message: "Confirmed" });
        });
    }
    addAdmin(logged_id, dto) {
        return __awaiter(this, void 0, void 0, function* () {
            const { event_id, member_id } = dto;
            var user = yield typeorm_2.getRepository(event_member_entity_1.EventMembers)
                .createQueryBuilder("event_members")
                .where("event_id = :event_id", { event_id: event_id })
                .andWhere("member_id = :member_id", { member_id: member_id })
                .getRawOne();
            if (user) {
                yield typeorm_2.getRepository(event_member_entity_1.EventMembers)
                    .createQueryBuilder("event_members")
                    .update(event_member_entity_1.EventMembers)
                    .set({ is_admin: 1 })
                    .where("event_id = :event_id", { event_id: event_id })
                    .andWhere("member_id = :member_id", { member_id: member_id })
                    .execute();
            }
            else {
                yield typeorm_2.getRepository(event_member_entity_1.EventMembers)
                    .createQueryBuilder("event_members")
                    .insert()
                    .into(event_member_entity_1.EventMembers)
                    .values({ event_id: event_id,
                    member_id: member_id,
                    is_admin: 1 })
                    .execute();
            }
            console.log("start");
            const event = yield this.eventRepository.findOne(event_id);
            if (event) {
                var title = event.title;
                var userInfo = yield typeorm_2.getRepository(user_entity_1.Users)
                    .createQueryBuilder("users")
                    .where("id= :id", { id: logged_id })
                    .getOne();
            }
            this.notificationService.save(7, logged_id, event_id, null, null, userInfo.first_name + " " + userInfo.last_name + " has added you as an organizer of " + title + " session", null, member_id);
            var joinMembers = [];
            const joinMemberIds = yield typeorm_2.getRepository(event_member_entity_1.EventMembers)
                .createQueryBuilder("event_members")
                .select("member_id")
                .where("event_id = :event_id", { event_id: event_id })
                .andWhere("member_id IS NOT NULL AND is_admin=1")
                .andWhere("member_id !=" + logged_id + " AND member_id !=" + member_id)
                .getRawMany();
            for (const i in joinMemberIds) {
                joinMembers.push(joinMemberIds[i].member_id);
            }
            if (joinMembers.length > 0) {
                console.log(joinMembers);
                console.log("start");
                this.notificationService.save(16, logged_id, event_id, null, null, "Another organizer is added to " + title + " session", joinMembers, null);
            }
            return ({ message: "Admin added" });
        });
    }
    leftEvent(logged_id, req) {
        return __awaiter(this, void 0, void 0, function* () {
            const event_id = req.query.id;
            yield typeorm_2.getRepository(event_member_entity_1.EventMembers)
                .createQueryBuilder("event_members")
                .delete()
                .where(" event_id = :event_id", { event_id: event_id })
                .andWhere("member_id = :member_id", { member_id: logged_id })
                .execute();
            console.log("start");
            var event = yield typeorm_2.getRepository(event_entity_1.Events)
                .createQueryBuilder("events")
                .select("user_id,title")
                .where("id=:event_id", { event_id: event_id })
                .getRawOne();
            var user = yield this.userRepository.findOne({
                where: {
                    id: logged_id
                }
            });
            this.notificationService.save(12, logged_id, event_id, null, null, user.first_name + " " + user.last_name + " left the session " + event.title, null, event.user_id);
            var max_members_count = yield typeorm_2.getRepository(event_entity_1.Events)
                .createQueryBuilder("events")
                .select("max_players")
                .where(" id = :event_id", { event_id: event_id })
                .getRawOne();
            console.log(max_members_count);
            var joined_members_count = yield typeorm_2.getRepository(event_member_entity_1.EventMembers)
                .createQueryBuilder("event_members")
                .where(" event_id = :event_id", { event_id: event_id })
                .andWhere(" is_join =1")
                .getCount();
            console.log(joined_members_count);
            var remaining_members = max_members_count.max_players - joined_members_count;
            console.log(remaining_members);
            if (remaining_members > 0) {
                var queued_members = [];
                var queued_member = yield typeorm_2.getRepository(event_member_entity_1.EventMembers)
                    .createQueryBuilder("event_members")
                    .select("id, member_id")
                    .where(" event_id = :event_id", { event_id: event_id })
                    .andWhere("(is_join!=1 AND is_applied=1)")
                    .orderBy("id", "ASC")
                    .limit(1)
                    .getRawOne();
                console.log(queued_member);
                if (queued_member) {
                    yield typeorm_2.getRepository(event_member_entity_1.EventMembers)
                        .createQueryBuilder("event_members")
                        .update(event_member_entity_1.EventMembers)
                        .set({ is_join: 1,
                        is_applied: 0
                    })
                        .where(" event_id = :event_id", { event_id: event_id })
                        .andWhere(" id =:id", { id: queued_member.id })
                        .execute();
                    var joinMembers = [];
                    const joinMemberIds = yield typeorm_2.getRepository(event_member_entity_1.EventMembers)
                        .createQueryBuilder("event_members")
                        .select("member_id")
                        .where("event_id = :event_id", { event_id: event_id })
                        .andWhere("member_id IS NOT NULL AND (is_join=1 OR is_admin=1)")
                        .getRawMany();
                    for (const i in joinMemberIds) {
                        joinMembers.push(joinMemberIds[i].member_id);
                    }
                    if (joinMembers.length > 0) {
                        console.log(joinMembers);
                        console.log("start");
                        if (queued_member.member_id != null) {
                            var userInfo = yield this.userRepository.findOne({
                                where: {
                                    id: queued_member.member_id
                                }
                            });
                            console.log(userInfo);
                            this.notificationService.save(8, queued_member.member_id, event_id, null, null, userInfo.first_name + " " + userInfo.last_name + " join event", joinMembers, null);
                        }
                    }
                }
            }
            return ({ message: "You left event" });
        });
    }
    deleteEvent(logged_id, req) {
        return __awaiter(this, void 0, void 0, function* () {
            const event_id = req.query.id;
            const type = req.query.type;
            const current_time = new Date();
            if (type == 1 || type == 2) {
                const qb = yield typeorm_2.getRepository(event_entity_1.Events)
                    .createQueryBuilder("events")
                    .update(event_entity_1.Events);
                if (type == 1) {
                    qb.set({ deleted_at: current_time });
                }
                if (type == 2) {
                    qb.set({ cancelled_at: current_time,
                        status: 4 });
                    var notifMessage = "event has been cancel";
                    var joinMembers = [];
                    const joinMemberIds = yield typeorm_2.getRepository(event_member_entity_1.EventMembers)
                        .createQueryBuilder("event_members")
                        .select("member_id")
                        .where("event_id = :event_id", { event_id: event_id })
                        .andWhere("member_id IS NOT NULL AND (is_join=1 OR is_admin=1)")
                        .andWhere(" member_id !=" + logged_id)
                        .getRawMany();
                    for (const i in joinMemberIds) {
                        joinMembers.push(joinMemberIds[i].member_id);
                    }
                    console.log(joinMembers);
                    console.log("start");
                    this.notificationService.save(17, logged_id, event_id, null, null, notifMessage, joinMembers, null);
                }
                qb.where("id = :event_id", { event_id: event_id })
                    .execute();
            }
            if (type == 3) {
                const deleteAll = yield typeorm_2.getRepository(event_entity_1.Events)
                    .createQueryBuilder("events")
                    .update(event_entity_1.Events)
                    .set({ is_clear: 1 })
                    .where("user_id = :user_id", { user_id: logged_id })
                    .execute();
            }
            return ({ message: "successful" });
        });
    }
    deleteMember(logged_id, req) {
        return __awaiter(this, void 0, void 0, function* () {
            const event_id = req.query.id;
            const type = req.query.type;
            const add_plus_id = req.query.add_plus_id;
            const member_id = req.query.member_id;
            if (member_id) {
                const user = yield typeorm_2.getRepository(event_member_entity_1.EventMembers)
                    .createQueryBuilder("event_members")
                    .select("*")
                    .where(" event_id = :event_id", { event_id: event_id })
                    .andWhere(" member_id = :member_id", { member_id: member_id })
                    .getRawOne();
                console.log(user);
                if (user) {
                    console.log(user.is_admin);
                    if (user.is_admin == 1 && (user.is_join == 1 || user.is_applied == 1 || user.is_invited == 1)) {
                        const qb = yield typeorm_2.getRepository(event_member_entity_1.EventMembers)
                            .createQueryBuilder("event_members")
                            .update(event_member_entity_1.EventMembers);
                        if (type == 1) {
                            qb.set({ is_join: 0 });
                            var notifMessage = "you have been removed from event";
                            var notifType = 9;
                        }
                        if (type == 2) {
                            qb.set({ is_invited: 0 });
                        }
                        if (type == 3) {
                            qb.set({ is_applied: 0 });
                            var notifMessage = "You have been removed from the waiting list for event_title";
                            var notifType = 20;
                        }
                        qb.where("member_id = :member_id", { member_id: member_id })
                            .execute();
                    }
                    else {
                        yield typeorm_2.getRepository(event_member_entity_1.EventMembers)
                            .createQueryBuilder("event_members")
                            .delete()
                            .from(event_member_entity_1.EventMembers)
                            .where(" event_id = :event_id", { event_id: event_id })
                            .andWhere(" member_id = :member_id", { member_id: member_id })
                            .execute();
                    }
                }
                console.log("start");
                if (notifType) {
                    this.notificationService.save(notifType, logged_id, event_id, null, null, notifMessage, null, member_id);
                }
            }
            if (add_plus_id) {
                yield typeorm_2.getRepository(event_member_entity_1.EventMembers)
                    .createQueryBuilder("event_members")
                    .delete()
                    .from(event_member_entity_1.EventMembers)
                    .where(" event_id = :event_id", { event_id: event_id })
                    .andWhere(" id = :add_plus_id", { add_plus_id: add_plus_id })
                    .execute();
            }
            if (type == 1 || add_plus_id) {
                var max_members_count = yield typeorm_2.getRepository(event_entity_1.Events)
                    .createQueryBuilder("events")
                    .select("max_players")
                    .where(" id = :event_id", { event_id: event_id })
                    .getRawOne();
                console.log(max_members_count);
                var joined_members_count = yield typeorm_2.getRepository(event_member_entity_1.EventMembers)
                    .createQueryBuilder("event_members")
                    .where(" event_id = :event_id", { event_id: event_id })
                    .andWhere(" is_join =1")
                    .getCount();
                console.log(joined_members_count);
                var remaining_members = max_members_count.max_players - joined_members_count;
                console.log(remaining_members);
                if (remaining_members > 0) {
                    var queued_members = [];
                    var queued_member = yield typeorm_2.getRepository(event_member_entity_1.EventMembers)
                        .createQueryBuilder("event_members")
                        .select("id, member_id")
                        .where(" event_id = :event_id", { event_id: event_id })
                        .andWhere("(is_join!=1 AND is_applied=1)")
                        .orderBy("id", "ASC")
                        .limit(1)
                        .getRawOne();
                    console.log(queued_member);
                    if (queued_member) {
                        yield typeorm_2.getRepository(event_member_entity_1.EventMembers)
                            .createQueryBuilder("event_members")
                            .update(event_member_entity_1.EventMembers)
                            .set({ is_join: 1,
                            is_applied: 0
                        })
                            .where(" event_id = :event_id", { event_id: event_id })
                            .andWhere(" id =:id", { id: queued_member.id })
                            .execute();
                        var joinMembers = [];
                        const joinMemberIds = yield typeorm_2.getRepository(event_member_entity_1.EventMembers)
                            .createQueryBuilder("event_members")
                            .select("member_id")
                            .where("event_id = :event_id", { event_id: event_id })
                            .andWhere("member_id IS NOT NULL AND (is_join=1 OR is_admin=1)")
                            .getRawMany();
                        for (const i in joinMemberIds) {
                            joinMembers.push(joinMemberIds[i].member_id);
                        }
                        if (joinMembers.length > 0) {
                            console.log(joinMembers);
                            console.log("start");
                            if (queued_member.member_id != null) {
                                var userInfo = yield this.userRepository.findOne({
                                    where: {
                                        id: queued_member.member_id
                                    }
                                });
                                console.log(userInfo);
                                this.notificationService.save(8, queued_member.member_id, event_id, null, null, userInfo.first_name + " " + userInfo.last_name + " join event", joinMembers, null);
                            }
                        }
                    }
                }
            }
            return ({ message: "Member Deleted" });
        });
    }
    deleteAdmin(logged_id, req) {
        return __awaiter(this, void 0, void 0, function* () {
            const event_id = req.query.event_id;
            const member_id = req.query.admin_id;
            const user = yield typeorm_2.getRepository(event_member_entity_1.EventMembers)
                .createQueryBuilder("event_members")
                .select("*")
                .where(" event_id = :event_id", { event_id: event_id })
                .andWhere(" member_id = :member_id", { member_id: member_id })
                .getRawOne();
            if (user) {
                console.log(user.is_admin);
                if (user.is_admin == 1 && (user.is_join == 1 || user.is_applied == 1 || user.is_invited == 1)) {
                    yield typeorm_2.getRepository(event_member_entity_1.EventMembers)
                        .createQueryBuilder("event_members")
                        .update(event_member_entity_1.EventMembers)
                        .set({ is_admin: 0 })
                        .where("member_id = :member_id", { member_id: member_id })
                        .execute();
                }
                else {
                    yield typeorm_2.getRepository(event_member_entity_1.EventMembers)
                        .createQueryBuilder("event_members")
                        .delete()
                        .from(event_member_entity_1.EventMembers)
                        .where("event_id = :event_id", { event_id: event_id })
                        .andWhere("member_id = :member_id", { member_id: member_id })
                        .execute();
                }
            }
            console.log("start");
            this.notificationService.save(19, logged_id, event_id, null, null, "You have been removed as an organizer of the event", null, member_id);
            return ({ message: "Admin removed" });
        });
    }
    markFull(logged_id, dto) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = dto;
            const current_time = new Date();
            var event_full = yield typeorm_2.getRepository(event_entity_1.Events)
                .createQueryBuilder("events")
                .select("*")
                .where("events.id= :event_id", { event_id: id })
                .getRawOne();
            var user = yield typeorm_2.getRepository(user_entity_1.Users)
                .createQueryBuilder("users")
                .where("id= :id", { id: event_full.user_id })
                .getOne();
            console.log(event_full);
            const qb = yield typeorm_2.getRepository(event_entity_1.Events)
                .createQueryBuilder("events")
                .update(event_entity_1.Events);
            if (event_full.mark_session_full != 1) {
                qb.set({ mark_session_full: 1,
                    updated_at: current_time });
                var message = "session marked as full";
                var notifMessage = user.first_name + " " + user.last_name + " marked " + event_full.title + " session as full ";
            }
            else {
                qb.set({ mark_session_full: 0,
                    updated_at: current_time });
                var message = user.first_name + " " + user.last_name + " has opened up the session " + event_full.title + " for more players.";
                var notifMessage = message;
            }
            qb.where("id= :event_id", { event_id: id })
                .execute();
            var allMembers = [];
            const allMemberIds = yield typeorm_2.getRepository(event_member_entity_1.EventMembers)
                .createQueryBuilder("event_members")
                .select("member_id")
                .where("event_id = :event_id", { event_id: id })
                .andWhere("(member_id IS NOT NULL AND (is_join=1 OR is_admin=1 OR is_applied=1 OR is_invited=1))")
                .andWhere("member_id !=:logged_id", { logged_id })
                .getRawMany();
            for (const i in allMemberIds) {
                allMembers.push(allMemberIds[i].member_id);
            }
            console.log("start");
            if (allMembers) {
                this.notificationService.save(28, logged_id, id, null, null, notifMessage, allMembers, null);
            }
            return ({ message: message });
        });
    }
    declineInvitation(logged_id, req) {
        return __awaiter(this, void 0, void 0, function* () {
            var event_id = req.query.event_id;
            const current_time = new Date();
            var event = yield typeorm_2.getRepository(event_member_entity_1.EventMembers)
                .createQueryBuilder("event_members")
                .select("is_admin, is_invited")
                .where(" event_id = :event_id", { event_id: event_id })
                .andWhere(" member_id = :member_id", { member_id: logged_id })
                .getRawOne();
            console.log(event);
            if (event.is_admin == 0 && event.is_invited == 1) {
                yield typeorm_2.getRepository(event_member_entity_1.EventMembers)
                    .createQueryBuilder("event_members")
                    .delete()
                    .from(event_member_entity_1.EventMembers)
                    .where("event_id= :event_id", { event_id: event_id })
                    .andWhere("member_id= :member_id", { member_id: logged_id })
                    .execute();
            }
            if (event.is_admin == 1 && event.is_invited == 1) {
                yield typeorm_2.getRepository(event_member_entity_1.EventMembers)
                    .createQueryBuilder("event_members")
                    .update(event_member_entity_1.EventMembers)
                    .set({
                    is_invited: 0,
                    updated_at: current_time
                })
                    .where("event_id= :event_id", { event_id: event_id })
                    .andWhere("member_id= :member_id", { member_id: logged_id })
                    .execute();
            }
            console.log("start");
            var user = yield this.userRepository.findOne({
                where: {
                    id: logged_id
                }
            });
            var event1 = yield typeorm_2.getRepository(event_entity_1.Events)
                .createQueryBuilder("events")
                .select("user_id")
                .where("id=:event_id", { event_id: event_id })
                .getRawOne();
            console.log(event1);
            this.notificationService.save(10, logged_id, event_id, null, null, user.first_name + " " + user.last_name + " declined your invitation", null, event1.user_id);
            return ({ message: "declined" });
        });
    }
    eventList(logged_id, req) {
        return __awaiter(this, void 0, void 0, function* () {
            var keyword = req.query.keyword;
            var type = req.query.type;
            var latitude = req.query.latitude;
            var longitude = req.query.longitude;
            var time_zone = req.query.time_zone;
            var skip = 0;
            var limit = req.query.limit ? req.query.limit : 10;
            if (req.query.page) {
                const page = req.query.page;
                var skip = limit * page;
            }
            const user = yield this.userRepository.findOne(logged_id);
            const qb = yield typeorm_2.getRepository(event_entity_1.Events)
                .createQueryBuilder("events")
                .select("events.*,CONVERT_TZ(`events`.`date`,'+00:00', '" + time_zone + "') AS date,CONVERT_TZ(`events`.`from_time`,'+00:00', '" + time_zone + "') AS from_time , CONVERT_TZ(`events`.`to_time`,'+00:00', '" + time_zone + "') AS to_time  ,(SELECT COUNT(id) FROM `event_members` where event_id=events.id AND is_join=1) AS joined_members_count, (SELECT is_join FROM `event_members` WHERE event_id=events.id AND member_id=" + logged_id + ") AS membershipConfirmed, IF(UTC_TIMESTAMP() BETWEEN events.from_time AND events.to_time, 1, 0) as is_ongoing ,IF(UTC_TIMESTAMP()>events.to_time, 1, 0) as is_completed, IF(UTC_TIMESTAMP() < events.from_time, 1, 0) as is_pending, (SELECT ROUND(AVG(rating),1) FROM `event_reviews` where event_id = events.id AND rating IS NOT NULL) AS rating, (SELECT COUNT(id) FROM `event_reviews` where event_id=events.id AND user_id=" + logged_id + ") AS is_rated")
                .addSelect("IFNULL(ROUND(( 6371 * acos (cos ( radians( " + latitude + " ) ) * cos( radians( events.latitude) )"
                + " * cos( radians( events.longitude ) - radians( " + longitude + " ) ) + sin ( radians( " + latitude + " ) )"
                + " * sin( radians( events.latitude ) ) ) ), 2), 0)", "distance")
                .where("events.deleted_at IS NULL");
            if (type) {
                if (type == 4) {
                    qb.andWhere("events.user_id= :user_id", { user_id: logged_id });
                    qb.andWhere("events.is_clear IS NULL");
                }
                else {
                    qb.andWhere("events.cancelled_at is NULL");
                }
                if (type == 2) {
                    qb.leftJoin(event_member_entity_1.EventMembers, "event_members", "event_members.event_id = events.id");
                    qb.andWhere(" (SELECT is_join FROM `event_members` WHERE event_id=events.id AND member_id=" + logged_id + ") = 1");
                    qb.andWhere(" ((IF(UTC_TIMESTAMP() BETWEEN events.from_time AND events.to_time, 1, 0) =1) OR (IF(UTC_TIMESTAMP() < events.from_time, 1, 0) =1))");
                    qb.groupBy("events.id");
                    qb.addOrderBy("events.created_at", "DESC");
                }
                if (type == 1) {
                    if (!latitude || !longitude) {
                        throw new http_exception_1.HttpException({ error: 'bad_request', message: 'Please add lat and lng for this location.' }, common_2.HttpStatus.BAD_REQUEST);
                    }
                    qb.leftJoin(event_member_entity_1.EventMembers, "event_members", "event_members.event_id = events.id");
                    qb.andWhere("(events.is_notif=0 AND (SELECT Count(id) FROM event_members where event_members.event_id = events.id AND event_members.member_id = " + logged_id + " AND event_members.is_join = 1) = 0)");
                    qb.andWhere(" IF(UTC_TIMESTAMP() < events.from_time, 1, 0) =1");
                    qb.andWhere("IFNULL(ROUND(( 6371 * acos (cos ( radians( " + latitude + " ) ) * cos( radians( events.latitude) )"
                        + " * cos( radians( events.longitude ) - radians( " + longitude + " ) ) + sin ( radians( " + latitude + " ) )"
                        + " * sin( radians( events.latitude ) ) ) ), 2), 0)<=" + user.session_range);
                    qb.groupBy("events.id");
                    qb.addOrderBy("distance", "ASC");
                }
                if (type == 3) {
                    qb.andWhere(" IF(UTC_TIMESTAMP() > events.to_time, 1, 0) =1");
                    qb.andWhere(" (SELECT is_join FROM `event_members` WHERE event_id=events.id AND member_id=" + logged_id + ") = 1");
                    qb.addOrderBy("events.created_at", "DESC");
                }
            }
            if (keyword) {
                qb.andWhere('(events.title like :keyword)', { keyword: '%' + keyword + '%' });
            }
            const data = yield qb.offset(skip).limit(limit).getRawMany();
            for (const i in data) {
                if (data[i].status == 4) {
                    data[i].status = 4;
                }
                else if (data[i].is_completed == 1) {
                    data[i].status = 3;
                }
                else if (data[i].is_ongoing == 1) {
                    data[i].status = 2;
                }
                else if (data[i].is_pending == 1) {
                    data[i].status = 1;
                }
            }
            const count = yield qb.getCount();
            return { data, count };
        });
    }
    eventDetail(logged_id, req) {
        return __awaiter(this, void 0, void 0, function* () {
            var keyword = req.query.keyword;
            var id = req.query.id;
            var latitude = req.query.latitude;
            var longitude = req.query.longitude;
            var time_zone = req.query.time_zone;
            this.notificationService.readNotifEvent(logged_id, null, id, null);
            const data = yield typeorm_2.getRepository(event_entity_1.Events)
                .createQueryBuilder("events")
                .select("events.*,CONVERT_TZ(`events`.`date`,'+00:00', '" + time_zone + "') AS date,CONVERT_TZ(`events`.`from_time`,'+00:00', '" + time_zone + "') AS from_time , CONVERT_TZ(`events`.`to_time`,'+00:00', '" + time_zone + "') AS to_time  ,(SELECT COUNT(id) FROM `event_members` where event_id=events.id AND is_join=1) AS joined_members_count,(SELECT COUNT(id) FROM `event_members` where event_id=events.id AND is_join=0 AND is_applied=1) AS queued_members_count,(SELECT COUNT(id) FROM `event_members` where event_id=events.id AND is_admin=1) AS admins_count,(SELECT COUNT(id) FROM `event_members` where event_id=events.id AND is_join=0 AND is_invited=1) AS pending_members_count, (SELECT is_join FROM `event_members` WHERE event_id=events.id AND member_id=" + logged_id + " LIMIT 1) AS membershipConfirmed, IF(UTC_TIMESTAMP() BETWEEN events.from_time AND events.to_time, 1, 0) as is_ongoing ,IF(UTC_TIMESTAMP() > events.to_time, 1, 0) as is_completed, IF(UTC_TIMESTAMP() < events.from_time, 1, 0) as is_pending,(SELECT COUNT(id) FROM `event_reviews` where event_id=events.id AND user_id=" + logged_id + ") AS is_rated,(SELECT is_join FROM `event_members` where event_id=events.id AND member_id=" + logged_id + ") AS is_join,(SELECT is_admin FROM `event_members` where event_id=events.id AND member_id=" + logged_id + " LIMIT 1) AS is_admin,(SELECT ROUND(AVG(rating),1) FROM `event_reviews` where event_id=events.id AND rating IS NOT NULL) AS rating,(SELECT count(id) FROM `event_reviews` where event_id=events.id AND rating IS NOT NULL) AS total_reviews")
                .addSelect("IFNULL(ROUND(( 6371 * acos (cos ( radians( " + latitude + " ) ) * cos( radians( events.latitude) )"
                + " * cos( radians( events.longitude ) - radians( " + longitude + " ) ) + sin ( radians( " + latitude + " ) )"
                + " * sin( radians( events.latitude ) ) ) ), 2), 0)", "distance")
                .where("events.id= :event_id", { event_id: id })
                .getRawOne();
            var confirmed_members = yield typeorm_2.getRepository(event_member_entity_1.EventMembers)
                .createQueryBuilder("event_members")
                .leftJoin(user_entity_1.Users, "users", "users.id = event_members.member_id")
                .select("users.id,users.profile_image,users.user_name,users.first_name,users.last_name,event_members.id AS event_member_id,event_members.member_name, event_members.added_by")
                .where("event_members.event_id= :event_id", { event_id: id })
                .andWhere("event_members.is_join= 1")
                .getRawMany();
            var queue_members = yield typeorm_2.getRepository(event_member_entity_1.EventMembers)
                .createQueryBuilder("event_members")
                .leftJoin(user_entity_1.Users, "users", "users.id = event_members.member_id")
                .select("users.id,users.profile_image,users.user_name,users.first_name,users.last_name,  event_members.id AS event_member_id,event_members.member_name, event_members.added_by")
                .where("event_members.event_id= :event_id", { event_id: id })
                .andWhere("event_members.is_join= 0")
                .andWhere("event_members.is_applied= 1")
                .orderBy("event_members.id", "DESC")
                .getRawMany();
            var admins = yield typeorm_2.getRepository(event_member_entity_1.EventMembers)
                .createQueryBuilder("event_members")
                .leftJoin(user_entity_1.Users, "users", "users.id = event_members.member_id")
                .select("users.id,users.profile_image,users.user_name,users.first_name,users.last_name,  event_members.id AS event_member_id,event_members.member_name, event_members.added_by")
                .where("event_members.event_id= :event_id", { event_id: id })
                .andWhere("event_members.is_admin= 1")
                .getRawMany();
            var pending_members = yield typeorm_2.getRepository(event_member_entity_1.EventMembers)
                .createQueryBuilder("event_members")
                .leftJoin(user_entity_1.Users, "users", "users.id = event_members.member_id")
                .select("users.id,users.profile_image,users.user_name,users.first_name,users.last_name,  event_members.id AS event_member_id,event_members.member_name, event_members.added_by")
                .where("event_members.event_id= :event_id", { event_id: id })
                .andWhere("event_members.is_invited= 1 AND event_members.is_join= 0")
                .getRawMany();
            var session_reviews = yield typeorm_2.getRepository(event_review_entity_1.EventReviews)
                .createQueryBuilder("event_reviews")
                .leftJoin(user_entity_1.Users, "users", "users.id =event_reviews.user_id")
                .select("users.id,users.profile_image,users.first_name, users.last_name,users.user_name,(SELECT rating FROM `event_reviews` where rating IS NOT NULL AND user_id=users.id AND event_id=" + id + ") AS rating,(SELECT review FROM `event_reviews` where review IS NOT NULL AND user_id=users.id AND event_id=" + id + ") AS review")
                .addSelect(" ( CASE "
                + " WHEN TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, event_reviews.`created_at`) != 0 THEN"
                + " IF(abs(TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, event_reviews.`created_at`)) > 1 , CONCAT(abs(TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, event_reviews.`created_at`)) ,' years ago'), CONCAT(abs(TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, event_reviews.`created_at`)) ,' year ago'))"
                + " WHEN TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, event_reviews.`created_at`) != 0 THEN"
                + " IF(abs(TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, event_reviews.`created_at`)) > 1 , CONCAT(abs(TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, event_reviews.`created_at`)) ,' months ago'), CONCAT(abs(TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, event_reviews.`created_at`)) ,' month ago'))"
                + " WHEN HOUR(TIMEDIFF(UTC_TIMESTAMP, event_reviews.`created_at`)) != 0 THEN"
                + " IF(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, event_reviews.`created_at`))) < 24 , IF(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, event_reviews.`created_at`))) > 1 , CONCAT(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, event_reviews.`created_at`))) ,' hrs ago'), CONCAT(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, event_reviews.`created_at`))) ,' hr ago')),"
                + " IF(DATEDIFF(UTC_TIMESTAMP, event_reviews.`created_at`) > 1 , CONCAT(DATEDIFF(UTC_TIMESTAMP, event_reviews.`created_at`) ,' days ago'), CONCAT(DATEDIFF(UTC_TIMESTAMP, event_reviews.`created_at`) ,' day ago')))"
                + " WHEN MINUTE(TIMEDIFF(UTC_TIMESTAMP, event_reviews.`created_at`)) != 0 THEN CONCAT(MINUTE(TIMEDIFF(UTC_TIMESTAMP, event_reviews.`created_at`)) ,' min ago')"
                + " ELSE "
                + " CONCAT(SECOND(TIMEDIFF(UTC_TIMESTAMP, event_reviews.`created_at`)) ,'s ago')"
                + " END  )", "reviewed_since")
                .where("event_reviews.event_id= :event_id", { event_id: id })
                .orderBy("event_reviews.id", "DESC")
                .limit(3)
                .getRawMany();
            let detail = data;
            if (detail.status == 4) {
                detail.status = 4;
            }
            else if (detail.is_completed == 1) {
                detail.status = 3;
            }
            else if (detail.is_ongoing == 1) {
                detail.status = 2;
            }
            else if (detail.is_pending == 1) {
                detail.status = 1;
            }
            detail.confirmed_members = confirmed_members ? confirmed_members : null;
            detail.queue_members = queue_members ? queue_members : null;
            detail.admins = admins ? admins : null;
            detail.pending_members = pending_members ? pending_members : null;
            detail.session_reviews = session_reviews ? session_reviews : null;
            return detail;
        });
    }
    closeEvent() {
        return __awaiter(this, void 0, void 0, function* () {
            var eventIds = yield typeorm_2.getRepository(event_entity_1.Events)
                .createQueryBuilder("events")
                .select("id")
                .where("(UTC_TIMESTAMP() >= events.to_time)")
                .andWhere("status!=3 AND cancelled_at IS NULL AND deleted_at IS NULL")
                .getRawMany();
            console.log(eventIds);
            var event_ids = [];
            for (const i in eventIds) {
                event_ids.push(eventIds[i].id);
            }
            console.log(event_ids);
            if (event_ids.length > 0) {
                const qb = yield typeorm_2.getRepository(event_entity_1.Events)
                    .createQueryBuilder("events")
                    .update(event_entity_1.Events)
                    .set({
                    status: 3
                })
                    .where("events.id IN(:event_ids)", { event_ids: event_ids })
                    .execute();
                for (const i in event_ids) {
                    var joinMembers = [];
                    const joinMemberIds = yield typeorm_2.getRepository(event_member_entity_1.EventMembers)
                        .createQueryBuilder("event_members")
                        .select("member_id")
                        .where("event_id = :event_id", { event_id: event_ids[i] })
                        .andWhere("is_join=1 AND member_id IS NOT NULL")
                        .getRawMany();
                    for (const i in joinMemberIds) {
                        joinMembers.push(joinMemberIds[i].member_id);
                    }
                    console.log(joinMembers);
                    console.log("start");
                    var event = yield typeorm_2.getRepository(event_entity_1.Events)
                        .createQueryBuilder("events")
                        .select("user_id,title")
                        .where("id=:id", { id: event_ids[i] })
                        .getRawOne();
                    console.log(event);
                    if (event) {
                        var title = event.title;
                    }
                    console.log(title);
                    var notifMessage = title + " is completed. Click to rate the session and other players.";
                    console.log(notifMessage);
                    if (joinMembers.length > 0) {
                        this.notificationService.save(15, event.user_id, event_ids[i], null, null, notifMessage, joinMembers, null);
                    }
                }
            }
            return ({ message: "successful" });
        });
    }
    startEvent() {
        return __awaiter(this, void 0, void 0, function* () {
            var eventIds = yield typeorm_2.getRepository(event_entity_1.Events)
                .createQueryBuilder("events")
                .select("id")
                .where("TIMESTAMPDIFF(MINUTE,UTC_TIMESTAMP(),events.from_time)<=60")
                .andWhere("is_notif=0 AND cancelled_at IS NULL AND deleted_at IS NULL")
                .getRawMany();
            console.log(eventIds);
            var event_ids = [];
            for (const i in eventIds) {
                event_ids.push(eventIds[i].id);
            }
            console.log(event_ids);
            if (event_ids.length > 0) {
                const qb = yield typeorm_2.getRepository(event_entity_1.Events)
                    .createQueryBuilder("events")
                    .update(event_entity_1.Events)
                    .set({
                    is_notif: 1
                })
                    .where("events.id IN(:event_ids)", { event_ids: event_ids })
                    .execute();
                for (const i in event_ids) {
                    this.eventTeamDivision(event_ids[i]);
                    var joinMembers = [];
                    const joinMemberIds = yield typeorm_2.getRepository(event_member_entity_1.EventMembers)
                        .createQueryBuilder("event_members")
                        .select("member_id")
                        .where("event_id = :event_id", { event_id: event_ids[i] })
                        .andWhere("((is_join=1 OR is_admin=1) AND member_id IS NOT NULL)")
                        .getRawMany();
                    for (const i in joinMemberIds) {
                        joinMembers.push(joinMemberIds[i].member_id);
                    }
                    console.log(joinMembers);
                    console.log("start");
                    var event = yield typeorm_2.getRepository(event_entity_1.Events)
                        .createQueryBuilder("events")
                        .select("user_id,title")
                        .where("id=:id", { id: event_ids[i] })
                        .getRawOne();
                    console.log(event);
                    if (event) {
                        var title = event.title;
                    }
                    console.log(title);
                    var notifMessage = title + " is starting very soon. We have attempted to make a fair team. Check it out.";
                    console.log(notifMessage);
                    if (joinMembers.length > 0) {
                        this.notificationService.save(21, event.user_id, event_ids[i], null, null, notifMessage, joinMembers, null);
                    }
                }
            }
            return ({ message: "successful" });
        });
    }
    review(logged_id, dto) {
        return __awaiter(this, void 0, void 0, function* () {
            const { event_id, review, rating } = dto;
            const review_already = yield this.eventReviewRepository.findOne({
                where: {
                    user_id: logged_id,
                    event_id: event_id
                }
            });
            if (review_already) {
                return { message: "You have already reviewed" };
            }
            else {
                let new_review = new event_review_entity_1.EventReviews();
                new_review.user_id = logged_id;
                new_review.event_id = event_id;
                new_review.review = review;
                new_review.rating = rating;
                const savedReview = yield this.eventReviewRepository.save(new_review);
                console.log("start");
                var user = yield this.userRepository.findOne({
                    where: {
                        id: logged_id
                    }
                });
                var event = yield typeorm_2.getRepository(event_entity_1.Events)
                    .createQueryBuilder("events")
                    .select("user_id")
                    .where("id=:event_id", { event_id: event_id })
                    .getRawOne();
                console.log(event);
                if (logged_id != event.user_id) {
                    this.notificationService.save(26, logged_id, event_id, null, null, user.first_name + " " + user.last_name + "  left a review on the session " + event.title, null, event.user_id);
                }
                return { message: "Review successfully given" };
            }
        });
    }
    findAllReviews(id, req) {
        return __awaiter(this, void 0, void 0, function* () {
            var event_id = req.query.id;
            var skip = 0;
            var limit = req.query.limit ? req.query.limit : 10;
            if (req.query.page) {
                const page = req.query.page;
                var skip = limit * page;
            }
            const qb = yield typeorm_2.getRepository(event_review_entity_1.EventReviews)
                .createQueryBuilder("event_reviews")
                .leftJoinAndSelect(user_entity_1.Users, "users", "users.id = event_reviews.user_id")
                .select(["event_reviews.*", "users.first_name as first_name", "users.last_name as last_name", "users.user_name as user_name", "users.profile_image AS profile_image"])
                .addSelect(" ( CASE "
                + " WHEN TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, event_reviews.`created_at`) != 0 THEN"
                + " IF(abs(TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, event_reviews.`created_at`)) > 1 , CONCAT(abs(TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, event_reviews.`created_at`)) ,' years ago'), CONCAT(abs(TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, event_reviews.`created_at`)) ,' year ago'))"
                + " WHEN TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, event_reviews.`created_at`) != 0 THEN"
                + " IF(abs(TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, event_reviews.`created_at`)) > 1 , CONCAT(abs(TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, event_reviews.`created_at`)) ,' months ago'), CONCAT(abs(TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, event_reviews.`created_at`)) ,' month ago'))"
                + " WHEN HOUR(TIMEDIFF(UTC_TIMESTAMP, event_reviews.`created_at`)) != 0 THEN"
                + " IF(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, event_reviews.`created_at`))) < 24 , IF(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, event_reviews.`created_at`))) > 1 , CONCAT(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, event_reviews.`created_at`))) ,' hrs ago'), CONCAT(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, event_reviews.`created_at`))) ,' hr ago')),"
                + " IF(DATEDIFF(UTC_TIMESTAMP, event_reviews.`created_at`) > 1 , CONCAT(DATEDIFF(UTC_TIMESTAMP, event_reviews.`created_at`) ,' days ago'), CONCAT(DATEDIFF(UTC_TIMESTAMP, event_reviews.`created_at`) ,' day ago')))"
                + " WHEN MINUTE(TIMEDIFF(UTC_TIMESTAMP, event_reviews.`created_at`)) != 0 THEN CONCAT(MINUTE(TIMEDIFF(UTC_TIMESTAMP, event_reviews.`created_at`)) ,' min ago')"
                + " ELSE "
                + " CONCAT(SECOND(TIMEDIFF(UTC_TIMESTAMP, event_reviews.`created_at`)) ,'s ago')"
                + " END  )", "reviewed_since")
                .where("event_reviews.event_id = :event_id", { event_id: event_id });
            const reviews = yield qb.offset(skip).limit(limit)
                .orderBy("event_reviews.id", "DESC")
                .getRawMany();
            const count = yield qb.getCount();
            return { reviews: reviews, count: count };
        });
    }
    eventTeams(logged_id, req) {
        return __awaiter(this, void 0, void 0, function* () {
            var event_id = req.query.id;
            this.notificationService.readNotifEvent(logged_id, null, event_id, null);
            const member_ids = yield typeorm_2.getRepository(event_team_member_entity_1.EventTeamMembers)
                .createQueryBuilder("event_team_members")
                .leftJoin(user_entity_1.Users, "users", "users.id=event_team_members.member_id")
                .select("event_team_members.member_id,event_team_members.team,users.first_name, users.last_name, users.user_name,users.position,users.profile_image,ROUND(IFNULL((((SELECT ROUND(AVG(passing_ability),1) FROM `user_ratings` where user_id_2=users.id AND passing_ability IS NOT NULL) + (SELECT ROUND(AVG(shooting_ability),1) FROM `user_ratings` where user_id_2=users.id AND shooting_ability IS NOT NULL) +( SELECT ROUND(AVG(dribbling_ability),1) FROM `user_ratings` where user_id_2=users.id AND dribbling_ability IS NOT NULL)+ ( SELECT ROUND(AVG(steals),1) FROM `user_ratings` where user_id_2=users.id AND steals IS NOT NULL)+( SELECT ROUND(AVG(rebounds),1) FROM `user_ratings` where user_id_2=users.id AND rebounds IS NOT NULL)+( SELECT ROUND(AVG(blocks),1) FROM `user_ratings` where user_id_2=users.id AND blocks IS NOT NULL))/3)*10,0),1) AS pgr ")
                .where("event_team_members.event_id= :event_id", { event_id: event_id })
                .getRawMany();
            var team_a = [];
            var team_b = [];
            var extras = [];
            console.log(member_ids);
            for (const i in member_ids) {
                if (member_ids[i].team == 1) {
                    team_a.push(member_ids[i]);
                }
                if (member_ids[i].team == 2) {
                    team_b.push(member_ids[i]);
                }
                if (member_ids[i].team == 3) {
                    extras.push(member_ids[i]);
                }
            }
            return { team_a, team_b, extras };
        });
    }
    eventTeamDivision(event_id) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log("team division");
            var team_a = [];
            var team_b = [];
            const member_ids = yield typeorm_2.getRepository(event_member_entity_1.EventMembers)
                .createQueryBuilder("event_members")
                .select("member_id")
                .where("event_members.is_join=1")
                .andWhere("event_members.event_id= :event_id AND member_id IS NOT NULL", { event_id: event_id });
            const add_plus_ids = yield typeorm_2.getRepository(event_member_entity_1.EventMembers)
                .createQueryBuilder("event_members")
                .select("*")
                .where("event_members.is_join=1")
                .andWhere("event_members.event_id= :event_id AND member_id IS NULL", { event_id: event_id })
                .execute();
            const query = yield typeorm_2.getRepository(user_entity_1.Users)
                .createQueryBuilder("users")
                .select("users.id,users.first_name, users.last_name, users.user_name,(((SELECT ROUND(AVG(passing_ability),1) FROM `user_ratings` where user_id_2=users.id AND passing_ability IS NOT NULL) + (SELECT ROUND(AVG(shooting_ability),1) FROM `user_ratings` where user_id_2=users.id AND shooting_ability IS NOT NULL) +( SELECT ROUND(AVG(dribbling_ability),1) FROM `user_ratings` where user_id_2=users.id AND dribbling_ability IS NOT NULL)+ ( SELECT ROUND(AVG(steals),1) FROM `user_ratings` where user_id_2=users.id AND steals IS NOT NULL)+( SELECT ROUND(AVG(rebounds),1) FROM `user_ratings` where user_id_2=users.id AND rebounds IS NOT NULL)+( SELECT ROUND(AVG(blocks),1) FROM `user_ratings` where user_id_2=users.id AND blocks IS NOT NULL))/3)*10 AS pgr").where("users.id IN (" + member_ids.getQuery() + ")")
                .setParameters(member_ids.getParameters())
                .orderBy("pgr", "DESC");
            const total_mems = yield member_ids.getCount();
            console.log(total_mems);
            if (total_mems > 10) {
                var query1 = query;
                var sub_mem_count = total_mems - 10;
                var team = yield query1.limit(10).getRawMany();
                console.log(sub_mem_count);
                var sub_mem = yield query1.offset(10).limit(sub_mem_count).getRawMany();
                for (var i = 0; i < team.length; i++) {
                    if (i == 0 || i == 3 || i == 4 || i == 7 || i == 8) {
                        team_a.push(team[i]);
                    }
                    else {
                        team_b.push(team[i]);
                    }
                }
            }
            else {
                if (total_mems % 2 == 0) {
                    console.log("even");
                    var team = yield query.limit(total_mems).getRawMany();
                    for (var i = 0; i < team.length; i++) {
                        if (i == 0 || i == 3 || i == 4 || i == 7 || i == 8) {
                            team_a.push(team[i]);
                        }
                        else {
                            team_b.push(team[i]);
                        }
                    }
                }
                else {
                    console.log("odd");
                    var remain_mem = total_mems - 1;
                    var query1 = query;
                    var query2 = query;
                    var sub_mem_count = 1;
                    console.log(remain_mem);
                    var team = yield query2.limit(remain_mem).getRawMany();
                    if (remain_mem == 0) {
                        var team = [];
                    }
                    var sub_mem = yield query1.offset(remain_mem).limit(sub_mem_count).getRawMany();
                    console.log("team");
                    console.log(team);
                    console.log("sub_mem");
                    console.log(sub_mem);
                    for (var i = 0; i < team.length; i++) {
                        if (i == 0 || i == 3 || i == 4 || i == 7 || i == 8) {
                            team_a.push(team[i]);
                        }
                        else
                            team_b.push(team[i]);
                    }
                }
            }
            const event_teams = [];
            for (const i in team_a) {
                const etm = new event_team_member_entity_1.EventTeamMembers();
                etm.event_id = event_id;
                etm.member_id = team_a[i].id;
                etm.team = 1;
                event_teams.push(etm);
            }
            for (const i in team_b) {
                const etm = new event_team_member_entity_1.EventTeamMembers();
                etm.event_id = event_id;
                etm.member_id = team_b[i].id;
                etm.team = 2;
                event_teams.push(etm);
            }
            for (const i in sub_mem) {
                const etm = new event_team_member_entity_1.EventTeamMembers();
                etm.event_id = event_id;
                etm.member_id = sub_mem[i].id;
                etm.team = 3;
                event_teams.push(etm);
            }
            var a = team_a.length;
            var b = team_b.length;
            var x = 5 - a;
            var y = 5 - b;
            var z = add_plus_ids.length;
            if (total_mems > 10) {
                for (const i in add_plus_ids) {
                    const etm = new event_team_member_entity_1.EventTeamMembers();
                    etm.event_id = event_id;
                    etm.add_plus_id = add_plus_ids[i].id;
                    etm.team = 3;
                    event_teams.push(etm);
                }
            }
            else {
                for (var i = 0; i < add_plus_ids.length; i++) {
                    if ((add_plus_ids.length) % 2 != 0) {
                        if (i == 0) {
                            const etm = new event_team_member_entity_1.EventTeamMembers();
                            etm.event_id = event_id;
                            etm.add_plus_id = add_plus_ids[i].id;
                            etm.team = 3;
                            event_teams.push(etm);
                            continue;
                        }
                    }
                    if (i % 2 == 0) {
                        const etm = new event_team_member_entity_1.EventTeamMembers();
                        etm.event_id = event_id;
                        etm.add_plus_id = add_plus_ids[i].id;
                        etm.team = 1;
                        event_teams.push(etm);
                    }
                    if (i % 2 != 0) {
                        const etm = new event_team_member_entity_1.EventTeamMembers();
                        etm.event_id = event_id;
                        etm.add_plus_id = add_plus_ids[i].id;
                        etm.team = 2;
                        event_teams.push(etm);
                    }
                }
            }
            yield this.eventTeamMemberRepository.save(event_teams);
        });
    }
};
EventService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(event_entity_1.Events)),
    __param(1, typeorm_1.InjectRepository(event_member_entity_1.EventMembers)),
    __param(2, typeorm_1.InjectRepository(event_review_entity_1.EventReviews)),
    __param(3, typeorm_1.InjectRepository(event_team_member_entity_1.EventTeamMembers)),
    __param(4, typeorm_1.InjectRepository(user_entity_1.Users)),
    __param(5, typeorm_1.InjectRepository(follow_entity_1.Followers)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        mailer_1.MailerService,
        notification_service_1.NotificationService])
], EventService);
exports.EventService = EventService;
//# sourceMappingURL=event.service.js.map