"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const user_entity_1 = require("../../user/entities/user.entity");
const event_member_entity_1 = require("./event-member.entity");
const event_team_member_entity_1 = require("./event-team-member.entity");
const event_review_entity_1 = require("./event-review.entity");
const notification_entity_1 = require("../../notification/entities/notification.entity");
const chat_entity_1 = require("../../chat/entities/chat.entity");
let Events = class Events {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], Events.prototype, "id", void 0);
__decorate([
    typeorm_1.ManyToOne(type => user_entity_1.Users, Users => Users.Events, {
        cascade: true,
        onDelete: "CASCADE",
        onUpdate: "CASCADE"
    }),
    typeorm_1.JoinColumn({ name: "user_id" }),
    __metadata("design:type", Number)
], Events.prototype, "user_id", void 0);
__decorate([
    typeorm_1.Column({ default: null, collation: "utf8mb4_bin" }),
    __metadata("design:type", String)
], Events.prototype, "title", void 0);
__decorate([
    typeorm_1.Column({ default: null }),
    __metadata("design:type", String)
], Events.prototype, "image", void 0);
__decorate([
    typeorm_1.Column({ default: null }),
    __metadata("design:type", String)
], Events.prototype, "venue", void 0);
__decorate([
    typeorm_1.Column({ default: null, collation: "utf8mb4_bin" }),
    __metadata("design:type", String)
], Events.prototype, "description", void 0);
__decorate([
    typeorm_1.Column({ type: 'timestamp', default: null }),
    __metadata("design:type", Date)
], Events.prototype, "date", void 0);
__decorate([
    typeorm_1.Column({ type: 'timestamp', default: null }),
    __metadata("design:type", Date)
], Events.prototype, "from_time", void 0);
__decorate([
    typeorm_1.Column({ type: 'timestamp', default: null }),
    __metadata("design:type", Date)
], Events.prototype, "to_time", void 0);
__decorate([
    typeorm_1.Column({ default: null, type: "double" }),
    __metadata("design:type", Number)
], Events.prototype, "latitude", void 0);
__decorate([
    typeorm_1.Column({ default: null, type: "double" }),
    __metadata("design:type", Number)
], Events.prototype, "longitude", void 0);
__decorate([
    typeorm_1.Column({ default: null }),
    __metadata("design:type", Number)
], Events.prototype, "min_players", void 0);
__decorate([
    typeorm_1.Column({ default: null }),
    __metadata("design:type", Number)
], Events.prototype, "max_players", void 0);
__decorate([
    typeorm_1.Column({ default: null, type: "tinyint", comment: "1-public,2-private" }),
    __metadata("design:type", Number)
], Events.prototype, "privacy_type", void 0);
__decorate([
    typeorm_1.Column({ default: null, type: "tinyint", comment: "1-cleared,0-unclear" }),
    __metadata("design:type", Number)
], Events.prototype, "is_clear", void 0);
__decorate([
    typeorm_1.Column({ default: null, type: "tinyint", comment: "1-free,2-fixed,3-variable" }),
    __metadata("design:type", Number)
], Events.prototype, "price_type", void 0);
__decorate([
    typeorm_1.Column({ default: null, type: "tinyint", comment: "1-training,2-game,3-pickup" }),
    __metadata("design:type", Number)
], Events.prototype, "session_type", void 0);
__decorate([
    typeorm_1.Column({ default: null, type: "tinyint", comment: "1-pending,2-ongoing,3-complete,4-cancelled" }),
    __metadata("design:type", Number)
], Events.prototype, "status", void 0);
__decorate([
    typeorm_1.Column({ default: 0, type: "tinyint", comment: "0-not sent,1-sent" }),
    __metadata("design:type", Number)
], Events.prototype, "is_notif", void 0);
__decorate([
    typeorm_1.Column({ default: null, collation: "utf8mb4_bin" }),
    __metadata("design:type", String)
], Events.prototype, "currency", void 0);
__decorate([
    typeorm_1.Column({ default: null, type: "double" }),
    __metadata("design:type", Number)
], Events.prototype, "price", void 0);
__decorate([
    typeorm_1.Column({ default: null, type: "tinyint", comment: "0-no,1-yes" }),
    __metadata("design:type", Number)
], Events.prototype, "mark_session_full", void 0);
__decorate([
    typeorm_1.Column({ type: 'timestamp', default: () => "CURRENT_TIMESTAMP" }),
    __metadata("design:type", Date)
], Events.prototype, "created_at", void 0);
__decorate([
    typeorm_1.Column({ type: 'timestamp', default: null }),
    __metadata("design:type", Date)
], Events.prototype, "updated_at", void 0);
__decorate([
    typeorm_1.Column({ type: 'timestamp', default: null }),
    __metadata("design:type", Date)
], Events.prototype, "deleted_at", void 0);
__decorate([
    typeorm_1.Column({ type: 'timestamp', default: null }),
    __metadata("design:type", Date)
], Events.prototype, "cancelled_at", void 0);
__decorate([
    typeorm_1.Column({ type: 'timestamp', default: null }),
    __metadata("design:type", Date)
], Events.prototype, "completed_at", void 0);
__decorate([
    typeorm_1.OneToMany(type => event_member_entity_1.EventMembers, EventMembers => EventMembers.event_id),
    __metadata("design:type", Array)
], Events.prototype, "EventMembers", void 0);
__decorate([
    typeorm_1.OneToMany(type => event_team_member_entity_1.EventTeamMembers, EventTeamMembers => EventTeamMembers.event_id),
    __metadata("design:type", Array)
], Events.prototype, "EventTeamMembers", void 0);
__decorate([
    typeorm_1.OneToMany(type => event_team_member_entity_1.EventTeamMembers, EventTeamMembers => EventTeamMembers.add_plus_id),
    __metadata("design:type", Array)
], Events.prototype, "EventTeamMembers2", void 0);
__decorate([
    typeorm_1.OneToMany(type => event_review_entity_1.EventReviews, EventReviews => EventReviews.event_id),
    __metadata("design:type", Array)
], Events.prototype, "EventReviews", void 0);
__decorate([
    typeorm_1.OneToMany(type => notification_entity_1.Notifications, Notifications => Notifications.event_id),
    __metadata("design:type", Array)
], Events.prototype, "Notifications", void 0);
__decorate([
    typeorm_1.OneToMany(type => chat_entity_1.Chats, Chats => Chats.event_id),
    __metadata("design:type", Array)
], Events.prototype, "Chats", void 0);
Events = __decorate([
    typeorm_1.Entity()
], Events);
exports.Events = Events;
//# sourceMappingURL=event.entity.js.map