export declare class EventTeamMembers {
    id: number;
    member_id: number;
    event_id: number;
    add_plus_id: number;
    team: number;
    created_at: Date;
    updated_at: Date;
}
