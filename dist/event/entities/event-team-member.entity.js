"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const user_entity_1 = require("../../user/entities/user.entity");
const event_entity_1 = require("./event.entity");
let EventTeamMembers = class EventTeamMembers {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], EventTeamMembers.prototype, "id", void 0);
__decorate([
    typeorm_1.ManyToOne(type => user_entity_1.Users, Users => Users.EventTeamMembers, {
        cascade: true,
        onDelete: "CASCADE",
        onUpdate: "CASCADE"
    }),
    typeorm_1.JoinColumn({ name: "member_id" }),
    __metadata("design:type", Number)
], EventTeamMembers.prototype, "member_id", void 0);
__decorate([
    typeorm_1.ManyToOne(type => event_entity_1.Events, Events => Events.EventTeamMembers, {
        cascade: true,
        onDelete: "CASCADE",
        onUpdate: "CASCADE"
    }),
    typeorm_1.JoinColumn({ name: "event_id" }),
    __metadata("design:type", Number)
], EventTeamMembers.prototype, "event_id", void 0);
__decorate([
    typeorm_1.ManyToOne(type => event_entity_1.Events, Events => Events.EventTeamMembers2, {
        cascade: true,
        onDelete: "CASCADE",
        onUpdate: "CASCADE"
    }),
    typeorm_1.JoinColumn({ name: "add_plus_id" }),
    __metadata("design:type", Number)
], EventTeamMembers.prototype, "add_plus_id", void 0);
__decorate([
    typeorm_1.Column({ default: null, type: "tinyint", comment: "1-A, 2-B, 3-other" }),
    __metadata("design:type", Number)
], EventTeamMembers.prototype, "team", void 0);
__decorate([
    typeorm_1.Column({ type: 'timestamp', default: () => "CURRENT_TIMESTAMP" }),
    __metadata("design:type", Date)
], EventTeamMembers.prototype, "created_at", void 0);
__decorate([
    typeorm_1.Column({ type: 'timestamp', default: null }),
    __metadata("design:type", Date)
], EventTeamMembers.prototype, "updated_at", void 0);
EventTeamMembers = __decorate([
    typeorm_1.Entity()
], EventTeamMembers);
exports.EventTeamMembers = EventTeamMembers;
//# sourceMappingURL=event-team-member.entity.js.map