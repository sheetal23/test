export declare class EventMembers {
    id: number;
    member_id: number;
    added_by: number;
    event_id: number;
    member_name: string;
    is_admin: number;
    is_join: number;
    is_applied: number;
    is_invited: number;
    is_left: number;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
}
