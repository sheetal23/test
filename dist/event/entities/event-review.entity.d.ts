export declare class EventReviews {
    id: number;
    user_id: number;
    event_id: number;
    review: string;
    rating: number;
    created_at: Date;
}
