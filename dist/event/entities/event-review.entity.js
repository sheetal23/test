"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const user_entity_1 = require("../../user/entities/user.entity");
const event_entity_1 = require("./event.entity");
let EventReviews = class EventReviews {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], EventReviews.prototype, "id", void 0);
__decorate([
    typeorm_1.ManyToOne(type => user_entity_1.Users, Users => Users.EventReviews, {
        cascade: true,
        onDelete: "CASCADE",
        onUpdate: "CASCADE"
    }),
    typeorm_1.JoinColumn({ name: "user_id" }),
    __metadata("design:type", Number)
], EventReviews.prototype, "user_id", void 0);
__decorate([
    typeorm_1.ManyToOne(type => event_entity_1.Events, Events => Events.EventReviews, {
        cascade: true,
        onDelete: "CASCADE",
        onUpdate: "CASCADE"
    }),
    typeorm_1.JoinColumn({ name: "event_id" }),
    __metadata("design:type", Number)
], EventReviews.prototype, "event_id", void 0);
__decorate([
    typeorm_1.Column({ default: null, collation: "utf8mb4_unicode_ci", charset: "utf8mb4" }),
    __metadata("design:type", String)
], EventReviews.prototype, "review", void 0);
__decorate([
    typeorm_1.Column({ default: null, type: "double" }),
    __metadata("design:type", Number)
], EventReviews.prototype, "rating", void 0);
__decorate([
    typeorm_1.Column({ type: 'timestamp', default: () => "CURRENT_TIMESTAMP" }),
    __metadata("design:type", Date)
], EventReviews.prototype, "created_at", void 0);
EventReviews = __decorate([
    typeorm_1.Entity()
], EventReviews);
exports.EventReviews = EventReviews;
//# sourceMappingURL=event-review.entity.js.map