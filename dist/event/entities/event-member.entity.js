"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const user_entity_1 = require("../../user/entities/user.entity");
const event_entity_1 = require("./event.entity");
let EventMembers = class EventMembers {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], EventMembers.prototype, "id", void 0);
__decorate([
    typeorm_1.ManyToOne(type => user_entity_1.Users, Users => Users.EventMembers, {
        cascade: true,
        onDelete: "CASCADE",
        onUpdate: "CASCADE"
    }),
    typeorm_1.JoinColumn({ name: "member_id" }),
    __metadata("design:type", Number)
], EventMembers.prototype, "member_id", void 0);
__decorate([
    typeorm_1.ManyToOne(type => user_entity_1.Users, Users => Users.EventMembers1, {
        cascade: true,
        onDelete: "CASCADE",
        onUpdate: "CASCADE"
    }),
    typeorm_1.JoinColumn({ name: "added_by" }),
    __metadata("design:type", Number)
], EventMembers.prototype, "added_by", void 0);
__decorate([
    typeorm_1.ManyToOne(type => event_entity_1.Events, Events => Events.EventMembers, {
        cascade: true,
        onDelete: "CASCADE",
        onUpdate: "CASCADE"
    }),
    typeorm_1.JoinColumn({ name: "event_id" }),
    __metadata("design:type", Number)
], EventMembers.prototype, "event_id", void 0);
__decorate([
    typeorm_1.Column({ default: null, comment: "plus one member name" }),
    __metadata("design:type", String)
], EventMembers.prototype, "member_name", void 0);
__decorate([
    typeorm_1.Column({ default: 0, type: "tinyint", comment: "0-no, 1-yes" }),
    __metadata("design:type", Number)
], EventMembers.prototype, "is_admin", void 0);
__decorate([
    typeorm_1.Column({ default: 0, type: "tinyint", comment: "0-no, 1-yes" }),
    __metadata("design:type", Number)
], EventMembers.prototype, "is_join", void 0);
__decorate([
    typeorm_1.Column({ default: 0, type: "tinyint", comment: "0-no, 1-yes" }),
    __metadata("design:type", Number)
], EventMembers.prototype, "is_applied", void 0);
__decorate([
    typeorm_1.Column({ default: 0, type: "tinyint", comment: "0-no, 1-yes" }),
    __metadata("design:type", Number)
], EventMembers.prototype, "is_invited", void 0);
__decorate([
    typeorm_1.Column({ default: 0, type: "tinyint", comment: "0-no, 1-yes" }),
    __metadata("design:type", Number)
], EventMembers.prototype, "is_left", void 0);
__decorate([
    typeorm_1.Column({ type: 'timestamp', default: () => "CURRENT_TIMESTAMP" }),
    __metadata("design:type", Date)
], EventMembers.prototype, "created_at", void 0);
__decorate([
    typeorm_1.Column({ type: 'timestamp', default: null }),
    __metadata("design:type", Date)
], EventMembers.prototype, "updated_at", void 0);
__decorate([
    typeorm_1.Column({ type: 'timestamp', default: null }),
    __metadata("design:type", Date)
], EventMembers.prototype, "deleted_at", void 0);
EventMembers = __decorate([
    typeorm_1.Entity()
], EventMembers);
exports.EventMembers = EventMembers;
//# sourceMappingURL=event-member.entity.js.map