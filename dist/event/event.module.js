"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const event_service_1 = require("./event.service");
const event_controller_1 = require("./event.controller");
const event_entity_1 = require("./entities/event.entity");
const event_member_entity_1 = require("./entities/event-member.entity");
const event_review_entity_1 = require("./entities/event-review.entity");
const event_team_member_entity_1 = require("./entities/event-team-member.entity");
const typeorm_1 = require("@nestjs/typeorm");
const user_authmiddleware_1 = require("../user/user.authmiddleware");
const user_entity_1 = require("../user/entities/user.entity");
const user_module_1 = require("../user/user.module");
const notification_module_1 = require("../notification/notification.module");
const follow_entity_1 = require("../user/entities/follow.entity");
let EventModule = class EventModule {
    configure(consumer) {
        consumer
            .apply(user_authmiddleware_1.AuthMiddleware)
            .forRoutes({ path: 'event', method: common_1.RequestMethod.POST }, { path: 'event/member', method: common_1.RequestMethod.POST }, { path: 'event/accept', method: common_1.RequestMethod.POST }, { path: 'event/apply', method: common_1.RequestMethod.POST }, { path: 'event/admin', method: common_1.RequestMethod.POST }, { path: 'event/member', method: common_1.RequestMethod.DELETE }, { path: 'event/left', method: common_1.RequestMethod.DELETE }, { path: 'event', method: common_1.RequestMethod.DELETE }, { path: 'event/decline', method: common_1.RequestMethod.DELETE }, { path: 'event/admin', method: common_1.RequestMethod.DELETE }, { path: 'event/mark-full', method: common_1.RequestMethod.PUT }, { path: 'event/listing', method: common_1.RequestMethod.GET }, { path: 'event', method: common_1.RequestMethod.GET }, { path: 'event/team', method: common_1.RequestMethod.GET });
    }
};
EventModule = __decorate([
    common_1.Module({
        imports: [
            typeorm_1.TypeOrmModule.forFeature([event_entity_1.Events]),
            typeorm_1.TypeOrmModule.forFeature([event_member_entity_1.EventMembers]),
            typeorm_1.TypeOrmModule.forFeature([event_review_entity_1.EventReviews]),
            typeorm_1.TypeOrmModule.forFeature([event_team_member_entity_1.EventTeamMembers]),
            typeorm_1.TypeOrmModule.forFeature([user_entity_1.Users]),
            typeorm_1.TypeOrmModule.forFeature([follow_entity_1.Followers]),
            user_module_1.UserModule,
            notification_module_1.NotificationModule
        ],
        providers: [event_service_1.EventService],
        controllers: [event_controller_1.EventController]
    })
], EventModule);
exports.EventModule = EventModule;
//# sourceMappingURL=event.module.js.map