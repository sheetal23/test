import { MiddlewareConsumer, NestModule } from '@nestjs/common';
export declare class EventModule implements NestModule {
    configure(consumer: MiddlewareConsumer): void;
}
