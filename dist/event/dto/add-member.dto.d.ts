export declare class AddEventMemberDto {
    readonly event_id: number;
    readonly member_id: number;
    readonly type: number;
    readonly member_name: string;
    readonly member_ids: Array<number>;
}
