export declare class CreateEventDto {
    readonly id: number;
    readonly title: string;
    readonly session_type: number;
    readonly image: string;
    readonly description: string;
    readonly venue: string;
    readonly latitude: number;
    readonly longitude: number;
    readonly date: Date;
    readonly from_time: Date;
    readonly to_time: Date;
    readonly max_players: number;
    readonly min_players: number;
    readonly price_type: number;
    readonly privacy_type: number;
    readonly price: number;
    readonly currency: string;
    readonly player_ids: Array<number>;
}
