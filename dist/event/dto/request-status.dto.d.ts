export declare class EventRequestDto {
    readonly event_id: number;
    readonly id: number;
    readonly review: string;
    readonly rating: number;
}
