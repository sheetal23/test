"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const event_service_1 = require("./event.service");
const user_service_1 = require("../user/user.service");
const dto_1 = require("./dto");
const swagger_1 = require("@nestjs/swagger");
let EventController = class EventController {
    constructor(userService, eventService) {
        this.userService = userService;
        this.eventService = eventService;
    }
    event(request, dto) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            return yield this.eventService.create(jwtData.id, dto);
        });
    }
    addEventMember(request, dto) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            return yield this.eventService.addEventMember(jwtData.id, dto);
        });
    }
    applyEvent(request, dto) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            console.log(request.headers.authorization);
            return yield this.eventService.applyEvent(jwtData.id, dto);
        });
    }
    acceptEvent(request, dto) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            console.log(request.headers.authorization);
            return yield this.eventService.acceptEvent(jwtData.id, request);
        });
    }
    confirmEvent(request, dto) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            console.log(request.headers.authorization);
            return yield this.eventService.confirmEvent(jwtData.id, dto);
        });
    }
    addAdmin(request, dto) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            console.log(request.headers.authorization);
            return yield this.eventService.addAdmin(jwtData.id, dto);
        });
    }
    deleteMember(request) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            console.log(request.headers.authorization);
            return yield this.eventService.deleteMember(jwtData.id, request);
        });
    }
    leftEvent(request) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            console.log(request.headers.authorization);
            return yield this.eventService.leftEvent(jwtData.id, request);
        });
    }
    declineInvitation(request) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            console.log(request.headers.authorization);
            return yield this.eventService.declineInvitation(jwtData.id, request);
        });
    }
    deleteEvent(request) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            console.log(request.headers.authorization);
            return yield this.eventService.deleteEvent(jwtData.id, request);
        });
    }
    deleteAdmin(request) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            console.log(request.headers.authorization);
            return yield this.eventService.deleteAdmin(jwtData.id, request);
        });
    }
    markFull(request, dto) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            console.log(request.headers.authorization);
            return yield this.eventService.markFull(jwtData.id, dto);
        });
    }
    eventList(request) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            console.log(request.headers.authorization);
            return yield this.eventService.eventList(jwtData.id, request);
        });
    }
    eventDetail(request) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            console.log(request.headers.authorization);
            return yield this.eventService.eventDetail(jwtData.id, request);
        });
    }
    eventTeams(request) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            console.log(request.headers.authorization);
            return yield this.eventService.eventTeams(jwtData.id, request);
        });
    }
    review(request, dto) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            return yield this.eventService.review(jwtData.id, dto);
        });
    }
    findAllReviews(request) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            console.log(request.headers.authorization);
            return yield this.eventService.findAllReviews(jwtData.id, request);
        });
    }
    closeEvent() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.eventService.closeEvent();
        });
    }
    startEvent() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.eventService.startEvent();
        });
    }
};
__decorate([
    common_1.Post(''),
    common_1.HttpCode(200),
    __param(0, common_1.Req()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, dto_1.CreateEventDto]),
    __metadata("design:returntype", Promise)
], EventController.prototype, "event", null);
__decorate([
    common_1.Post('member'),
    common_1.HttpCode(200),
    __param(0, common_1.Req()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, dto_1.AddEventMemberDto]),
    __metadata("design:returntype", Promise)
], EventController.prototype, "addEventMember", null);
__decorate([
    common_1.Post('apply'),
    common_1.HttpCode(200),
    __param(0, common_1.Req()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, dto_1.AddEventMemberDto]),
    __metadata("design:returntype", Promise)
], EventController.prototype, "applyEvent", null);
__decorate([
    common_1.Post('accept'),
    common_1.HttpCode(200),
    __param(0, common_1.Req()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, dto_1.AddEventMemberDto]),
    __metadata("design:returntype", Promise)
], EventController.prototype, "acceptEvent", null);
__decorate([
    common_1.Post('confirm'),
    common_1.HttpCode(200),
    __param(0, common_1.Req()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, dto_1.AddEventMemberDto]),
    __metadata("design:returntype", Promise)
], EventController.prototype, "confirmEvent", null);
__decorate([
    common_1.Post('admin'),
    common_1.HttpCode(200),
    __param(0, common_1.Req()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, dto_1.AddEventMemberDto]),
    __metadata("design:returntype", Promise)
], EventController.prototype, "addAdmin", null);
__decorate([
    common_1.Delete('member'),
    common_1.HttpCode(200),
    __param(0, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], EventController.prototype, "deleteMember", null);
__decorate([
    common_1.Delete('left'),
    common_1.HttpCode(200),
    __param(0, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], EventController.prototype, "leftEvent", null);
__decorate([
    common_1.Delete('decline'),
    common_1.HttpCode(200),
    __param(0, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], EventController.prototype, "declineInvitation", null);
__decorate([
    common_1.Delete(''),
    common_1.HttpCode(200),
    __param(0, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], EventController.prototype, "deleteEvent", null);
__decorate([
    common_1.Delete('admin'),
    common_1.HttpCode(200),
    __param(0, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], EventController.prototype, "deleteAdmin", null);
__decorate([
    common_1.Put('mark-full'),
    common_1.HttpCode(200),
    __param(0, common_1.Req()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, dto_1.EventRequestDto]),
    __metadata("design:returntype", Promise)
], EventController.prototype, "markFull", null);
__decorate([
    common_1.Get('listing'),
    common_1.HttpCode(200),
    __param(0, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], EventController.prototype, "eventList", null);
__decorate([
    common_1.Get(''),
    common_1.HttpCode(200),
    __param(0, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], EventController.prototype, "eventDetail", null);
__decorate([
    common_1.Get('team'),
    common_1.HttpCode(200),
    __param(0, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], EventController.prototype, "eventTeams", null);
__decorate([
    common_1.Post('review'),
    common_1.HttpCode(200),
    __param(0, common_1.Req()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, dto_1.EventRequestDto]),
    __metadata("design:returntype", Promise)
], EventController.prototype, "review", null);
__decorate([
    common_1.Get('reviews'),
    common_1.HttpCode(200),
    __param(0, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], EventController.prototype, "findAllReviews", null);
__decorate([
    common_1.Get('close'),
    common_1.HttpCode(200),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], EventController.prototype, "closeEvent", null);
__decorate([
    common_1.Get('start-reminder'),
    common_1.HttpCode(200),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], EventController.prototype, "startEvent", null);
EventController = __decorate([
    swagger_1.ApiBearerAuth(),
    swagger_1.ApiUseTags('event'),
    common_1.Controller('event'),
    __metadata("design:paramtypes", [user_service_1.UserService, event_service_1.EventService])
], EventController);
exports.EventController = EventController;
//# sourceMappingURL=event.controller.js.map