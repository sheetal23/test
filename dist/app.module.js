"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const app_controller_1 = require("./app.controller");
const app_service_1 = require("./app.service");
const user_controller_1 = require("./user/user.controller");
const user_service_1 = require("./user/user.service");
const user_module_1 = require("./user/user.module");
const user_entity_1 = require("./user/entities/user.entity");
const user_login_entity_1 = require("./user/entities/user-login.entity");
const block_entity_1 = require("./user/entities/block.entity");
const follow_entity_1 = require("./user/entities/follow.entity");
const user_review_entity_1 = require("./user/entities/user-review.entity");
const feedback_entity_1 = require("./user/entities/feedback.entity");
const user_rating_entity_1 = require("./user/entities/user-rating.entity");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const mailer_1 = require("@nest-modules/mailer");
const core_1 = require("@nestjs/core");
const logging_interceptor_1 = require("./shared/logging.interceptor");
const http_error_filter_1 = require("./shared/http-error.filter");
const log_entity_1 = require("./shared/entities/log.entity");
const chat_entity_1 = require("./chat/entities/chat.entity");
const chat_to_entity_1 = require("./chat/entities/chat-to.entity");
const chat_controller_1 = require("./chat/chat.controller");
const chat_service_1 = require("./chat/chat.service");
const chat_gateway_1 = require("./chat/chat.gateway");
require('dotenv').config();
let AppModule = class AppModule {
    constructor(connection) {
        this.connection = connection;
    }
};
AppModule = __decorate([
    common_1.Module({
        imports: [
            typeorm_1.TypeOrmModule.forFeature([user_entity_1.Users]),
            typeorm_1.TypeOrmModule.forFeature([user_login_entity_1.UserLogins]),
            typeorm_1.TypeOrmModule.forFeature([user_review_entity_1.UserReviews]),
            typeorm_1.TypeOrmModule.forFeature([user_rating_entity_1.UserRatings]),
            typeorm_1.TypeOrmModule.forFeature([follow_entity_1.Followers]),
            typeorm_1.TypeOrmModule.forFeature([block_entity_1.Blocks]),
            typeorm_1.TypeOrmModule.forFeature([feedback_entity_1.Feedbacks]),
            typeorm_1.TypeOrmModule.forFeature([chat_entity_1.Chats]),
            typeorm_1.TypeOrmModule.forFeature([chat_to_entity_1.ChatTos]),
            typeorm_1.TypeOrmModule.forFeature([log_entity_1.Logs]),
            typeorm_1.TypeOrmModule.forRoot(),
            user_module_1.UserModule,
            mailer_1.MailerModule.forRoot({
                transport: process.env.TRANSPORT,
                defaults: {
                    from: '"Team" <sheetal.henceforth@gmail.com>'
                },
                template: {
                    adapter: new mailer_1.HandlebarsAdapter(),
                    options: {
                        strict: true,
                    },
                },
            })
        ],
        controllers: [
            app_controller_1.AppController,
            user_controller_1.UserController,
            chat_controller_1.ChatController
        ],
        providers: [
            user_service_1.UserService,
            app_service_1.AppService,
            chat_service_1.ChatService,
            chat_gateway_1.ChatGateway,
            {
                provide: core_1.APP_INTERCEPTOR,
                useClass: logging_interceptor_1.LoggingInterceptor
            },
            {
                provide: core_1.APP_FILTER,
                useClass: http_error_filter_1.HttpErrorFilter,
            },
        ]
    }),
    __metadata("design:paramtypes", [typeorm_2.Connection])
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map