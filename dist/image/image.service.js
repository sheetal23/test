"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const AWS = require("aws-sdk");
const http_exception_1 = require("@nestjs/common/exceptions/http.exception");
const user_service_1 = require("../user/user.service");
const sharp = require("sharp");
const AWS_S3_BUCKET_NAME = 'v3-bucket';
const s3 = new AWS.S3();
AWS.config.update({
    accessKeyId: 'AKIAJCL6I3NDPBOZN7BA',
    secretAccessKey: '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',
    region: 'us-east-2'
});
let ImageService = class ImageService {
    constructor(userService) {
        this.userService = userService;
    }
    addImage(file) {
        return __awaiter(this, void 0, void 0, function* () {
            const random = yield this.userService.generateRandomString(6);
            if (!file) {
                throw new http_exception_1.HttpException({ error: 'bad_request', message: 'Image should not be empty.' }, common_1.HttpStatus.BAD_REQUEST);
            }
            const name = file.originalname.split(' ').join('-');
            const filename = random + '-' + name;
            console.log(file);
            var params = {
                Body: file.buffer,
                Bucket: AWS_S3_BUCKET_NAME,
                Key: 'uploads/images/' + filename,
                ACL: 'public-read',
            };
            console.log(params);
            const s3 = new AWS.S3();
            try {
                s3.putObject(params).promise().then(data => {
                    console.log('done' + params.Key);
                    return params.Key;
                }, err => {
                    console.log('error' + err);
                    return err;
                });
            }
            catch (err) {
                return err;
            }
            sharp(file.buffer).resize(50).toBuffer(function (err, data) {
                if (err) {
                    throw err;
                }
                ;
                s3.putObject({
                    Body: data,
                    Bucket: AWS_S3_BUCKET_NAME,
                    Key: 'uploads/images/small/' + filename,
                    ACL: 'public-read'
                }, function (err, data) {
                    if (err) {
                        console.log('Failed to resize image due to an error: ' + err);
                        return {
                            message: 'Failed to resize image due to an error: ' + err
                        };
                    }
                });
            });
            sharp(file.buffer).resize(250).toBuffer(function (err, data) {
                if (err) {
                    throw err;
                }
                ;
                s3.putObject({
                    Body: data,
                    Bucket: AWS_S3_BUCKET_NAME,
                    Key: 'uploads/images/medium/' + filename,
                    ACL: 'public-read'
                }, function (err, data) {
                    if (err) {
                        console.log('Failed to resize image due to an error: ' + err);
                        return {
                            message: 'Failed to resize image due to an error: ' + err
                        };
                    }
                });
            });
            return { message: 'Image Uploaded', filename: filename };
        });
    }
    getUpload(filename, res, req) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log(filename);
            var folder = 'uploads/images/';
            if (!filename.filename) {
                throw new http_exception_1.HttpException({ error: 'bad_request', message: 'filename required' }, common_1.HttpStatus.BAD_REQUEST);
            }
            if (req.query.folder) {
                var folder = 'uploads/images/' + req.query.folder + '/';
            }
            const params = {
                Bucket: AWS_S3_BUCKET_NAME,
                Key: folder + filename.filename
            };
            console.log(params.Key);
            console.log(folder);
            const s3 = new AWS.S3();
            s3.getObject(params, function (err, data) {
                res.writeHead(200, { 'Content-Type': 'image/jpeg' });
                res.write(data.Body, 'binary');
                res.end(null, 'binary');
            });
        });
    }
};
ImageService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [user_service_1.UserService])
], ImageService);
exports.ImageService = ImageService;
//# sourceMappingURL=image.service.js.map