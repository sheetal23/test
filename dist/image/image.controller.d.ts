import { ImageService } from './image.service';
export declare class ImageController {
    private readonly imageService;
    constructor(imageService: ImageService);
    uploadImage(file: any): Promise<object>;
    getUpload(filename: string, res: any, req: any): Promise<void>;
}
