import { UserService } from '../user/user.service';
export declare class ImageService {
    private readonly userService;
    constructor(userService: UserService);
    addImage(file: any): Promise<object>;
    getUpload(filename: any, res: any, req: any): Promise<void>;
}
