"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var ImageModule_1;
const common_1 = require("@nestjs/common");
const image_controller_1 = require("./image.controller");
const image_service_1 = require("./image.service");
const user_module_1 = require("../user/user.module");
let ImageModule = ImageModule_1 = class ImageModule {
    configure(consumer) {
    }
};
ImageModule = ImageModule_1 = __decorate([
    common_1.Module({
        imports: [
            ImageModule_1,
            user_module_1.UserModule
        ],
        providers: [image_service_1.ImageService],
        controllers: [
            image_controller_1.ImageController
        ],
        exports: [image_service_1.ImageService]
    })
], ImageModule);
exports.ImageModule = ImageModule;
//# sourceMappingURL=image.module.js.map