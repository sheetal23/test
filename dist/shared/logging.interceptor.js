"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const operators_1 = require("rxjs/operators");
class LoggingInterceptor {
    intercept(context, next) {
        var now = Date.now();
        var req = context.switchToHttp().getRequest();
        var res = context.switchToHttp().getResponse();
        var code = res.statusCode;
        var method = req.method;
        var auth = req.headers.authorization;
        var url = req.url;
        var query = req.body ? JSON.stringify(req.body) : JSON.stringify(req.query);
        console.log(code);
        console.log(auth);
        if (req) {
            return next
                .handle()
                .pipe(operators_1.tap(() => common_1.Logger.log(`${method} ${url} ${Date.now() - now}ms`, context.getClass().name)));
        }
        else {
            return next
                .handle()
                .pipe(operators_1.tap(() => common_1.Logger.error(`${method} ${url}`, 'BaseExceptionFilter')));
        }
    }
}
exports.LoggingInterceptor = LoggingInterceptor;
//# sourceMappingURL=logging.interceptor.js.map