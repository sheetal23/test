"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const common_2 = require("@nestjs/common");
const log_entity_1 = require("./entities/log.entity");
let HttpErrorFilter = class HttpErrorFilter {
    constructor(logRepository) {
        this.logRepository = logRepository;
    }
    catch(exception, host) {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse();
        const request = ctx.getRequest();
        const status = exception.getStatus
            ? exception.getStatus()
            : common_2.HttpStatus.INTERNAL_SERVER_ERROR;
        const errorResponse = {
            code: status,
            created_at: new Date(),
            path: request.url,
            params: JSON.stringify(request.body),
            query: JSON.stringify(request.query),
            auth: request.headers.authorization,
            method: request.method,
            message: status !== common_2.HttpStatus.INTERNAL_SERVER_ERROR
                ? exception.message.error || exception.message || null
                : 'Internal server error',
        };
        const error = {
            error: exception.message.error,
            statusCode: status,
            message: exception.message.message
        };
        if (status === common_2.HttpStatus.INTERNAL_SERVER_ERROR) {
            common_2.Logger.error(`${request.method} ${request.url}`, exception.stack, 'ExceptionFilter');
        }
        else {
            common_2.Logger.error(`${request.method} ${request.url}`, JSON.stringify(error), 'ExceptionFilter');
        }
        this.logRepository.save(errorResponse);
        response.status(status).json(error);
    }
};
HttpErrorFilter = __decorate([
    common_1.Injectable(),
    common_2.Catch(),
    __param(0, typeorm_1.InjectRepository(log_entity_1.Logs)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], HttpErrorFilter);
exports.HttpErrorFilter = HttpErrorFilter;
//# sourceMappingURL=http-error.filter.js.map