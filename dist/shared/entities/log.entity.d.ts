export declare class Logs {
    id: number;
    path: string;
    code: Number;
    message: string;
    params: string;
    query: string;
    method: string;
    auth: string;
    created_at: Date;
}
