import { Repository } from 'typeorm';
import { ExceptionFilter, ArgumentsHost, HttpException } from '@nestjs/common';
import { Logs } from './entities/log.entity';
export declare class HttpErrorFilter implements ExceptionFilter {
    private readonly logRepository;
    constructor(logRepository: Repository<Logs>);
    catch(exception: HttpException, host: ArgumentsHost): void;
}
