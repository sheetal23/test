"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const user_module_1 = require("../user/user.module");
const post_service_1 = require("./post.service");
const post_controller_1 = require("./post.controller");
const post_entity_1 = require("./entities/post.entity");
const typeorm_1 = require("@nestjs/typeorm");
const user_authmiddleware_1 = require("../user/user.authmiddleware");
const post_image_entity_1 = require("./entities/post-image.entity");
const user_entity_1 = require("../user/entities/user.entity");
const user_rating_entity_1 = require("../user/entities/user-rating.entity");
const post_review_entity_1 = require("./entities/post-review.entity");
const post_like_entity_1 = require("./entities/post-like.entity");
const post_comment_entity_1 = require("./entities/post-comment.entity");
const event_module_1 = require("../event/event.module");
const event_entity_1 = require("../event/entities/event.entity");
const notification_module_1 = require("../notification/notification.module");
const post_report_entity_1 = require("./entities/post-report.entity");
const follow_entity_1 = require("../user/entities/follow.entity");
let PostModule = class PostModule {
    configure(consumer) {
        consumer
            .apply(user_authmiddleware_1.AuthMiddleware)
            .forRoutes({ path: 'post', method: common_1.RequestMethod.POST }, { path: 'post/review', method: common_1.RequestMethod.POST }, { path: 'post/like', method: common_1.RequestMethod.POST }, { path: 'post/report', method: common_1.RequestMethod.POST }, { path: 'post/comment', method: common_1.RequestMethod.POST }, { path: 'post/comments', method: common_1.RequestMethod.GET }, { path: 'post/likes', method: common_1.RequestMethod.GET }, { path: 'post', method: common_1.RequestMethod.DELETE }, { path: 'post/ratings', method: common_1.RequestMethod.GET }, { path: 'post/my-posts', method: common_1.RequestMethod.GET });
    }
};
PostModule = __decorate([
    common_1.Module({
        imports: [
            typeorm_1.TypeOrmModule.forFeature([post_entity_1.Posts]),
            typeorm_1.TypeOrmModule.forFeature([user_entity_1.Users]),
            typeorm_1.TypeOrmModule.forFeature([user_rating_entity_1.UserRatings]),
            typeorm_1.TypeOrmModule.forFeature([follow_entity_1.Followers]),
            typeorm_1.TypeOrmModule.forFeature([event_entity_1.Events]),
            typeorm_1.TypeOrmModule.forFeature([post_image_entity_1.PostImages]),
            typeorm_1.TypeOrmModule.forFeature([post_review_entity_1.PostReviews]),
            typeorm_1.TypeOrmModule.forFeature([post_like_entity_1.PostLikes]),
            typeorm_1.TypeOrmModule.forFeature([post_comment_entity_1.PostComments]),
            typeorm_1.TypeOrmModule.forFeature([post_report_entity_1.PostReports]),
            user_module_1.UserModule,
            event_module_1.EventModule,
            notification_module_1.NotificationModule
        ],
        providers: [post_service_1.PostService],
        controllers: [post_controller_1.PostController]
    })
], PostModule);
exports.PostModule = PostModule;
//# sourceMappingURL=post.module.js.map