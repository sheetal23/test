"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const post_service_1 = require("./post.service");
const user_service_1 = require("../user/user.service");
const dto_1 = require("./dto");
const swagger_1 = require("@nestjs/swagger");
let PostController = class PostController {
    constructor(userService, postService) {
        this.userService = userService;
        this.postService = postService;
    }
    homeFeed(request) {
        return __awaiter(this, void 0, void 0, function* () {
            var user_id = 0;
            if (request.headers.authorization) {
                const jwtData = yield this.userService.jwtData(request.headers.authorization);
                var user_id = jwtData.id;
            }
            return yield this.postService.homeFeed(user_id, request);
        });
    }
    create(request, postData) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            console.log(request.headers.authorization);
            return yield this.postService.create(jwtData.id, postData);
        });
    }
    reviewPost(request, reviewData) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            console.log(request.headers.authorization);
            return yield this.postService.reviewPost(jwtData.id, reviewData);
        });
    }
    likePost(request, likeData) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            console.log(request.headers.authorization);
            return yield this.postService.likePost(jwtData.id, likeData);
        });
    }
    commentPost(request, commentData) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            console.log(request.headers.authorization);
            return yield this.postService.commentPost(jwtData.id, commentData);
        });
    }
    reportPost(request, reportData) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            console.log(request.headers.authorization);
            return yield this.postService.reportPost(jwtData.id, reportData);
        });
    }
    deletePost(request, deleteData) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            console.log(request.headers.authorization);
            return yield this.postService.deletePost(jwtData.id, deleteData);
        });
    }
    deleteComment(request, deleteData) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            console.log(request.headers.authorization);
            return yield this.postService.deleteComment(jwtData.id, deleteData);
        });
    }
    getComments(request) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            console.log(request.headers.authorization);
            return yield this.postService.getComments(jwtData.id, request);
        });
    }
    getLikes(request) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            console.log(request.headers.authorization);
            return yield this.postService.getLikes(jwtData.id, request);
        });
    }
    getRatings(request) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            console.log(request.headers.authorization);
            return yield this.postService.getRatings(jwtData.id, request);
        });
    }
    myPosts(request) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            console.log(request.headers.authorization);
            return yield this.postService.myPosts(jwtData.id, request);
        });
    }
    getPost(request) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            console.log(request.headers.authorization);
            return yield this.postService.getPost(jwtData.id, request);
        });
    }
};
__decorate([
    common_1.Get('homefeed'),
    common_1.HttpCode(200),
    __param(0, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], PostController.prototype, "homeFeed", null);
__decorate([
    common_1.Post(''),
    common_1.HttpCode(200),
    __param(0, common_1.Req()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, dto_1.CreatePostDto]),
    __metadata("design:returntype", Promise)
], PostController.prototype, "create", null);
__decorate([
    common_1.Post('review'),
    common_1.HttpCode(200),
    __param(0, common_1.Req()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, dto_1.PostReviewDto]),
    __metadata("design:returntype", Promise)
], PostController.prototype, "reviewPost", null);
__decorate([
    common_1.Post('like'),
    common_1.HttpCode(200),
    __param(0, common_1.Req()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, dto_1.PostReviewDto]),
    __metadata("design:returntype", Promise)
], PostController.prototype, "likePost", null);
__decorate([
    common_1.Post('comment'),
    common_1.HttpCode(200),
    __param(0, common_1.Req()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, dto_1.PostReviewDto]),
    __metadata("design:returntype", Promise)
], PostController.prototype, "commentPost", null);
__decorate([
    common_1.Post('report'),
    common_1.HttpCode(200),
    __param(0, common_1.Req()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, dto_1.PostReviewDto]),
    __metadata("design:returntype", Promise)
], PostController.prototype, "reportPost", null);
__decorate([
    common_1.Delete(),
    common_1.HttpCode(200),
    __param(0, common_1.Req()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, dto_1.PostReviewDto]),
    __metadata("design:returntype", Promise)
], PostController.prototype, "deletePost", null);
__decorate([
    common_1.Delete('comment'),
    common_1.HttpCode(200),
    __param(0, common_1.Req()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, dto_1.PostReviewDto]),
    __metadata("design:returntype", Promise)
], PostController.prototype, "deleteComment", null);
__decorate([
    common_1.Get('comments'),
    common_1.HttpCode(200),
    __param(0, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], PostController.prototype, "getComments", null);
__decorate([
    common_1.Get('likes'),
    common_1.HttpCode(200),
    __param(0, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], PostController.prototype, "getLikes", null);
__decorate([
    common_1.Get('ratings'),
    common_1.HttpCode(200),
    __param(0, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], PostController.prototype, "getRatings", null);
__decorate([
    common_1.Get('my-posts'),
    common_1.HttpCode(200),
    __param(0, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], PostController.prototype, "myPosts", null);
__decorate([
    common_1.Get(''),
    common_1.HttpCode(200),
    __param(0, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], PostController.prototype, "getPost", null);
PostController = __decorate([
    swagger_1.ApiBearerAuth(),
    swagger_1.ApiUseTags('post'),
    common_1.Controller('post'),
    __metadata("design:paramtypes", [user_service_1.UserService, post_service_1.PostService])
], PostController);
exports.PostController = PostController;
//# sourceMappingURL=post.controller.js.map