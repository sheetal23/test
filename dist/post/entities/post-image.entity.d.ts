export declare class PostImages {
    id: number;
    post_id: number;
    image: string;
    type: number;
    video: string;
    video_thumb: string;
    created_at: Date;
}
