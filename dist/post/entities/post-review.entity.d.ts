export declare class PostReviews {
    id: number;
    user_id: number;
    post_id: number;
    review: string;
    rating: number;
    created_at: Date;
    updated_at: Date;
}
