import { PostComments } from '../../post/entities/post-comment.entity';
import { PostImages } from '../../post/entities/post-image.entity';
import { PostLikes } from '../../post/entities/post-like.entity';
import { PostReviews } from '../../post/entities/post-review.entity';
import { Notifications } from '../../notification/entities/notification.entity';
import { PostReports } from './post-report.entity';
export declare class Posts {
    id: number;
    user_id: number;
    caption: string;
    location: string;
    latitude: number;
    longitude: number;
    comment_enable: number;
    created_at: Date;
    updated_at: Date;
    PostComments: PostComments[];
    PostImages: PostImages[];
    PostLikes: PostLikes[];
    PostReviews: PostReviews[];
    PostReports: PostReports[];
    Notifications: Notifications[];
}
