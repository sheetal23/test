"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const user_entity_1 = require("../../user/entities/user.entity");
const post_comment_entity_1 = require("../../post/entities/post-comment.entity");
const post_image_entity_1 = require("../../post/entities/post-image.entity");
const post_like_entity_1 = require("../../post/entities/post-like.entity");
const post_review_entity_1 = require("../../post/entities/post-review.entity");
const notification_entity_1 = require("../../notification/entities/notification.entity");
const post_report_entity_1 = require("./post-report.entity");
let Posts = class Posts {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], Posts.prototype, "id", void 0);
__decorate([
    typeorm_1.ManyToOne(type => user_entity_1.Users, Users => Users.Posts, {
        cascade: true,
        onDelete: "CASCADE",
        onUpdate: "CASCADE"
    }),
    typeorm_1.JoinColumn({ name: "user_id" }),
    __metadata("design:type", Number)
], Posts.prototype, "user_id", void 0);
__decorate([
    typeorm_1.Column({ default: null, type: "text", collation: "utf8mb4_bin" }),
    __metadata("design:type", String)
], Posts.prototype, "caption", void 0);
__decorate([
    typeorm_1.Column({ default: null }),
    __metadata("design:type", String)
], Posts.prototype, "location", void 0);
__decorate([
    typeorm_1.Column({ default: null, type: "double" }),
    __metadata("design:type", Number)
], Posts.prototype, "latitude", void 0);
__decorate([
    typeorm_1.Column({ default: null, type: "double" }),
    __metadata("design:type", Number)
], Posts.prototype, "longitude", void 0);
__decorate([
    typeorm_1.Column({ default: null, type: "boolean", comment: "2-disable,1-enable" }),
    __metadata("design:type", Number)
], Posts.prototype, "comment_enable", void 0);
__decorate([
    typeorm_1.Column({ type: 'timestamp', default: () => "CURRENT_TIMESTAMP" }),
    __metadata("design:type", Date)
], Posts.prototype, "created_at", void 0);
__decorate([
    typeorm_1.Column({ type: 'timestamp', default: null }),
    __metadata("design:type", Date)
], Posts.prototype, "updated_at", void 0);
__decorate([
    typeorm_1.OneToMany(type => post_comment_entity_1.PostComments, PostComments => PostComments.post_id),
    __metadata("design:type", Array)
], Posts.prototype, "PostComments", void 0);
__decorate([
    typeorm_1.OneToMany(type => post_image_entity_1.PostImages, PostImages => PostImages.post_id),
    __metadata("design:type", Array)
], Posts.prototype, "PostImages", void 0);
__decorate([
    typeorm_1.OneToMany(type => post_like_entity_1.PostLikes, PostLikes => PostLikes.post_id),
    __metadata("design:type", Array)
], Posts.prototype, "PostLikes", void 0);
__decorate([
    typeorm_1.OneToMany(type => post_review_entity_1.PostReviews, PostReviews => PostReviews.post_id),
    __metadata("design:type", Array)
], Posts.prototype, "PostReviews", void 0);
__decorate([
    typeorm_1.OneToMany(type => post_report_entity_1.PostReports, PostReports => PostReports.post_id),
    __metadata("design:type", Array)
], Posts.prototype, "PostReports", void 0);
__decorate([
    typeorm_1.OneToMany(type => notification_entity_1.Notifications, Notifications => Notifications.post_id),
    __metadata("design:type", Array)
], Posts.prototype, "Notifications", void 0);
Posts = __decorate([
    typeorm_1.Entity()
], Posts);
exports.Posts = Posts;
//# sourceMappingURL=post.entity.js.map