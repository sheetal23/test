export declare class PostLikes {
    id: number;
    user_id: number;
    post_id: number;
    created_at: Date;
}
