export declare class PostComments {
    id: number;
    user_id: number;
    post_id: number;
    comment: string;
    created_at: Date;
    updated_at: Date;
}
