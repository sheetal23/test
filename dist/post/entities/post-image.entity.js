"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const post_entity_1 = require("../../post/entities/post.entity");
let PostImages = class PostImages {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], PostImages.prototype, "id", void 0);
__decorate([
    typeorm_1.ManyToOne(type => post_entity_1.Posts, Posts => Posts.PostImages, {
        cascade: true,
        onDelete: "CASCADE",
        onUpdate: "CASCADE"
    }),
    typeorm_1.JoinColumn({ name: "post_id" }),
    __metadata("design:type", Number)
], PostImages.prototype, "post_id", void 0);
__decorate([
    typeorm_1.Column({ default: null }),
    __metadata("design:type", String)
], PostImages.prototype, "image", void 0);
__decorate([
    typeorm_1.Column({ default: 1, type: "tinyint", comment: "1-image,2-video" }),
    __metadata("design:type", Number)
], PostImages.prototype, "type", void 0);
__decorate([
    typeorm_1.Column({ default: null }),
    __metadata("design:type", String)
], PostImages.prototype, "video", void 0);
__decorate([
    typeorm_1.Column({ default: null }),
    __metadata("design:type", String)
], PostImages.prototype, "video_thumb", void 0);
__decorate([
    typeorm_1.Column({ type: 'timestamp', default: () => "CURRENT_TIMESTAMP" }),
    __metadata("design:type", Date)
], PostImages.prototype, "created_at", void 0);
PostImages = __decorate([
    typeorm_1.Entity()
], PostImages);
exports.PostImages = PostImages;
//# sourceMappingURL=post-image.entity.js.map