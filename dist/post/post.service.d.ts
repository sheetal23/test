import { Users } from '../user/entities/user.entity';
import { UserRatings } from '../user/entities/user-rating.entity';
import { Events } from '../event/entities/event.entity';
import { Posts } from './entities/post.entity';
import { PostImages } from './entities/post-image.entity';
import { PostReviews } from './entities/post-review.entity';
import { PostLikes } from './entities/post-like.entity';
import { PostComments } from './entities/post-comment.entity';
import { Repository } from 'typeorm';
import { CreatePostDto, PostReviewDto } from './dto';
import { MailerService } from '@nest-modules/mailer';
import { NotificationService } from '../notification/notification.service';
import { PostReports } from './entities/post-report.entity';
import { Followers } from '../user/entities/follow.entity';
export declare class PostService {
    private readonly postRepository;
    private readonly postImageRepository;
    private readonly postReviewRepository;
    private readonly postLikeRepository;
    private readonly postCommentRepository;
    private readonly postReportRepository;
    private readonly userRepository;
    private readonly followRepository;
    private readonly userRatingRepository;
    private readonly eventRepository;
    private readonly notificationService;
    private readonly mailerService;
    constructor(postRepository: Repository<Posts>, postImageRepository: Repository<PostImages>, postReviewRepository: Repository<PostReviews>, postLikeRepository: Repository<PostLikes>, postCommentRepository: Repository<PostComments>, postReportRepository: Repository<PostReports>, userRepository: Repository<Users>, followRepository: Repository<Followers>, userRatingRepository: Repository<UserRatings>, eventRepository: Repository<Events>, notificationService: NotificationService, mailerService: MailerService);
    homeFeed(logged_id: any, req: any): Promise<{
        posts: any[];
        total_posts: number;
        follow_users: any[];
        near_by_sessions: any[];
        total_near_by_sessions: number;
    }>;
    create(logged_id: number, dto: CreatePostDto): Promise<{
        message: string;
    }>;
    reviewPost(logged_id: number, dto: PostReviewDto): Promise<{
        message: string;
    }>;
    likePost(logged_id: number, dto: PostReviewDto): Promise<{
        message: string;
    }>;
    commentPost(logged_id: number, dto: PostReviewDto): Promise<{
        id: number;
        message: string;
    }>;
    reportPost(logged_id: number, dto: PostReviewDto): Promise<{
        message: string;
    }>;
    getComments(logged_id: any, req: any): Promise<{
        comments: any[];
        count: number;
    }>;
    getPost(logged_id: any, req: any): Promise<any>;
    getLikes(logged_id: any, req: any): Promise<{
        data: any[];
        count: number;
    }>;
    getRatings(logged_id: any, req: any): Promise<{
        data: any[];
        count: number;
    }>;
    myPosts(logged_id: any, req: any): Promise<{
        posts: any[];
        total_posts: number;
    }>;
    deletePost(logged_id: number, dto: PostReviewDto): Promise<{
        message: string;
    }>;
    deleteComment(logged_id: number, dto: PostReviewDto): Promise<{
        message: string;
    }>;
}
