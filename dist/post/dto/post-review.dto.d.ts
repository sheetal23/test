export declare class PostReviewDto {
    readonly id: number;
    readonly post_id: number;
    readonly review: string;
    readonly rating: number;
    readonly comment: string;
}
