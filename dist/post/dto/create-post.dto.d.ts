export declare class CreatePostDto {
    readonly id: number;
    readonly caption: string;
    readonly images: Array<string>;
    readonly data: Data[];
    readonly video: string;
    readonly image: string;
    readonly video_thumb: string;
    readonly location: string;
    readonly latitude: number;
    readonly longitude: number;
    readonly comment_enable: number;
}
export declare class Data {
    readonly type: number;
    readonly image: string;
    readonly video: string;
    readonly video_thumb: string;
}
