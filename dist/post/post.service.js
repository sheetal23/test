"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const user_entity_1 = require("../user/entities/user.entity");
const user_rating_entity_1 = require("../user/entities/user-rating.entity");
const event_entity_1 = require("../event/entities/event.entity");
const post_entity_1 = require("./entities/post.entity");
const post_image_entity_1 = require("./entities/post-image.entity");
const post_review_entity_1 = require("./entities/post-review.entity");
const post_like_entity_1 = require("./entities/post-like.entity");
const post_comment_entity_1 = require("./entities/post-comment.entity");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const http_exception_1 = require("@nestjs/common/exceptions/http.exception");
const common_2 = require("@nestjs/common");
const mailer_1 = require("@nest-modules/mailer");
const NodeGeocoder = require("node-geocoder");
const notification_service_1 = require("../notification/notification.service");
const post_report_entity_1 = require("./entities/post-report.entity");
const follow_entity_1 = require("../user/entities/follow.entity");
let PostService = class PostService {
    constructor(postRepository, postImageRepository, postReviewRepository, postLikeRepository, postCommentRepository, postReportRepository, userRepository, followRepository, userRatingRepository, eventRepository, notificationService, mailerService) {
        this.postRepository = postRepository;
        this.postImageRepository = postImageRepository;
        this.postReviewRepository = postReviewRepository;
        this.postLikeRepository = postLikeRepository;
        this.postCommentRepository = postCommentRepository;
        this.postReportRepository = postReportRepository;
        this.userRepository = userRepository;
        this.followRepository = followRepository;
        this.userRatingRepository = userRatingRepository;
        this.eventRepository = eventRepository;
        this.notificationService = notificationService;
        this.mailerService = mailerService;
    }
    homeFeed(logged_id, req) {
        return __awaiter(this, void 0, void 0, function* () {
            var latitude = req.query.latitude;
            var longitude = req.query.longitude;
            var location = req.query.location;
            var profile_lat = req.query.profile_lat;
            var profile_lng = req.query.profile_lng;
            var keyword = req.query.keyword;
            var position = req.query.position;
            var gender = req.query.gender;
            var current_team = req.query.current_team;
            var fav_nba_team = req.query.fav_nba_team;
            var min_age = req.query.min_age;
            var max_age = req.query.max_age;
            var min_height = req.query.min_height;
            var max_height = req.query.max_height;
            var min_blocks = req.query.min_blocks;
            var max_blocks = req.query.max_blocks;
            var min_steals = req.query.min_steals;
            var max_steals = req.query.max_steals;
            var min_passing = req.query.min_passing;
            var max_passing = req.query.max_passing;
            var min_rebounds = req.query.min_rebounds;
            var max_rebounds = req.query.max_rebounds;
            var min_shooting = req.query.min_shooting;
            var max_shooting = req.query.max_shooting;
            var min_dribbling = req.query.min_dribbling;
            var max_dribbling = req.query.max_dribbling;
            var min_passing = req.query.min_passing;
            var max_passing = req.query.max_passing;
            var min_rating = req.query.min_rating;
            var max_rating = req.query.max_rating;
            var time_zone = req.query.time_zone;
            var options = {
                provider: 'google',
                httpAdapter: 'https',
                apiKey: 'AIzaSyCG4I9ttJHiInDiSgN9bnGgr5wbm5bUrJM',
                formatter: 'json'
            };
            var geocoder = NodeGeocoder(options);
            var skip = 0;
            var limit = req.query.limit ? req.query.limit : 10;
            if (req.query.page) {
                const page = req.query.page;
                var skip = limit * page;
            }
            const current_time = new Date();
            var profile_location = "";
            if (profile_lat && profile_lng) {
                yield geocoder.reverse({ lat: profile_lat, lon: profile_lng })
                    .then(function (res) {
                    return profile_location = res[0].city;
                })
                    .catch(function (err) {
                    console.log(err);
                });
                yield typeorm_2.getConnection()
                    .createQueryBuilder()
                    .update(user_entity_1.Users)
                    .set({
                    location: profile_location,
                    latitude: profile_lat,
                    longitude: profile_lng,
                    updated_at: current_time
                })
                    .where("id = :user_id", { user_id: logged_id })
                    .execute();
            }
            if (logged_id != 0) {
                var session_range = yield typeorm_2.getRepository(user_entity_1.Users)
                    .createQueryBuilder("users")
                    .select("session_range")
                    .where("id=:logged_id", { logged_id: logged_id })
                    .getRawOne();
                var range = session_range.session_range;
                var following_users = [];
                var followingIds = yield typeorm_2.getRepository(follow_entity_1.Followers)
                    .createQueryBuilder("followers")
                    .select("user_id_2")
                    .where("user_id_1=:logged_id", { logged_id: logged_id })
                    .getRawMany();
                for (const i in followingIds) {
                    following_users.push(followingIds[i].user_id_2);
                }
            }
            else {
                var range = 200;
                following_users = [];
            }
            console.log("session_range");
            console.log(session_range);
            const follow = yield typeorm_2.getRepository(user_entity_1.Users)
                .createQueryBuilder("users")
                .select("id, user_name, first_name, last_name, IF(TIMESTAMPDIFF(MINUTE, users.`updated_at`,UTC_TIMESTAMP)>5,0,1) AS is_online, profile_image,position,(SELECT count(id) FROM followers WHERE user_id_1 = " + logged_id + " AND user_id_2=users.id ) AS is_follow")
                .addSelect("IFNULL(ROUND(( 6371 * acos (cos ( radians( " + latitude + " ) ) * cos( radians( users.latitude) )"
                + " * cos( radians( users.longitude ) - radians( " + longitude + " ) ) + sin ( radians( " + latitude + " ) )"
                + " * sin( radians( users.latitude ) ) ) ), 2), 0)", "distance")
                .where("(SELECT count(id) FROM followers WHERE user_id_1 = " + logged_id + " AND user_id_2=users.id ) =0")
                .andWhere("users.id !=" + logged_id)
                .having("distance <" + range)
                .orderBy("RAND()")
                .addOrderBy("distance", "ASC");
            const near_by = yield typeorm_2.getRepository(event_entity_1.Events)
                .createQueryBuilder("events")
                .leftJoinAndSelect(user_entity_1.Users, "users", "users.id = events.user_id")
                .select("users.id AS user_id, users.user_name,users.first_name,users.last_name,events.*,CONVERT_TZ(`events`.`date`,'+00:00', '" + time_zone + "') AS date,CONVERT_TZ(`events`.`from_time`,'+00:00', '" + time_zone + "') AS from_time , CONVERT_TZ(`events`.`to_time`,'+00:00', '" + time_zone + "') AS to_time  ,(SELECT COUNT(id) FROM `event_members` where event_id=events.id AND is_join=1) AS joined_members_count, (SELECT is_join FROM `event_members` WHERE event_id=events.id AND member_id=" + logged_id + ") AS membershipConfirmed, IF(UTC_TIMESTAMP() BETWEEN events.from_time AND events.to_time, 1, 0) as is_ongoing ,IF(UTC_TIMESTAMP() > events.to_time, 1, 0) as is_completed, IF(UTC_TIMESTAMP() < events.from_time, 1, 0) as is_pending,(SELECT ROUND(AVG(rating),1) FROM `event_reviews` where event_id = events.id AND rating IS NOT NULL) AS rating")
                .addSelect("IFNULL(ROUND(( 6371 * acos (cos ( radians( " + latitude + " ) ) * cos( radians( events.latitude) )"
                + " * cos( radians( events.longitude ) - radians( " + longitude + " ) ) + sin ( radians( " + latitude + " ) )"
                + " * sin( radians( events.latitude ) ) ) ), 2), 0)", "distance")
                .where("events.deleted_at is NULL AND  events.cancelled_at is NULL AND events.is_notif=0")
                .andWhere(" (IF(UTC_TIMESTAMP() < events.from_time, 1, 0) =1)")
                .andWhere("((SELECT Count(id) FROM event_members where event_members.event_id = events.id AND event_members.member_id = " + logged_id + " AND event_members.is_join = 1) = 0)")
                .andWhere(" IFNULL(ROUND(( 6371 * acos (cos ( radians( " + latitude + " ) ) * cos( radians( events.latitude) )"
                + " * cos( radians( events.longitude ) - radians( " + longitude + " ) ) + sin ( radians( " + latitude + " ) )"
                + " * sin( radians( events.latitude ) ) ) ), 2), 0)<" + range)
                .orderBy("distance", "ASC")
                .addOrderBy("events.created_at", "DESC");
            const qb = yield typeorm_2.getRepository(post_entity_1.Posts)
                .createQueryBuilder("posts")
                .leftJoinAndSelect(user_entity_1.Users, "users", "users.id = posts.user_id")
                .select("users.id AS user_id, users.user_name,users.first_name,users.last_name, users.height, users.profile_image,users.position,IF(TIMESTAMPDIFF(MINUTE, users.`updated_at`,UTC_TIMESTAMP)>5,0,1) AS is_online,users.fav_nba_team,users.gender,DATE_FORMAT(users.age, '%Y-%m-%d') AS age, posts.caption, posts.id, posts.location, posts.latitude, posts.longitude ,posts.comment_enable, DATE(posts.created_at) as date,DATEDIFF(UTC_TIMESTAMP(), posts.created_at) AS days,posts.updated_at,"
                + "IF(posts.user_id=" + logged_id + ",1,0) AS my_post,"
                + "(SELECT ROUND(AVG(passing_ability),1) FROM user_ratings where user_id_2=users.id AND passing_ability IS NOT NULL) AS passing_ability ,"
                + " (SELECT ROUND(AVG(shooting_ability),1) FROM user_ratings where user_id_2=users.id AND shooting_ability IS NOT NULL) AS shooting_ability ,"
                + "( SELECT ROUND(AVG(dribbling_ability),1) FROM user_ratings where user_id_2=users.id AND dribbling_ability IS NOT NULL) As dribbling_ability,"
                + "( SELECT ROUND(AVG(steals),1) FROM user_ratings where user_id_2=users.id AND steals IS NOT NULL) AS steals,"
                + "( SELECT ROUND(AVG(rebounds),1) FROM user_ratings where user_id_2=users.id AND rebounds IS NOT NULL) AS rebounds ,"
                + " ( SELECT ROUND(AVG(blocks),1) FROM user_ratings where user_id_2=users.id AND blocks IS NOT NULL) AS blocks,"
                + "(SELECT count(id) FROM post_likes WHERE post_id=posts.id AND user_id=" + logged_id + ") AS is_liked,(SELECT count(id) FROM post_likes WHERE post_id=posts.id) AS likes,(SELECT count(id) FROM post_reviews WHERE post_id=posts.id AND user_id=" + logged_id + ") AS is_rated,(SELECT rating FROM post_reviews WHERE post_id=posts.id AND user_id=" + logged_id + ") AS rating,(SELECT review FROM post_reviews WHERE post_id=posts.id AND user_id=" + logged_id + ") AS review, (SELECT count(id) FROM post_reviews WHERE post_id=posts.id) AS ratings,(SELECT count(id) FROM post_comments WHERE post_id=posts.id) AS total_comments")
                .addSelect(" ( CASE "
                + " WHEN TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, posts.`created_at`) != 0 THEN"
                + " IF(abs(TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, posts.`created_at`)) > 1 , CONCAT(abs(TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, posts.`created_at`)) ,' yrs ago'), CONCAT(abs(TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, posts.`created_at`)) ,' yr ago'))"
                + " WHEN TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, posts.`created_at`) != 0 THEN"
                + " IF(abs(TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, posts.`created_at`)) > 1 , CONCAT(abs(TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, posts.`created_at`)) ,' months ago'), CONCAT(abs(TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, posts.`created_at`)) ,' month ago'))"
                + " WHEN HOUR(TIMEDIFF(UTC_TIMESTAMP, posts.`created_at`)) != 0 THEN"
                + " IF(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, posts.`created_at`))) < 24 , IF(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, posts.`created_at`))) > 1 , CONCAT(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, posts.`created_at`))) ,' hrs ago'), CONCAT(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, posts.`created_at`))) ,' hr ago')),"
                + " IF(DATEDIFF(UTC_TIMESTAMP, posts.`created_at`) > 1 , CONCAT(DATEDIFF(UTC_TIMESTAMP, posts.`created_at`) ,' days ago'), CONCAT(DATEDIFF(UTC_TIMESTAMP, posts.`created_at`) ,' day ago')))"
                + " WHEN MINUTE(TIMEDIFF(UTC_TIMESTAMP, posts.`created_at`)) != 0 THEN CONCAT(MINUTE(TIMEDIFF(UTC_TIMESTAMP, posts.`created_at`)) ,' min ago')"
                + " ELSE "
                + " CONCAT(SECOND(TIMEDIFF(UTC_TIMESTAMP, posts.`created_at`)) ,'s ago')"
                + " END  )", "posted_since");
            if (following_users.length > 0) {
                qb.where("((posts.user_id IN (:following_users)) OR (posts.user_id=:logged_id))", { following_users: following_users, logged_id: logged_id });
            }
            else {
                qb.where("(posts.user_id=:logged_id)", { logged_id: logged_id });
            }
            if (keyword) {
                qb.andWhere("(users.user_name like :keyword OR users.first_name like :keyword OR users.last_name like :keyword OR users.email like :keyword)", { keyword: '%' + keyword + '%' });
                near_by.andWhere("(users.user_name like :keyword OR users.first_name like :keyword OR users.last_name like :keyword OR users.email like :keyword)", { keyword: '%' + keyword + '%' });
                follow.andWhere("(users.user_name like :keyword OR users.first_name like :keyword OR users.last_name like :keyword OR users.email like :keyword)", { keyword: '%' + keyword + '%' });
            }
            if (latitude && longitude) {
                qb.andWhere("((IFNULL(ROUND(( 6371 * acos (cos ( radians( " + latitude + " ) ) * cos( radians( posts.latitude) )"
                    + " * cos( radians( posts.longitude ) - radians( " + longitude + " ) ) + sin ( radians( " + latitude + " ) )"
                    + " * sin( radians( posts.latitude ) ) ) ), 2), 0) < :session_range) OR (posts.user_id= :logged_id))", { session_range: range, logged_id: logged_id });
            }
            if (current_team) {
                qb.andWhere("(users.current_team like :current_team)", { current_team: '%' + current_team + '%' });
            }
            if (gender) {
                qb.andWhere('(users.gender = :gender)', { gender: gender });
            }
            if (position) {
                qb.andWhere('(users.position like :position)', { position: '%' + position + '%' });
            }
            if (fav_nba_team) {
                qb.andWhere('(users.fav_nba_team=:fav_nba_team)', { fav_nba_team: fav_nba_team });
            }
            if (min_rating && max_rating) {
                if (min_rating != 0 && max_rating != 100) {
                    qb.andWhere("(((SELECT ROUND(AVG(passing_ability),1) FROM user_ratings where user_id_2=users.id AND passing_ability IS NOT NULL) + (SELECT ROUND(AVG(shooting_ability),1) FROM user_ratings where user_id_2=users.id AND shooting_ability IS NOT NULL) +( SELECT ROUND(AVG(dribbling_ability),1) FROM user_ratings where user_id_2=users.id AND dribbling_ability IS NOT NULL)+ ( SELECT ROUND(AVG(steals),1) FROM user_ratings where user_id_2=users.id AND steals IS NOT NULL)+( SELECT ROUND(AVG(rebounds),1) FROM user_ratings where user_id_2=users.id AND rebounds IS NOT NULL)+( SELECT ROUND(AVG(blocks),1) FROM user_ratings where user_id_2=users.id AND blocks IS NOT NULL))/6 BETWEEN " + min_rating + " AND " + max_rating + ")");
                }
            }
            if (min_age && max_age) {
                qb.andWhere("(YEAR(CURDATE()) - YEAR(users.age) BETWEEN " + min_age + " AND " + max_age + ")");
            }
            if (min_height && max_height) {
                qb.andWhere("(users.height BETWEEN " + min_height + " AND " + max_height + ")");
            }
            if (min_blocks && max_blocks) {
                if (min_blocks != 0 && max_blocks != 5) {
                    qb.andWhere("((SELECT ROUND(AVG(blocks),1) FROM user_ratings where user_id_2=users.id AND blocks IS NOT NULL) BETWEEN " + min_blocks + " AND " + max_blocks + ")");
                }
            }
            if (min_steals && max_steals) {
                if (min_steals != 0 && max_steals != 5) {
                    qb.andWhere("((SELECT ROUND(AVG(steals),1) FROM user_ratings where user_id_2=users.id AND steals IS NOT NULL) BETWEEN " + min_steals + " AND " + min_steals + ")");
                }
            }
            if (min_rebounds && max_rebounds) {
                if (min_rebounds != 0 && max_rebounds != 5) {
                    qb.andWhere("(( SELECT ROUND(AVG(rebounds),1) FROM user_ratings where user_id_2=users.id AND rebounds IS NOT NULL) BETWEEN " + min_rebounds + " AND " + max_rebounds + ")");
                }
            }
            if (min_dribbling && max_dribbling) {
                if (min_dribbling != 0 && max_dribbling != 5) {
                    qb.andWhere("(( SELECT ROUND(AVG(dribbling_ability),1) FROM user_ratings where user_id_2=users.id AND dribbling_ability IS NOT NULL) BETWEEN " + min_dribbling + " AND " + max_dribbling + ")");
                }
            }
            if (min_shooting && max_shooting) {
                if (min_shooting != 0 && max_shooting != 5) {
                    qb.andWhere("(( SELECT ROUND(AVG(shooting_ability),1) FROM user_ratings where user_id_2=users.id AND shooting_ability IS NOT NULL)  BETWEEN " + min_shooting + " AND " + max_shooting + ")");
                }
            }
            if (min_passing && max_passing) {
                if (min_passing != 0 && max_passing != 5) {
                    qb.andWhere("(( SELECT ROUND(AVG(passing_ability),1) FROM user_ratings where user_id_2=users.id AND passing_ability IS NOT NULL) BETWEEN " + min_passing + " AND " + max_passing + ")");
                }
            }
            const posts = yield qb.andWhere("(DATEDIFF(CURRENT_TIMESTAMP(), posts.created_at)<=7)").orderBy("my_post", "DESC").addOrderBy("days", "DESC").addOrderBy("IFNULL((SELECT ROUND(AVG(rating),1) FROM post_reviews where post_id=posts.id),0)", "DESC").addOrderBy("posts.created_at", "DESC").offset(skip).limit(limit).getRawMany();
            const total_posts = yield qb.getCount();
            for (const i in posts) {
                posts[i].comments = yield typeorm_2.getRepository(post_comment_entity_1.PostComments)
                    .createQueryBuilder("post_comments")
                    .leftJoinAndSelect(user_entity_1.Users, "users", "users.id = post_comments.user_id")
                    .select("users.id AS user_id, users.user_name ,users.first_name, users.last_name, users.profile_image,IF(TIMESTAMPDIFF(MINUTE, users.`updated_at`,UTC_TIMESTAMP)>5,0,1) AS is_online, users.position,post_comments.id, post_comments.comment")
                    .addSelect(" ( CASE "
                    + " WHEN TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, post_comments.`created_at`) != 0 THEN"
                    + " IF(abs(TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, post_comments.`created_at`)) > 1 , CONCAT(abs(TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, post_comments.`created_at`)) ,' yrs ago'), CONCAT(abs(TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, post_comments.`created_at`)) ,' yr ago'))"
                    + " WHEN TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, post_comments.`created_at`) != 0 THEN"
                    + " IF(abs(TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, post_comments.`created_at`)) > 1 , CONCAT(abs(TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, post_comments.`created_at`)) ,' months ago'), CONCAT(abs(TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, post_comments.`created_at`)) ,' month ago'))"
                    + " WHEN HOUR(TIMEDIFF(UTC_TIMESTAMP, post_comments.`created_at`)) != 0 THEN"
                    + " IF(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, post_comments.`created_at`))) < 24 , IF(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, post_comments.`created_at`))) > 1 , CONCAT(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, post_comments.`created_at`))) ,' hrs ago'), CONCAT(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, post_comments.`created_at`))) ,' hr ago')),"
                    + " IF(DATEDIFF(UTC_TIMESTAMP, post_comments.`created_at`) > 1 , CONCAT(DATEDIFF(UTC_TIMESTAMP, post_comments.`created_at`) ,' days ago'), CONCAT(DATEDIFF(UTC_TIMESTAMP, post_comments.`created_at`) ,' day ago')))"
                    + " WHEN MINUTE(TIMEDIFF(UTC_TIMESTAMP, post_comments.`created_at`)) != 0 THEN CONCAT(MINUTE(TIMEDIFF(UTC_TIMESTAMP, post_comments.`created_at`)) ,' min ago')"
                    + " ELSE "
                    + " CONCAT(SECOND(TIMEDIFF(UTC_TIMESTAMP, post_comments.`created_at`)) ,'s ago')"
                    + " END  )", "commented_since")
                    .where('post_comments.post_id = :post_id', { post_id: posts[i].id })
                    .orderBy("post_comments.id", "DESC")
                    .limit(1)
                    .getRawMany();
                const all_media = yield typeorm_2.getRepository(post_image_entity_1.PostImages)
                    .createQueryBuilder("post_images")
                    .select("*")
                    .where('post_images.post_id = :post_id', { post_id: posts[i].id })
                    .getRawMany();
                posts[i].media = all_media;
            }
            const near_by_sessions = yield near_by.offset(skip).limit(limit).getRawMany();
            for (const i in near_by_sessions) {
                if (near_by_sessions[i].is_completed == 1) {
                    near_by_sessions[i].status = 3;
                }
                if (near_by_sessions[i].is_ongoing == 1) {
                    near_by_sessions[i].status = 2;
                }
                if (near_by_sessions[i].is_pending == 1) {
                    near_by_sessions[i].status = 1;
                }
            }
            const total_near_by_sessions = yield near_by.getCount();
            var follow_users = yield follow.getRawMany();
            console.log(posts, follow_users, near_by_sessions);
            return { posts, total_posts, follow_users, near_by_sessions, total_near_by_sessions };
        });
    }
    create(logged_id, dto) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id, caption, video, video_thumb, location, latitude, longitude, comment_enable, image, data, images } = dto;
            const eventMembers = [];
            if (id) {
                const current_time = new Date();
                const qb = yield this.postRepository.findOne(id);
                yield typeorm_2.getConnection()
                    .createQueryBuilder()
                    .update(post_entity_1.Posts)
                    .set({ caption: caption,
                    location: location ? location : qb.location,
                    latitude: latitude ? latitude : qb.latitude,
                    longitude: longitude ? longitude : qb.longitude,
                    comment_enable: comment_enable ? comment_enable : qb.comment_enable,
                    updated_at: current_time
                })
                    .where("id = :id", { id: id })
                    .execute();
            }
            else {
                try {
                    let newPost = new post_entity_1.Posts();
                    newPost.user_id = logged_id;
                    newPost.caption = caption;
                    newPost.location = location;
                    newPost.latitude = latitude;
                    newPost.longitude = longitude;
                    newPost.comment_enable = comment_enable;
                    const savedPost = yield this.postRepository.save(newPost);
                    const post_images = [];
                    for (const i in data) {
                        const img = new post_image_entity_1.PostImages();
                        img.post_id = id ? id : newPost.id;
                        img.image = data[i].image;
                        img.video = data[i].video;
                        img.type = data[i].type;
                        img.video_thumb = data[i].video_thumb;
                        post_images.push(img);
                    }
                    yield this.postImageRepository.save(post_images);
                }
                catch (err) {
                    throw new http_exception_1.HttpException({ error: 'bad_request', message: 'Invalid Input' }, common_2.HttpStatus.BAD_REQUEST);
                }
            }
            return ({ message: "Post added" });
        });
    }
    reviewPost(logged_id, dto) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id, post_id, rating, review } = dto;
            const current_time = new Date();
            var user = yield this.userRepository.findOne({
                where: {
                    id: logged_id
                }
            });
            const reviewed = yield this.postReviewRepository.findOne({ post_id: post_id, user_id: logged_id });
            if (reviewed) {
                yield typeorm_2.getConnection()
                    .createQueryBuilder()
                    .update(post_review_entity_1.PostReviews)
                    .set({
                    rating: rating ? rating : reviewed.rating,
                    review: review ? review : reviewed.review,
                    updated_at: current_time
                })
                    .where("user_id = :user_id", { user_id: logged_id })
                    .andWhere("post_id = :post_id", { post_id: post_id })
                    .andWhere("id = :id", { id: reviewed.id })
                    .execute();
                var notifMessage = user.first_name + " " + user.last_name + " edit review for your post";
            }
            else {
                const qb = yield typeorm_2.getRepository(post_review_entity_1.PostReviews)
                    .createQueryBuilder("post_reviews")
                    .insert()
                    .into(post_review_entity_1.PostReviews).values({ post_id: post_id,
                    user_id: logged_id,
                    rating: rating,
                    review: review })
                    .execute();
                var notifMessage = user.first_name + " " + user.last_name + " reviewed your post";
            }
            console.log("start");
            var post = yield typeorm_2.getRepository(post_entity_1.Posts)
                .createQueryBuilder("posts")
                .select("user_id")
                .where("id=:post_id", { post_id: post_id })
                .getRawOne();
            if (logged_id != post.user_id) {
                this.notificationService.save(25, logged_id, null, post_id, null, notifMessage, null, post.user_id);
            }
            return { message: "successful" };
        });
    }
    likePost(logged_id, dto) {
        return __awaiter(this, void 0, void 0, function* () {
            const { post_id } = dto;
            var user = yield this.userRepository.findOne({
                where: {
                    id: logged_id
                }
            });
            const like = yield typeorm_2.getRepository(post_like_entity_1.PostLikes)
                .createQueryBuilder("post_likes")
                .where("post_likes.user_id= :user_id", { user_id: logged_id })
                .andWhere("post_likes.post_id= :post_id", { post_id: post_id })
                .getOne();
            if (like) {
                yield typeorm_2.getRepository(post_like_entity_1.PostLikes)
                    .createQueryBuilder("post_likes")
                    .delete()
                    .from(post_like_entity_1.PostLikes)
                    .where("post_likes.user_id= :user_id", { user_id: logged_id })
                    .andWhere("post_likes.post_id= :post_id", { post_id: post_id })
                    .execute();
            }
            else {
                let like = new post_like_entity_1.PostLikes();
                like.user_id = logged_id;
                like.post_id = post_id;
                var notifMessage = user.first_name + " " + user.last_name + " liked your post";
                const savedReview = yield this.postLikeRepository.save(like);
                console.log("start");
                var post = yield typeorm_2.getRepository(post_entity_1.Posts)
                    .createQueryBuilder("posts")
                    .select("user_id")
                    .where("id=:post_id", { post_id: post_id })
                    .getRawOne();
                if (logged_id != post.user_id) {
                    this.notificationService.save(23, logged_id, null, post_id, null, notifMessage, null, post.user_id);
                }
            }
            return { message: "successful" };
        });
    }
    commentPost(logged_id, dto) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id, post_id, comment } = dto;
            const current_time = new Date();
            var user = yield this.userRepository.findOne({
                where: {
                    id: logged_id
                }
            });
            const qb = yield this.postCommentRepository.findOne(id);
            if (id) {
                var comment_id = id;
                yield typeorm_2.getConnection()
                    .createQueryBuilder()
                    .update(post_comment_entity_1.PostComments)
                    .set({
                    comment: comment ? comment : qb.comment,
                    updated_at: current_time
                })
                    .where("id = :id", { id: id })
                    .execute();
            }
            else {
                let new_comment = new post_comment_entity_1.PostComments();
                new_comment.post_id = post_id;
                new_comment.user_id = logged_id;
                new_comment.comment = comment;
                const savedComment = yield this.postCommentRepository.save(new_comment);
                var comment_id = savedComment.id;
            }
            console.log("start");
            var notifMessage = user.first_name + " " + user.last_name + " commented on your post";
            var post = yield typeorm_2.getRepository(post_entity_1.Posts)
                .createQueryBuilder("posts")
                .select("user_id")
                .where("id=:post_id", { post_id: post_id })
                .getRawOne();
            console.log(post);
            if (logged_id != post.user_id) {
                this.notificationService.save(24, logged_id, null, post_id, null, notifMessage, null, post.user_id);
            }
            return { id: id ? id : comment_id, message: "successful" };
        });
    }
    reportPost(logged_id, dto) {
        return __awaiter(this, void 0, void 0, function* () {
            const { post_id } = dto;
            const report = yield typeorm_2.getRepository(post_report_entity_1.PostReports)
                .createQueryBuilder("post_reports")
                .where("user_id = :user_id", { user_id: logged_id })
                .andWhere("post_id = :post_id", { post_id: post_id })
                .getOne();
            if (report) {
                throw new http_exception_1.HttpException({ error: 'bad_request', message: 'You have already reported for this post' }, common_2.HttpStatus.BAD_REQUEST);
            }
            else {
                let report = new post_report_entity_1.PostReports();
                report.user_id = logged_id;
                report.post_id = post_id;
                const savedReport = yield this.postReportRepository.save(report);
            }
            return { message: "successfully reported" };
        });
    }
    getComments(logged_id, req) {
        return __awaiter(this, void 0, void 0, function* () {
            var post_id = req.query.id;
            var last_comment_id = req.query.last_comment_id;
            var skip = 0;
            var limit = req.query.limit ? req.query.limit : 10;
            if (req.query.page) {
                const page = req.query.page;
                var skip = limit * page;
            }
            const qb = yield typeorm_2.getRepository(post_comment_entity_1.PostComments)
                .createQueryBuilder("post_comments")
                .leftJoinAndSelect(user_entity_1.Users, "users", "users.id = post_comments.user_id")
                .select("users.id AS user_id, users.user_name,users.first_name, users.last_name, users.profile_image,IF(TIMESTAMPDIFF(MINUTE, users.`updated_at`,UTC_TIMESTAMP)>5,0,1) AS is_online, users.position,post_comments.id, post_comments.comment")
                .addSelect(" ( CASE "
                + " WHEN TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, post_comments.`created_at`) != 0 THEN"
                + " IF(abs(TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, post_comments.`created_at`)) > 1 , CONCAT(abs(TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, post_comments.`created_at`)) ,' yrs ago'), CONCAT(abs(TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, post_comments.`created_at`)) ,' yr ago'))"
                + " WHEN TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, post_comments.`created_at`) != 0 THEN"
                + " IF(abs(TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, post_comments.`created_at`)) > 1 , CONCAT(abs(TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, post_comments.`created_at`)) ,' months ago'), CONCAT(abs(TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, post_comments.`created_at`)) ,' month ago'))"
                + " WHEN HOUR(TIMEDIFF(UTC_TIMESTAMP, post_comments.`created_at`)) != 0 THEN"
                + " IF(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, post_comments.`created_at`))) < 24 , IF(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, post_comments.`created_at`))) > 1 , CONCAT(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, post_comments.`created_at`))) ,' hrs ago'), CONCAT(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, post_comments.`created_at`))) ,' hr ago')),"
                + " IF(DATEDIFF(UTC_TIMESTAMP, post_comments.`created_at`) > 1 , CONCAT(DATEDIFF(UTC_TIMESTAMP, post_comments.`created_at`) ,' days ago'), CONCAT(DATEDIFF(UTC_TIMESTAMP, post_comments.`created_at`) ,' day ago')))"
                + " WHEN MINUTE(TIMEDIFF(UTC_TIMESTAMP, post_comments.`created_at`)) != 0 THEN CONCAT(MINUTE(TIMEDIFF(UTC_TIMESTAMP, post_comments.`created_at`)) ,' min ago')"
                + " ELSE "
                + " CONCAT(SECOND(TIMEDIFF(UTC_TIMESTAMP, post_comments.`created_at`)) ,'s ago')"
                + " END  )", "commented_since")
                .where("post_comments.post_id=" + post_id)
                .orderBy("post_comments.id", "DESC");
            if (last_comment_id) {
                qb.andWhere("post_comments.id<" + last_comment_id);
            }
            const comments = yield qb.offset(skip).limit(limit).getRawMany();
            const count = yield qb.getCount();
            return { comments, count };
        });
    }
    getPost(logged_id, req) {
        return __awaiter(this, void 0, void 0, function* () {
            var post_id = req.query.id;
            this.notificationService.readNotifEvent(logged_id, null, null, post_id);
            const data = yield typeorm_2.getRepository(post_entity_1.Posts)
                .createQueryBuilder("posts")
                .leftJoinAndSelect(user_entity_1.Users, "users", "users.id = posts.user_id")
                .select("users.id AS user_id, users.user_name, users.first_name, users.last_name, IF(TIMESTAMPDIFF(MINUTE, users.`updated_at`,UTC_TIMESTAMP)>5,0,1) AS is_online, users.profile_image, users.position, posts.caption,posts.comment_enable, posts.id, posts.location, posts.latitude, posts.longitude ,posts.updated_at,(SELECT count(id) FROM post_likes WHERE post_id=" + post_id + " AND user_id=" + logged_id + ") AS is_liked,(SELECT count(id) FROM post_likes WHERE post_id=" + post_id + ") AS likes,(SELECT count(id) FROM post_reviews WHERE rating IS NOT NULL AND  post_id=" + post_id + " AND user_id=" + logged_id + ") AS is_rated, (SELECT count(id) FROM post_reviews WHERE rating IS NOT NULL AND post_id=" + post_id + ") AS ratings, (SELECT count(id) FROM post_comments WHERE post_id=" + post_id + ") AS total_comments,(SELECT rating FROM post_reviews WHERE post_id=posts.id AND user_id=" + logged_id + ") AS rating")
                .addSelect(" ( CASE "
                + " WHEN TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, posts.`created_at`) != 0 THEN"
                + " IF(abs(TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, posts.`created_at`)) > 1 , CONCAT(abs(TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, posts.`created_at`)) ,' yrs ago'), CONCAT(abs(TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, posts.`created_at`)) ,' yr ago'))"
                + " WHEN TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, posts.`created_at`) != 0 THEN"
                + " IF(abs(TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, posts.`created_at`)) > 1 , CONCAT(abs(TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, posts.`created_at`)) ,' months ago'), CONCAT(abs(TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, posts.`created_at`)) ,' month ago'))"
                + " WHEN HOUR(TIMEDIFF(UTC_TIMESTAMP, posts.`created_at`)) != 0 THEN"
                + " IF(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, posts.`created_at`))) < 24 , IF(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, posts.`created_at`))) > 1 , CONCAT(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, posts.`created_at`))) ,' hrs ago'), CONCAT(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, posts.`created_at`))) ,' hr ago')),"
                + " IF(DATEDIFF(UTC_TIMESTAMP, posts.`created_at`) > 1 , CONCAT(DATEDIFF(UTC_TIMESTAMP, posts.`created_at`) ,' days ago'), CONCAT(DATEDIFF(UTC_TIMESTAMP, posts.`created_at`) ,' day ago')))"
                + " WHEN MINUTE(TIMEDIFF(UTC_TIMESTAMP, posts.`created_at`)) != 0 THEN CONCAT(MINUTE(TIMEDIFF(UTC_TIMESTAMP, posts.`created_at`)) ,' min ago')"
                + " ELSE "
                + " CONCAT(SECOND(TIMEDIFF(UTC_TIMESTAMP, posts.`created_at`)) ,'s ago')"
                + " END  )", "posted_since")
                .where("posts.id=" + post_id)
                .getRawOne();
            const comments = yield typeorm_2.getRepository(post_comment_entity_1.PostComments)
                .createQueryBuilder("post_comments")
                .leftJoinAndSelect(user_entity_1.Users, "users", "users.id = post_comments.user_id")
                .select("users.id, users.user_name,users.first_name, users.last_name,IF(TIMESTAMPDIFF(MINUTE, users.`updated_at`,UTC_TIMESTAMP)>5,0,1) AS is_online, users.profile_image, users.position,post_comments.id, post_comments.comment")
                .addSelect(" ( CASE "
                + " WHEN TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, post_comments.`created_at`) != 0 THEN"
                + " IF(abs(TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, post_comments.`created_at`)) > 1 , CONCAT(abs(TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, post_comments.`created_at`)) ,' years ago'), CONCAT(abs(TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, post_comments.`created_at`)) ,' year ago'))"
                + " WHEN TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, post_comments.`created_at`) != 0 THEN"
                + " IF(abs(TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, post_comments.`created_at`)) > 1 , CONCAT(abs(TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, post_comments.`created_at`)) ,' months ago'), CONCAT(abs(TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, post_comments.`created_at`)) ,' month ago'))"
                + " WHEN HOUR(TIMEDIFF(UTC_TIMESTAMP, post_comments.`created_at`)) != 0 THEN"
                + " IF(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, post_comments.`created_at`))) < 24 , IF(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, post_comments.`created_at`))) > 1 , CONCAT(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, post_comments.`created_at`))) ,' hrs ago'), CONCAT(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, post_comments.`created_at`))) ,' hr ago')),"
                + " IF(DATEDIFF(UTC_TIMESTAMP, post_comments.`created_at`) > 1 , CONCAT(DATEDIFF(UTC_TIMESTAMP, post_comments.`created_at`) ,' days ago'), CONCAT(DATEDIFF(UTC_TIMESTAMP, post_comments.`created_at`) ,' day ago')))"
                + " WHEN MINUTE(TIMEDIFF(UTC_TIMESTAMP, post_comments.`created_at`)) != 0 THEN CONCAT(MINUTE(TIMEDIFF(UTC_TIMESTAMP, post_comments.`created_at`)) ,' min ago')"
                + " ELSE "
                + " CONCAT(SECOND(TIMEDIFF(UTC_TIMESTAMP, post_comments.`created_at`)) ,'s ago')"
                + " END  )", "commented_since")
                .where("post_comments.post_id=" + post_id)
                .orderBy("post_comments.id", "DESC")
                .limit(1)
                .getRawMany();
            const media = yield typeorm_2.getRepository(post_image_entity_1.PostImages)
                .createQueryBuilder("post_images")
                .select("*")
                .where('post_images.post_id = :post_id', { post_id: post_id })
                .getRawMany();
            let detail = data;
            detail.comments = comments;
            detail.media = media;
            return detail;
        });
    }
    getLikes(logged_id, req) {
        return __awaiter(this, void 0, void 0, function* () {
            var post_id = req.query.id;
            var skip = 0;
            var limit = req.query.limit ? req.query.limit : 10;
            if (req.query.page) {
                const page = req.query.page;
                var skip = limit * page;
            }
            const qb = yield typeorm_2.getRepository(post_like_entity_1.PostLikes)
                .createQueryBuilder("post_likes")
                .leftJoinAndSelect(user_entity_1.Users, "users", "users.id = post_likes.user_id")
                .select("users.id AS user_id, users.user_name,users.first_name,IF(TIMESTAMPDIFF(MINUTE, users.`updated_at`,UTC_TIMESTAMP)>5,0,1) AS is_online, users.last_name, users.profile_image, users.position,post_likes.id,(SELECT count(id) FROM followers WHERE user_id_1=" + logged_id + " AND user_id_2=post_likes.user_id) AS is_follow")
                .where("post_likes.post_id=:post_id", { post_id: post_id })
                .orderBy("post_likes.id", "DESC");
            const data = yield qb.offset(skip).limit(limit).getRawMany();
            const count = yield qb.getCount();
            return { data, count };
        });
    }
    getRatings(logged_id, req) {
        return __awaiter(this, void 0, void 0, function* () {
            var post_id = req.query.id;
            var skip = 0;
            var limit = req.query.limit ? req.query.limit : 10;
            if (req.query.page) {
                const page = req.query.page;
                var skip = limit * page;
            }
            const qb = yield typeorm_2.getRepository(post_review_entity_1.PostReviews)
                .createQueryBuilder("post_reviews")
                .leftJoinAndSelect(user_entity_1.Users, "users", "users.id = post_reviews.user_id")
                .select("users.id AS user_id, users.user_name,users.first_name, users.last_name, users.profile_image,IF(TIMESTAMPDIFF(MINUTE, users.`updated_at`,UTC_TIMESTAMP)>5,0,1) AS is_online, users.position, post_reviews.id, post_reviews.rating,(SELECT count(id) FROM followers WHERE user_id_1=" + logged_id + " AND user_id_2=post_reviews.user_id) AS is_follow")
                .where("post_reviews.post_id=" + post_id)
                .orderBy("post_reviews.id", "DESC");
            const data = yield qb.offset(skip).limit(limit).getRawMany();
            const count = yield qb.getCount();
            return { data, count };
        });
    }
    myPosts(logged_id, req) {
        return __awaiter(this, void 0, void 0, function* () {
            var skip = 0;
            var limit = req.query.limit ? req.query.limit : 10;
            if (req.query.page) {
                const page = req.query.page;
                var skip = limit * page;
            }
            const qb = yield typeorm_2.getRepository(post_entity_1.Posts)
                .createQueryBuilder("posts")
                .leftJoinAndSelect(user_entity_1.Users, "users", "users.id = posts.user_id")
                .select("users.id AS user_id, users.user_name,users.first_name,users.last_name, users.height, users.profile_image, users.position, IF(TIMESTAMPDIFF(MINUTE, users.`updated_at`,UTC_TIMESTAMP)>5,0,1) AS is_online,users.fav_nba_team,users.gender, posts.caption, posts.id, posts.location, posts.latitude, posts.longitude ,posts.comment_enable, DATE(posts.created_at) as date,posts.updated_at,(select image from post_images where post_id=posts.id LIMIT 1) AS image,"
                + "(SELECT ROUND(AVG(passing_ability),1) FROM user_ratings where user_id_2=users.id AND passing_ability IS NOT NULL) AS passing_ability ,"
                + " (SELECT ROUND(AVG(shooting_ability),1) FROM user_ratings where user_id_2=users.id AND shooting_ability IS NOT NULL) AS shooting_ability ,"
                + "( SELECT ROUND(AVG(dribbling_ability),1) FROM user_ratings where user_id_2=users.id AND dribbling_ability IS NOT NULL) As dribbling_ability,"
                + "( SELECT ROUND(AVG(steals),1) FROM user_ratings where user_id_2=users.id AND steals IS NOT NULL) AS steals,"
                + "( SELECT ROUND(AVG(rebounds),1) FROM user_ratings where user_id_2=users.id AND rebounds IS NOT NULL) AS rebounds ,"
                + " ( SELECT ROUND(AVG(blocks),1) FROM user_ratings where user_id_2=users.id AND blocks IS NOT NULL) AS blocks,"
                + "(SELECT count(id) FROM post_likes WHERE post_id=posts.id AND user_id=" + logged_id + ") AS is_liked,(SELECT count(id) FROM post_likes WHERE post_id=posts.id) AS likes,(SELECT count(id) FROM post_reviews WHERE post_id=posts.id AND user_id=" + logged_id + ") AS is_rated,(SELECT rating FROM post_reviews WHERE post_id=posts.id AND user_id=" + logged_id + ") AS rating,(SELECT review FROM post_reviews WHERE post_id=posts.id AND user_id=" + logged_id + ") AS review, (SELECT count(id) FROM post_reviews WHERE post_id=posts.id) AS ratings,(SELECT count(id) FROM post_comments WHERE post_id=posts.id) AS total_comments")
                .addSelect(" ( CASE "
                + " WHEN TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, posts.`created_at`) != 0 THEN"
                + " IF(abs(TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, posts.`created_at`)) > 1 , CONCAT(abs(TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, posts.`created_at`)) ,' yrs ago'), CONCAT(abs(TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, posts.`created_at`)) ,' yr ago'))"
                + " WHEN TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, posts.`created_at`) != 0 THEN"
                + " IF(abs(TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, posts.`created_at`)) > 1 , CONCAT(abs(TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, posts.`created_at`)) ,' months ago'), CONCAT(abs(TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, posts.`created_at`)) ,' month ago'))"
                + " WHEN HOUR(TIMEDIFF(UTC_TIMESTAMP, posts.`created_at`)) != 0 THEN"
                + " IF(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, posts.`created_at`))) < 24 , IF(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, posts.`created_at`))) > 1 , CONCAT(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, posts.`created_at`))) ,' hrs ago'), CONCAT(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, posts.`created_at`))) ,' hr ago')),"
                + " IF(DATEDIFF(UTC_TIMESTAMP, posts.`created_at`) > 1 , CONCAT(DATEDIFF(UTC_TIMESTAMP, posts.`created_at`) ,' days ago'), CONCAT(DATEDIFF(UTC_TIMESTAMP, posts.`created_at`) ,' day ago')))"
                + " WHEN MINUTE(TIMEDIFF(UTC_TIMESTAMP, posts.`created_at`)) != 0 THEN CONCAT(MINUTE(TIMEDIFF(UTC_TIMESTAMP, posts.`created_at`)) ,' min ago')"
                + " ELSE "
                + " CONCAT(SECOND(TIMEDIFF(UTC_TIMESTAMP, posts.`created_at`)) ,'s ago')"
                + " END  )", "posted_since")
                .where("posts.user_id=" + logged_id);
            const posts = yield qb.orderBy("(SELECT ROUND(AVG(rating),1) FROM post_reviews where post_id=posts.id IS NOT NULL)", "DESC").addOrderBy("posts.created_at", "DESC").offset(skip).limit(limit).getRawMany();
            const total_posts = yield qb.getCount();
            for (const i in posts) {
                posts[i].comments = yield typeorm_2.getRepository(post_comment_entity_1.PostComments)
                    .createQueryBuilder("post_comments")
                    .leftJoinAndSelect(user_entity_1.Users, "users", "users.id = post_comments.user_id")
                    .select("users.id AS user_id, users.user_name ,users.first_name, users.last_name, users.profile_image, users.position,post_comments.id, post_comments.comment")
                    .addSelect(" ( CASE "
                    + " WHEN TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, post_comments.`created_at`) != 0 THEN"
                    + " IF(abs(TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, post_comments.`created_at`)) > 1 , CONCAT(abs(TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, post_comments.`created_at`)) ,' years ago'), CONCAT(abs(TIMESTAMPDIFF(YEAR,UTC_TIMESTAMP, post_comments.`created_at`)) ,' year ago'))"
                    + " WHEN TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, post_comments.`created_at`) != 0 THEN"
                    + " IF(abs(TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, post_comments.`created_at`)) > 1 , CONCAT(abs(TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, post_comments.`created_at`)) ,' months ago'), CONCAT(abs(TIMESTAMPDIFF(MONTH,UTC_TIMESTAMP, post_comments.`created_at`)) ,' month ago'))"
                    + " WHEN HOUR(TIMEDIFF(UTC_TIMESTAMP, post_comments.`created_at`)) != 0 THEN"
                    + " IF(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, post_comments.`created_at`))) < 24 , IF(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, post_comments.`created_at`))) > 1 , CONCAT(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, post_comments.`created_at`))) ,' hours ago'), CONCAT(abs(HOUR(TIMEDIFF(UTC_TIMESTAMP, post_comments.`created_at`))) ,' hour ago')),"
                    + " IF(DATEDIFF(UTC_TIMESTAMP, post_comments.`created_at`) > 1 , CONCAT(DATEDIFF(UTC_TIMESTAMP, post_comments.`created_at`) ,' days ago'), CONCAT(DATEDIFF(UTC_TIMESTAMP, post_comments.`created_at`) ,' day ago')))"
                    + " WHEN MINUTE(TIMEDIFF(UTC_TIMESTAMP, post_comments.`created_at`)) != 0 THEN CONCAT(MINUTE(TIMEDIFF(UTC_TIMESTAMP, post_comments.`created_at`)) ,' min ago')"
                    + " ELSE "
                    + " CONCAT(SECOND(TIMEDIFF(UTC_TIMESTAMP, post_comments.`created_at`)) ,'s ago')"
                    + " END  )", "commented_since")
                    .where('post_comments.post_id = :post_id', { post_id: posts[i].id })
                    .orderBy("post_comments.id", "DESC")
                    .limit(1)
                    .getRawMany();
                const all_images = yield typeorm_2.getRepository(post_image_entity_1.PostImages)
                    .createQueryBuilder("post_images")
                    .select("*")
                    .where('post_images.post_id = :post_id', { post_id: posts[i].id })
                    .getRawMany();
                posts[i].media = all_images;
            }
            return { posts, total_posts };
        });
    }
    deletePost(logged_id, dto) {
        return __awaiter(this, void 0, void 0, function* () {
            const { post_id } = dto;
            yield typeorm_2.getRepository(post_entity_1.Posts)
                .createQueryBuilder("posts")
                .delete()
                .from(post_entity_1.Posts)
                .where("posts.id= :post_id", { post_id: post_id })
                .execute();
            return { message: "Post deleted" };
        });
    }
    deleteComment(logged_id, dto) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = dto;
            yield typeorm_2.getRepository(post_comment_entity_1.PostComments)
                .createQueryBuilder("post_comments")
                .delete()
                .from(post_comment_entity_1.PostComments)
                .where("id= :comment_id", { comment_id: id })
                .execute();
            return { message: "Comment deleted" };
        });
    }
};
PostService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(post_entity_1.Posts)),
    __param(1, typeorm_1.InjectRepository(post_image_entity_1.PostImages)),
    __param(2, typeorm_1.InjectRepository(post_review_entity_1.PostReviews)),
    __param(3, typeorm_1.InjectRepository(post_like_entity_1.PostLikes)),
    __param(4, typeorm_1.InjectRepository(post_comment_entity_1.PostComments)),
    __param(5, typeorm_1.InjectRepository(post_report_entity_1.PostReports)),
    __param(6, typeorm_1.InjectRepository(user_entity_1.Users)),
    __param(7, typeorm_1.InjectRepository(follow_entity_1.Followers)),
    __param(8, typeorm_1.InjectRepository(user_rating_entity_1.UserRatings)),
    __param(9, typeorm_1.InjectRepository(event_entity_1.Events)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        notification_service_1.NotificationService,
        mailer_1.MailerService])
], PostService);
exports.PostService = PostService;
//# sourceMappingURL=post.service.js.map