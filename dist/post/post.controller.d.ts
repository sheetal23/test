import { PostService } from './post.service';
import { UserService } from '../user/user.service';
import { CreatePostDto, PostReviewDto } from './dto';
export declare class PostController {
    private readonly userService;
    private readonly postService;
    constructor(userService: UserService, postService: PostService);
    homeFeed(request: any): Promise<{
        posts: any[];
        total_posts: number;
        follow_users: any[];
        near_by_sessions: any[];
        total_near_by_sessions: number;
    }>;
    create(request: any, postData: CreatePostDto): Promise<{
        message: string;
    }>;
    reviewPost(request: any, reviewData: PostReviewDto): Promise<{
        message: string;
    }>;
    likePost(request: any, likeData: PostReviewDto): Promise<{
        message: string;
    }>;
    commentPost(request: any, commentData: PostReviewDto): Promise<{
        id: number;
        message: string;
    }>;
    reportPost(request: any, reportData: PostReviewDto): Promise<{
        message: string;
    }>;
    deletePost(request: any, deleteData: PostReviewDto): Promise<{
        message: string;
    }>;
    deleteComment(request: any, deleteData: PostReviewDto): Promise<{
        message: string;
    }>;
    getComments(request: any): Promise<{
        comments: any[];
        count: number;
    }>;
    getLikes(request: any): Promise<{
        data: any[];
        count: number;
    }>;
    getRatings(request: any): Promise<{
        data: any[];
        count: number;
    }>;
    myPosts(request: any): Promise<{
        posts: any[];
        total_posts: number;
    }>;
    getPost(request: any): Promise<any>;
}
