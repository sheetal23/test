"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const AWS = require("aws-sdk");
const http_exception_1 = require("@nestjs/common/exceptions/http.exception");
const user_service_1 = require("../user/user.service");
const Ffmpeg = require("fluent-ffmpeg");
const fs = require("fs");
const http = require("http");
const path = require("path");
const HLSServer = require("hls-server");
const AWS_S3_BUCKET_NAME = 'v3-bucket';
AWS.config.update({
    accessKeyId: 'AKIAJCL6I3NDPBOZN7BA',
    secretAccessKey: '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',
    region: 'us-east-2'
});
let VideoService = class VideoService {
    constructor(userService) {
        this.userService = userService;
    }
    addVideo(file) {
        console.log(file);
        if (!file) {
            throw new http_exception_1.HttpException({ error: 'bad_request', message: 'Video should not be empty.' }, common_1.HttpStatus.BAD_REQUEST);
        }
        var filename = file.originalname;
        var filesize = file.size;
        console.log(filesize);
        if (filesize > 52428800) {
            throw new http_exception_1.HttpException({ error: 'bad_request', message: 'Video should not be larger than 50 mb' }, common_1.HttpStatus.BAD_REQUEST);
        }
        console.log(2);
        filename = this.generateRandomString(6) + "-" + filename;
        filename = filename.split(' ').join('-');
        console.log(filename);
        var ext = path.extname(filename);
        var only_file = path.basename(filename, ext);
        var folder = "videos";
        var params = {
            Body: file.buffer,
            Bucket: AWS_S3_BUCKET_NAME,
            Key: 'uploads/videos/' + filename,
            ACL: 'public-read'
        };
        console.log(params);
        const s3 = new AWS.S3();
        try {
            console.log(22);
            s3
                .putObject(params)
                .promise()
                .then(data => {
                this.hlsCommon(only_file, folder, ext);
                return params.Key;
            }, err => {
                return err;
            });
        }
        catch (err) {
            return err;
        }
        const url = "https://v3-bucket.s3.us-east-2.amazonaws.com/uploads/videos/stream-files/" + only_file + "/output.m3u8";
        return { message: 'Video Uploaded.', filename: filename, url: url };
    }
    hlsCommon(file_name, folder, ext) {
        const s3 = new AWS.S3();
        console.log('hls common');
        try {
            console.log(file_name);
            fs.mkdirSync('public/' + file_name, { recursive: true });
            var server = http.createServer();
            var hls = new HLSServer(server, {
                path: '/streams',
                dir: 'public/' + file_name
            });
            console.log(333);
            console.log("https://v3-bucket.s3.us-east-2.amazonaws.com/uploads/videos/" + file_name + ext);
            Ffmpeg({ source: "https://v3-bucket.s3.us-east-2.amazonaws.com/uploads/videos/" + file_name + ext, nolog: true }, { timeout: 432000 }).audioBitrate(50).addOptions([
                '-profile:v baseline',
                '-level 3.0',
                '-start_number 0',
                '-hls_time 30',
                '-hls_list_size 0',
                '-f hls'
            ]).output('public/' + file_name + '/output.m3u8').on('end', function () {
                console.log('Finished processing');
                var directoryName = 'public/' + file_name, directoryPath = path.resolve(directoryName);
                var files = fs.readdirSync(directoryPath);
                console.log(files);
                for (const i in files) {
                    var filePath = path.join(directoryPath, files[i]);
                    var params = {
                        Bucket: AWS_S3_BUCKET_NAME,
                        Key: 'uploads/' + folder + '/stream-files/' + file_name + '/' + files[i],
                        Body: fs.readFileSync(filePath),
                        ACL: 'public-read'
                    };
                    try {
                        s3
                            .putObject(params)
                            .promise()
                            .then(data => {
                            console.log("Upload Success");
                            fs.unlink('public/' + file_name + '/' + files[i], function (err) {
                                if (err) {
                                    return err;
                                }
                                else {
                                    console.log("file removed");
                                }
                            });
                            fs.readdir('public/' + file_name, function (err, files) {
                                if (err) {
                                }
                                else {
                                    if (!files.length) {
                                        fs.rmdir('public/' + file_name, err => {
                                            if (err) {
                                                console.log(err);
                                            }
                                            else {
                                                console.log("directory deleted");
                                            }
                                        });
                                    }
                                }
                            });
                        }, err => {
                            console.log(err);
                        });
                    }
                    catch (err) {
                        return err;
                    }
                }
            }).run();
        }
        catch (err) {
            return err;
        }
        return 1;
    }
    generateRandomString(length) {
        var text = "";
        const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < length; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        return text;
    }
};
VideoService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [user_service_1.UserService])
], VideoService);
exports.VideoService = VideoService;
//# sourceMappingURL=video.service.js.map