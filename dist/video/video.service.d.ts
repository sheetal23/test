import { UserService } from '../user/user.service';
export declare class VideoService {
    private readonly userService;
    constructor(userService: UserService);
    addVideo(file: any): any;
    hlsCommon(file_name: any, folder: any, ext: any): any;
    generateRandomString(length: any): string;
}
