"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const team_service_1 = require("./team.service");
const user_service_1 = require("../user/user.service");
const dto_1 = require("./dto");
const swagger_1 = require("@nestjs/swagger");
let TeamController = class TeamController {
    constructor(userService, teamService) {
        this.userService = userService;
        this.teamService = teamService;
    }
    team(request, dto) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            return yield this.teamService.create(jwtData.id, dto);
        });
    }
    addTeamMember(request, dto) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            return yield this.teamService.addTeamMember(jwtData.id, dto);
        });
    }
    teamUnique(request, dto) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            return yield this.teamService.teamUnique(jwtData.id, dto);
        });
    }
    findTeams(request) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            console.log(request.headers.authorization);
            return yield this.teamService.findTeams(jwtData.id, request);
        });
    }
    teamMembers(request) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            console.log(request.headers.authorization);
            return yield this.teamService.teamMembers(jwtData.id, request);
        });
    }
    teamDelete(request) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            console.log(request.headers.authorization);
            return yield this.teamService.teamDelete(jwtData.id, request);
        });
    }
    teamPlayerDelete(request) {
        return __awaiter(this, void 0, void 0, function* () {
            const jwtData = yield this.userService.jwtData(request.headers.authorization);
            console.log(request.headers.authorization);
            return yield this.teamService.teamPlayerDelete(jwtData.id, request);
        });
    }
};
__decorate([
    common_1.Post(''),
    common_1.HttpCode(200),
    __param(0, common_1.Req()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, dto_1.CreateTeamDto]),
    __metadata("design:returntype", Promise)
], TeamController.prototype, "team", null);
__decorate([
    common_1.Post('players'),
    common_1.HttpCode(200),
    __param(0, common_1.Req()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, dto_1.AddPlayerDto]),
    __metadata("design:returntype", Promise)
], TeamController.prototype, "addTeamMember", null);
__decorate([
    common_1.Post('unique'),
    common_1.HttpCode(200),
    __param(0, common_1.Req()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, dto_1.CreateTeamDto]),
    __metadata("design:returntype", Promise)
], TeamController.prototype, "teamUnique", null);
__decorate([
    common_1.Get(''),
    common_1.HttpCode(200),
    __param(0, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], TeamController.prototype, "findTeams", null);
__decorate([
    common_1.Get('players'),
    common_1.HttpCode(200),
    __param(0, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], TeamController.prototype, "teamMembers", null);
__decorate([
    common_1.Delete(''),
    common_1.HttpCode(200),
    __param(0, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], TeamController.prototype, "teamDelete", null);
__decorate([
    common_1.Delete('player'),
    common_1.HttpCode(200),
    __param(0, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], TeamController.prototype, "teamPlayerDelete", null);
TeamController = __decorate([
    swagger_1.ApiBearerAuth(),
    swagger_1.ApiUseTags('team'),
    common_1.Controller('team'),
    __metadata("design:paramtypes", [user_service_1.UserService, team_service_1.TeamService])
], TeamController);
exports.TeamController = TeamController;
//# sourceMappingURL=team.controller.js.map