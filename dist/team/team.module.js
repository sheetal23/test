"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const team_entity_1 = require("./entities/team.entity");
const team_member_entity_1 = require("./entities/team-member.entity");
const team_controller_1 = require("./team.controller");
const typeorm_1 = require("@nestjs/typeorm");
const team_service_1 = require("./team.service");
const user_authmiddleware_1 = require("../user/user.authmiddleware");
const user_module_1 = require("../user/user.module");
const event_member_entity_1 = require("../event/entities/event-member.entity");
let TeamModule = class TeamModule {
    configure(consumer) {
        consumer
            .apply(user_authmiddleware_1.AuthMiddleware)
            .forRoutes({ path: 'team', method: common_1.RequestMethod.POST }, { path: 'team/member', method: common_1.RequestMethod.POST }, { path: 'team', method: common_1.RequestMethod.GET }, { path: 'team/players', method: common_1.RequestMethod.GET }, { path: 'team', method: common_1.RequestMethod.DELETE }, { path: 'team/player', method: common_1.RequestMethod.DELETE }, { path: 'team/unique', method: common_1.RequestMethod.POST });
    }
};
TeamModule = __decorate([
    common_1.Module({
        imports: [
            typeorm_1.TypeOrmModule.forFeature([team_entity_1.Teams]),
            typeorm_1.TypeOrmModule.forFeature([team_member_entity_1.TeamMembers]),
            typeorm_1.TypeOrmModule.forFeature([event_member_entity_1.EventMembers]),
            user_module_1.UserModule
        ],
        providers: [team_service_1.TeamService],
        controllers: [
            team_controller_1.TeamController
        ],
        exports: [team_service_1.TeamService]
    })
], TeamModule);
exports.TeamModule = TeamModule;
//# sourceMappingURL=team.module.js.map