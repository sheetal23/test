import { TeamMembers } from './entities/team-member.entity';
import { Teams } from './entities/team.entity';
import { Repository } from 'typeorm';
import { CreateTeamDto, AddPlayerDto } from './dto';
import { MailerService } from '@nest-modules/mailer';
import { EventMembers } from '../event/entities/event-member.entity';
export declare class TeamService {
    private readonly teamRepository;
    private readonly teamMemberRepository;
    private readonly eventMemberRepository;
    private readonly mailerService;
    constructor(teamRepository: Repository<Teams>, teamMemberRepository: Repository<TeamMembers>, eventMemberRepository: Repository<EventMembers>, mailerService: MailerService);
    create(id: number, dto: CreateTeamDto): Promise<{
        id: number;
        name: string;
        message: string;
    }>;
    teamUnique(id: number, dto: CreateTeamDto): Promise<{
        message: string;
    }>;
    addTeamMember(id: number, dto: AddPlayerDto): Promise<{
        message: string;
    }>;
    findTeams(logged_id: number, req: any): Promise<{
        teams: Teams[];
        count: number;
    }>;
    teamMembers(logged_id: number, req: any): Promise<{
        team_members: any[];
        count: number;
    }>;
    teamDelete(logged_id: number, req: any): Promise<{
        message: string;
    }>;
    teamPlayerDelete(logged_id: number, req: any): Promise<{
        message: string;
    }>;
}
