export declare class TeamMembers {
    id: number;
    team_id: number;
    member_id: number;
    created_at: Date;
    updated_at: Date;
}
