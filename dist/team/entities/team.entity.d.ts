import { TeamMembers } from './team-member.entity';
import { Chats } from '../../chat/entities/chat.entity';
export declare class Teams {
    id: number;
    user_id: number;
    name: string;
    created_at: Date;
    updated_at: Date;
    TeamMembers: TeamMembers[];
    Chats: Chats[];
}
