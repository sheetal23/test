"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const user_entity_1 = require("../../user/entities/user.entity");
const team_member_entity_1 = require("./team-member.entity");
const chat_entity_1 = require("../../chat/entities/chat.entity");
let Teams = class Teams {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], Teams.prototype, "id", void 0);
__decorate([
    typeorm_1.ManyToOne(type => user_entity_1.Users, Users => Users.Teams, {
        cascade: true,
        onDelete: "CASCADE",
        onUpdate: "CASCADE"
    }),
    typeorm_1.JoinColumn({ name: "user_id" }),
    __metadata("design:type", Number)
], Teams.prototype, "user_id", void 0);
__decorate([
    typeorm_1.Column({ default: null }),
    __metadata("design:type", String)
], Teams.prototype, "name", void 0);
__decorate([
    typeorm_1.Column({ type: 'timestamp', default: () => "CURRENT_TIMESTAMP" }),
    __metadata("design:type", Date)
], Teams.prototype, "created_at", void 0);
__decorate([
    typeorm_1.Column({ type: 'timestamp', default: null }),
    __metadata("design:type", Date)
], Teams.prototype, "updated_at", void 0);
__decorate([
    typeorm_1.OneToMany(type => team_member_entity_1.TeamMembers, TeamMembers => TeamMembers.team_id),
    __metadata("design:type", Array)
], Teams.prototype, "TeamMembers", void 0);
__decorate([
    typeorm_1.OneToMany(type => chat_entity_1.Chats, Chats => Chats.team_id),
    __metadata("design:type", Array)
], Teams.prototype, "Chats", void 0);
Teams = __decorate([
    typeorm_1.Entity()
], Teams);
exports.Teams = Teams;
//# sourceMappingURL=team.entity.js.map