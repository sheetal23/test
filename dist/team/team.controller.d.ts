import { TeamService } from './team.service';
import { UserService } from '../user/user.service';
import { CreateTeamDto, AddPlayerDto } from './dto';
export declare class TeamController {
    private readonly userService;
    private readonly teamService;
    constructor(userService: UserService, teamService: TeamService);
    team(request: any, dto: CreateTeamDto): Promise<{
        id: number;
        name: string;
        message: string;
    }>;
    addTeamMember(request: any, dto: AddPlayerDto): Promise<{
        message: string;
    }>;
    teamUnique(request: any, dto: CreateTeamDto): Promise<{
        message: string;
    }>;
    findTeams(request: any): Promise<{
        teams: import("./entities/team.entity").Teams[];
        count: number;
    }>;
    teamMembers(request: any): Promise<{
        team_members: any[];
        count: number;
    }>;
    teamDelete(request: any): Promise<{
        message: string;
    }>;
    teamPlayerDelete(request: any): Promise<{
        message: string;
    }>;
}
