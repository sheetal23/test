"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const team_member_entity_1 = require("./entities/team-member.entity");
const team_entity_1 = require("./entities/team.entity");
const user_entity_1 = require("../user/entities/user.entity");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const http_exception_1 = require("@nestjs/common/exceptions/http.exception");
const common_2 = require("@nestjs/common");
const mailer_1 = require("@nest-modules/mailer");
const event_member_entity_1 = require("../event/entities/event-member.entity");
let TeamService = class TeamService {
    constructor(teamRepository, teamMemberRepository, eventMemberRepository, mailerService) {
        this.teamRepository = teamRepository;
        this.teamMemberRepository = teamMemberRepository;
        this.eventMemberRepository = eventMemberRepository;
        this.mailerService = mailerService;
    }
    create(id, dto) {
        return __awaiter(this, void 0, void 0, function* () {
            const { name, member_ids } = dto;
            console.log(name);
            console.log(member_ids);
            try {
                let newTeam = new team_entity_1.Teams();
                newTeam.user_id = id;
                newTeam.name = name;
                yield this.teamRepository.save(newTeam);
                console.log(member_ids);
                const teamMembers = [];
                const tm = new team_member_entity_1.TeamMembers();
                tm.team_id = newTeam.id;
                tm.member_id = id;
                console.log("loop");
                teamMembers.push(tm);
                for (const i in member_ids) {
                    const tm = new team_member_entity_1.TeamMembers();
                    tm.team_id = newTeam.id;
                    tm.member_id = member_ids[i];
                    console.log("loop");
                    teamMembers.push(tm);
                }
                yield this.teamMemberRepository.save(teamMembers);
                return ({ id: newTeam.id, name: newTeam.name, message: "Team added" });
            }
            catch (err) {
                console.log(err);
                throw new http_exception_1.HttpException({ error: 'bad_request', message: 'Invalid input' }, common_2.HttpStatus.BAD_REQUEST);
            }
        });
    }
    teamUnique(id, dto) {
        return __awaiter(this, void 0, void 0, function* () {
            const { name } = dto;
            console.log(name);
            try {
                const team = yield typeorm_2.getRepository(team_entity_1.Teams)
                    .createQueryBuilder("teams")
                    .where("name= :team_name", { team_name: name })
                    .andWhere("user_id= :user_id", { user_id: id })
                    .getOne();
                if (team) {
                    throw new http_exception_1.HttpException({ error: 'bad_request', message: 'Team already exist' }, common_2.HttpStatus.BAD_REQUEST);
                }
                else {
                    return ({ message: "Successful" });
                }
            }
            catch (err) {
                console.log(err);
                if (err.response.message == "Team already exist") {
                    var msg = "Team already exist";
                }
                else {
                    var msg = "Invalid Input";
                }
                throw new http_exception_1.HttpException({ error: 'bad_request', message: msg }, common_2.HttpStatus.BAD_REQUEST);
            }
        });
    }
    addTeamMember(id, dto) {
        return __awaiter(this, void 0, void 0, function* () {
            const { team_id, member_ids } = dto;
            const oldMembers = [];
            const oldMemberIds = yield typeorm_2.getRepository(team_member_entity_1.TeamMembers)
                .createQueryBuilder("team_members")
                .select("member_id")
                .where("team_id = :team_id", { team_id: team_id })
                .getRawMany();
            for (const i in oldMemberIds) {
                oldMembers.push(oldMemberIds[i].member_id);
            }
            console.log("oldMembers:" + oldMembers);
            if (oldMembers != []) {
                if (member_ids) {
                    var newMembers = member_ids.filter(function (obj) { return oldMembers.indexOf(obj) == -1; });
                }
                else {
                    newMembers = [];
                }
            }
            else {
                newMembers = member_ids;
            }
            console.log("newMembers:" + newMembers);
            if (newMembers.length > 0) {
                const teamMembers = [];
                for (const i in newMembers) {
                    const tm = new team_member_entity_1.TeamMembers();
                    tm.team_id = team_id;
                    tm.member_id = newMembers[i];
                    console.log("loop");
                    teamMembers.push(tm);
                }
                yield this.teamMemberRepository.save(teamMembers);
            }
            return ({ message: "Member added to team" });
        });
    }
    findTeams(logged_id, req) {
        return __awaiter(this, void 0, void 0, function* () {
            var user_id = req.query.user_id;
            var skip = 0;
            var limit = req.query.limit ? req.query.limit : 10;
            if (req.query.page) {
                const page = req.query.page;
                var skip = limit * page;
            }
            const qb = yield typeorm_2.getRepository(team_entity_1.Teams)
                .createQueryBuilder("teams");
            if (user_id) {
                qb.where("user_id = :user_id", { user_id: user_id });
            }
            var teams = yield qb.offset(skip).limit(limit).getMany();
            const count = yield qb.getCount();
            return { teams: teams, count };
        });
    }
    teamMembers(logged_id, req) {
        return __awaiter(this, void 0, void 0, function* () {
            var team_id = req.query.team_id;
            var event_id = req.query.event_id;
            var type = req.query.type;
            var skip = 0;
            var limit = req.query.limit ? req.query.limit : 10;
            if (req.query.page) {
                const page = req.query.page;
                var skip = limit * page;
            }
            const qb = yield typeorm_2.getRepository(team_member_entity_1.TeamMembers)
                .createQueryBuilder("team_members")
                .leftJoinAndSelect(user_entity_1.Users, "users", "users.id = team_members.member_id")
                .select("team_members.member_id as id, user_name, first_name, last_name, profile_image,position,((SELECT ROUND(AVG(passing_ability),1) FROM `user_ratings` where user_id_2=users.id AND passing_ability IS NOT NULL) + (SELECT ROUND(AVG(shooting_ability),1) FROM `user_ratings` where user_id_2=users.id AND shooting_ability IS NOT NULL) +( SELECT ROUND(AVG(dribbling_ability),1) FROM `user_ratings` where user_id_2=users.id AND dribbling_ability IS NOT NULL)+ ( SELECT ROUND(AVG(steals),1) FROM `user_ratings` where user_id_2=users.id AND steals IS NOT NULL)+( SELECT ROUND(AVG(rebounds),1) FROM `user_ratings` where user_id_2=users.id AND rebounds IS NOT NULL)+( SELECT ROUND(AVG(blocks),1) FROM `user_ratings` where user_id_2=users.id AND blocks IS NOT NULL))/6 AS avg_rating")
                .where("team_id = :team_id", { team_id: team_id });
            if (event_id) {
                const oldMembers = [];
                const oldMemberIds = yield typeorm_2.getRepository(event_member_entity_1.EventMembers)
                    .createQueryBuilder("event_members")
                    .select("member_id")
                    .where("event_id = :event_id", { event_id: event_id })
                    .andWhere("member_id IS NOT NULL")
                    .getRawMany();
                for (const i in oldMemberIds) {
                    oldMembers.push(oldMemberIds[i].member_id);
                }
                qb.addSelect("(SELECT is_join FROM event_members WHERE event_id = " + event_id + " AND member_id=users.id ) AS is_join,(SELECT is_admin FROM event_members WHERE event_id = " + event_id + " AND member_id=users.id ) AS is_admin,(SELECT is_applied FROM event_members WHERE event_id = " + event_id + " AND member_id=users.id ) AS is_applied, (SELECT is_invited FROM event_members WHERE event_id = " + event_id + " AND member_id=users.id ) AS is_invited")
                    .andWhere("users.id not in (" + oldMembers + ")");
            }
            if (type == 1) {
                qb.offset(skip).limit(limit);
            }
            var team_members = yield qb.getRawMany();
            const count = yield qb.getCount();
            return { team_members: team_members, count };
        });
    }
    teamDelete(logged_id, req) {
        return __awaiter(this, void 0, void 0, function* () {
            const team_id = req.query.id;
            const team = yield typeorm_2.getRepository(team_entity_1.Teams)
                .createQueryBuilder("teams")
                .select("user_id")
                .where("id = :team_id", { team_id: team_id })
                .getRawOne();
            if (team.user_id == logged_id) {
                yield typeorm_2.getRepository(team_entity_1.Teams)
                    .createQueryBuilder("teams")
                    .delete()
                    .from(team_entity_1.Teams)
                    .where("id = :team_id", { team_id: team_id })
                    .andWhere("user_id = :user_id", { user_id: logged_id })
                    .execute();
                return { message: ' successfully deleted.' };
            }
            else {
                return ({ message: "You don't have authorization to delete this team" });
            }
        });
    }
    teamPlayerDelete(logged_id, req) {
        return __awaiter(this, void 0, void 0, function* () {
            const team_id = req.query.team_id;
            const member_id = req.query.member_id;
            const team = yield typeorm_2.getRepository(team_entity_1.Teams)
                .createQueryBuilder("teams")
                .select("user_id")
                .where("id = :team_id", { team_id: team_id })
                .getRawOne();
            if (team.user_id == logged_id) {
                yield typeorm_2.getRepository(team_member_entity_1.TeamMembers)
                    .createQueryBuilder("team_members")
                    .delete()
                    .from(team_member_entity_1.TeamMembers)
                    .where("team_id = :team_id", { team_id: team_id })
                    .andWhere("member_id = :member_id", { member_id: member_id })
                    .execute();
                return {
                    message: ' successfully deleted.'
                };
            }
            else {
                return ({
                    message: "You don't have authorization to delete this member"
                });
            }
        });
    }
};
TeamService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(team_entity_1.Teams)),
    __param(1, typeorm_1.InjectRepository(team_member_entity_1.TeamMembers)),
    __param(2, typeorm_1.InjectRepository(event_member_entity_1.EventMembers)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        mailer_1.MailerService])
], TeamService);
exports.TeamService = TeamService;
//# sourceMappingURL=team.service.js.map