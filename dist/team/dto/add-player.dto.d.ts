export declare class AddPlayerDto {
    readonly team_id: number;
    readonly member_ids: Array<number>;
}
