export declare class CreateTeamDto {
    readonly name: string;
    readonly member_ids: Array<number>;
}
